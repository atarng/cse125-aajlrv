//LoadingManager.h

#ifndef _LOADING_MANAGER_
#define _LOADING_MANAGER_

#include <string>

class LoadingManager
{
public:
	//declare variables here
	static bool			fullscreen;
	static std::string	applicationName;
	static int			windowHeight;
	static int			windowWidth;
    static int          cameraWeight;

    static std::string  shaderVersion; // NOT BEING USED
    static bool         multiTextures; //

    static bool			holdOnExit;    // 
    static bool         printDbgMsgs;  //
    static bool displayBoundingSpheres;
    static bool         useInstancing;
    static bool         lockLightToCamera;
	static bool         tightCameraLock;

    static std::string  serverIP;



//// END CONFIG VARIABLES /////////////////////////////////////

	LoadingManager();
	LoadingManager(std::string loadingFileName);
	~LoadingManager();

	static bool Initialize(std::string loadingFileName);
};

#endif