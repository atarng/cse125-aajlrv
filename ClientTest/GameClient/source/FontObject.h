////////////////////////////////////////////////////////////////////////////////
// Filename: FontObject.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _FONTOBJECT_H_
#define _FONTOBJECT_H_


//////////////
// INCLUDES //
//////////////
#include "D3DManager.h"
#include <fstream>
using namespace std;


///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "TextureObject.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: FontObject
////////////////////////////////////////////////////////////////////////////////
class FontObject
{
private:
	struct FontType
	{
		float left, right;
		int   size;
	};
	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};
    FontType* m_Font;
	TextureObject* m_Texture;

    bool LoadFontData(char*);
	void ReleaseFontData();
	//bool LoadTexture(ID3D11Device*, char*);
	//void ReleaseTexture();

public:
	FontObject();
	FontObject(const FontObject&);
	~FontObject();

	bool Initialize(ID3D11Device*, char*);
	void Shutdown();

	TextureObject * GetTextureObject();
    void SetTextureObject(TextureObject &);

	void BuildVertexArray(void*, char*, float, float);
};

#endif