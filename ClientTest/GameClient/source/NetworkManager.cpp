/***
 * Filename:
 * Contributors: Anthony Chen, Vincent Chen
 * Purpose: 
 */

#include "NetworkManager.h"
#include "UnitManagerClient.h"
#include "LoadingManager.h"
#include "SoundManager.h"
#include "SystemManager.h"
#include "HUDManager.h"

#include <ws2tcpip.h>

#include <iostream>
#include <vector>
#include "Log.h"
#include "Unit.h"
#include "HUDBar.h"

#include "Event.h"
#include "MovementEvent.h"
#include "DeleteEvent.h"
#include "AssignEvent.h"
#include "UnassignEvent.h"
#include "WinEvent.h"
#include "LoseEvent.h"

#include "rapidxml_print.hpp"

#pragma comment(lib, "Ws2_32.lib")

#define VERSION_2_2 0x0202
#define MAX_MESSAGE_SIZE 4096

#define DEFAULT_PORT 7700


//#define SERVER_NAME "localhost"
//#define SERVER_NAME "128.54.70.24"

using namespace std;
using namespace rapidxml;

NetworkManager * NetworkManager::m_Instance = NULL; 

NetworkManager::NetworkManager(unsigned short port, string serverName) {
	this->port = port;
	this->serverName = serverName;
    this->playerId = 0;
    this->currentSparkId = UNINITIALIZED;
    this->currentRobotId = UNINITIALIZED;
	this->currentRobotHealth = UNINITIALIZED;
}

NetworkManager::~NetworkManager(void) {
}

/**
 * Starts the connection to the server. 
 * @param       port the port to bind to
 * @param serverName the name of the server
 */
bool NetworkManager::Initialize() {
	WSAData wsaData;

	if ((WSAStartup(VERSION_2_2, &wsaData)) == SOCKET_ERROR) {
		cerr << "[ERROR] Client: Winsock failed to start!\n";
		return false;
	}

    if (wsaData.wVersion != VERSION_2_2) {
        cerr << "[ERROR] Client: Winsock version incorrect!" << endl;
        WSACleanup(); 
        return false;
    }

	SOCKET tempSocket = socket(AF_INET, SOCK_STREAM, 0);

	if (tempSocket == SOCKET_ERROR) {
		cerr << "[ERROR] Client: failed to establish socket!" << endl;
        WSACleanup(); 
		return false;
	} else
		serverSocket = tempSocket;


	struct hostent * host_entry = gethostbyname(serverName.c_str());
	if (host_entry == NULL) {
		cerr << "[ERROR] Client: unable to retrieve address of host " << serverName << endl;
        WSACleanup(); 
		return false;
	}

	struct sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = *(unsigned long*) host_entry->h_addr;

	if (connect(serverSocket, (sockaddr *) &server, sizeof(server)) == SOCKET_ERROR) 
    {
		cerr << "[ERROR] Client: unable to establish connection to host " << serverName << endl;
        WSACleanup(); 
		return false;
	}

	// wait for player id from the server
	int nBytes = recv(serverSocket, (char *) &playerId, sizeof(playerId), 0);

	if (nBytes == SOCKET_ERROR) {
        cerr << "[ERROR] Server: failed to receive player id" << endl;
		WSACleanup(); 
		return false;
    }

	cerr << "[DEBUG] Client (" << playerId << ") started." << endl;
	return true;
}

/**
 * Shuts down the client.
 * @param clientSocket the socket associated with the client
 */
void NetworkManager::EndConnection() {
    cerr << "[DEBUG] Client: Client shutting down..." << endl;

	shutdown (serverSocket, SD_SEND);  

    // TODO check if any last data arrived

    closesocket (serverSocket);
	WSACleanup();

	cerr << "[DEBUG] Client: Client shut down." << endl;
}

NetworkManager * NetworkManager::Instance()
{
	if ( !m_Instance )
	{
		HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
		LoadingManager::serverIP = std::string(hm->getField2()->getValue());
        cout << "[NetworkManager] serverID : " << LoadingManager::serverIP << endl ;
        m_Instance= new NetworkManager( DEFAULT_PORT, LoadingManager::serverIP.c_str());
        m_Instance->Initialize();
    }
    return m_Instance;           
}

void NetworkManager::updateFromServer() {
    unsigned __int64 startTick = __rdtsc();
	unsigned long messageSize = 0;

	//allocate a fd_set with only one socket
	FD_SET tempSet;
	FD_ZERO(&tempSet); 
	FD_SET(serverSocket, &tempSet); 

	timeval waitTime; 
    waitTime.tv_sec = 0; 
    waitTime.tv_usec = 0; 

	int numOfSockets = 0;	// number of sockets that are ready

	// keep going until there are no more messages
	while ( (numOfSockets = select(tempSet.fd_count, &tempSet, NULL, NULL, &waitTime)) != 0 ) {
		if (numOfSockets == SOCKET_ERROR) {
            if ( "localhost" != LoadingManager::serverIP )
            {
			    cerr << "[ERROR] Server: Error in one of the established sockets (" 
                     << WSAGetLastError() << ")" << endl;
            }
			return;
		}

		// receive the message size
		int nBytes = recv(serverSocket, (char *) &messageSize, sizeof(messageSize), 0);

		// check for error or no bytes sent
		if (nBytes == SOCKET_ERROR) {
			cerr << "[ERROR] Client: failed to receive message size from server" << endl;
			return;
		} else if (nBytes == 0) {
			cerr << "[ERROR] Client: No updates received from the server" << endl;
			return;
		}

		// used to store update message from server
		char buffer[MAX_MESSAGE_SIZE];
		messageSize = ntohl(messageSize); //convert byte order

        if (messageSize > MAX_MESSAGE_SIZE) 
            cerr << "[ERROR] Client: message received greater than maximum message size (" 
                 << messageSize << ")" << endl;

        int totalBytesReceived = 0;
        int bytesLeftToReceive = messageSize;

        do {
            nBytes = recv(serverSocket, buffer + totalBytesReceived, bytesLeftToReceive, 0);

            //check for error or no bytes sent
		    if (nBytes == SOCKET_ERROR || nBytes == 0) {
			    cerr << "[ERROR] Client: failed to receive update message from server" << endl;
			    return;
		    } 

            totalBytesReceived += nBytes;
            bytesLeftToReceive -= nBytes;
        } while (totalBytesReceived < messageSize);

		if ( totalBytesReceived != messageSize ) 
			cerr << "[WARNING] Client: Byte received not equal to advertised message size " 
                 << totalBytesReceived << "/" << messageSize << endl; 

		xml_document<> doc;
		doc.parse<0>(buffer);

		xml_node<> * currentNode;

        if ( (currentNode = doc.first_node(UNIT)) != NULL)
            updateUnitsWithMessage(doc, currentNode);
        else if ( (currentNode = doc.first_node(EVENT)) != NULL) {
		    xml_node<> * tempUnit = doc.clone_node(currentNode);

            Event * event = Event::createEvent(tempUnit);
            processEvent(event);
        }
	}
    unsigned __int64 endTick = __rdtsc();

    //cerr << "processTime: " << endTick - startTick << endl;

}

/**
 * Updates the internal unit map with the specified xml strings.
 */
void NetworkManager::updateUnitsWithMessage(xml_document<> & doc, xml_node<> * currentUnit) {
    while (currentUnit != NULL) {
		Unit * updateUnit = new Unit();

		xml_node<> * tempUnit = doc.clone_node(currentUnit);

        updateUnit->Deserialize(tempUnit);
        
        //if (updateUnit->getUnitId() == currentRobotId) 
        //   cerr << "anim type " << updateUnit->getCurrentAnimType() << endl;

		if(!UnitManagerClient::Instance()->updateClient(updateUnit)) 
            delete updateUnit;

		currentUnit = currentUnit->next_sibling(UNIT);
	}
}

void NetworkManager::processEvent(Event * event) {
    if (event->isDelete()) {
        DeleteEvent deleteEvent = *(DeleteEvent *) event;
        UnitManagerClient::Instance()->deleteUnit(deleteEvent.getUnitId());

        if (deleteEvent.getUnitId() == currentRobotId)
            currentRobotId = UNINITIALIZED;
    } else if (event->isAssign()) {
        AssignEvent assignEvent = *(AssignEvent *) event;

        if (assignEvent.getPlayerId() == playerId) {
            if (currentSparkId == UNINITIALIZED)
                currentSparkId = assignEvent.getUnitId();
            else {
                currentRobotId = assignEvent.getUnitId();
				//setCurrentRobotHealth();
			}

            //scerr << "controlling robot " << currentRobotId << endl;
        }
    } else if (event->isUnassign()) {
        UnassignEvent unassignEvent = *(UnassignEvent *) event;

        if (playerId == unassignEvent.getPlayerId())
            currentRobotId = UNINITIALIZED;
    } else if (event->isLose()) {
        LoseEvent loseEvent = *(LoseEvent *) event;

        if (playerId == loseEvent.getPlayerId())
            handleLose();
    } else if (event->isWin()) {
        WinEvent winEvent = *(WinEvent *) event;

        if (playerId == winEvent.getPlayerId())
            handleWin();
    }
}

void NetworkManager::handleLose()
{
	SoundManager* sound = SystemManager::Instance()->GetSoundManager();
	if (sound) {
		sound->StopMusicLoop();
		sound->PlaySoundSpec(SoundManager::LOSE_SOUND);
		sound->PlayEndgameLoop();
	}
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
	hm->getCrosshairs()->setDisplay(false);
	hm->getCrosshairs2()->setDisplay(false);
	hm->getBar()->setValue(0);
	hm->getBar()->setMax(1);
	hm->getBar2()->setValue(0);
	hm->getBar2()->setMax(1);
	hm->getBar3()->setValue(0);
	hm->getBar3()->setMax(1);
	hm->getBar4()->setValue(0);
	hm->getBar4()->setMax(1);

	hm->getLose()->setDisplay(true);
	hm->getWin()->setDisplay(false);
}

void NetworkManager::handleWin()
{
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());

	hm->getWin()->setDisplay(true);
}

/**
 * Returns the player id of this client.
 * @return the current player id
 */
int NetworkManager::getPlayerId() {
    return playerId;
}

/**
 * Returns the id of the robot being controlled.
 * @return the current robot id
 */
int NetworkManager::getCurrentRobotId() { 
    return currentRobotId; 
}

/**
 * Returns the current health of the robot being controlled.
 * @return the current robot id
 */
int NetworkManager::getCurrentRobotHealth() { 
    return currentRobotHealth; 
}


void NetworkManager::setCurrentRobotHealth(int currentRobotHealth) {
	this->currentRobotHealth = currentRobotHealth;
}


/**
 * Returns the id of the spark being controlled.
 * @return the current spark id
 */
int NetworkManager::getCurrentSparkId() {
    return currentSparkId;
}

/**
 * Sends the specified Event to the server.
 * @param event  the event to send over
 */
int NetworkManager::sendEvent(Event * event)
{
    std::string serializeStr = event->Serialize();

    // setting up the message to be sent
	int messageSize = serializeStr.length() + 1;
	char * message = new char[messageSize];
	strncpy_s(message, messageSize, serializeStr.c_str(), serializeStr.length());
	message[messageSize - 1] = '\0';

	// change byte order and include null character
	unsigned long messageSizeToSend = htonl(messageSize);

	// warning, since we haven't implemented bound check and splitting up packets
	if (messageSize >= MAX_MESSAGE_SIZE)
		cerr << "[WARNING] Client: A message size greater than the maximum message size" << endl;

    // send the size to client first
	int nBytes = send(serverSocket, (char *) &messageSizeToSend,
						sizeof(messageSizeToSend), 0);

	// check for error
	bool error = checkSocketError(nBytes, serverSocket);

	if (!error) {
        nBytes = send(serverSocket, serializeStr.c_str(), messageSize, 0);
		checkSocketError(nBytes, serverSocket);
	}

    delete [] message;
	return 0;
}

/**
 * Check if there is a socket error or if the client closed the socket.
 * @param nBytes the number of bytes transmitted
 * @param clientSocket the socket of the client
 * @return true if there is an error
 */
bool NetworkManager::checkSocketError(int nBytes, SOCKET clientSocket) {
	if (nBytes == SOCKET_ERROR || nBytes == 0)
		return true;	
	else
		return false;	
}