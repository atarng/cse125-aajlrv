#ifndef _HUD_MENU_BUTTON_H_
#define _HUD_MENU_BUTTON_H_

#include "HUDMenuItem.h"
#include "HUDDisplayable.h"

#include <vector>

class HUDMenuButton : public HUDMenuItem
{
private:
    typedef HUDMenuItem super;

   

public:
    HUDMenuButton();
    ~HUDMenuButton();
    
   

////INHERITED MEMBERS:
//struct HUDItemConfig
//{
//    int positionX;
//    int positionY;
//    int id;
//};
//std::vector<HUDItemConfig> HUDItemConfigVector;
//bool displayOn;
//bool clickable;
//float originX, originY;
//float mi_width, mi_height;
//bool originSet;
//bool isToBeDisplayed();
//bool isClickable();
//bool isInBounds(float x, float y);
//void getBounds(float &w, float &h);
//void getPosition(float &x, float &y);
//void setBounds(float, float);
//void setPosition(float, float);
//void setDisplay(bool);
//void setClickable(bool);
//static HUDItemConfig createHUDItemConfig(int, int, int);
//std::vector<HUDItemConfig>& getItemConfig();
};

#endif