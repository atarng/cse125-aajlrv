//LightNode.cpp
#include "LightNode.h"

LightNode::LightNode()
{
    light = NULL;
}
LightNode::LightNode(const LightNode &)
{
}
LightNode::~LightNode()
{
    delete light;
    light = NULL;
}

void LightNode::UpdateMatrix(){
    light->GetMatrix(matrix);
}

void LightNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    UpdateMatrix();
    stack->Push();
    
    stack->MultMatrix(&matrix);

    if (drawMode == 0) Render(d3dMan);
  
    std::vector<SceneNode *>::iterator it;
    for( it = children.begin(); it < children.end(); it++ )
    {
        (*it)->Draw( d3dMan, stack, drawMode );
    }

    stack->Pop();
}

void LightNode::Render(D3DManager* d3dMan)
{    
    
}