#include "HUDDisplayable.h"

HUDDisplayable::HUDDisplayable()
{
    alphaComponent = false;
}
HUDDisplayable::HUDDisplayable(HUDDisplayable &){}
HUDDisplayable::~HUDDisplayable(){}

void HUDDisplayable::setAlphaComponent(bool alphaOn) { alphaComponent = alphaOn; }
bool HUDDisplayable::hasAlphaComponent() { return alphaComponent; }

bool HUDDisplayable::isText()    { return false; }
bool HUDDisplayable::isGraphic() { return false; }