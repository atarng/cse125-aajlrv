///////////////////////////////////////////////////////////////////////
// A BETTER DESCRIPTOR MIGHT BE TO CALL IT THE GAMESTATE MANAGER....
///////////////////////////////////////////////////////////////////////

#ifndef _UNIT_MANAGER_CLIENT_
#define _UNIT_MANAGER_CLIENT_

#include <map>

class Unit;
class UnitNode;
class UnitObject;
class HUDMiniMap;

class UnitManagerClient
{
private:
    static  UnitManagerClient * m_Instance;

    struct UnitPackage
    {
        UnitObject* unitObject;
        Unit*       unit;
        int mapIndex;
    };
    std::map<int, UnitPackage> units;

	void updateCounter();
public:
	UnitManagerClient();
	UnitManagerClient(const UnitManagerClient&);
	~UnitManagerClient();

    bool updateClient(Unit *);
    UnitPackage getUnit(Unit *, bool &);
	UnitPackage* getUnitByID(int i);

    void SetSpecificUnitProperties( UnitNode& unitNode, UnitObject& unitObjRef, int ID );

    static UnitManagerClient * Instance();
    void deleteUnit(int networkId);
};

#endif