//MeshManager.h
#ifndef _MESH_MANAGER_H_
#define _MESH_MANAGER_H_

#include <vector>
#include "Mesh.h"

class MeshManager
{
private:
    std::vector<Mesh *> meshObjects;

    struct MeshSelecter
    {
        int TERRAIN_OBJECT;
        int LASER, LASER2;
		int AOE;
        int ELECTRICSPARK;
        int METEOR;
        int BALLS;

        int TABLET, TABLET2;
        int SKYBOX;

        int PROBE, PROBE_WALK, PROBE_ATTACK;
        int TEST_DROID, DROID_WALK, DROID_ATTACK;
		int TEST_ELECTRICTHING, FAN_BOT_WALK, FAN_BOT_ATTACK;

        int TEST_CUBE;

        int TEST_ANIM_DAE;
        int TEST_SKELETAL_ANIM_DAE;
        int TEST_COLLISION_SPHERE;

		int SATELLITE_SHIP;

    } meshSelector;

public:
    MeshManager();
    //MeshManager(const MeshManager &);
    ~MeshManager();

    bool Initialize();
    int AddMesh(Mesh& newMesh);
    Mesh & GetMesh(int index);
    
    void resetAnims();

    inline MeshSelecter GetMeshSelector(){ return meshSelector; };

    inline std::vector<Mesh *> GetMeshs(){ return meshObjects; };
};

#endif