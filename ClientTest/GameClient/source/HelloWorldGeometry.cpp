#include "HelloWorldGeometry.h"
#include "Debugger.h"

HelloWorldGeometry::HelloWorldGeometry()
{
};
HelloWorldGeometry::~HelloWorldGeometry()
{
};

// CREATE SHADERS.
void HelloWorldGeometry::Shaders(D3DManager * d3dm_ptr)
{ 
        ID3D10Blob *VS, *PS;
        D3DX11CompileFromFile("resource/rainbow_shader.hlsl", 0, 0, "VShader", "vs_4_0", 0, 0, 0, &VS, 0, 0);
        D3DX11CompileFromFile("resource/rainbow_shader.hlsl", 0, 0, "PShader", "ps_4_0", 0, 0, 0, &PS, 0, 0);

        // encapsulate both shaders into shader objects
        d3dm_ptr -> GetDevice() -> CreateVertexShader(VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &VertexShaderPtr);
        d3dm_ptr -> GetDevice() -> CreatePixelShader(PS->GetBufferPointer(),  PS->GetBufferSize(), NULL,  &PixelShaderPtr);

        // create the input layout object
        D3D11_INPUT_ELEMENT_DESC ied[] =
        {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"COLOR",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
        };
        d3dm_ptr -> GetDevice() ->CreateInputLayout(ied, 2, VS->GetBufferPointer(), VS->GetBufferSize(), &InputLayoutPtr);

        //// set the shader objects
        //d3dm_ptr -> GetDeviceContext() -> VSSetShader(VertexShaderPtr, 0, 0); // V
        //d3dm_ptr -> GetDeviceContext() -> PSSetShader(PixelShaderPtr, 0, 0);  // V

        //d3dm_ptr -> GetDeviceContext() -> IASetInputLayout(InputLayoutPtr); // V
}

void HelloWorldGeometry::Triangle(D3DManager * d3dm_ptr)
{
//    cout << "Initializing Triangle..." << endl;

    VERTEX TriangleVertices[] =
    {
        {  0.0f,  0.5f, 0.0f, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f)},
        { 0.45f, -0.5f, 0.0f, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f)},
        {-0.45f, -0.5f, 0.0f, D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f)}
    };

    // create the vertex buffer
    D3D11_BUFFER_DESC BufferDescription;
    ZeroMemory(&BufferDescription, sizeof(BufferDescription));

    BufferDescription.Usage = D3D11_USAGE_DYNAMIC;                // write access access by CPU and GPU
    BufferDescription.ByteWidth = sizeof(VERTEX) * 3;             // size is the VERTEX struct * 3
    BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;       // use as a vertex buffer
    BufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;    // allow CPU to write in buffer

    // create the buffer
    d3dm_ptr -> GetDevice() -> CreateBuffer(&BufferDescription, NULL, &VertexBufferPtr);

    // copy the vertices into the buffer
    D3D11_MAPPED_SUBRESOURCE ms;
    d3dm_ptr -> GetDeviceContext() -> Map(VertexBufferPtr, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // map the buffer
    memcpy(ms.pData, TriangleVertices, sizeof(TriangleVertices));                                     // copy the data
    d3dm_ptr -> GetDeviceContext() ->Unmap(VertexBufferPtr, NULL);                                    // unmap the buffer
}

void HelloWorldGeometry::ParticleSystem(D3DManager*d3dm_ptr)
{
}

///////////////////////////////////////////////////////////////////

void HelloWorldGeometry::setShaders_TEMPORARY( D3DManager * d3dm_ptr )
{
    // set the shader objects
    d3dm_ptr -> GetDeviceContext() -> VSSetShader( VertexShaderPtr, 0, 0 ); // V
    d3dm_ptr -> GetDeviceContext() -> PSSetShader( PixelShaderPtr, 0, 0  );  // V

    d3dm_ptr -> GetDeviceContext() -> IASetInputLayout(InputLayoutPtr); // V
}

void HelloWorldGeometry::Render(D3DManager * d3dm_ptr)
{
//    cout << "Calling HelloWorldGeometry::Render() " << endl;

    // select which vertex buffer to display
    UINT stride = sizeof(VERTEX);
    UINT offset = 0;

    d3dm_ptr -> GetDeviceContext() -> IASetVertexBuffers(0, 1, &VertexBufferPtr, &stride, &offset);

    // select which primtive type we are using
    d3dm_ptr -> GetDeviceContext() -> IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    //SET SHADERS
    setShaders_TEMPORARY(d3dm_ptr);

    // draw the vertex buffer to the back buffer
    //STDMETHODCALLTYPE Draw( 
    //        __in  UINT VertexCount,
    //        __in  UINT StartVertexLocation)
    d3dm_ptr -> GetDeviceContext() -> Draw(3, 0);
}