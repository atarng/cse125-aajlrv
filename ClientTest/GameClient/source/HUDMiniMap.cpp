/***
 * Filename: HUDMiniMap.cpp
 * Contributor(s): Alfred
 * Purpose:
 */

#include "HUDMiniMap.h"
#include "HUDManager.h"
#include <iostream>
#include <iterator>

#include "SystemManager.h"
#include "UnitObject.h"

#define MAPSCALE .025f

using namespace std;

HUDMiniMap::HUDMiniMap()
{
	displayOn = true;
    clickable = false;
    originX = originY = 0;
	originSet = false;
	alphaFloor = 0.50f;

    m_Map = 0;
}
HUDMiniMap::~HUDMiniMap()
{
}

bool HUDMiniMap::Initialize(HUDDisplayable& map)
{
    super::Initialize();
    m_Map = &map;
    return true;
}

void HUDMiniMap::Render( D3DManager * d3dMan,
                         D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX orthoMatrix)
{
    UnitObject* cpu = SystemManager::Instance()->GetCurrentPlayerUnit();
    if(m_Map != NULL && cpu != NULL)
    {
        ((HUDGraphic* )m_Map)->ChangePartitionLocation( cpu->GetPosition().x / (256.f*5.f) , 1 - cpu->GetPosition().z / (256.f*5.f) );
        ((HUDGraphic* )m_Map)->m_partitionOffset = ((cpu->GetRotation().y)*0.0174532925f);

        //if( cpu != NULL ) D3DXMatrixRotationYawPitchRoll( &worldMatrix, 0.f,0.f, 22.5f*0.0174532925f );
    }
    super::Render(d3dMan, worldMatrix, viewMatrix, orthoMatrix);

    D3DXMATRIX testGUIViewMatrix;
    D3DXVECTOR3 testPosition = D3DXVECTOR3( 0.0, 0.0, -10.0f ); //D3DXVECTOR3(0,0,0);
    D3DXVECTOR3 testLookAtVec = D3DXVECTOR3(0,0,0);
    D3DXVECTOR3 testUp = D3DXVECTOR3(0,1,0);
    D3DXMatrixLookAtLH(&testGUIViewMatrix, &testPosition, &testLookAtVec, &testUp);

    d3dMan->TurnZBufferOff( 0 );
    if( m_Map == NULL )
    {
        // originX, originY, mi_width, mi_height,
        std::map<UnitObject*,MapIconWrapper>::iterator it;
        for( it = MapElementsMap.begin(); it != MapElementsMap.end(); ++it )
        {
            if ((*it).second.toBeRemoved)
            {// TEMPORARILY HIDE ELEMENTS INSTEAD OF DELETING THEM.
                it = MapElementsMap.erase(it);
                if (it == MapElementsMap.end()) break;
                continue;
            }else
            {
                if ( ((HUDGraphic*)(*it).second.displayable)->hasAlphaComponent() ) d3dMan->EnableAlphaBlending(0);
                else                                                                d3dMan->DisableAlphaBlending();

                if(!(HUDGraphic*)(*it).second.displayable->isGraphic()) continue;
                D3DXVECTOR3 position = (*it).second.associatedUnitObj->GetPosition();
                float unitYaw = 0.0f;
                D3DXMATRIX   unitRot, unitTrans, unitScale;
                float renderPosX, renderPosY, scale;

                renderPosX = position.x / mi_width;
                renderPosY = position.z / mi_height;

                unitYaw  = 180 - (*it).second.associatedUnitObj->GetRotation().y;
                unitYaw *= 0.0174532925f;
                /*
			    scale      = //( abs(1 / (position.y * MAPSCALE)) <= 1.0f ) ? abs( 1 / (position.y * MAPSCALE) ) : 1.0f ;
                               ( abs(1 / (position.y * MAPSCALE)) <= 0.5f ) ? 1 - abs( 1 / (position.y * MAPSCALE) ) : 0.5f ;
                */
                scale = min(max(1-(1 / (position.y * MAPSCALE)), 0.25f), 2.00f);
                D3DXMatrixRotationYawPitchRoll(&unitRot, 0.0f, 0.0f, unitYaw);
                D3DXMatrixScaling( &unitScale, scale,scale,scale);

                // HARDCODED VALUE HELP:
                // 10 : scale factor (5 times because map is scale 5, another 2, because map is compressed by two times)
                // 16 : half of image dimensions.
                float mapScaleFactor = 12.8f;
                int dimensionsHalved = 16;

                float wwd2 = (SystemManager::Instance()->windowWidth / 2.0f);
                float whd2 = (SystemManager::Instance()->windowHeight / 2.0f);
                D3DXMatrixTranslation(&unitTrans, ( originX - wwd2               + (renderPosX * mapScaleFactor)),
                                                  (-(originY-whd2) - (mi_height) + (renderPosY * mapScaleFactor)),
                                                  0.0f);

                // Places map icon vertices in buffer.
                ((HUDGraphic*)(*it).second.displayable)->Render( d3dMan->GetDeviceContext(),
                                                                (int)(wwd2 - dimensionsHalved),
                                                                (int)(whd2 - dimensionsHalved) );

                // tints map icon.
                ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
                D3DXVECTOR4 textColor = D3DXVECTOR4( ((*it).second.associatedUnitObj->GetTint()->x),
                                                     ((*it).second.associatedUnitObj->GetTint()->y),
                                                     ((*it).second.associatedUnitObj->GetTint()->z),
                                                     0.0f );

                myRSC.tintColor = &( textColor );
			    myRSC.alphaCap  = 0.75f;
                HUDManager::Instance() -> GetShader() ->
                            Render( d3dMan->GetDeviceContext(), ((HUDGraphic*)(*it).second.displayable)->GetIndexCount(), 0,
                                    unitRot * unitScale * unitTrans * worldMatrix, testGUIViewMatrix, orthoMatrix,
                                    ((HUDGraphic*)(*it).second.displayable)->GetTextureObject()->GetTextures(),
                                    ((HUDGraphic*)(*it).second.displayable)->GetTextureObject()->numTextures(),
                                    myRSC
                                   );

                if ( ((HUDGraphic*)(*it).second.displayable)->hasAlphaComponent() ) d3dMan->DisableAlphaBlending();
            }
        }
    }else
    {// RADAR MAP
        // originX, originY, mi_width, mi_height,
        std::map<UnitObject*,MapIconWrapper>::iterator it;
        for( it = MapElementsMap.begin(); it != MapElementsMap.end(); ++it )
        {
            if ((*it).second.toBeRemoved)
            {
                it = MapElementsMap.erase(it);
                if (it == MapElementsMap.end()) break;
                continue;
            }else
            {
                D3DXVECTOR3 position = (*it).second.associatedUnitObj->GetPosition();
                D3DXVECTOR3 relativePos;
                D3DXVECTOR3 renderPos;
                float unitYaw = 0.0f, cpuYaw, scale;
                D3DXMATRIX   unitRot, unitTrans, unitScale;

                relativePos.x = cpu->GetPosition().x - position.x;
                relativePos.y = cpu->GetPosition().y - position.y;
                relativePos.z = cpu->GetPosition().z - position.z;

                cpuYaw = ((cpu->GetRotation().y) * 0.0174532925f);
                D3DXMatrixRotationYawPitchRoll(&unitRot, 0.0,0.0, cpuYaw);
                //D3DXMatrixMultiply(&unitTrans, &unitTrans, &unitRot);

                renderPos.x = (float)relativePos.x / (float)mi_width;
                renderPos.y = (float)relativePos.z / (float)mi_height;
                renderPos.z = 0.f;

                D3DXVec3TransformCoord(&renderPos, &renderPos, &unitRot );

                float mapScaleFactor = 12.8f;
                int dimensionsHalved = 16;
                float wwd2 = (SystemManager::Instance()->windowWidth / 2.0f);
                float whd2 = (SystemManager::Instance()->windowHeight / 2.0f);
                D3DXMatrixTranslation(&unitTrans,  originX-wwd2 + (mi_width/2.f)  - (renderPos.x*mapScaleFactor * 2 ),
                                                  -originY+whd2 - (mi_height/2.f) - (renderPos.y*mapScaleFactor * 2 ),
                                                  0.0f);

                //unitYaw  = 180 - (*it).second.associatedUnitObj->GetRotation().y;
                //unitYaw *= 0.0174532925f;
                scale = min(max(1-(1 / (position.y * MAPSCALE)), 0.25f), 2.00f);
                D3DXMatrixRotationYawPitchRoll(&unitRot, 0.0f, 0.0f, unitYaw);
                D3DXMatrixScaling( &unitScale, scale,scale,scale);

                ((HUDGraphic*)(*it).second.displayable)->Render( d3dMan->GetDeviceContext(),
                                                                (int)(wwd2 - dimensionsHalved),
                                                                (int)(whd2 - dimensionsHalved) );

                ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
                D3DXVECTOR4 textColor = D3DXVECTOR4( ((*it).second.associatedUnitObj->GetTint()->x),
                                                     ((*it).second.associatedUnitObj->GetTint()->y),
                                                     ((*it).second.associatedUnitObj->GetTint()->z),
                                                     0.0f );
                myRSC.tintColor = &( textColor );

                HUDManager::Instance() -> GetShader() ->
                            Render( d3dMan->GetDeviceContext(), ((HUDGraphic*)(*it).second.displayable)->GetIndexCount(), 0,
                                    //unitRot *
                                    unitScale * unitTrans * worldMatrix, testGUIViewMatrix, orthoMatrix,
                                    ((HUDGraphic*)(*it).second.displayable)->GetTextureObject()->GetTextures(),
                                    ((HUDGraphic*)(*it).second.displayable)->GetTextureObject()->numTextures(),
                                    myRSC
                                   );
            }
        }
    }

    d3dMan->TurnZBufferOn();

    return;
}

int HUDMiniMap::addMiniMapItem( HUDDisplayable& displayableHudElement, UnitObject& associatedUnitObject )
{
    MapIconWrapper mic;
    mic.displayable       = &displayableHudElement;
    mic.associatedUnitObj = &associatedUnitObject;
    mic.toBeRemoved = false;

    //MapElements.push_back(mic);
    MapElementsMap.insert( pair<UnitObject*, MapIconWrapper>(&associatedUnitObject, mic) );

    //return MapElements.size() - 1;
    return 0;
}
//void HUDMiniMap::markForRemoval(int hideItem)
void HUDMiniMap::markForRemoval(UnitObject& uo)
{
    //MapElements.at(hideItem).toBeRemoved = true;
    MapElementsMap.find(&uo)->second.toBeRemoved = true;
}