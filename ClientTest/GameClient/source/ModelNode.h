#ifndef _MODELNODE_H_
#define _MODELNODE_H_

#include "D3DManager.h"
#include "SceneNode.h"
#include "ShaderObject.h"

// FORWARD DECLARATIONS
class Model;

class ModelNode : public SceneNode
{
private:
    Model * geode;

protected:
    D3DXVECTOR3 mn_scale, mn_rotation, mn_translation;
    ShaderObject* shader;
    D3DXMATRIX calculatedMatrix;

public:
    ModelNode();
    ModelNode(Model&);
    ModelNode(const ModelNode &);
    ~ModelNode();

    void setPosition(D3DXVECTOR3);
    void setOrientation(D3DXVECTOR3);
    void setScale(D3DXVECTOR3);

    inline void SetShader(ShaderObject* newShader){ shader = newShader; }

    virtual void UpdateMatrix();
    virtual void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );//=0;
    virtual void Render(D3DManager* d3dMan);//=0;
    virtual void RenderDepth(D3DManager* d3dMan);
};

#endif