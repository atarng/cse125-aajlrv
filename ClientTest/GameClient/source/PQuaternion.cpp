//PQuaternion

#include "PQuaternion.h"
#include <math.h>

//Modeled after the d3d10math header
PQuaternion::PQuaternion(){
}
PQuaternion::~PQuaternion(){
}
/////////////////////////////////////////////
// Assignment operators
/////////////////////////////////////////////
PQuaternion& PQuaternion::operator += ( const PQuaternion& )
{
    return *this;
}
PQuaternion& PQuaternion::operator -= ( const PQuaternion& ){
    return *this;
}
PQuaternion& PQuaternion::operator *= ( const PQuaternion& ){
    return *this;
}
PQuaternion& PQuaternion::operator *= ( float scalar ){
    return *this;
}
PQuaternion& PQuaternion::operator /= ( float scalar ){
    return *this;
}

/////////////////////////////////////////////
// Basic Arithmatic
/////////////////////////////////////////////
PQuaternion PQuaternion::operator + ( const PQuaternion& ) const{
    return *this;
}
PQuaternion PQuaternion::operator - ( const PQuaternion& ) const{
    return *this;
}
PQuaternion PQuaternion::operator * ( const PQuaternion& ) const{
    return *this;
}
PQuaternion PQuaternion::operator * ( float scalar ) const{
    return *this;
}
PQuaternion PQuaternion::operator / ( float scalar ) const{
    return *this;
}

/////////////////////////////////////////////
//Boolean Operators
/////////////////////////////////////////////
bool PQuaternion::operator == ( const PQuaternion& other) const{
    return ( x==other.x && y==other.y && z==other.z && w==other.w );
}
bool PQuaternion::operator != ( const PQuaternion& other) const{
    return ( x!=other.x && y!=other.y && z!=other.z && w!=other.w );
}

/////////////////////////////////////////////
//USEFUL STUFF
/////////////////////////////////////////////
float PQuaternion::magnitude(){
    float temp = magnitude2();
    if(temp != 0){
        return sqrt(temp);
    }
    return 0;
}
float PQuaternion::magnitude2(){
    return x*x+y*y+z*z+w*w;
}

std::string PQuaternion::toString() const{
    std::stringstream out;
    out << '(' << x << ',' << y << ',' << z << ',' << w << ',' << ')';
    return out.str();
}