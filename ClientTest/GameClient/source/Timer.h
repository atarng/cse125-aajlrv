//Timer.h

#ifndef _TIMER_H_
#define _TIMER_H_

#include <ctime>
#include <time.h>

class Timer
{
public:
    Timer();
    Timer(const Timer&);
    ~Timer();

    float GetStartTime();
    float GetCurrTime();
    float GetTimeSinceStart();
    float Update();

    inline clock_t GetClockStart(){ return startTime; }
    inline clock_t GetClockCurr(){ return currTime; }
    
    // divide a clock_t by "CLOCKS_PER_SEC" to convert to seconds

private:
    clock_t startTime;
    clock_t currTime;

};

#endif