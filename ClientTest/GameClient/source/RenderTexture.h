////////////////////////////////////////////////////////////////////////////////
// Filename: RenderTexture.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _RENDERTEXTURE_H_
#define _RENDERTEXTURE_H_


//////////////
// INCLUDES //
//////////////
#include "D3DManager.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: RenderTexture
////////////////////////////////////////////////////////////////////////////////
class RenderTexture
{
public:
	RenderTexture();
	RenderTexture(const RenderTexture&);
	~RenderTexture();

    bool Initialize(ID3D11Device*, int, int);
	bool Initialize(ID3D11Device*, int, int, float, float); // RRR
	void Shutdown();

	void SetRenderTarget(ID3D11DeviceContext*);
	void ClearRenderTarget(ID3D11DeviceContext*, float, float, float, float);

    void SetRenderTarget(ID3D11DeviceContext*, ID3D11DepthStencilView*); // RRR
	void ClearRenderTarget(ID3D11DeviceContext*, ID3D11DepthStencilView*, float, float, float, float); // RRR

	ID3D11ShaderResourceView* GetShaderResourceView();
	void GetProjectionMatrix(D3DXMATRIX&);
	void GetOrthoMatrix(D3DXMATRIX&);
    
private:
	ID3D11Texture2D          * rt_renderTargetTexture, * rt_depthStencilBuffer;
	ID3D11RenderTargetView   * rt_renderTargetView;
	ID3D11ShaderResourceView * rt_shaderResourceView;
	ID3D11DepthStencilView   * rt_depthStencilView;
	D3D11_VIEWPORT rt_viewport;
	D3DXMATRIX rt_projectionMatrix, rt_orthoMatrix;
};

#endif