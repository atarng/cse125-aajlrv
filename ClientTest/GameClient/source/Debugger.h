#ifndef _DEBUGGER_
#define _DEBUGGER_

#include <stdio.h>
#include <iostream>

using namespace std;

class Debugger
{
public:
    static bool printFPS;
    static bool useGraphicsPipeline;
    static bool holdOnDebug;
    static bool printDebugMsgs;

    static float testFloat;

    Debugger();
    ~Debugger();

    bool Initialize();
};

#endif