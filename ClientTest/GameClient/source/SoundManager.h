////////////////////////////////////////////////////////////////////////////////
// Filename: SoundManager.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SOUND_MANAGER_H_
#define _SOUND_MANAGER_H_


#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")
 
 
#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>
#include <stdio.h>

/***********************************
 * Local includes
 ***********************************/
#include "Timer.h"
//#include "D3DManager.h"

class SoundManager
{
private:
	//structure adapted from rastertek.com
	struct WaveHeaderType
	{
		char chunkId[4];
		unsigned long chunkSize;
		char format[4];
		char subChunkId[4];
		unsigned long subChunkSize;
		unsigned short audioFormat;
		unsigned short numChannels;
		unsigned long sampleRate;
		unsigned long bytesPerSecond;
		unsigned short blockAlign;
		unsigned short bitsPerSample;
		char dataChunkId[4];
		unsigned long dataSize;
	};

	bool looping;
public:
	SoundManager();
	SoundManager(const SoundManager&);
	~SoundManager();
	bool PlaySoundSpec(int, float distance = 0);
	bool PlayIntroLoop();
	void StopIntroLoop();
	bool PlayEndgameLoop();
	void StopEndgameLoop();
	bool PlayMusicLoop();
	void StopMusicLoop();

	bool Initialize(HWND);
	void Shutdown();

	const static int INTRO_SOUND = 0;
	const static int SHOT_SOUND = 1;
	const static int SPACESHIP_SOUND = 2;
	const static int BUZZER_SOUND = 3;
	const static int LOSE_SOUND = 4;
	const static int START_SOUND = 5;
	const static int EXPLODE_SOUND = 6;
	const static int ENDGAME_SOUND = 7;
	const static int MUSIC_SOUND = 8;
	const static int DING_SOUND = 9;
	const static int POWERUP_SOUND = 10;
	const static int AOE_SOUND = 11;
	const static int BULLET2_SOUND = 12;
	const static int METEOR_DETONATE_SOUND = 13;
	const static int METEOR_WHISTLE_SOUND = 14;
	const static int NUCLEAR_SOUND = 15;

private:
	bool InitializeDirectSound(HWND);
	void ShutdownDirectSound();
	bool LoadWaveFile(char*, IDirectSoundBuffer8**);
	void ShutdownWaveFile(IDirectSoundBuffer8**);

	bool PlayWaveFile(IDirectSoundBuffer8*, float);
	

	IDirectSound8* m_DirectSound;
	IDirectSoundBuffer* m_primaryBuffer;

	IDirectSoundBuffer8* m_secondaryBufferIntro;
	IDirectSoundBuffer8* m_secondaryBufferShot;
	IDirectSoundBuffer8* m_secondaryBufferSpaceShip;
	IDirectSoundBuffer8* m_secondaryBufferBuzzer;
	IDirectSoundBuffer8* m_secondaryBufferLose;
	IDirectSoundBuffer8* m_secondaryBufferStart;
	IDirectSoundBuffer8* m_secondaryBufferExplode;
	IDirectSoundBuffer8* m_secondaryBufferEndgame;
	IDirectSoundBuffer8* m_secondaryBufferMusic;
	IDirectSoundBuffer8* m_secondaryBufferPowerup;
	IDirectSoundBuffer8* m_secondaryBufferDing;
	IDirectSoundBuffer8* m_secondaryBufferAOE;
	IDirectSoundBuffer8* m_secondaryBufferBullet2;
	IDirectSoundBuffer8* m_secondaryBufferMeteorDetonate;
	IDirectSoundBuffer8* m_secondaryBufferMeteorWhistle;
	IDirectSoundBuffer8* m_secondaryBufferNuclear;

};

#endif