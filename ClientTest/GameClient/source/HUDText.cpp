#include "HUDText.h"
#include "Log.h"

#include <iostream>

#include "ShaderObject.h"

#define MAXLENGTH 140

HUDText::HUDText()
{
    m_Font = 0;
    m_FontShader = 0;

    m_Sentence = 0;
	m_positionX = 0;
	m_positionY = 0;
	m_red = 1.0f;
	m_green = 1.0f;
	m_blue = 0.0f;
    m_textMessage = "";
}
HUDText::HUDText(const HUDText& other){}
HUDText::~HUDText(){}

bool HUDText::Initialize(ID3D11Device* device, ID3D11DeviceContext* deviceContext,
                         int screenWidth, int screenHeight,
                         char * fontdata, TextureObject * to,//char * fonttexture,
                         char * textMessage
                         )
{
    bool result;

    // Store the screen width and height.
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
	m_textMessage = textMessage;

    // Create the font object.
    m_Font = new FontObject;
    if(!m_Font) return false;

    // Initialize the font object.
    result = m_Font->Initialize(device, fontdata);
    if(!result)
    {
        cout << "[HUDText] Could not initialize the font object." << endl;
        Log::Instance()->PrintErr("[HUDText] Could not initialize the font object.");
        return false;
    }

    m_Font -> SetTextureObject(*to);

    // Initialize the first sentence.
    result = InitializeSentence(&m_Sentence, MAXLENGTH , device);
    if(!result)
    {
        cout << "[HUDText] Could not Initialize the first sentence." << endl;
        Log::Instance()->PrintErr("[HUDText] Could not Initialize the first sentence.");
        return false;
    }

    // Now update the sentence vertex buffer with the new string information.
    result = UpdateSentence(m_Sentence , textMessage, m_positionX, m_positionY, 1.0f, 1.0f, 0.0f, deviceContext);
    if(!result)
    {
        cout << "[HUDText] Could not update the sentence vertex buffer with the new string information." << endl;
        Log::Instance()->PrintErr("[HUDText] Could not update the sentence vertex buffer with the new string information.");
        return false;
    }

    return true;
}
bool HUDText::InitializeSentence(SentenceType** sentence, int maxLength, ID3D11Device* device)
{
	//cout << "[HUDText] InitializeSentence" << endl;
    HRESULT result;
    VertexType* vertices;
    unsigned long* indices;
    D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData;

    // Create a new sentence object.
    *sentence = new SentenceType;
    if(!*sentence) return false;

    // Initialize the sentence buffers to null.
    (*sentence)->vertexBuffer = 0;
    (*sentence)->indexBuffer = 0;

    // Set the maximum length of the sentence.
    (*sentence)->maxLength = maxLength;

    // Set the number of vertices in the vertex array.
    (*sentence)->vertexCount = 6 * maxLength;

    // Set the number of indexes in the index array.
    (*sentence)->indexCount = (*sentence)->vertexCount;

    // Create the vertex array.
    vertices = new VertexType[(*sentence)->vertexCount];
    if(!vertices) return false;

    // Create the index array.
    indices = new unsigned long[(*sentence)->indexCount];
    if(!indices) return false;

    // Initialize vertex array to zeros at first.
    memset(vertices, 0, (sizeof(VertexType) * (*sentence)->vertexCount));

    // Initialize the index array.
    for(int i=0; i<(*sentence)->indexCount; i++)
    {
        indices[i] = i;
    }

    // Set up the description of the dynamic vertex buffer.
    vertexBufferDesc.Usage          = D3D11_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth      = sizeof(VertexType) * (*sentence)->vertexCount;
    vertexBufferDesc.BindFlags      = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags      = 0;
    vertexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
    vertexData.SysMemPitch = 0;
    vertexData.SysMemSlicePitch = 0;

    // Create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &(*sentence)->vertexBuffer);
    if(FAILED(result))
    {
        cerr << "[HUDText] FAILED Create the vertex buffer." << endl;
        Log::Instance()->PrintErr("[HUDText] FAILED Create the vertex buffer.");
        return false;
    }

    // Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * (*sentence)->indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
    indexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
    indexData.SysMemPitch = 0;
    indexData.SysMemSlicePitch = 0;

    // Create the index buffer.
    result = device->CreateBuffer(&indexBufferDesc, &indexData, &(*sentence)->indexBuffer);
    if(FAILED(result))
    {
        cerr << "[HUDText] FAILED Create the index buffer." << endl;
        Log::Instance()->PrintErr("[HUDText] FAILED Create the index buffer.");
        return false;
    }

    // Release the vertex array as it is no longer needed.
    delete [] vertices;
    vertices = 0;

    // Release the index array as it is no longer needed.
    delete [] indices;
    indices = 0;

    return true;
}

bool HUDText::UpdateSentence(SentenceType* sentence, char* text, int positionX, int positionY,
                             float red, float green, float blue,
                             ID3D11DeviceContext* deviceContext)
{
    int numLetters;
    VertexType* vertices;
    float drawX, drawY;
    HRESULT result;
    D3D11_MAPPED_SUBRESOURCE mappedResource;
    VertexType* verticesPtr;

    // Store the color of the sentence.
    sentence->red   = red;
    sentence->green = green;
    sentence->blue  = blue;

    // Get the number of letters in the sentence.
    numLetters = (int)strlen(text);

    // Check for possible buffer overflow.
    if(numLetters > sentence->maxLength)
    {
        return false;
    }

    // Create the vertex array.
    vertices = new VertexType[sentence->vertexCount];
    if(!vertices)
    {
        return false;
    }

    // Initialize vertex array to zeros at first.
    memset(vertices, 0, (sizeof(VertexType) * sentence->vertexCount));

    // Calculate the X and Y pixel position on the screen to start drawing to.
    drawX = (float)(((m_screenWidth / 2) * -1) + positionX);//m_positionX); //was positionX
    drawY = (float)((m_screenHeight / 2)       - positionY);//m_positionY);


    //printf("Updating Text to: %s\n", text);

    // Use the font class to build the vertex array from the sentence text and sentence draw location.
    m_Font->BuildVertexArray((void*)vertices, text, drawX, drawY);

    // Lock the vertex buffer so it can be written to.
    result = deviceContext->Map(sentence->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
    if(FAILED(result))
    {
        return false;
    }

    // Get a pointer to the data in the vertex buffer.
    verticesPtr = (VertexType*)mappedResource.pData;

    // Copy the data into the vertex buffer.
    memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * sentence->vertexCount));

    // Unlock the vertex buffer.
    deviceContext->Unmap(sentence->vertexBuffer, 0);

    // Release the vertex array as it is no longer needed.
    delete [] vertices;
    vertices = 0;

    return true;
}

void HUDText::ChangeText(char * text) {
	m_textMessage = text;
}

void HUDText::ChangeColor(float red, float green, float blue) {
	m_red = red;
	m_green = green;
	m_blue = blue;
}

bool HUDText::Render( ID3D11DeviceContext* deviceContext, int positionX, int positionY )
{
	//cout << "[HUDText] Render" << endl;
    bool result;

	m_positionX = positionX;
	m_positionY = positionY;

	UpdateSentence(m_Sentence , m_textMessage, m_positionX, m_positionY, m_red, m_green, m_blue, deviceContext);

    result = RenderSentence(deviceContext, m_Sentence); // , worldMatrix, orthoMatrix, viewMatrix);
    if(!result)
    {
        cout << "[HUDText] Failed to render sentence" << endl;
        return false;
    }
    return true;
}
bool HUDText::RenderSentence( ID3D11DeviceContext* deviceContext, SentenceType* sentence
                             )
{
	//cout << "[HUDText] RenderSentence" << endl;
    unsigned int stride, offset;

	D3DXVECTOR4 pixelColor;
    bool result = true;

    // Set vertex buffer stride and offset.
    stride = sizeof(VertexType); 
    offset = 0;

    // Set the vertex buffer to active in the input assembler so it can be rendered.
    deviceContext->IASetVertexBuffers(0, 1, &sentence->vertexBuffer, &stride, &offset);
    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext->IASetIndexBuffer(sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);
    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
    deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    // Create a pixel color vector with the input sentence color. // Deprecated.
    pixelColor = D3DXVECTOR4(sentence->red, sentence->green, sentence->blue, 1.0f);

    return result;
}

void HUDText::SetShader(ShaderObject* setShader)
{
    m_FontShader = &(*setShader);
}

void HUDText::Shutdown()
{
    // Release the first sentence.
    ReleaseSentence(&m_Sentence );

    // Release the font shader object.
    if(m_FontShader)
    {
        m_FontShader->Shutdown();
        delete m_FontShader;
        m_FontShader = 0;
    }

    // Release the font object.
    if(m_Font)
    {
        m_Font->Shutdown();
        delete m_Font;
        m_Font = 0;
    }

    return;
}
void HUDText::ReleaseSentence(SentenceType** sentence)
{
    if(*sentence)
    {
        // Release the sentence vertex buffer.
        if((*sentence)->vertexBuffer)
        {
            (*sentence)->vertexBuffer->Release();
            (*sentence)->vertexBuffer = 0;
        }

        // Release the sentence index buffer.
        if((*sentence)->indexBuffer)
        {
            (*sentence)->indexBuffer->Release();
            (*sentence)->indexBuffer = 0;
        }

        // Release the sentence.
        delete *sentence;
        *sentence = 0;
    }

    return;
}

bool HUDText::isText() { return true; }