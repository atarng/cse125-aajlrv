////////////////////////////////////////////////////////////////////////////////
// Filename: CameraObject.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CAMERAOBJECT_H_
#define _CAMERAOBJECT_H_


//////////////
// INCLUDES //
//////////////
#include <math.h>
#include <D3DX/d3d10_1.h>
#include <D3DX/d3dx10math.h>
#include "UnitObject.h"

#define MAX_ZOOM 200.0f //far from object
#define MIN_ZOOM 25.0f  //near the object
#define ZOOM_PERCENT 0.15f
#define MAX_NUM_SHAKE_FRAMES 300.0f

enum CameraType
{
    DISABLED,
    FREE,
    FOLLOW,
    FIRST_PERSON,
};

////////////////////////////////////////////////////////////////////////////////
// Class name: CameraObject
////////////////////////////////////////////////////////////////////////////////
class CameraObject
{
public:
	CameraObject();
	CameraObject(const CameraObject&);
	~CameraObject();
    
    inline void SetPosition(D3DXVECTOR3 newPos)         { pos = newPos; }    
    inline void SetRotation(D3DXVECTOR3 newDir)         { dir = newDir; }
    inline void SetCameraType(CameraType newCamType)    { camType = newCamType; }
    inline void SetToFollowUnit(UnitObject* playerUnit) { followUnit = playerUnit; }

    inline void ZoomIn() { zoom = (zoom < MIN_ZOOM)?MIN_ZOOM:zoom*(1.0f-ZOOM_PERCENT);}
    inline void ZoomOut(){ zoom = (zoom > MAX_ZOOM)?MAX_ZOOM:zoom*(1.0f+ZOOM_PERCENT);}
    
    void AddShakeFrames(int numShakes){ shakeFrames += (shakeFrames < MAX_NUM_SHAKE_FRAMES )?numShakes:0;}

    D3DXVECTOR3 GetPosition()       { return pos; }
    D3DXVECTOR3 GetPOffset()        { return pOffset; }
    D3DXVECTOR3 GetLookAt()         { return c_lookAt; }
    D3DXVECTOR3 GetRotation()       { return dir; }
    UnitObject* GetFollowunit()     { return followUnit; }
    CameraType GetCameraType()      { return camType; }    
    float GetCameraZoom()           { return cameraZoom; }

    void GetMatrix(D3DXMATRIX& temp){temp = wtf_matrix;}
    void GetMatrix(D3DXMATRIX&, float deltaTime);
    void GetBillBoardMatrix(D3DXMATRIX&); 

    void Update(D3DXVECTOR3 move, D3DXVECTOR3 rotate);    
  
    void FollowTo(D3DXVECTOR3 move, D3DXVECTOR3 rotate, bool moved);    

    void RenderReflection(float); // RRR
    D3DXMATRIX GetReflectionViewMatrix(); // RRR

private:
    D3DXVECTOR3     pos;
    D3DXVECTOR3     c_lookAt;
    D3DXVECTOR3     pOffset;
    D3DXVECTOR3     dir;
    float cameraZoom;    

    UnitObject* followUnit;

    CameraType camType;    
    int shakeFrames;       

    static float zoom;

    D3DXMATRIX m_reflectionViewMatrix; // RRR

    D3DXMATRIX wtf_matrix;
};

#endif