#include "SceneNode.h"

#include "GraphicsManager.h"

SceneNode::SceneNode()
{
    nodeID = 0;
}
SceneNode::SceneNode(const SceneNode &)
{
}
SceneNode::~SceneNode()
{
    /*
    vector<SceneNode *>::iterator it;
    for( it = children.begin(); it < children.end(); it++ ){ delete (*it); }
    */
}

void SceneNode::addChild(SceneNode& child)
{
    children.push_back(&child);
    child.nodeID = children.size();
}

int SceneNode::numChildren()
{
    return children.size();
}

void SceneNode::OptimizationCheck( int& lodLevel, bool& frustumCull )
{
    lodLevel = 0;
    frustumCull = false;
}