////////////////////////////////////////////////////////////////////////////////
// Filename: InputHandler.cpp
// Contributor(s): Joey Ly
// Purpose: Handles the inputs that the client makes.
// Currently mapped keys:
//    P: Shakes Camera
//    Q: Moves "Up"
//    W: Moves "Forward"
//    A: Strafes "Left"
//    S: Moves "Backward"
//    D: Strafes "Right"
//    Z: Moves "Down"
//
// TODO: Function Pointer up the keyboard input so you can then bind keys to
//       be assigned to other functions.
////////////////////////////////////////////////////////////////////////////////
#include "SystemManager.h"
#include "GraphicsManager.h"
#include "LoadingManager.h"
#include "InputHandler.h"
#include "NetworkManager.h"
#include "HUDBar.h"
#include "HUDCounter.h"
#include "HUDManager.h"

#include "UnitNode.h"
#include "UnitObject.h"

#include <iostream>

#include "AttackObject.h"
#include "MovementEvent.h"
#include "FireEvent.h"
#include "EnterEvent.h"
#include "ExitEvent.h"

void HandleKey(int value) {
	GraphicsManager* graphics = SystemManager::Instance()->GetGraphicsManager();
    CameraObject* camera = graphics->GetCamera();
	InputManager * im = (SystemManager::Instance()->GetInputManager());
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
	if(!hm->SplashIsOn())
		 return;
	//cerr << value << endl;
	
	if (value == 190) //period
		value = 0x2E;
	else if (value == 13) {
			hm->getStart()->onClickEvent();
			return;
	}
	else if (value == VK_BACK) {
		if (hm->getField()->hasFocus()) {
			if (!hm->getField()->backspace()) {
				SoundManager* sound = SystemManager::Instance()->GetSoundManager();
				if (sound) {
					sound->PlaySoundSpec(SoundManager::BUZZER_SOUND);
				}
			}
			return;
		}
		else if (hm->getField2()->hasFocus()) {
			if (!hm->getField2()->backspace()) {
				SoundManager* sound = SystemManager::Instance()->GetSoundManager();
				if (sound) {
					sound->PlaySoundSpec(SoundManager::BUZZER_SOUND);
				}
			}
			return;
		}
	}
	
	//pre-load
	if (hm->getField3()->hasFocus()) {
		hm->getField3()->appendValue((char)value);
		return;
	}

	if (hm->getField2()->hasFocus()) {
		if (value >=0x41 && value <= 0x5A)
			value += 0x20;
		if (isIP(value)) {
			if (!hm->getField2()->appendValue((char)value)) {
				SoundManager* sound = SystemManager::Instance()->GetSoundManager();
				if (sound) {
					sound->PlaySoundSpec(SoundManager::BUZZER_SOUND);
				}
			}
			return;
		}
		else if (value == 9 && im->IsKeyDown(16)) {
			hm->getField()->onClickEvent();
			return;
		}
	}

	if (!im->IsKeyDown(16) && value >=0x41 && value <= 0x5A) //shift
		value += 0x20;
	
	if (hm->getField()->hasFocus()) {
		//tab
		if (value == 9) {
			hm->getField2()->onClickEvent();
			return;
		}
		if (isLetter(value)) {
			if (!hm->getField()->appendValue((char)value)) {
				SoundManager* sound = SystemManager::Instance()->GetSoundManager();
				if (sound) {
					sound->PlaySoundSpec(SoundManager::BUZZER_SOUND);
				}
			}
			return;
		}
		
		
	}

	
}

void HandleInput(InputManager* userInput, float deltaTime)
{
	NetworkManager * networkManager = NULL;

    GraphicsManager* graphics = SystemManager::Instance()->GetGraphicsManager();
    CameraObject* camera = graphics->GetCamera();
    float delta_x = 0, delta_y = 0;
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());

	if (userInput->IsButtonPressed(LEFT))
		HUDManager::Instance()->CheckClickEvent( (float)userInput->mouseX, (float)userInput->mouseY );

	//with splash screen, no more input is valid; try and speed things up this way
	if (hm->SplashIsOn()) return;

	networkManager = NetworkManager::Instance();
	hm->getCrosshairs()->setDisplay(networkManager->getCurrentRobotId() != UNINITIALIZED);
    //hm->getCrosshairs2()->setDisplay(networkManager->getCurrentRobotId() != UNINITIALIZED);

	//MOUSEPRESSES
    static clock_t leftElapsedTime = 0;  // for rapid fire of left click events
	static clock_t rightElapsedTime = 0;  // for rapid fire of right click events
    if(userInput->IsButtonPressed(LEFT) || (userInput->IsButtonDown(LEFT) && clock()-leftElapsedTime > 100)){
        //cout << "lButtonPressed: x=" << userInput->mouseX << "  y=" << userInput->mouseY << endl;

		if (camera && camera->GetCameraType() != DISABLED)
		{
			if (networkManager != NULL)
			{
				int robotId = networkManager->getCurrentRobotId();
				if (robotId != UNINITIALIZED)
				{
					int attackType = BULLET_TYPE;
					FireEvent fireEvent(robotId, attackType);
					networkManager->sendEvent(&fireEvent);
			
					//play shot sound
					/*
					SoundManager* sound = SystemManager::Instance()->GetSoundManager();
					if (sound && hm && hm->getBar2()->getValue() > 0) {
						sound->PlaySoundSpec(SoundManager::SHOT_SOUND);
					}
					*/
				} else {
                    int attackType = 0;
                    int sparkId = networkManager->getCurrentSparkId();
					FireEvent fireEvent(sparkId, attackType);
					networkManager->sendEvent(&fireEvent);
			
					//play shot sound
					SoundManager* sound = SystemManager::Instance()->GetSoundManager();
					if (sound && hm && hm->getBar2()->getValue() > 0) {
						sound->PlaySoundSpec(SoundManager::SHOT_SOUND);
					}
                }
			}
		}
		leftElapsedTime = clock();
    }

	if(userInput->IsButtonPressed(RIGHT) || (userInput->IsButtonDown(RIGHT) && clock()-rightElapsedTime > 1000)){
        //cout << "lButtonPressed: x=" << userInput->mouseX << "  y=" << userInput->mouseY << endl;

		if (camera && camera->GetCameraType() != DISABLED)
		{
			if (networkManager != NULL)
			{
				int robotId = networkManager->getCurrentRobotId();
				if (robotId != UNINITIALIZED)
				{
					int attackType = AOE_TYPE;
					FireEvent fireEvent(robotId, attackType);
					networkManager->sendEvent(&fireEvent);
				}
			}
		}
		rightElapsedTime = clock();
    }

    if(userInput->IsKeyPressed(VK_ESCAPE)) // check if shutdown, later implement a menu system here
    { 
        
        //Toggle between the menu
        if(hm->DisplayPopUpMenu())
        {
            camera->SetCameraType(DISABLED);
            userInput->SetCenterTheMouse(false);
            userInput->ShowMouseCursor();
			SystemManager::Instance()->GetHUDManager()->getWin()->setDisplay(false);
			SystemManager::Instance()->GetHUDManager()->getLose()->setDisplay(false);
			SystemManager::Instance()->GetHUDManager()->getCrosshairs()->setDisplay(false);
            //SystemManager::Instance()->GetHUDManager()->getCrosshairs2()->setDisplay(false);
        }
        else
        {
            camera->SetCameraType(FOLLOW);
            userInput->SetCenterTheMouse(true);
            userInput->HideMouseCursor(); 
			//SystemManager::Instance()->GetHUDManager()->getField()->setDisplay(false);
			hm->getCrosshairs()->setDisplay(true);
            //hm->getCrosshairs2()->setDisplay(true);
        }
		return;
    }

    // to
    if(userInput->IsKeyPressed(VK_SHIFT)) //SHIFT
    {
        if(camera)
        {
            if(camera->GetCameraType() != FOLLOW)
            {
                camera->SetCameraType(FOLLOW);
            } else {
                camera->SetCameraType(FIRST_PERSON);
            }
        }
    }
    
    ///////////////////////////////////////
    //      TESTING  SOUND AND CAMERA    //
    ///////////////////////////////////////
    //shaky camera, use for hits or contacts
    if(camera)
    {        
        if( userInput->IsKeyPressed(VK_P) && camera->GetCameraType() != DISABLED)
            camera->AddShakeFrames(100);
    }

    if(userInput->IsKeyPressed(VK_X) && camera && camera->GetCameraType() != DISABLED)
    {
        GraphicsManager* gManager = SystemManager::Instance()->GetGraphicsManager();
     // Test to create an explosion.
        //if (gManager) gManager->CreateExplosion(0.0f,0.0f,0.0f);

     // Test Swap UnitObj on ScenegraphNode.
        if (gManager) gManager->SwapUnitChain();
    }

    // EXIT FROM ROBOT
    if(userInput->IsKeyPressed(VK_SPACE) && camera && camera->GetCameraType() != DISABLED)
	{
		if ( networkManager == NULL );
		else
		{
			if (networkManager->getCurrentRobotId() == UNINITIALIZED) {
				int playerId = networkManager->getPlayerId();
				int sparkId = networkManager->getCurrentSparkId();

				EnterEvent enterEvent(playerId, sparkId);
				networkManager->sendEvent(&enterEvent);
			} else {
				int playerId = networkManager->getPlayerId();
				int sparkId = networkManager->getCurrentSparkId();
				int robotId = networkManager->getCurrentRobotId();

				ExitEvent exitEvent(playerId, sparkId, robotId);
				networkManager->sendEvent(&exitEvent);

			}
		}
    }

    

    /////////////////////////////////////////////////////////////////
    // Camera Setting Keys
    if(userInput->IsKeyPressed(VK_TAB))
    {
        if(camera)
        {
            if(camera->GetCameraType() != FOLLOW)
            {             
                // turn off the hud
                if(hm->HudIsOn()) 
				{
				    hm->DisplayPopUpMenu();
					hm->getField()->setDisplay(false);
				}
                hm->getCrosshairs()->setDisplay(true);
                //hm->getCrosshairs2()->setDisplay(true);
                // set the camera to Follow Mode
                camera->SetCameraType(FOLLOW);
                userInput->SetCenterTheMouse(true);
                userInput->HideMouseCursor();
            }
        }
    }

    if(userInput->IsKeyPressed(VK_F1))
    {
        if(camera)
        {
            if(camera->GetCameraType() != FREE)
            {
				hm->getCrosshairs()->setDisplay(false);
                //hm->getCrosshairs2()->setDisplay(false);
                // turn off the hud
                 if(hm->HudIsOn()) {
				    hm->DisplayPopUpMenu();
					hm->getField()->setDisplay(false);
					
				 }
                // set the camera to Follow Mode
                camera->SetCameraType(FREE);
                userInput->SetCenterTheMouse(true);
                userInput->HideMouseCursor();                
            }
        }
    }
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    ////     WASD + MOUSE MOVEMENT STUFF        
    
    float sensitivity = userInput->GetMouseSensitivity();
    int wheel = userInput->GetWheelState();

    if(camera)
    {              
        if(camera->GetCameraType() != DISABLED){
            delta_x = (float)userInput->deltaMouseX;
            delta_y = (float)userInput->deltaMouseY;
        }
        //need to figure out how to do this automatically whenever we get those values
        userInput->ResetMouse(); // sets the deltaMouseX and deltaMouseY to 0

        D3DXVECTOR3 cameraDir = camera->GetRotation();
        FLOAT a,b,c; //a,b,c used to store "wasdqz" values
        a = b = c = 0;

        delta_x *= -1/sensitivity;
        delta_y *= -1/sensitivity;

        //CameraDirection
        FLOAT pitch = cameraDir.x + delta_y;
        FLOAT yaw = cameraDir.y + delta_x;
        FLOAT roll = cameraDir.z;
        
        //wasd values
        if(userInput->IsKeyDown(VK_D)) a += 1; 
        if(userInput->IsKeyDown(VK_A)) a -= 1;
        if(userInput->IsKeyDown(VK_Q)) b += 1; // only for free cam
        if(userInput->IsKeyDown(VK_Z)) b -= 1; // only for free cam
        if(userInput->IsKeyDown(VK_W)) c += 1;
        if(userInput->IsKeyDown(VK_S)) c -= 1;

        camera->Update(D3DXVECTOR3(0,0,0), D3DXVECTOR3(pitch,yaw,roll));            
        ////////////////////
        //FREE CAMERA UPDATE
        if(camera->GetCameraType() == FREE){
            //MoveFast
            if(wheel > 0) c += 10;
            if(wheel < 0) c -= 10;
            
            camera->Update(D3DXVECTOR3(a,b,c), D3DXVECTOR3(pitch,yaw,roll));                        
        }

        //////////////////////////////////
        // //NORMAL PLAY SESSION HERE// //
        if(camera->GetCameraType() == FOLLOW || camera->GetCameraType() == FIRST_PERSON)
		{
            
            //PREPARE MESSAGE TO SERVER HERE//          

            UnitObject* playerUnit = SystemManager::Instance()->GetCurrentPlayerUnit();
            D3DXVECTOR3 pos = playerUnit->GetPosition();
            D3DXVECTOR3 dir = playerUnit->GetRotation();

            //CameraZoom
            if(wheel > 0) camera->ZoomIn();
            if(wheel < 0) camera->ZoomOut();
            pitch = dir.x + delta_y;
            yaw = dir.y + delta_x;
            roll = dir.z;        


            //////////////////
            D3DXMATRIX rotationMatrix;
            D3DXVECTOR3 rot = D3DXVECTOR3(pitch,yaw,roll);
            D3DXVECTOR3 move = D3DXVECTOR3(a,b,c);

            float yaw1, pitch1, roll1;    

            rot.x = (rot.x > 80.0f)? 80.0f:((rot.x < -80.0f)? -80.0f: rot.x);

            //basically converts to radians
            pitch1   = rot.x * 0.0174532925f; 
            yaw1 = rot.y * 0.0174532925f;
            roll1  = rot.z * 0.0174532925f;

            D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw1, pitch1, roll1);
            D3DXVec3TransformCoord(&move, &move, &rotationMatrix); // orient the movement to the direction 
            pos += move;

            playerUnit->SetPosition( pos );
            playerUnit->SetRotation( rot );

        ///////////////////////
        //SEND INFO TO NETWORK
        ///////////////////////
            // calculate change in movement
            float deltaX = move.x;
            float deltaY = move.y;
            float deltaZ = move.z;			
			if (deltaX != 0 || deltaY != 0 || deltaZ != 0 || delta_x != 0 || delta_y != 0)
			{
				if (networkManager != NULL)
				{
					int robotId = networkManager->getCurrentRobotId();
					int sparkId = networkManager->getCurrentSparkId();

					int unitIdToMove = (robotId == UNINITIALIZED) ? sparkId : robotId;

					MovementEvent moveSparkEvent(unitIdToMove, deltaX, deltaY, deltaZ, delta_x, delta_y);

					networkManager->sendEvent(&moveSparkEvent);
				}
			}
            ///////////////////////            
        }
    }
     if(userInput->IsKeyPressed(VK_BACK) && camera && camera->GetCameraType() != DISABLED)
    {
        LoadingManager::Initialize("resource/loadingConfig.txt");  // the loading manager 
    }
} 

bool isLetter(int value) 
{
	if (value >= 0x30 && value <= 0x5A) //numbers 39, uppercase
		return true;
	//if (value >= 0x41 && value <= 0x5A) //uppercase
	//	return true;
	if (value >= 0x61 && value <= 0x7A) //lowercase
		return true;
	if (value == 0x20) //space
		return true;

	return false;
}

bool isIP(int value) {
	if (value >= 0x30 && value <= 0x39) //numbers
		return true;
	//if (value >= 0x41 && value <= 0x5A) //uppercase
	//	return true;
	if (value >= 0x61 && value <= 0x7A) //lowercase
		return true;
	if (value == 0x2E) //period
		return true;

	return false;
}