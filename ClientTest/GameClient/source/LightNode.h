//LightNode.h
#ifndef _LIGHTNODE_H_
#define _LIGHTNODE_H_

#include "D3DManager.h"
#include "SceneNode.h"
#include "LightObject.h"

class LightNode : public SceneNode
{
protected:
    LightObject* light;
public:
    LightNode();
    LightNode(const LightNode &);
    ~LightNode();

    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);
};

#endif