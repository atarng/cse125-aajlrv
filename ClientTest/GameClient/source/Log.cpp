//Log.cpp
#include <stdio.h>

#include "Log.h"
#include <time.h>
#include <ctime>
#include <sstream>
#include <string>

//////////////////////////////////////////////
#define LOG_FOLDERPATH "ClientLog"
#define LOG_FILENAME "Client"

bool Log::useLog = false;
bool Log::useClockTime = false;

//////////////////////////////////////////////
Log* Log::m_Instance = NULL; 
//////////////////////////////////////////////

Log::Log()
{   
}

Log::~Log()
{
    file.close();
}

Log* Log::Instance()
{
    if (!m_Instance)
    {        
        m_Instance = new Log;
        if(Log::useLog) m_Instance->Initialize();
    }    
    return m_Instance;
}
void Log::Initialize()
{
    std::string path = LOG_FOLDERPATH;
    std::string filename = LOG_FILENAME;

    if(_mkdir(path.c_str())==0)
    {
        //folder was created
    }else
    {
        //folder already exists
    }

    time_t now;
    char the_date[24];

    the_date[0] = '\0';

    now = time(NULL);

    if (now != -1)
    {
        strftime(the_date, 24, "_%Y_%m_%d_%H%M%S.txt", gmtime(&now));
    }

    std::string derp = std::string(the_date);
    filename = path+'\\'+ filename + derp;       

    file.open(filename.c_str(), std::ofstream::trunc);
    if(file.is_open())
        printf("LogFile %s is open\n", filename.c_str());
}


void Log::Print(const char* format, ...)
{    
    if(Log::useLog){
        char dest[1024 * 16];
        va_list argptr;
        va_start(argptr, format);
        vsprintf(dest, format, argptr);
        va_end(argptr);

        if(file.is_open()){
            if(Log::useClockTime){
                file << clock() << ": ";
            }
            file << dest << std::endl; // the line
        }
        file.flush();
    }
}

void Log::PrintErr(const char* format, ...)
{
    if(Log::useLog){
        char dest[1024 * 16];
        va_list argptr;
        va_start(argptr, format);
        vsprintf(dest, format, argptr);
        va_end(argptr);

        if(file.is_open()){            
            file << clock() << ": ERROR\t " << dest << std::endl; // the line
        }
        file.flush();
    }
}
