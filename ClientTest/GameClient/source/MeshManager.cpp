/***
 * Filename: MeshManager.cpp
 * Author(s): Alfred Tarng
 * Purpose: The Mesh Manager loads up all of the model resources and stores
 *          their indices into a struct saving those indexed values for easy
 *          access.
 */
#include "MeshManager.h"
#include "SystemManager.h"
#include "GraphicsManager.h"
#include "TextureManager.h"
#include "D3DManager.h"

#include "ModelSkinned.h"
#include "UnitObject.h"
#include "Terrain.h"
#include "Log.h"

#include <iostream>

// FORWARD DECLARATIONS
class Model;

MeshManager::MeshManager()
{
}
MeshManager::~MeshManager()
{
    while( !meshObjects.empty() )
    {
        Mesh * me = meshObjects.back();
        if ( me != NULL )
        {
            if (me->isBonedModel()) delete (ModelSkinned*)me;
            else delete me;
            me = 0;
        }
        meshObjects.pop_back();
    }
}

bool MeshManager::Initialize()
{
    D3DManager* d3dman = SystemManager::Instance()->GetGraphicsManager()->GetD3DManager();
    bool     result = true;

    // Create the model object(s)
    Model* test_Model;
    test_Model = new Model;
    if(!test_Model)  return false;

    TextureManager * texMan = SystemManager::Instance()->GetGraphicsManager()->GetTextureManager();

/// 0: Initialize the model object(s)//////////////////////////////////////
    result = test_Model->Initialize( d3dman->GetDevice(), "resource/skyBox.dae", 1 );
    if(!result)
    {
        cout << "could not load skyBox model file" << endl;
        Log::Instance()->PrintErr("could not load skyBox model file");
        return false;
    } 
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().SKYBOX_TEXTURE));
    meshSelector.SKYBOX = AddMesh(*test_Model);

/// 1: Initialize the Probe Model //////////////////////////////////////
    test_Model = new Model;
    result = test_Model->Initialize( d3dman->GetDevice(), "resource/probe_Default.dae", 1 );
    //result = test_Model->Initialize( d3dman->GetDevice(), "resource/eThing_Centered.dae", 1 );
    if(!result)
    {
        cout << "could not load probe Model file" << endl;
        Log::Instance()->PrintErr("could not load probe Model file" );
        return false;
    }
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().PROBE_UV));
	meshSelector.PROBE = AddMesh(*test_Model);

/// 2: INITIALIZING DROID MESH /////////////////////////////////////////////////////////////
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/droid.dae", 1 );
    if(!result)
    {
        cout << "could not load testDroid model file" << endl;
        Log::Instance()->PrintErr("could not load testDroid model file");
        return false;
    }
    test_Model->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().DROID_UV ));
    meshSelector.TEST_DROID = AddMesh( *test_Model );

/// 3: INITIALIZING TESTCUBE MESH /////////////////////////////////////////////////////////////
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/cubetmp.txt", 0 );
    if(!result)
    {
        cout << "could not load testBox model file" << endl;
        Log::Instance()->PrintErr("could not load testBox model file");
        return false;
    }
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_STONE_TEXTURE));
    test_Model->setDrawAsWireFrame(false);
    meshSelector.TEST_CUBE = AddMesh( *test_Model );


/// 4: INITIALIZING ELECTRICROBOT /////////////////////////////////////////////////////////////
    test_Model = new Model;
	result = test_Model -> Initialize( d3dman->GetDevice(), "resource/FanThing_Default.dae", 1 );
    if(!result)
    {
        cout << "could not load test hover electric dae model file" << endl;
        Log::Instance()->PrintErr("could not load test hover electric dae model file");
        return false;
    }
	test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().ETHING_UV ));
    meshSelector.TEST_ELECTRICTHING = AddMesh( *test_Model );

/// 5: INITIALIZING TEST ANIMATION OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing testAnimation Object...: " << endl;

    test_Model = new Model;

    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/testAnimation.dae", 1 );
    if(!result)
    {
        cout << "could not load test animation dae model file" << endl;
        Log::Instance()->PrintErr("could not load test animation dae model file");
        return false;
    }

    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_STONE_TEXTURE_BUMPED));
    meshSelector.TEST_ANIM_DAE = AddMesh( *test_Model );

/// 6: INITIALIZING TEST SKELETAL_ANIMATION OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing testSkeletalAnimation Object...: " << endl;
    test_Model = new ModelSkinned;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/cat.dae", 1 );
    if(!result)
    {
        cout << "could not load test cat animation dae model file" << endl;
        Log::Instance()->PrintErr("could not load test cat animation dae model file");
        return false;
    } meshSelector.TEST_SKELETAL_ANIM_DAE = AddMesh( *test_Model );
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_CAT_TEXTURE));

/// 7: INITIALIZING TEST SPHERE COLLISION OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing testSphereCollisionObject Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/sphere.dae", 1 );
    if(!result)
    {
        cout << "could not load test sphere dae model file" << endl;
        Log::Instance()->PrintErr("could not load test sphere animation dae model file");
        return false;
    } meshSelector.TEST_COLLISION_SPHERE = AddMesh( *test_Model );
    test_Model->setDrawAsWireFrame(true);

    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_CAT_TEXTURE));

/// 8: INITIALIZING ELECTRIC SPHERE COLLISION OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing Electric Ball Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/sphere.dae", 1 );
    if(!result)
    {
        cout << "could not load test sphere dae model file" << endl;
        Log::Instance()->PrintErr("could not load test sphere animation dae model file");
        return false;
	} meshSelector.ELECTRICSPARK = AddMesh( *test_Model );
    test_Model->setDrawAsWireFrame(false);
    test_Model->setAlphaBlend(true);
    test_Model->SetNoShadow();
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().LIGHTNING_TEXTURE_SPHERE));

/// 9: INITIALIZING LASER OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing LASER Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/laser.dae", 1 );
    if(!result)
    {
        cout << "could not load test laser dae model file" << endl;
        Log::Instance()->PrintErr("could not load test laser animation dae model file");
        return false;
    } meshSelector.LASER = AddMesh( *test_Model );
    test_Model->setAlphaBlend(false);
    test_Model->SetNoShadow();
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_LASER_TEXTURE));

/// 10: INITIALIZING TABLET OBJECT  /////////////////////////////////////////////////////
    cout << "[MeshManager] Initializing Tablet Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/tablet.dae", 1 );
    if(!result)
    {
        cerr << "could not load test tablet dae model file" << endl;
        Log::Instance()->PrintErr("could not load test tablet animation dae model file");
        return false;
    } meshSelector.TABLET = AddMesh( *test_Model );
    test_Model->setAlphaBlend(false);
	test_Model->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().TABLET_UV_0 ));
/// 11: INITIALIZING TABLET (2) OBJECT 
	cout << "[MeshManager] Initializing Tablet Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/lightningBolt.dae", 1 );
    if(!result)
    {
        cerr << "could not load test tablet dae model file" << endl;
        Log::Instance()->PrintErr("could not load test tablet animation dae model file");
        return false;
    } meshSelector.TABLET2 = AddMesh( *test_Model );
    test_Model->setAlphaBlend(false);
	test_Model->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().TABLET_UV_1 ));

/// 12: INITIALIZING ELECTRIC SPHERE COLLISION OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing Electric Ball Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/sphere.dae", 1 );
    if(!result)
    {
        cout << "could not load test sphere dae model file" << endl;
        Log::Instance()->PrintErr("could not load test sphere animation dae model file");
        return false;
	} meshSelector.AOE = AddMesh( *test_Model );
    meshSelector.METEOR = AddMesh( *test_Model );
    meshSelector.BALLS = AddMesh( *test_Model );
    test_Model->setDrawAsWireFrame(false);
    test_Model->setAlphaBlend(true);
    test_Model->SetNoShadow();
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().LIGHTNING_TEXTURE_SPHERE));

/// (11) 2b: DroidWalk //////////
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/droid_Walk.dae", 1 );
    if(!result)
    {
        cout << "[MeshManager] Could not load DroidWalking model file" << endl;
        Log::Instance()->PrintErr("[MeshManager] could not load DroidWalking model file");
        return false;
    }
    test_Model->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().DROID_UV ));
    meshSelector.DROID_WALK = AddMesh( *test_Model );

/// (12) 2c: DroidATTACK //////////
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/Droid_Attack.dae", 1 );
    if(!result)
    {
        cout << "[MeshManager] Could not load DroidAttack model file" << endl;
        Log::Instance()->PrintErr("[MeshManager] could not load DroidAttack model file");
        return false;
    }
    test_Model->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().DROID_UV ));
    meshSelector.DROID_ATTACK = AddMesh( *test_Model );

/// (13) 1b: Initialize the Probe Walk object //////////////////////////////////////
    test_Model = new Model;
    result = test_Model->Initialize( d3dman->GetDevice(), "resource/probe_Walk.dae", 1 );
    if(!result)
    {
        cout << "could not load probe-Walk Model file" << endl;
        Log::Instance()->PrintErr("could not load probe-Walk Model file" );
        return false;
    }
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().PROBE_UV));
	meshSelector.PROBE_WALK = AddMesh(*test_Model);

/// (14) 4b: Initialize the FANBOTATTACK object //////////////////////////////////////
	test_Model = new Model;
	result = test_Model -> Initialize( d3dman->GetDevice(), "resource/FanThing_Attack.dae", 1 );
	if(!result)
    {
        cout << "could not load FanBot-Attack Model file" << endl;
        Log::Instance()->PrintErr("could not load FanBot-Attack Model file" );
        return false;
    }
	test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().ETHING_UV ));
	meshSelector.FAN_BOT_ATTACK = AddMesh(*test_Model);

/// (15) 4c: Initialize the FANBOTWALK object //////////////////////////////////////
	test_Model = new Model;
	result = test_Model -> Initialize( d3dman->GetDevice(), "resource/FanThing_Walk.dae", 1 );
	if(!result)
    {
        cout << "could not load FanBot-Walk Model file" << endl;
        Log::Instance()->PrintErr("could not load FanBot-Walk Model file" );
        return false;
    }
	test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().ETHING_UV ));
	meshSelector.FAN_BOT_WALK = AddMesh(*test_Model);

/// (16) 11: INITIALIZING TERRAIN MESHES /////////////////////////////////////////////////////
    Terrain* test_Terrain = new Terrain;
    result = test_Terrain->Initialize(d3dman->GetDevice(), "resource/heightmap01.bmp");
    if(!result)
    {
        cout << "could not load terrainMap" << endl;
        Log::Instance()->PrintErr("could not load terrainMap");
        return false;
    }
    meshSelector.TERRAIN_OBJECT = AddMesh( *test_Terrain );
    test_Terrain->SetTextureObject( texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_SAND_TEXTURE) );

/// (17) 9b: INITIALIZING LASER OBJECT //////////////////////////////////////////////
    cout << "[MeshManager] Initializing LASER2 Object...: " << endl;
    test_Model = new Model;
    result = test_Model -> Initialize( d3dman->GetDevice(), "resource/laser.dae", 1 );
    if(!result)
    {
        cout << "could not load test laser dae model file" << endl;
        Log::Instance()->PrintErr("could not load test laser animation dae model file");
        return false;
    } meshSelector.LASER2 = AddMesh( *test_Model );
    test_Model->setAlphaBlend(true);
    test_Model->SetNoShadow();
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().LIGHTNING_TEXTURE_SPHERE ));

/// (18) 1b: Initialize the Probe Walk object //////////////////////////////////////
    test_Model = new Model;
    result = test_Model->Initialize( d3dman->GetDevice(), "resource/probe_Attack.dae", 1 );
    if(!result)
    {
        cout << "could not load probe-Walk Model file" << endl;
        Log::Instance()->PrintErr("could not load probe-Walk Model file" );
        return false;
    }
    test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().PROBE_UV));
	meshSelector.PROBE_ATTACK = AddMesh(*test_Model);

/// (19) Initialize the SATELLITE SHIP //////////////////////////////////////
    test_Model = new Model;
    result = test_Model->Initialize( d3dman->GetDevice(), "resource/satellite_shipDAE.dae", 1 );
    if(!result)
    {
        cout << "could not load SatelliteShip Model file" << endl;
        Log::Instance()->PrintErr("could not load SatelliteShip Model file" );
        return false;
    }
	test_Model->SetTextureObject(texMan->GetTextureObject(texMan->GetTextureSelecter().SATELLITESHIP_UV));
	meshSelector.SATELLITE_SHIP = AddMesh(*test_Model);

    return result;
}
int MeshManager::AddMesh(Mesh & addMesh)
{
    meshObjects.push_back( &addMesh );
    return (meshObjects.size() - 1);
}
Mesh& MeshManager::GetMesh(int index)
{
    return *(meshObjects.at(index));
}

void MeshManager::resetAnims()
{
    std::vector<Mesh*>::iterator it;
    for(it = meshObjects.begin(); it != meshObjects.end(); ++it )
    {
        if ((*it)->isModel())
        {
            ((Model*)(*it))->setEval();
        }
    }
}