#ifndef _HW_GEOMETRY_
#define _HW_GEOMETRY_

#include "D3DManager.h"

// a struct to define a single vertex
struct VERTEX{FLOAT X, Y, Z; D3DXCOLOR Color;};

class HelloWorldGeometry
{
public:
    HelloWorldGeometry();
    ~HelloWorldGeometry();
    void Triangle(D3DManager*);
    void Shaders(D3DManager*);

    void ParticleSystem(D3DManager*);

    void Render(D3DManager*);
    void setShaders_TEMPORARY(D3DManager*);
private:
    ID3D11Buffer* VertexBufferPtr;                // the pointer to the vertex buffer
    ID3D11VertexShader* VertexShaderPtr;               // the pointer to the vertex shader
    ID3D11PixelShader*  PixelShaderPtr;                // the pointer to the pixel shader

    ID3D11InputLayout* InputLayoutPtr;            // the pointer to the input layout

};

#endif