////////////////////////////////////////////////////////////////////////////////
// Filename: TextureManager.cpp
// Contributor(s): Alfred Tarng
// Purpose: Loads all the texture resources into packaged TextureObjects.
////////////////////////////////////////////////////////////////////////////////

#include "TextureManager.h"
#include <iostream>

using namespace std;

TextureManager::TextureManager()
{
}
TextureManager::~TextureManager()
{
    while( !textureObjects.empty() )
    {
        TextureObject * te = textureObjects.back();
        if ( te != NULL )
        {
            te ->Shutdown();
            delete te;
            te = 0;
        }
        textureObjects.pop_back();
    }
}

bool TextureManager::Initialize(ID3D11Device* device)
{
    bool result;
    vector<char*> textureNameVector;
    TextureObject * insertTexture;
	char ** textureNames;

/// Initialize SkyBox Texture
    //textureNameVector.push_back("resource/skyBox.dds");
	textureNameVector.push_back("resource/skyBoxUV.png");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector);
	if(!result)
	{
        cout << "[TextureManager] Skybox Texture Initialization failed..." << endl;
		return false;
	} textureSelecter.SKYBOX_TEXTURE = AddTexture(*insertTexture);

    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// Initialize BumpedStone Texture
    textureNameVector.push_back("resource/stone01.dds");
    textureNameVector.push_back("resource/bump01.dds");
    insertTexture = new TextureObject;
    textureNames = ((char **)( malloc( sizeof(char *) * textureNameVector.size() ) ));
    for (unsigned int i = 0; i < textureNameVector.size(); ++i)
    {
        textureNames[i] = textureNameVector.at(i);
    }
    // Initialize the texture objects
	result = insertTexture->Initialize(device, textureNames, textureNameVector.size() );
	if(!result)
	{
        cout << "[TextureManager] Bumped Stone Texture Initialization failed..." << endl;
		return false;
	} textureSelecter.TEST_STONE_TEXTURE_BUMPED= AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();
    delete [] textureNames;
    textureNames = 0;

/// Initialize Electric Thing UV Texture
    textureNameVector.push_back("resource/FanBot_UV_mapped.png");
    textureNameVector.push_back("resource/bump01.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector);
	if(!result)
	{
        cout << "[TextureManager] EThing_Bumped Texture Initialization failed..." << endl;
		return false;
	} textureSelecter.ETHING_UV = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();


/// Initialize Probe Thing UV Texture
    textureNameVector.push_back("resource/testProbe_UV.png");
    textureNameVector.push_back("resource/bump01.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector);
	if(!result)
	{
        cout << "[TextureManager] Probe Texture Initialization failed..." << endl;
		return false;
	} textureSelecter.PROBE_UV = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// Initialize Droid Thing UV Texture
    textureNameVector.push_back("resource/droid_UVMapped.png");
    textureNameVector.push_back("resource/bump01.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector);
	if(!result)
	{
        cout << "[TextureManager] Droid Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.DROID_UV = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// Initialize TEST_CAT Texture
    textureNameVector.push_back("resource/20_cat_fix.jpg");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Cat Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_CAT_TEXTURE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// Initialize Lightning Texture
    textureNameVector.push_back("resource/Lightning.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Lightning Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.LIGHTNING_TEXTURE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// Initialize TEST_PARICLE_GLITTER
    textureNameVector.push_back("resource/star.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Glitter Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_GLITTER_TEXTURE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE TEST HUD BUTTON ALPHA
    textureNameVector.push_back("resource/button.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TestAlpha2 Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_HUD_BUTTON = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE TEST HUD BAR
    textureNameVector.push_back("resource/greybar.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TestAlpha2 Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.GRAY_BAR = AddTexture(*insertTexture);
	 while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE TEST HUD BAR
    textureNameVector.push_back("resource/bluebar.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] bluebar Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.BLUE_BAR = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE TEST TERRAIN TEXTURE
    textureNameVector.push_back("resource/seafloor.dds");
    textureNameVector.push_back("resource/bump01.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TestAlpha2 Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_SAND_TEXTURE = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE FONT TEXTURE
    textureNameVector.push_back("resource/testfontdata.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] FONT Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.FONT = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE STONE NO BUMP TEXTURE
    textureNameVector.push_back("resource/stone01.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] FONT Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_STONE_TEXTURE = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

/// INITIALIZE Electric Sphere Texture 
    textureNameVector.push_back("resource/electricTexture.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TestAlpha2 Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.LIGHTNING_TEXTURE_SPHERE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();


/// INITIALIZE TEST Laser Texture
    textureNameVector.push_back("resource/test_laser.dds");
    insertTexture = new TextureObject;
	// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TestAlpha2 Texture Initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_LASER_TEXTURE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

// INITIALIZE TEST Minimap Icon Texture
    textureNameVector.push_back("resource/test_mmico.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Test minimap icon initialization failed..." << endl;
		return false;
    } textureSelecter.MINIMAP_ICON = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

    // INITIALIZE TEST Minimap Icon Texture
    textureNameVector.push_back("resource/smallerHeightMap.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] MiniMap initialization failed..." << endl;
		return false;
    } textureSelecter.MINIMAP = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

 // INITIALIZE TEST Minimap Icon Texture
    textureNameVector.push_back("resource/playerCentricMapTest.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] TEST_MiniMap initialization failed..." << endl;
		return false;
    } textureSelecter.TEST_MAP = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

    // INITIALIZE Explosion Particle Texture Texture
    textureNameVector.push_back("resource/explosion.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Explosion initialization failed..." << endl;
		return false;
    } textureSelecter.EXPLOSION = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE Field texture
    textureNameVector.push_back("resource/field.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Field initialization failed..." << endl;
		return false;
    } textureSelecter.FIELD = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE crosshairs texture
    textureNameVector.push_back("resource/crosshairs.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Field initialization failed..." << endl;
		return false;
    } textureSelecter.CROSSHAIRS = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE splash texture
    textureNameVector.push_back("resource/splash.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Splash initialization failed..." << endl;
		return false;
    } textureSelecter.SPLASH = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE Focus texture
    textureNameVector.push_back("resource/focus.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Focus initialization failed..." << endl;
		return false;
    } textureSelecter.FOCUS = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE game over
    textureNameVector.push_back("resource/lose.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Game over initialization failed..." << endl;
		return false;
    } textureSelecter.LOSE = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE win
    textureNameVector.push_back("resource/win.dds");
    insertTexture = new TextureObject;
	//// Initialize the texture objects
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result)
	{
        cout << "[TextureManager] Game over initialization failed..." << endl;
		return false;
    } textureSelecter.WIN = AddTexture(*insertTexture);
    // Empty Out Vector
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE TABLET
    textureNameVector.push_back("resource/tablet_HealthUV.png");
	textureNameVector.push_back("resource/tablet_HEALTHNormal.png");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: TabletTextureInitFailure..." ); return false; }
	textureSelecter.TABLET_UV_0 = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE TABLET
    textureNameVector.push_back("resource/tablet_POWERUV.png");
	textureNameVector.push_back("resource/tablet_POWERNormal.png");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: TabletTextureInitFailure..." ); return false; }
	textureSelecter.TABLET_UV_1 = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE icon
    textureNameVector.push_back("resource/spark_Bar.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: bar icons..." ); return false; }
	textureSelecter.SPARK_BAR = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE icon
    textureNameVector.push_back("resource/robot_Bar.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: bar icons..\n." ); return false; }
	textureSelecter.ROBOT_BAR = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

	// INITIALIZE lightnighgbolt icon
    textureNameVector.push_back("resource/lightning_Bar.dds");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: bar icons...\n" ); return false; }
	textureSelecter.LIGHTNING_BAR = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();


	// INITIALIZE SATELLITESHIPUV
    textureNameVector.push_back("resource/satellite_shipUV.png");
    insertTexture = new TextureObject;
	result = insertTexture->Initialize(device, textureNameVector );
	if(!result){ printf( "[TextureManager] FAILURE: SatelliteShipUV\n" ); return false; }
	textureSelecter.SATELLITESHIP_UV = AddTexture(*insertTexture);
    while(!textureNameVector.empty()) textureNameVector.pop_back();

    return result;
}

int TextureManager::AddTexture(TextureObject& newTextureObject)
{
    textureObjects.push_back( &newTextureObject );
    return (textureObjects.size() - 1);
}

TextureObject& TextureManager::GetTextureObject (int index)
{
    return *(textureObjects.at(index));
}