/***
 * ParticleSystem.cpp
 * Author: Alfred Tarng
 * Purpose:
 */

#include "ParticleSystem.h"
#include "TextureObject.h"
#include <math.h>
#include "Log.h"

ParticleSystem::ParticleSystem()
{
	m_Texture = 0;
	m_particleList = 0;
	m_vertices = 0;
	m_vertexBuffer = 0;
	m_indexBuffer = 0;

    timeSinceLastFrame = 0;

	p_expires = p_expired = false;

    memset( &particleBehavior, 0, sizeof(particleBehavior));
    particleBehavior.SPEED = 1.0f;
    particleBehavior.P_LIFESPAN = 250.f;
}
ParticleSystem::ParticleSystem(const ParticleSystem& other)
{
}
ParticleSystem::~ParticleSystem()
{
    Shutdown();
}

bool ParticleSystem::Initialize(ID3D11Device* device)
{
	bool result;

	// Initialize the particle system.
	result = InitializeParticleSystem();
	if(!result)	{ return false; }

	// Create the buffers that will be used to render the particles with.
	result = InitializeBuffers(device);
	if(!result) { return false; }

	return true;
}
bool ParticleSystem::InitializeParticleSystem()
{
	// Set the random deviation of where the particles can be located when emitted.
	m_particleDeviationX = 0.5f;
	m_particleDeviationY = 0.1f;
	m_particleDeviationZ = 2.0f;

	// Set the speed and speed variation of particles.
	m_particleVelocity = this->particleBehavior.SPEED; //1.0f;
	m_particleVelocityVariation = 0.2f;

	// Set the physical size of the particles.
	m_particleSize = 1.0f;

	// Set the number of particles to emit per second.
	m_particlesPerSecond = 10.0f;

	// Set the maximum number of particles allowed in the particle system.
	m_maxParticles = 100;//particleBehavior.PARTICLE_CAP;

	// Create the particle list.
	m_particleList = new ParticleType[m_maxParticles];
	if(!m_particleList)
	{
		return false;
	} memset(m_particleList, 0, sizeof(ParticleType) * m_maxParticles);

	// Initialize the current particle count to zero since none are emitted yet.
	m_currentParticleCount = 0;

	// Clear the initial accumulated time for the particle per second emission rate.
	m_accumulatedTime = 0.0f;

	return true;
}
bool ParticleSystem::InitializeBuffers(ID3D11Device* device)
{
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the maximum number of vertices in the vertex array.
	m_vertexCount = m_maxParticles * 6;

	// Set the maximum number of indices in the index array.
	m_indexCount = m_vertexCount;

	// Create the vertex array for the particles that will be rendered.
	m_vertices = new VertexType[m_vertexCount];
	if(!m_vertices) return false; 

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices) return false;

	// Initialize vertex array to zeros at first.
	memset(m_vertices, 0, (sizeof(VertexType) * m_vertexCount));

	// Initialize the index array.
	for(int i=0; i<m_indexCount; i++) indices[i] = i;

	// Set up the description of the dynamic vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = m_vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result)) return false;

	// Release the index array since it is no longer needed.
	delete [] indices;
	indices = 0;

	return true;
}
void ParticleSystem::SetTextureObject(TextureObject & textureObj)
{
    m_Texture = &textureObj;
}

/***
 * Calls Kill (old Particles)
 *       Emit
 *       Update
 */
bool ParticleSystem::Frame(float frameTime, ID3D11DeviceContext* deviceContext)
{
	bool result = true;

    timeSinceLastFrame = frameTime;
    
	// Release old particles.
	KillParticles();

	// Emit new particles.
    EmitParticles(frameTime);
	
	// Update the position of the particles.
	UpdateParticles(frameTime);

	// Update the dynamic vertex buffer with the new position of each particle.
	result = UpdateBuffers(deviceContext);
	if(!result)
	{
		return false;
	}

	return result;
}
/***
 * 
 */
void ParticleSystem::EmitParticles(float frameTime)
{
	bool emitParticle, found;
	float positionX, positionY, positionZ, velocity, red, green, blue, dirX, dirY, dirZ;
	int index, i, j;
    float currentLifeSpan, maxLifeSpan;

	// Increment the frame time.
	m_accumulatedTime += frameTime;

	// Set emit particle to false for now.
	emitParticle = false;
	
	// Check if it is time to emit a new particle or not.
	if( m_accumulatedTime > (1000.0f / m_particlesPerSecond) )
	{
		m_accumulatedTime = 0.0f;
		emitParticle = true;
	}

	// If there are particles to emit then emit one per frame.
    int renderParticles = 1;
    if(emitParticle == true)
    {
        if (particleBehavior.WAVE)
        {
            renderParticles = -1;
        }
	    while( renderParticles-- != 0 && (m_currentParticleCount < (m_maxParticles - 1)) )
	    {
		    m_currentParticleCount++;

            int randomNumber1 = rand();
            int randomNumber2 = rand();
            int randomNumber3 = rand();

		    // Now generate the randomized particle properties.
            positionX = (((float)randomNumber1-(float)randomNumber2)/RAND_MAX) * m_particleDeviationX;
		    positionY = (((float)randomNumber2-(float)randomNumber3)/RAND_MAX) * m_particleDeviationY;
		    positionZ = (((float)randomNumber3-(float)randomNumber1)/RAND_MAX) * m_particleDeviationZ;

            if(particleBehavior.TRAILING) positionZ -= 1.f;

            randomNumber1 = rand();
            randomNumber2 = rand();
            randomNumber3 = rand();

            if (particleBehavior.WAVE)
            {
                dirX = cos( (m_currentParticleCount * 10 ) * 3.14f / 180 );
                dirY = 0;
                dirZ = sin( (m_currentParticleCount * 10 ) * 3.14f / 180 );

                velocity = m_particleVelocity;
            }else
            {
		        dirX = ((float)randomNumber1-(float)randomNumber2)/RAND_MAX ;
		        dirY = ((float)randomNumber2-(float)randomNumber3)/RAND_MAX ;
		        dirZ = ((float)randomNumber3-(float)randomNumber1)/RAND_MAX ;

                randomNumber1 = rand();
                randomNumber2 = rand();

		        velocity = m_particleVelocity + (((float)randomNumber1-(float)randomNumber2)/RAND_MAX) * m_particleVelocityVariation;
            }
            randomNumber1 = rand() - rand();
            randomNumber2 = rand() - rand();
            randomNumber3 = rand() - rand();

            switch(particleBehavior.COLORTYPE)
            {
                case CT_FIRE:
                    red   = ((((float)randomNumber1)/RAND_MAX) * 0.25f) + 0.75f;
                    green = (((float)randomNumber2)/RAND_MAX);
                    blue  = 0.0f;
                    break;
                case CT_BANDW:
                    red   = (((float)randomNumber3)/RAND_MAX);//0.0f;
		            green = (((float)randomNumber3)/RAND_MAX);//0.0f;
		            blue  = (((float)randomNumber3)/RAND_MAX);//0.0f;
                    break;
                case CT_RAINBOW:
                default: 
                    red   = (((float)randomNumber1)/RAND_MAX) + 0.5f;
		            green = (((float)randomNumber2)/RAND_MAX) + 0.5f;
		            blue  = (((float)randomNumber3)/RAND_MAX) + 0.5f;
                    break;
            }

            currentLifeSpan = 0.0f; // *hardcoded*
            maxLifeSpan     = particleBehavior.P_LIFESPAN; // *hardcoded*

		    // Now since the particles need to be rendered from back to front for blending we have to sort the particle array.
		    // We will sort using Z depth so we need to find where in the list the particle should be inserted.
		    index = 0;
		    found = false;
		    while(!found)
		    {
			    if((m_particleList[index].active == false) || (m_particleList[index].positionZ < positionZ))
			    {
                    found = true;
			    } else { index++; }
		    }

		    // Now that we know the location to insert into we need to copy the array over by one position from the index to make room for the new particle.
		    i = m_currentParticleCount;
		    j = i - 1;

		    while(i != index)
		    {// Does a memcpy so should be fine.
                m_particleList[i] = m_particleList[j];
			    i--; j--;
		    }

		    // Now insert it into the particle array in the correct depth order.
		    m_particleList[index].positionX       = positionX;
		    m_particleList[index].positionY       = positionY;
		    m_particleList[index].positionZ       = positionZ;

            m_particleList[index].dirX            = dirX;
            m_particleList[index].dirY            = dirY;
		    m_particleList[index].dirZ            = dirZ;

		    m_particleList[index].red             = red;
		    m_particleList[index].green           = green;
		    m_particleList[index].blue            = blue;
		    m_particleList[index].velocity        = velocity;
		    m_particleList[index].active          = true;
            m_particleList[index].currentLifeSpan = currentLifeSpan;
            m_particleList[index].maxLifeSpan     = maxLifeSpan;

            m_particleList[index].roll            = atan(dirX/dirY);//3.14/4;// * (dirX/abs(dirX)); //* (3.14 / 180);
	    }
    }
	return;
}
/***
 * Called during each frame to update Particle based on Behavior.
 */
void ParticleSystem::UpdateParticles(float frameTime)
{
	// Each frame we update all the particles by making them move downwards using their position, velocity, and the frame time.
	for(int i=0; i<m_currentParticleCount; i++)
	{// TODO(?): Insert a switch to determine what kind of behavior the particle should exhibit
        // IF FREE FALLING SPECIFIED
        if (particleBehavior.FREEFALLING)
		    m_particleList[i].positionY = m_particleList[i].positionY - (m_particleList[i].velocity * frameTime * 0.001f);
        if (particleBehavior.RADIAL)
        {
            m_particleList[i].positionX = m_particleList[i].positionX + (m_particleList[i].dirX * m_particleList[i].velocity * frameTime * 0.001f);
            m_particleList[i].positionY = m_particleList[i].positionY + (m_particleList[i].dirY * m_particleList[i].velocity * frameTime * 0.001f);
            m_particleList[i].positionZ = m_particleList[i].positionZ + (m_particleList[i].dirZ * m_particleList[i].velocity * frameTime * 0.001f);
        }
        if (particleBehavior.TRAILING)
            m_particleList[i].positionZ = m_particleList[i].positionZ - (m_particleList[i].velocity * frameTime * 0.001f);

        m_particleList[i].currentLifeSpan += 0.1f * frameTime;
	}
    
    return;
}
/***
 * Eliminates expired particles..
 */
void ParticleSystem::KillParticles()
{
	for(int i=0; i < m_maxParticles; i++)
	{// Kill all the particles based on conditional
		if( CheckConditionalOnParticle(i) )
		{
			m_particleList[i].active = false;
			m_currentParticleCount--;

			// Now shift all the live particles back up the array to erase the destroyed particle and keep the array sorted correctly.
			for(int j=i; j<m_maxParticles-1; j++)
			{
                m_particleList[j] = m_particleList[j+1];
			}
		}
	}

	return;
}
/***
 * Check to see if this particular particle should expire.
 */
bool ParticleSystem::CheckConditionalOnParticle(int p)
{
    return (m_particleList[p].active == true) && (m_particleList[p].currentLifeSpan > m_particleList[p].maxLifeSpan) && ((m_particleList[p].currentLifeSpan = 0) == 0);
}

bool ParticleSystem::UpdateBuffers(ID3D11DeviceContext* deviceContext)
{
	int index;
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	// Initialize vertex array to zeros at first.
	memset(m_vertices, 0, (sizeof(VertexType) * m_vertexCount));

	// Now build the vertex array from the particle list array.  Each particle is a quad made out of two triangles.
	index = 0;

	for(int i=0; i < m_currentParticleCount; i++)
	{ // updates each square

        // Indices of the square the particle belongs to.
        for (int ic = 0; ic < 6; ic++ )
        { // --, -+, +-, +-, -+, ++
          // 01, 00, 11, 11, 00, 10
            int xmult = ((ic <= 1) || (ic == 4)) ? -1 : 1;
            int ymult = ((ic == 1) || (ic >= 4)) ? 1 : -1;

            float uVal = ((ic <= 1) || (ic == 4)) ? 0.0f : 1.0f;
            float vVal = ((ic == 0) || (ic == 2) || (ic == 3)) ? 1.0f : 0.0f;

            m_vertices[index].position = D3DXVECTOR3( (m_particleSize * xmult), (m_particleSize * ymult), 0 );
            m_vertices[index].offset   = D3DXVECTOR3( m_particleList[i].positionX,m_particleList[i].positionY, m_particleList[i].positionZ ); 
		    m_vertices[index].texture  = D3DXVECTOR2( uVal , vVal );
		    m_vertices[index].color    = D3DXVECTOR4(m_particleList[i].red, m_particleList[i].green, m_particleList[i].blue, 1.0f);
            D3DXMatrixIdentity(&m_vertices[index].rotation);
            D3DXMatrixRotationYawPitchRoll(&m_vertices[index].rotation, 0,0, m_particleList[i].roll);
		    index++;
        }
	}
	
	// Lock the vertex buffer.
	result = deviceContext->Map(m_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result)) { return false; }

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)m_vertices, (sizeof(VertexType) * m_vertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(m_vertexBuffer, 0);

	return true;
}

void ParticleSystem::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);
	return;
}
void ParticleSystem::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
    stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);
    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
    // Set the type of primitive that should be rendered from this vertex buffer.
    deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

ParticleSystem::ParticleBehaviorConfig & ParticleSystem::GetParticleBehavior(){ return particleBehavior; }

void ParticleSystem::Shutdown()
{
    // Release the buffers.
	ShutdownBuffers();

	// Release the particle system.
	ShutdownParticleSystem();

	// Release the texture used for the particles.
	//ReleaseTexture();

	return;
}
void ParticleSystem::ShutdownParticleSystem()
{
	// Release the particle list.
	if(m_particleList)
	{
		delete [] m_particleList;
		m_particleList = 0;
	}

	return;
}
void ParticleSystem::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}

void ParticleSystem::SetExpirable()
{
	p_expires = true;
}
void ParticleSystem::SetExpiredSystem(bool expire)
{
    p_expired = p_expires && expire;
}