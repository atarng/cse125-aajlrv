/***
 * Filename: SoundManager.cpp
 * Author: Roy Chen
 * Purpose:
 *
 *
 */

#include "SoundManager.h"
#include "LoadingManager.h"
#include "D3DManager.h"
#include <stdio.h>
#include <iostream>
using namespace std;



SoundManager::SoundManager() {
	m_DirectSound = 0;
	m_primaryBuffer = 0;
	m_secondaryBufferIntro = 0;
	m_secondaryBufferShot = 0;
	m_secondaryBufferSpaceShip = 0;
	m_secondaryBufferBuzzer = 0;
	m_secondaryBufferLose = 0;
	m_secondaryBufferStart = 0;
	m_secondaryBufferExplode = 0;
	m_secondaryBufferEndgame = 0;
	m_secondaryBufferMusic = 0;
	m_secondaryBufferDing = 0;
	m_secondaryBufferPowerup = 0;
	m_secondaryBufferAOE = 0;
	m_secondaryBufferBullet2 = 0;
	m_secondaryBufferMeteorDetonate = 0;
	m_secondaryBufferMeteorWhistle = 0;
	m_secondaryBufferNuclear = 0;
	looping = 0;
}

SoundManager::SoundManager(const SoundManager& other) {
}

SoundManager::~SoundManager() {
}

bool SoundManager::Initialize(HWND hwnd) {

	//Initialize DirectSound, primary buffer
	if (!InitializeDirectSound(hwnd))
		return 0;
	//Load all 3 files
	if (!LoadWaveFile("resource/intro.wav",&m_secondaryBufferIntro) 
		|| !LoadWaveFile("resource/shot.wav",&m_secondaryBufferShot)
		|| !LoadWaveFile("resource/spaceship.wav",&m_secondaryBufferSpaceShip)
		|| !LoadWaveFile("resource/buzzer.wav",&m_secondaryBufferBuzzer)
		|| !LoadWaveFile("resource/lose.wav",&m_secondaryBufferLose)
		|| !LoadWaveFile("resource/start.wav",&m_secondaryBufferStart)
		|| !LoadWaveFile("resource/explode.wav",&m_secondaryBufferExplode)
		|| !LoadWaveFile("resource/endgame.wav",&m_secondaryBufferEndgame)
		|| !LoadWaveFile("resource/music.wav",&m_secondaryBufferMusic)
		|| !LoadWaveFile("resource/ding.wav",&m_secondaryBufferDing)
		|| !LoadWaveFile("resource/powerup.wav",&m_secondaryBufferPowerup)
		|| !LoadWaveFile("resource/weapon/aoe.wav",&m_secondaryBufferAOE)
		|| !LoadWaveFile("resource/weapon/bullet2.wav",&m_secondaryBufferBullet2)
		|| !LoadWaveFile("resource/weapon/meteor_detonate.wav",&m_secondaryBufferMeteorDetonate)
		|| !LoadWaveFile("resource/weapon/meteor_whistle.wav",&m_secondaryBufferMeteorWhistle)
		|| !LoadWaveFile("resource/weapon/nuclear.wav",&m_secondaryBufferNuclear)) {
			return 0;
	}

	m_secondaryBufferShot->SetVolume(-750);
	//if (!PlayLoop()) 
	//	return 0;

	return 1;
}
void SoundManager::Shutdown() {
	StopMusicLoop();
	StopEndgameLoop();
	//Release secondary buffers
	ShutdownWaveFile(&m_secondaryBufferIntro);
	ShutdownWaveFile(&m_secondaryBufferShot);
	ShutdownWaveFile(&m_secondaryBufferSpaceShip);
	ShutdownWaveFile(&m_secondaryBufferBuzzer);
	ShutdownWaveFile(&m_secondaryBufferLose);
	ShutdownWaveFile(&m_secondaryBufferStart);
	ShutdownWaveFile(&m_secondaryBufferExplode);
	ShutdownWaveFile(&m_secondaryBufferEndgame);
	ShutdownWaveFile(&m_secondaryBufferMusic);
	ShutdownWaveFile(&m_secondaryBufferPowerup);
	ShutdownWaveFile(&m_secondaryBufferDing);
	ShutdownWaveFile(&m_secondaryBufferAOE);
	ShutdownWaveFile(&m_secondaryBufferBullet2);
	ShutdownWaveFile(&m_secondaryBufferMeteorDetonate);
	ShutdownWaveFile(&m_secondaryBufferMeteorWhistle);
	ShutdownWaveFile(&m_secondaryBufferNuclear);
	//Shutdown API
	ShutdownDirectSound();
}

bool SoundManager::PlayWaveFile(IDirectSoundBuffer8* buf, float distance) {
	if (buf == 0)
		return 1;
	HRESULT result;
	result = buf->SetCurrentPosition(0); 
	if (FAILED(result)) return 0;

    distance /= 10;
    float reduceVolume =  -1 * ( distance * distance );
    //printf("Reduce Volume by: %f\n", reduceVolume);
	result = buf->SetVolume( (LONG) max(DSBVOLUME_MIN, reduceVolume) );//DSBVOLUME_MAX);
	if (FAILED(result)) return 0;

	result = buf->Play(0,0,0);
	if (FAILED(result))
		return 0;
	return 1;
}

bool SoundManager::PlayIntroLoop() {
	HRESULT result;
	result = m_secondaryBufferIntro->SetCurrentPosition(0); 
	if (FAILED(result))
		return 0;
	result = m_secondaryBufferIntro->SetVolume(DSBVOLUME_MAX);
	if (FAILED(result))
		return 0;
    
	result = m_secondaryBufferIntro->Play(0,0,DSBPLAY_LOOPING);
	if (FAILED(result))
		return 0;
	return 1;	
}

bool SoundManager::PlayEndgameLoop() {
	HRESULT result;
	result = m_secondaryBufferEndgame->SetCurrentPosition(0); 
	if (FAILED(result))
		return 0;
	result = m_secondaryBufferEndgame->SetVolume(DSBVOLUME_MAX);
	if (FAILED(result))
		return 0;
    
	result = m_secondaryBufferEndgame->Play(0,0,DSBPLAY_LOOPING);
	if (FAILED(result))
		return 0;
	return 1;	
}

bool SoundManager::PlayMusicLoop() {
	HRESULT result;
	result = m_secondaryBufferMusic->SetCurrentPosition(0); 
	if (FAILED(result))
		return 0;
	result = m_secondaryBufferMusic->SetVolume(DSBVOLUME_MAX);
	if (FAILED(result))
		return 0;
    
	result = m_secondaryBufferMusic->Play(0,0,DSBPLAY_LOOPING);
	if (FAILED(result))
		return 0;
	return 1;	
}

void SoundManager::StopIntroLoop() {
	m_secondaryBufferIntro->Stop();
}

void SoundManager::StopEndgameLoop() {
	m_secondaryBufferEndgame->Stop();
}

void SoundManager::StopMusicLoop() {
	m_secondaryBufferMusic->Stop();
}

bool SoundManager::PlaySoundSpec(int sound, float distance) {
	if (sound == SoundManager::INTRO_SOUND)
		return PlayWaveFile(m_secondaryBufferIntro, distance);
	if (sound == SoundManager::SHOT_SOUND)
		return PlayWaveFile(m_secondaryBufferShot, distance);
	if (sound == SoundManager::SPACESHIP_SOUND)
		return PlayWaveFile(m_secondaryBufferSpaceShip, distance);
	if (sound == SoundManager::BUZZER_SOUND)
		return PlayWaveFile(m_secondaryBufferBuzzer, distance);
	if (sound == SoundManager::START_SOUND)
		return PlayWaveFile(m_secondaryBufferStart, distance);
	if (sound == SoundManager::LOSE_SOUND)
		return PlayWaveFile(m_secondaryBufferLose, distance);
	if (sound == SoundManager::EXPLODE_SOUND)
		return PlayWaveFile(m_secondaryBufferExplode, distance);
	if (sound == SoundManager::ENDGAME_SOUND)
		return PlayWaveFile(m_secondaryBufferEndgame, distance);
	if (sound == SoundManager::MUSIC_SOUND)
		return PlayWaveFile(m_secondaryBufferMusic, distance);
	if (sound == SoundManager::DING_SOUND)
		return PlayWaveFile(m_secondaryBufferDing, distance);
	if (sound == SoundManager::POWERUP_SOUND)
		return PlayWaveFile(m_secondaryBufferPowerup, distance);
	if (sound == SoundManager::AOE_SOUND)
		return PlayWaveFile(m_secondaryBufferAOE, distance);
	if (sound == SoundManager::BULLET2_SOUND)
		return PlayWaveFile(m_secondaryBufferBullet2, distance);
	if (sound == SoundManager::METEOR_DETONATE_SOUND)
		return PlayWaveFile(m_secondaryBufferMeteorDetonate, distance);
	if (sound == SoundManager::METEOR_WHISTLE_SOUND)
		return PlayWaveFile(m_secondaryBufferMeteorWhistle, distance);
	if (sound == SoundManager::NUCLEAR_SOUND)
		return PlayWaveFile(m_secondaryBufferNuclear, distance);
	else
		return 0;
}

bool SoundManager::LoadWaveFile(char* path, IDirectSoundBuffer8** buf) {
	FILE* filePtr;
	unsigned int count;
	WaveHeaderType waveFileHeader;
	WAVEFORMATEX waveFormat;
	DSBUFFERDESC bufferDesc;
	IDirectSoundBuffer* tempBuffer;
	unsigned char* waveData;
	unsigned char* bufferPtr;
	unsigned long bufferSize;
	HRESULT result;
	// Open file
	if (fopen_s(&filePtr, path, "rb") )
		return 0;
	
	if((count = fread(&waveFileHeader, sizeof(waveFileHeader), 1, filePtr) != 1))
		return 0;
	
	if ((waveFileHeader.chunkId[0]!='R') || (waveFileHeader.chunkId[1]!='I') ||
		(waveFileHeader.chunkId[2]!='F') || (waveFileHeader.chunkId[3]!='F') )
		return 0;
	if ((waveFileHeader.format[0]!='W') || (waveFileHeader.format[1]!='A') ||
		(waveFileHeader.format[2]!='V') || (waveFileHeader.format[3]!='E') )
		return 0;
	if ((waveFileHeader.subChunkId[0]!='f') || (waveFileHeader.subChunkId[1]!='m') ||
		(waveFileHeader.subChunkId[2]!='t') || (waveFileHeader.subChunkId[3]!=' ') )
		return 0;
	if (waveFileHeader.audioFormat != WAVE_FORMAT_PCM)
		return 0;
	if (waveFileHeader.numChannels != 2)
		return 0;
	if (waveFileHeader.sampleRate != 44100) 
		return 0;
	if (waveFileHeader.bitsPerSample != 16) 
		return 0;
	if ((waveFileHeader.dataChunkId[0]!='d') || (waveFileHeader.dataChunkId[1]!='a') ||
		(waveFileHeader.dataChunkId[2]!='t') || (waveFileHeader.dataChunkId[3]!='a') )
		return 0;

	
	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nChannels = 2;
	waveFormat.nBlockAlign =  (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags = DSBCAPS_CTRLVOLUME;
	bufferDesc.dwBufferBytes = waveFileHeader.dataSize;
	bufferDesc.dwReserved = 0;
	bufferDesc.lpwfxFormat = &waveFormat;
	bufferDesc.guid3DAlgorithm = GUID_NULL;

	//create buffer
	result = m_DirectSound->CreateSoundBuffer(&bufferDesc, &tempBuffer, NULL);
	if (FAILED(result))
		return 0;
	result = tempBuffer->QueryInterface(IID_IDirectSoundBuffer8, (void**)&*buf);
	if (FAILED(result))
		return 0;
	tempBuffer->Release();
	tempBuffer=0;

	fseek(filePtr, sizeof(WaveHeaderType), SEEK_SET);

	waveData = new unsigned char[waveFileHeader.dataSize];
	//Read in data
	if ((count = fread(waveData, 1, waveFileHeader.dataSize, filePtr)) != waveFileHeader.dataSize)
		return 0;

	if (fclose(filePtr))
		return 0;

	//lock buffer
	result = (*buf)->Lock(0, waveFileHeader.dataSize, (void**)&bufferPtr, (DWORD*)&bufferSize, NULL, 0,0);
	if (FAILED(result))
		return 0;

	//copy
	memcpy(bufferPtr, waveData, waveFileHeader.dataSize);
	
	result = (*buf)->Unlock((void*)bufferPtr, bufferSize, NULL, 0);
	if (FAILED(result))
		return 0;

	delete [] waveData;
	waveData = 0;
	return 1;
}

bool SoundManager::InitializeDirectSound(HWND hwnd) {
	//Code based off rastertek.com tutorial
	HRESULT result;
	DSBUFFERDESC bufferDesc;
	WAVEFORMATEX waveFormat;

	result = DirectSoundCreate8(NULL, &m_DirectSound, NULL);
	if (FAILED(result))
		return 0;

	result = m_DirectSound->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);
	if (FAILED(result))
		return 0;

	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
	bufferDesc.dwBufferBytes = 0;
	bufferDesc.dwReserved = 0;
	bufferDesc.lpwfxFormat = NULL;
	bufferDesc.guid3DAlgorithm = GUID_NULL;

	result = m_DirectSound->CreateSoundBuffer(&bufferDesc, &m_primaryBuffer, NULL);
	if (FAILED(result))
		return 0;

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nChannels = 2;
	waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	result = m_primaryBuffer->SetFormat(&waveFormat);
	if (FAILED(result))
		return 0;
	return 1;
}

void SoundManager::ShutdownDirectSound() {
	if (m_primaryBuffer) {
		m_primaryBuffer->Release();
		m_primaryBuffer = 0;
	}
	if (m_DirectSound) {
		m_DirectSound->Release();
		m_DirectSound = 0;
	}
}

void SoundManager::ShutdownWaveFile(IDirectSoundBuffer8** buf) {
	//Release secondary buffer
	if (*buf)
	{
		(*buf)->Release();
		*buf = 0;
	}
}