#ifndef _PARTICLENODE_H_
#define _PARTICLENODE_H_

#include "ModelNode.h"
#include "ParticleSystemObject.h"
#include "ShaderObject.h"

class ParticleNode : public ModelNode
{
protected:
    ParticleSystemObject * particleObject;

public:
    ParticleNode();
    ~ParticleNode();

    void SetParticleObject( ParticleSystemObject* );
    //inline void SetShader(ShaderObject* newShader){shader = newShader;}

    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);

    inline virtual bool isParticleNode() { return true; };
    inline virtual bool isModelNode()    { return true; }; // might not actually have this.

    bool checkForRemoval();
};

#endif