#include "Model.h"

#include "SystemManager.h"

#include <iostream>
#include "Log.h"
#include <assimpCollada/aiPostProcess.h>

// FORWARD DECLARATIONS

double testFrame = 0.0;

Model::Model()
{
    m_vertexBuffer = NULL;
	m_indexBuffer  = NULL;

	m_model       = NULL;
    m_vertexCount = 0;

    animEvaluator = NULL;
    multMeshPtr   = NULL;

    scene      = NULL;
    deviceRef  = NULL;
    m_Textures = NULL;

    numMeshes      = 0;
    drawAsWireFrame = alphaBlend = skeletalRigged = hasAnimations  = false;

    evalOnce = false;
    castsShadow = true;
}
Model::Model(const Model& other)
{
}
Model::~Model(){ Shutdown(); }

// fileType
//  0: simplified txt.
//  1: Collada File
//bool Model::Initialize(ID3D11Device* device, char* modelFilename, vector<char*> textureFileVector, int fileType)
bool Model::Initialize(ID3D11Device* device, char* modelFilename, int fileType)
{
    bool result;

    deviceRef = device;

	// Load in the model data,
	result = LoadModel(modelFilename, fileType);
	if(!result)
	{
        cout << "LoadModel(mfn) failed..." << endl;
		return false;
	}


    //CalculateModelVectors();

	// Initialize the vertex and index buffers.
	result = InitializeBuffers(device);
	if(!result)
	{
        cout << "Init buffers failed..." << endl;
		return false;
	}

//    SystemManager::Instance()->GetGraphicsManager()->GetTextureManager()->AddTexture(*m_Textures);

    return true;
}
bool Model::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData; // ????
	HRESULT result;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices) { return false; }

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices) { return false; }

	// Load the vertex array and index array with data.
    int meshNum = 0;
    unsigned int offset = 0;
	for( unsigned int i = 0; i < (unsigned int) m_vertexCount; i++ )
	{
        if ( multMeshPtr != NULL )
        {
            if ( i >= (multMeshPtr[meshNum].numVerts + offset) )
            {
                ++meshNum;
                offset = i;
                if ( meshNum >= this->numMeshes)
                {
                    cout << "[Model] ERROR: You killed it." << endl;
                    return false;
                }
            }

            // SET POSITION TO RELATIVE POSITION....
            D3DXVECTOR3 d3dvec = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].x,  multMeshPtr[meshNum].meshObj[i - offset].y, multMeshPtr[meshNum].meshObj[i - offset].z);
            D3DXVec3TransformCoord( &(vertices[i].position) , &d3dvec, &multMeshPtr[meshNum].modelRelativeMatrix );

            vertices[i].texture  = D3DXVECTOR2(multMeshPtr[meshNum].meshObj[i - offset].tu, multMeshPtr[meshNum].meshObj[i - offset].tv);
		    vertices[i].normal   = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].nx, multMeshPtr[meshNum].meshObj[i - offset].ny, multMeshPtr[meshNum].meshObj[i - offset].nz);

    		//vertices[i].tangent  = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].tx, multMeshPtr[meshNum].meshObj[i - offset].ty, multMeshPtr[meshNum].meshObj[i - offset].tz);
    		//vertices[i].binormal = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].bx, multMeshPtr[meshNum].meshObj[i - offset].by, multMeshPtr[meshNum].meshObj[i - offset].bz);

            indices [i] = i;

        } else
        {
		    vertices[i].position = D3DXVECTOR3(m_model[i].x,  m_model[i].y, m_model[i].z);
		    vertices[i].texture  = D3DXVECTOR2(m_model[i].tu, m_model[i].tv);
		    vertices[i].normal   = D3DXVECTOR3(m_model[i].nx, m_model[i].ny, m_model[i].nz);

            //vertices[i].tangent = D3DXVECTOR3(m_model[i].tx, m_model[i].ty, m_model[i].tz);
            //vertices[i].binormal = D3DXVECTOR3(m_model[i].bx, m_model[i].by, m_model[i].bz);

            indices [i] = i;
        }

	}

	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage     = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; // V
    vertexBufferDesc.CPUAccessFlags = 0;                   // ???
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage     = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

bool Model::LoadModel(char* filename, int fileType)
{
    if ( fileType != 0 )
    {
        scene = aiImportFile(filename, ( aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded ));
        if ( scene == NULL )
        {
            cout << "stop borking..." << endl;
            return false;
        }

        cout << filename << " there are: " << scene->mNumMeshes              << " meshes"     << endl;
        cout << filename << " there are: " << scene->mNumAnimations          << " animations" << endl;
        cout << filename << " there are: " << scene->mRootNode->mNumChildren << " children "  << endl;
        
        numMeshes = scene->mNumMeshes; // ###
        multMeshPtr = new MultMeshObject[numMeshes];

        if ( scene->HasAnimations() )
        {
            animEvaluator = new AnimEvaluator(scene->mAnimations[0]);
            hasAnimations = true;
        }

        int vc = 0; // vertexCounter;
        int badMeshes = 0;
        int v_Offset = 0;
        
        aiNode* travNode = scene->mRootNode;
        for(unsigned int i = 0; i < scene->mNumMeshes; i++)
//      for(unsigned int i = 0; i < scene->mRootNode->mNumChildren; i++)
        {
            vc = 0;
            aiMesh * mesh = scene->mMeshes[i];
            if ( mesh->mNumFaces < 6 )
            {
                cout << "Skipping what is PROBABLY a bad mesh..." << endl;
                ++badMeshes;
                continue;
            }

            m_model = new ModelType[mesh->mNumFaces*3];

            multMeshPtr[i - badMeshes].meshObj  = m_model;
            multMeshPtr[i - badMeshes].numVerts = mesh->mNumFaces * 3;

            multMeshPtr[i-badMeshes].offset     = v_Offset;
            v_Offset += multMeshPtr[i - badMeshes].numVerts;

            multMeshPtr[i - badMeshes].modelRelativeMatrix = D3DManager::AssimpMat2D3DMat(travNode->mChildren[i - badMeshes]->mTransformation);

            if ( scene->mMeshes[i]->HasBones() )
            { // If Rigged with a skeleton.
                cout << "[Bones]: " << scene->mMeshes[i]->mNumBones << endl; /// !!!
                multMeshPtr[i - badMeshes].bones    = scene->mMeshes[i]->mBones[0];
                multMeshPtr[i - badMeshes].numBones = scene->mMeshes[i]->mNumBones;

                skeletalRigged = true;
            }

            m_vertexCount += mesh->mNumFaces * 3;
            m_indexCount  = m_vertexCount; // match up index count and vertex count

            for (unsigned int j = 0; j < mesh->mNumFaces; ++j){
                const struct aiFace * face = &(mesh->mFaces[j]);
                switch(face->mNumIndices)
                {
                    case 1: //cout << "draw points" << endl;
                            break;
                    case 2: //cout << "draw lines" << endl;
                            break;
                    case 3: //cout << "draw triangles" << endl;
                            break;
                    default: //cout << "draw polygons" << endl;
                            break;
                }

                for (unsigned int k = 0; k < face->mNumIndices; ++k ){
                    int vertexIndex = face->mIndices[k];

                    if(mesh->mNormals != NULL)
                    {

                        aiVector3D normals = mesh->mNormals[vertexIndex];

                        m_model[vc].nx = normals.x;
                        m_model[vc].ny = normals.y;
                        m_model[vc].nz = normals.z;

                    }
                    if(mesh->HasTextureCoords(0))
					{
                        aiVector3D uvVec   = mesh->mTextureCoords[0][vertexIndex];

                        m_model[vc].tu = (uvVec.x);
                        m_model[vc].tv = (uvVec.y);

                    }
                    aiVector3D vertex  = mesh->mVertices[vertexIndex];

                    m_model[vc].x   = vertex.x;
                    m_model[vc].y   = vertex.y;
                    m_model[vc++].z = vertex.z;
                }
            }
        }

        numMeshes = scene->mNumMeshes - badMeshes;

    }else
    {// Not Collada file.
        ifstream fin;
	    char input;

        // Open the model file.
	    fin.open(filename);
	
	    // If it could not open the file then exit.
	    if(fin.fail())
	    {
            cout << "could not open file..." << endl;
		    return false;
	    }

        // Read up to the value of vertex count.
	    fin.get(input);
	    while(input != ':'){ fin.get(input); }

        // Read in the vertex count. (Assuming the basic txt...)
	    fin >> m_vertexCount;

        // Set the number of indices to be the same as the vertex count.
	    m_indexCount = m_vertexCount;

	    // Create the model using the vertex count that was read in.
	    m_model = new ModelType[m_vertexCount];
	    if(!m_model) return false;

        // Read up to the beginning of the data.
	    fin.get(input);
	    while(input != ':')
	    {
		    fin.get(input);
	    }
	    fin.get(input); // moves it down a line I assume?
	    fin.get(input);

	    // Read in the vertex data.
        //int interval = m_vertexCount/10;
	    for(int i=0; i < m_vertexCount; i++)
	    {
		    fin >> m_model[i].x  >> m_model[i].y >> m_model[i].z;
		    fin >> m_model[i].tu >> m_model[i].tv;
		    fin >> m_model[i].nx >> m_model[i].ny >> m_model[i].nz;
	    }

	    // Close the model file.
	    fin.close();
    }

	return true;
}
void Model::CalculateModelVectors()
{
    int faceCount, index;
	TempVertexType vertex1, vertex2, vertex3;
	VectorType tangent, binormal, normal;

	// Calculate the number of faces in the model.
	faceCount = m_vertexCount / 3;

	// Initialize the index to the model data.
	index = 0;
    int meshNum = 0;
    unsigned int offset = 0;
    if ( multMeshPtr != NULL )
    {
        for (int i = 0; i < faceCount; ++i)
        {

            if ( (unsigned int) index >= (multMeshPtr[meshNum].numVerts + offset) )
            {
                ++meshNum;
                offset = index;
                if ( meshNum >= this->numMeshes)
                {
                    cout << "[Model] ERROR: You killed it." << endl;
                    return;
                }
            }
            for (int j = 0; j < 3; ++j )
            {
                TempVertexType * tvt_ptr;
                switch (j)
                {
                    case 0: tvt_ptr = &vertex1; break;
                    case 1: tvt_ptr = &vertex2; break;
                    case 2: tvt_ptr = &vertex3; break;
                }

                tvt_ptr->x  = multMeshPtr[meshNum].meshObj[index - offset].x;
		        tvt_ptr->y  = multMeshPtr[meshNum].meshObj[index - offset].y;
		        tvt_ptr->z  = multMeshPtr[meshNum].meshObj[index - offset].z;
		        tvt_ptr->tu = multMeshPtr[meshNum].meshObj[index - offset].tu;
		        tvt_ptr->tv = multMeshPtr[meshNum].meshObj[index - offset].tv;
		        tvt_ptr->nx = multMeshPtr[meshNum].meshObj[index - offset].nx;
		        tvt_ptr->ny = multMeshPtr[meshNum].meshObj[index - offset].ny;
		        tvt_ptr->nz = multMeshPtr[meshNum].meshObj[index - offset].nz;
                index++;
            }
            // Calculate the tangent and binormal of that face.
		    CalculateTangentBinormal(vertex1, vertex2, vertex3, tangent, binormal);
		    // Calculate the new normal using the tangent and binormal.
		    CalculateNormal(tangent, binormal, normal);
            for (int j = 1; j < 4; ++j )
            {
                multMeshPtr[meshNum].meshObj[index - offset - j].nx = normal.x;
		        multMeshPtr[meshNum].meshObj[index - offset - j].ny = normal.y;
		        multMeshPtr[meshNum].meshObj[index - offset - j].nz = normal.z;
		        multMeshPtr[meshNum].meshObj[index - offset - j].tx = tangent.x;
		        multMeshPtr[meshNum].meshObj[index - offset - j].ty = tangent.y;
		        multMeshPtr[meshNum].meshObj[index - offset - j].tz = tangent.z;
		        multMeshPtr[meshNum].meshObj[index - offset - j].bx = binormal.x;
		        multMeshPtr[meshNum].meshObj[index - offset - j].by = binormal.y;
		        multMeshPtr[meshNum].meshObj[index - offset - j].bz = binormal.z;
            }
        }
    }else
    {
	    // Go through all the faces and calculate the the tangent, binormal, and normal vectors.
	    for(int i=0; i<faceCount; ++i)
	    {
		    // Get the three vertices for this face from the model.
		    vertex1.x  = m_model[index].x;
		    vertex1.y  = m_model[index].y;
		    vertex1.z  = m_model[index].z;
		    vertex1.tu = m_model[index].tu;
		    vertex1.tv = m_model[index].tv;
		    vertex1.nx = m_model[index].nx;
		    vertex1.ny = m_model[index].ny;
		    vertex1.nz = m_model[index].nz;
		    index++;

		    vertex2.x = m_model[index].x;
		    vertex2.y = m_model[index].y;
		    vertex2.z = m_model[index].z;
		    vertex2.tu = m_model[index].tu;
		    vertex2.tv = m_model[index].tv;
		    vertex2.nx = m_model[index].nx;
		    vertex2.ny = m_model[index].ny;
		    vertex2.nz = m_model[index].nz;
		    index++;

		    vertex3.x = m_model[index].x;
		    vertex3.y = m_model[index].y;
		    vertex3.z = m_model[index].z;
		    vertex3.tu = m_model[index].tu;
		    vertex3.tv = m_model[index].tv;
		    vertex3.nx = m_model[index].nx;
		    vertex3.ny = m_model[index].ny;
		    vertex3.nz = m_model[index].nz;
		    index++;

		    // Calculate the tangent and binormal of that face.
		    CalculateTangentBinormal(vertex1, vertex2, vertex3, tangent, binormal);

		    // Calculate the new normal using the tangent and binormal.
		    CalculateNormal(tangent, binormal, normal);

		    // Store the normal, tangent, and binormal for this face back in the model structure.
		    m_model[index-1].nx = normal.x;
		    m_model[index-1].ny = normal.y;
		    m_model[index-1].nz = normal.z;
		    m_model[index-1].tx = tangent.x;
		    m_model[index-1].ty = tangent.y;
		    m_model[index-1].tz = tangent.z;
		    m_model[index-1].bx = binormal.x;
		    m_model[index-1].by = binormal.y;
		    m_model[index-1].bz = binormal.z;

		    m_model[index-2].nx = normal.x;
		    m_model[index-2].ny = normal.y;
		    m_model[index-2].nz = normal.z;
		    m_model[index-2].tx = tangent.x;
		    m_model[index-2].ty = tangent.y;
		    m_model[index-2].tz = tangent.z;
		    m_model[index-2].bx = binormal.x;
		    m_model[index-2].by = binormal.y;
		    m_model[index-2].bz = binormal.z;

		    m_model[index-3].nx = normal.x;
		    m_model[index-3].ny = normal.y;
		    m_model[index-3].nz = normal.z;
		    m_model[index-3].tx = tangent.x;
		    m_model[index-3].ty = tangent.y;
		    m_model[index-3].tz = tangent.z;
		    m_model[index-3].bx = binormal.x;
		    m_model[index-3].by = binormal.y;
		    m_model[index-3].bz = binormal.z;
	    }
    }
	return;
}
void Model::CalculateTangentBinormal(TempVertexType vertex1, TempVertexType vertex2, TempVertexType vertex3,
										  VectorType& tangent, VectorType& binormal)
{
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;

	// Calculate the two vectors for this face.
	vector1[0] = vertex2.x - vertex1.x;
	vector1[1] = vertex2.y - vertex1.y;
	vector1[2] = vertex2.z - vertex1.z;

	vector2[0] = vertex3.x - vertex1.x;
	vector2[1] = vertex3.y - vertex1.y;
	vector2[2] = vertex3.z - vertex1.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = vertex2.tu - vertex1.tu;
	tvVector[0] = vertex2.tv - vertex1.tv;

	tuVector[1] = vertex3.tu - vertex1.tu;
	tvVector[1] = vertex3.tv - vertex1.tv;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of this normal.
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));
			
	// Normalize the normal and then store it
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of this normal.
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));
			
	// Normalize the normal and then store it
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	return;
}
void Model::CalculateNormal(VectorType tangent, VectorType binormal, VectorType& normal)
{
	float length;

	// Calculate the cross product of the tangent and binormal which will give the normal vector.
	normal.x = (tangent.y * binormal.z) - (tangent.z * binormal.y);
	normal.y = (tangent.z * binormal.x) - (tangent.x * binormal.z);
	normal.z = (tangent.x * binormal.y) - (tangent.y * binormal.x);

	// Calculate the length of the normal.
	length = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));

	// Normalize the normal.
	normal.x = normal.x / length;
	normal.y = normal.y / length;
	normal.z = normal.z / length;

	return;
}

void Model::Render(ID3D11DeviceContext* deviceContext, double* animationFrame)
{
    if (    scene != NULL && scene->HasAnimations() )
    {// If the scene has animations than
        if (animationFrame == NULL) animationFrame = &testFrame;
        *animationFrame = (*animationFrame > 100) ? 0 : ( ++*animationFrame );
	    animEvaluator->Evaluate( *animationFrame / 50.0 );

	    std::vector<aiMatrix4x4> m = animEvaluator->GetTransformations();
        for (unsigned int i = 0; i < (unsigned int)GetNumMeshes(); ++i)
        {            
            unsigned int i2 = 0;

            if ( i < m.size() ) { i2 = i; }
            else                { i2 = 0; }

            // GRAB ANIMATION MATRIX
            multMeshPtr[i].animationMatrix = D3DManager::AssimpMat2D3DMat( m.at(i2) );
        }
    }

	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}
void Model::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0); // ???

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	
    if (isDrawAsWireFrame()) deviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);
    else                     deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

int Model::GetIndexCount()
{
    return m_indexCount;
}
int Model::GetNumMeshes(){ return numMeshes; }

bool Model::isDrawAsWireFrame(){ return drawAsWireFrame; }
void Model::setDrawAsWireFrame(bool wireframeit){ drawAsWireFrame = wireframeit; }

TextureObject* Model::GetTextureObject(){ return m_Textures; }
void Model::SetTextureObject(TextureObject& textureRef){ m_Textures = &textureRef; }

bool Model::isAlphaBlend()   { return alphaBlend; }
bool Model::hasSkeletalRig() { return skeletalRigged; }
void Model::setAlphaBlend(bool alpha){ alphaBlend = alpha; }
void Model::SetNoShadow(){ castsShadow = false; }

/***
 * Cleans up Model
 */ 
void Model::Shutdown()
{
	// Release the model texture.

	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();

    if (deviceRef)
    {
        delete deviceRef;
        deviceRef = NULL;
    }

	return;
}
void Model::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}
void Model::ReleaseModel()
{
	if(m_model)
	{
		delete [] m_model;
		m_model = 0;
	}

    if (scene)
    {
        delete scene;
        scene = NULL;
    }

	return;
}