#ifndef _NETWORKMANAGER_H_
#define _NETWORKMANAGER_H_

#include <winsock2.h>
#include <string>

#include "rapidxml.hpp"

#define UNINITIALIZED -1

class Event;

class NetworkManager
{
public:
	NetworkManager(unsigned short port, std::string serverName);
	~NetworkManager(void);

	bool Initialize();
	void EndConnection();

    static NetworkManager* Instance();

    int getCurrentSparkId();
	int getCurrentRobotId();
	int getCurrentRobotHealth();
	void setCurrentRobotHealth(int);
    int getPlayerId();

    int sendEvent(Event * event);
	void updateFromServer();

private:
	unsigned short port;
	std::string serverName;
	SOCKET serverSocket;
	
    int playerId;
    int currentSparkId;
	int currentRobotId;
	int currentRobotHealth;
    
    static NetworkManager * m_Instance;
    bool checkSocketError(int nBytes, SOCKET clientSocket);

    void updateUnitsWithMessage(rapidxml::xml_document<> & doc, rapidxml::xml_node<> * currentUnit);
    void processEvent(Event * event);

    void handleLose();
    void handleWin();
};

#endif