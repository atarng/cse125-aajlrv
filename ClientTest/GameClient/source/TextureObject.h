////////////////////////////////////////////////////////////////////////////////
// Filename: TextureObject.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTUREOBJECT_H_
#define _TEXTUREOBJECT_H_


//////////////
// INCLUDES //
//////////////
#include <D3DX/d3d11.h>
#include <D3DX/d3dx11tex.h>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
// Class name: TextureObject
////////////////////////////////////////////////////////////////////////////////
class TextureObject
{
private:
	//ID3D11ShaderResourceView* m_textures[2];  // For Bump Mapping
    ID3D11ShaderResourceView** m_textures;    // For Bump Mapping
    //ID3D11ShaderResourceView* m_textures;
    int n_textureSources;

public:
	TextureObject();
	TextureObject(const TextureObject&);
	~TextureObject();

	bool Initialize(ID3D11Device*, char**, int);
    bool Initialize(ID3D11Device*, std::vector<char*>);
	void Shutdown();

    //ID3D11ShaderResourceView* GetTexture();
	ID3D11ShaderResourceView** GetTextures();
    int numTextures();
};

#endif