////////////////////////////////////////////////////////////////////////////////
// Filename: lightobject.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _LIGHT_H_
#define _LIGHT_H_


//////////////
// INCLUDES //
//////////////
//#include <d3dx10math.h>
#include "D3DManager.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: LightObject
////////////////////////////////////////////////////////////////////////////////
class LightObject
{
public:
	LightObject();
	LightObject(const LightObject&);
	~LightObject();

    void SetAmbientColor(float, float, float, float); // S
	void SetDiffuseColor(float, float, float, float); // S
    void SetPosition(float, float, float);  // S
	void SetDirection(float, float, float); // lookat?

    void SetSpecularColor(float, float, float, float);
	void SetSpecularPower(float);

    void SetAmbientColor(D3DXVECTOR4 ambient) { m_ambientColor = ambient; }
    void SetDiffuseColor(D3DXVECTOR4 diffuse) { m_diffuseColor = diffuse; }
    void SetPosition(D3DXVECTOR3 newPos)      { m_position = newPos;  }
    void SetLookAt(D3DXVECTOR3 newLookAt)     { m_lookAt = newLookAt; };
    void SetDirection(D3DXVECTOR3 newDir);

    D3DXVECTOR4 GetAmbientColor();//{ return m_ambientColor; }
    D3DXVECTOR4 GetDiffuseColor();//{ return m_diffuseColor; }
    D3DXVECTOR4 GetSpecularColor();
    float GetSpecularPower();

    D3DXVECTOR3 GetPosition();  //{return m_position; }
    D3DXVECTOR3 GetDirection(); //{return m_direction; }
    inline D3DXVECTOR3 GetLookAt() { return m_lookAt; };
    
	void GenerateViewMatrix();
	void GenerateProjectionMatrix(float screenDepth, float screenNear);

    void GetMatrix(D3DXMATRIX&); // ### is this used?
    inline void GetViewMatrix(D3DXMATRIX& viewMatrix){ viewMatrix = m_viewMatrix; }; // $$$
    inline void GetProjectionMatrix(D3DXMATRIX& projectionMatrix ){ projectionMatrix = m_projectionMatrix; }; // $$$

private:
    D3DXVECTOR4 m_ambientColor; 
	D3DXVECTOR4 m_diffuseColor;

    D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_direction;
    D3DXVECTOR3 m_lookAt;          // $$$ direction?

    D3DXVECTOR4 m_specularColor;
	float m_specularPower;

	D3DXMATRIX m_viewMatrix;       // $$$
	D3DXMATRIX m_projectionMatrix; // $$$
};

#endif