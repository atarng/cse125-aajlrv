//LoadingManage.cpp

#include "LoadingManager.h"
#include "FileParser.h"
#include "Log.h"

//must initialize default variables here
std::string LoadingManager::applicationName = "NoName";
bool LoadingManager::fullscreen = false;
int LoadingManager::windowHeight = 800;
int	LoadingManager::windowWidth = 800;
int	LoadingManager::cameraWeight = 4;
std::string LoadingManager::shaderVersion = "_4_0";

bool LoadingManager::holdOnExit    = false;
bool LoadingManager::printDbgMsgs  = false;
bool LoadingManager::multiTextures = false;
bool LoadingManager::displayBoundingSpheres = false;
bool LoadingManager::useInstancing = false;
bool LoadingManager::lockLightToCamera = false;
bool LoadingManager::tightCameraLock = false;

std::string	LoadingManager::serverIP = "localhost";

LoadingManager::LoadingManager(){}
LoadingManager::LoadingManager(std::string loadingFileName)
{
	Initialize(loadingFileName);
}
LoadingManager::~LoadingManager(){}

/***
 * Initialize(std::string loadingFileName)
 * 
 */
bool LoadingManager::Initialize(std::string loadingFileName)
{
    FileParser* parser = new FileParser(loadingFileName);
	
	fullscreen = parser->ParseBool("fullscreen");
	windowHeight = parser->ParseInt("windowHeight");
	windowWidth = parser->ParseInt("windowWidth");
    cameraWeight = parser->ParseInt("cameraWeight");
	applicationName = parser->ParseString("applicationName");

    shaderVersion = parser->ParseString("shaderVersion");

    holdOnExit = parser->ParseBool("holdOnExit");    
    printDbgMsgs = parser->ParseBool("printDbgMsgs");
    displayBoundingSpheres = parser->ParseBool("displayBoundingSpheres");

    serverIP = parser->ParseString("serverIP");

    useInstancing = parser->ParseBool("useInstancing");
    lockLightToCamera = parser->ParseBool("lockLightToCamera");
	tightCameraLock   = parser->ParseBool("tightCameraLock");

    Log::useLog = parser->ParseBool("useLog");
    Log::useClockTime = parser->ParseBool("useClockTime");

	parser->Close();
	delete parser;

	return true;
}