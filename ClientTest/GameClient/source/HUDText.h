#ifndef _HUDTEXT_H_
#define _HUDTEXT_H_

#include "HUDDisplayable.h"
#include "D3DManager.h"

#include "TextureObject.h"
#include "FontObject.h"

class ShaderObject;

class HUDText : public HUDDisplayable
{
private:
    struct SentenceType
	{
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};
    struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

	float m_red, m_green, m_blue;
    FontObject* m_Font;
    ShaderObject* m_FontShader;

	int m_screenWidth, m_screenHeight, m_positionX, m_positionY;
	SentenceType* m_Sentence;
	char * m_textMessage;

    bool InitializeSentence(SentenceType**, int, ID3D11Device*);
	bool UpdateSentence(SentenceType*, char*, int, int, float, float, float, ID3D11DeviceContext*);
	void ReleaseSentence(SentenceType**);
    bool RenderSentence( ID3D11DeviceContext*, SentenceType* );
	//bool RenderSentence(ID3D11DeviceContext*, SentenceType*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
public:
    HUDText();
	HUDText(const HUDText&);
	~HUDText();

	bool Initialize(ID3D11Device*, ID3D11DeviceContext*, int, int, char*, TextureObject * to, char*);// ,char*);
	void Shutdown();

    bool Render(ID3D11DeviceContext*, int, int);
	bool Render(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);

    inline FontObject *   getFont()     { return m_Font; };
    inline SentenceType * getSentence() { return m_Sentence; };

    void SetShader(ShaderObject*);

	void ChangeText(char *);
	void ChangeColor(float, float, float);

    bool isText();
};

#endif