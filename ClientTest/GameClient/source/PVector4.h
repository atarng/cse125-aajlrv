//PVector4.h

#ifndef _PVECTOR4_H_
#define _PVECTOR4_H_
#include <string>
#include <sstream>
#include <iostream>

class PVector4
{
public:
    float x,y,z,w;

    PVector4();
    PVector4(const PVector4& rhs){ x=rhs.x; y=rhs.y; z=rhs.z; w=rhs.w; }
    PVector4(float rx, float ry, float rz, float rw){ x=rx; y=ry; z=rz; w=rw; }
    ~PVector4();

    void operator += ( const PVector4& );
    void operator -= ( const PVector4& );
    void operator *= ( const PVector4& );
    void operator /= ( const PVector4& );
    void operator *= ( float );
    void operator /= ( float );

    PVector4 operator + ( const PVector4& );
    PVector4 operator - ( const PVector4& );
    PVector4 operator * ( float );
    PVector4 operator / ( float );
    PVector4 operator - ();

    bool operator == ( const PVector4& ) const;
    bool operator != ( const PVector4& ) const;

    void normalize();

    float length();
    float length2();

    float dot( PVector4& );
    PVector4 cross( PVector4&, PVector4& );

    std::string PVector4::toString() const;

};
#endif 