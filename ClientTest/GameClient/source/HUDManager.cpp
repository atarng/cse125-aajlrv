/***
 * Filename: HUDManager.cpp
 * Contributor(s): Alfred Tarng, Roy Chen
 * Purpose:
 */

#include "HUDManager.h"

#include <iostream>

#include "SystemManager.h"
#include "GraphicsManager.h"
#include "TextureManager.h"
#include "TextureObject.h"
#include "LoadingManager.h"
#include "D3DManager.h"
#include "InputManager.h"

#include "HUDGraphic.h"
#include "HUDMenuButton.h"
#include "HUDMiniMap.h"
#include "HUDBar.h"

#include "HUDMenuItem.h"

#define UNINITIALIZED -1

// EXIT BUTTON FUNCTION
void HM_OCFXN_0(){ SystemManager::Instance()->PrepareShutdown(); } //
// TOGGLE SHADOWS
void HM_OCFXN_1()
{ 
	ShaderManager* sm_ptr = SystemManager::Instance()->GetGraphicsManager()->GetShaderManager();
	sm_ptr->SetUsingShadows(!(sm_ptr->IsUsingShadows()));
}
// Dismiss start screen
void HM_OCFXN_2()
{ 
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
	hm->getStart()->setDisplay(false);
	hm->getSplash()->setDisplay(false);
	hm->getField()->setDisplay(false);
	hm->getField2()->setDisplay(false);
	hm->getFocus1()->setDisplay(false);
	hm->getFocus2()->setDisplay(false);
	hm->SplashOff();
	GraphicsManager* graphics = SystemManager::Instance()->GetGraphicsManager();
    CameraObject* camera = graphics->GetCamera();
	InputManager* userInput = SystemManager::Instance()->GetInputManager();
	if(camera)
    {
        // turn off the hud
        hm->getCrosshairs()->setDisplay(true);
        //hm->getCrosshairs2()->setDisplay(true);
        // set the camera to Follow Mode
        camera->SetCameraType(FOLLOW);
        userInput->SetCenterTheMouse(true);
        userInput->HideMouseCursor();
    }

	//play start sound
	SoundManager* sound = SystemManager::Instance()->GetSoundManager();
	if (sound) {
		sound->StopIntroLoop();
		sound->PlaySoundSpec(SoundManager::START_SOUND);
		sound->PlayMusicLoop();
	}

	graphics -> AddSatellites();
}//

void HM_OCFXN_3()
{ 
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
	hm->getFocus1()->setDisplay(true);
	hm->getFocus2()->setDisplay(false);
	hm->getField()->setFocus(true);
	hm->getField2()->setFocus(false);
}

void HM_OCFXN_4()
{ 
	HUDManager * hm = (SystemManager::Instance()->GetHUDManager());
	hm->getFocus1()->setDisplay(false);
	hm->getFocus2()->setDisplay(true);
	hm->getField()->setFocus(false);
	hm->getField2()->setFocus(true);
}


HUDManager* HUDManager::m_Instance = NULL;
HUDManager* HUDManager::Instance()
{
    if (!m_Instance) m_Instance = new HUDManager;
    return m_Instance;
}

HUDManager::HUDManager()
{
	cerr << "[HUDManager] Allocated" << endl;
	displayingPopUpMenu = false;
	displayingSplash = true;
}
HUDManager::~HUDManager()
{
    while ( !hudDisplayables.empty() )
    {
        HUDDisplayable * he = hudDisplayables.back();
        if (he != NULL)
        {
            delete he;
            he = 0;
        }
        hudDisplayables.pop_back();
    }

}

/***
 * Initialize(D3DManager *)
 * TODO: Split this into calling two functions: initializeDisplayables() and initializeComponents()
 */
bool HUDManager::Initialize(D3DManager * man)
{
	cout << "[HUDManager] Initializing HUDManager..." << endl;
	bar4  = new HUDBar(  0, 256, 100);    
	bar2  = new HUDBar(  0, 256, 100);          
	bar3 = new HUDBar(300, 256, 300);
	bar = new HUDBar( 0, 256, 1);
	counter = new HUDCounter (0);
	field = new HUDField(24);
	field2 = new HUDField(15);
	field3 = new HUDField(100);
	crosshairs = new HUDMenuItem();
	crosshairs2 = new HUDMenuItem();
	focus1 = new HUDMenuItem();
	focus2 = new HUDMenuItem();
	lose = new HUDMenuItem();
	win = new HUDMenuItem();

    bool result;
    TextureManager * texMan = SystemManager::Instance()->GetGraphicsManager()->GetTextureManager(); 

	//// GRAY BAR
    HUDDisplayable * initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cout << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().GRAY_BAR ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.GRAY_BAR = AddHudDisplayable(initHudDisplayable);

	//BLUE BAR
	initHudDisplayable = bar->getBlue();
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cout << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().BLUE_BAR ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BLUE_BAR = AddHudDisplayable(initHudDisplayable);

	//BLUE BAR2
	initHudDisplayable = bar2->getBlue();
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cout << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().BLUE_BAR ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BLUE_BAR2 = AddHudDisplayable(initHudDisplayable);

	//BLUE BAR3
	initHudDisplayable = bar3->getBlue();
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().BLUE_BAR ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BLUE_BAR3 = AddHudDisplayable(initHudDisplayable);

	//BLUE BAR4
	initHudDisplayable = bar4->getBlue();
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().BLUE_BAR ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BLUE_BAR4 = AddHudDisplayable(initHudDisplayable);

    // Silver Hud Button
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               120, 55);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().TEST_HUD_BUTTON ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.TEST_GRAPHIC = AddHudDisplayable(initHudDisplayable);

	//blank field
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               228, 28);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().FIELD ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.TEST_FIELD = AddHudDisplayable(initHudDisplayable);

	//focus highlight
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               228, 28);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().FOCUS ) );
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.FOCUS = AddHudDisplayable(initHudDisplayable);

	//crosshairs
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               128, 128);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
	((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().CROSSHAIRS ) );
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.CROSSHAIRS = AddHudDisplayable(initHudDisplayable);


	//crosshairs2
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               256, 256);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
	((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().CROSSHAIRS ) );
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.CROSSHAIRS2 = AddHudDisplayable(initHudDisplayable);

	//splash
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               SystemManager::windowWidth, SystemManager::windowHeight);
                                                               //1024, 1024);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
	((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().SPLASH ) );
    initHudDisplayable->setAlphaComponent(false);
	displayablesSelector.SPLASH = AddHudDisplayable(initHudDisplayable);

	//lose
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               767, 167);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
	((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().LOSE ) );
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.LOSE = AddHudDisplayable(initHudDisplayable);

	//win
	initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               767, 167);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element" << endl;
		return false;
	}
	((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().WIN ) );
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.WIN = AddHudDisplayable(initHudDisplayable);

	// lightning bar
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               32, 32);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap Icon" << endl;
		return false;
	}
    initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().LIGHTNING_BAR ) );
    displayablesSelector.LIGHTNING_BAR = AddHudDisplayable(initHudDisplayable);

	// robot bar
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               32, 32);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap Icon" << endl;
		return false;
	}
    initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().ROBOT_BAR ) );
    displayablesSelector.ROBOT_BAR = AddHudDisplayable(initHudDisplayable);

	// lightning bar
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               32, 32);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap Icon" << endl;
		return false;
	}
    initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().SPARK_BAR ) );
    displayablesSelector.SPARK_BAR = AddHudDisplayable(initHudDisplayable);


    // MiniMap Icon (Probe)
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               32, 32);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap Icon" << endl;
		return false;
	}
    initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().MINIMAP_ICON ) );
    displayablesSelector.MINIMAP_ICON1 = AddHudDisplayable(initHudDisplayable);

    // MiniMap GRAPHIC (PLAYER CENTRIC)
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    ((HUDGraphic*) initHudDisplayable) ->SetHUDPartitionType(hpt_CIRCLE);
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                                 SystemManager::windowWidth, SystemManager::windowHeight,
                                                                 128, 128);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap" << endl;
		return false;
	}
    //initHudDisplayable->setAlphaComponent(false); //
	initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().TEST_MAP ) );
    displayablesSelector.MINIMAP_PC_GRAPHIC = AddHudDisplayable(initHudDisplayable);


    // MiniMap GRAPHIC OVERLAY
    initHudDisplayable = new HUDGraphic;
    if(!initHudDisplayable || !initHudDisplayable->isGraphic()) return false;
    result = ((HUDGraphic*) initHudDisplayable) -> Initialize( man->GetDevice(),
                                                               SystemManager::windowWidth, SystemManager::windowHeight,
                                                               128, 128);
                                                               //256, 256);
    if(!result)
	{
		cerr << "[HUDMANAGER] FAIL: Could not initialize the Hud Element: Minimap" << endl;
		return false;
	}
	initHudDisplayable->setAlphaComponent(true);
    ((HUDGraphic*) initHudDisplayable)->SetTextureObject( texMan->GetTextureObject( texMan->GetTextureSelecter().MINIMAP ) );
    displayablesSelector.MINIMAP_GRAPHIC = AddHudDisplayable(initHudDisplayable);

    // INITIALIZE TEXT OBJECT "Use Shadows"
    initHudDisplayable = new HUDText;
    if(!initHudDisplayable  || !initHudDisplayable->isText()) return false;
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )), //"resource/testfontdata.dds",
                                                          "UseShadows");
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
	((HUDText *)initHudDisplayable)->ChangeColor(0.0f, 0.0f, 0.0f);
	displayablesSelector.SHADOW_TEXT = AddHudDisplayable(initHudDisplayable);
	

	// INITIALIZE TEXT OBJECT "Start"
    initHudDisplayable = new HUDText;
    if(!initHudDisplayable  || !initHudDisplayable->isText()) return false;
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )), //"resource/testfontdata.dds",
                                                          "Start");
	((HUDText *)initHudDisplayable)->ChangeColor(0.0f, 0.0f, 0.0f);
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.START_TEXT = AddHudDisplayable(initHudDisplayable);
	
    // INITIALIZE TEXT OBJECT: "Exit"
    initHudDisplayable = new HUDText;
    if(!initHudDisplayable  || !initHudDisplayable->isText()) return false;
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          "Exit");
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
	((HUDText *)initHudDisplayable)->ChangeColor(0.0f, 0.0f, 0.0f);
    displayablesSelector.TEXT_EXIT = AddHudDisplayable(initHudDisplayable);

    //BAR VALUE
	initHudDisplayable = bar->getText();

    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          bar->getBuffer());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BAR_VALUE = AddHudDisplayable(initHudDisplayable);

	//BAR2 VALUE
	initHudDisplayable = bar2->getText();

    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          bar2->getBuffer());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.BAR_VALUE2 = AddHudDisplayable(initHudDisplayable);

	//BAR3 VALUE
	initHudDisplayable = bar3->getText();

    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          bar3->getBuffer());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.BAR_VALUE3 = AddHudDisplayable(initHudDisplayable);

	//BAR4 VALUE
	initHudDisplayable = bar4->getText();

    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          bar4->getBuffer());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
	displayablesSelector.BAR_VALUE4 = AddHudDisplayable(initHudDisplayable);

	//COUNTER VALUE
	initHudDisplayable = counter->getText();

    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          counter->getBuffer());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.COUNTER_VALUE = AddHudDisplayable(initHudDisplayable);

	//FIELD VALUE
	initHudDisplayable = field->getText();
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          field->getValue());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.FIELD_VALUE = AddHudDisplayable(initHudDisplayable);

	//FIELD 2 VALUE
	initHudDisplayable = field2->getText();
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          field2->getValue());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element2" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.FIELD_VALUE2 = AddHudDisplayable(initHudDisplayable);

	//FIELD 3 VALUE
	initHudDisplayable = field3->getText();
    result = ((HUDText *)initHudDisplayable)->Initialize( man->GetDevice(), man->GetDeviceContext(),
                                                          SystemManager::windowWidth, SystemManager::windowHeight,
                                                          "resource/testfontdata.txt", &(texMan->GetTextureObject( texMan->GetTextureSelecter().FONT )),
                                                          field3->getValue());
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the Hud_Text Element3" << endl;
        return false;
    }
    initHudDisplayable->setAlphaComponent(true);
    displayablesSelector.FIELD_VALUE3 = AddHudDisplayable(initHudDisplayable);


/** INITIALIZE THE HUD COMPONENTS *******************************************************************/

	// Exit Button
    HUDMenuItem * initHudComponent = new HUDMenuButton;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_GRAPHIC,      0,0) );
  //  initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_ALPHAGRAPHIC, 0,0) );
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEXT_EXIT,        48,25) );
    ((HUDMenuButton*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_0) );
    initHudComponent -> setDisplay(false);
    result = ((HUDMenuButton*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Button1" << endl;
        return false;
    } hudSelector.MENU_BUTTON_EXIT = AddHudElement( initHudComponent );

	// Shadow Button
	initHudComponent = new HUDMenuButton;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_GRAPHIC,      0,50) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.SHADOW_TEXT,       22,75) );
    ((HUDMenuButton*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_1) );
    initHudComponent -> setDisplay(false);
    result = ((HUDMenuButton*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Button1" << endl;
        return false;
    } hudSelector.MENU_BUTTON_SHADOWS = AddHudElement( initHudComponent );

	//MiniMap
    initHudComponent = new HUDMiniMap;
    initHudComponent -> getItemConfig().push_back(
                HUDMenuItem::createHUDItemConfig( displayablesSelector.MINIMAP_PC_GRAPHIC, 0, SystemManager::windowHeight - 256) );
    initHudComponent -> setDisplay(false);
    result = ((HUDMiniMap*)initHudComponent)->Initialize( *GetHUDDisplayables().at(displayablesSelector.MINIMAP_PC_GRAPHIC) );
           //(HUDMiniMap*)initHudComponent->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: MiniMap" << endl;
        return false;
    } hudSelector.MINIMAP = AddHudElement( initHudComponent );

	//MiniMapOverlay
    initHudComponent = new HUDMiniMap;
    initHudComponent -> getItemConfig().push_back(
        HUDMenuItem::createHUDItemConfig( displayablesSelector.MINIMAP_GRAPHIC, SystemManager::windowWidth - 128, SystemManager::windowHeight - 128) );
    initHudComponent -> setDisplay(true);
    result = initHudComponent->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: MiniMapOVERLAY" << endl;
        return false;
    } hudSelector.MINIMAP_OVERLAY = AddHudElement( initHudComponent );


	//Initialize bar
	initHudComponent = bar;
   // initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BAR_VALUE, SystemManager::windowWidth/2-157, SystemManager::windowHeight-17) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.GRAY_BAR, SystemManager::windowWidth/2-128,SystemManager::windowHeight-140) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BLUE_BAR, SystemManager::windowWidth/2-128,SystemManager::windowHeight-140) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.LIGHTNING_BAR, SystemManager::windowWidth/2+128,SystemManager::windowHeight-30) );
    initHudComponent -> setDisplay(true);
	result = ((HUDBar*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Bar" << endl;
        return false;
    } hudSelector.BAR = AddHudElement( initHudComponent );

	//Initialize bar2
	initHudComponent = bar2;
   // initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BAR_VALUE2, SystemManager::windowWidth/2-157, SystemManager::windowHeight-42) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.GRAY_BAR, SystemManager::windowWidth/2-128,SystemManager::windowHeight-165) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BLUE_BAR2, SystemManager::windowWidth/2-128,SystemManager::windowHeight-165) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.ROBOT_BAR, SystemManager::windowWidth/2+128,SystemManager::windowHeight-55) );
    initHudComponent -> setDisplay(true);
	result = ((HUDBar*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Bar" << endl;
        return false;
    } hudSelector.BAR2 = AddHudElement( initHudComponent );

	//Initialize bar3
	initHudComponent = bar3;
   // initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BAR_VALUE3, SystemManager::windowWidth/2-157, SystemManager::windowHeight-67) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.GRAY_BAR, SystemManager::windowWidth/2-128,SystemManager::windowHeight-190) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BLUE_BAR3, SystemManager::windowWidth/2-128,SystemManager::windowHeight-190) ) ;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.SPARK_BAR, SystemManager::windowWidth/2+128,SystemManager::windowHeight-80) );
	
    initHudComponent -> setDisplay(true);
	result = ((HUDBar*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Bar" << endl;
        return false;
    } hudSelector.BAR3 = AddHudElement( initHudComponent );

	//Initialize bar4
	initHudComponent = bar4;
   // initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BAR_VALUE4, SystemManager::windowWidth/2-157, 12) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.GRAY_BAR, SystemManager::windowWidth/2-128,-112) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.BLUE_BAR4, SystemManager::windowWidth/2-128,-112) ) ;
	//initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.SPARK_BAR, SystemManager::windowWidth/2+128,SystemManager::windowHeight-80) );
	
    initHudComponent -> setDisplay(true);
	result = ((HUDBar*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Bar" << endl;
        return false;
    } hudSelector.BAR4 = AddHudElement( initHudComponent );


	//Initialize counter
	initHudComponent = counter;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.COUNTER_VALUE, 15,SystemManager::windowHeight-15) );
    initHudComponent -> setDisplay(true);
	result = ((HUDCounter*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Field" << endl;
        return false;
    } hudSelector.COUNTER = AddHudElement( initHudComponent );

	//Initialize crosshairs
	initHudComponent = crosshairs;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.CROSSHAIRS, SystemManager::windowWidth/2-64,SystemManager::windowHeight/2-64-25) ) ;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.CROSSHAIRS2, SystemManager::windowWidth/2-128,SystemManager::windowHeight/2-128) ) ;
    initHudComponent -> setDisplay(false);
    initHudComponent->setAlphaCap(.5);
	result = ((HUDMenuItem*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Crosshairs" << endl;
        return false;
    } hudSelector.CROSSHAIRS = AddHudElement( initHudComponent );

	//Initialize crosshairs2
	initHudComponent = crosshairs2;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.CROSSHAIRS2,
                                                   SystemManager::windowWidth/2-128,SystemManager::windowHeight/2-128 ) ) ;
    initHudComponent -> setDisplay(false);
    initHudComponent->setAlphaCap(.5);
	result = ((HUDMenuItem*) initHudComponent)->Initialize();
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Crosshairs" << endl;
        return false;
    } hudSelector.CROSSHAIRS2 = AddHudElement( initHudComponent );

	//splash screen
	splash = initHudComponent = new HUDMenuItem;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.SPLASH, 0,0 ));
                                                                                   //SystemManager::windowWidth/2-512,0) );
    initHudComponent -> setDisplay(true);
    result = ((HUDMenuItem*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Splash1" << endl;
        return false;
    } hudSelector.SPLASH = AddHudElement( initHudComponent );

	//game over
	initHudComponent = lose;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.LOSE, SystemManager::windowWidth/2-384,SystemManager::windowHeight/3-84) );
    initHudComponent -> setDisplay(false);
    result = ((HUDMenuItem*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Splash1" << endl;
        return false;
    } hudSelector.LOSE = AddHudElement( initHudComponent );
	
	//win
	initHudComponent = win;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.WIN, SystemManager::windowWidth/2-384,SystemManager::windowHeight/3-84) );
    initHudComponent -> setDisplay(false);
    result = ((HUDMenuItem*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Splash1" << endl;
        return false;
    } hudSelector.WIN = AddHudElement( initHudComponent );

	// Start Button
	start = new HUDMenuButton;
	initHudComponent = start;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_GRAPHIC,    (int)(.75f * SystemManager::windowWidth),(int)(.75f * SystemManager::windowHeight) ) );
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.START_TEXT,      (int)(.75f * SystemManager::windowWidth) + 40,(int)(.75f * SystemManager::windowHeight) + 25 ) );
    ((HUDMenuButton*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_2) );
    initHudComponent -> setDisplay(true);
    result = ((HUDMenuButton*) initHudComponent)->Initialize();
    if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: start" << endl;
        return false;
    } hudSelector.START_BUTTON = AddHudElement( initHudComponent );

    int bdw, bdh;
    ((HUDGraphic*)GetHUDDisplayables().at(displayablesSelector.SPLASH))->GetBitmapDimensions(bdw, bdh);
	//Initialize field top
	initHudComponent = field;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_FIELD,  (int)(.24f * bdw),     (int)(.23f * bdh) ) );//SystemManager::windowWidth/2-512+250,245) ) ;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.FIELD_VALUE, (int)(.24f * bdw + 4), (int)(.23f * bdh + 8) ) );//SystemManager::windowWidth/2-512+4+250,245+8) );
    initHudComponent -> setDisplay(true);
	result = ((HUDField*) initHudComponent)->Initialize();
	field->setFocus(true);
	((HUDField*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_3) );
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Field" << endl;
        return false;
    } hudSelector.FIELD = AddHudElement( initHudComponent );

	//Initialize field2 bottom
	initHudComponent = field2;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_FIELD,   (int)(.24f * bdw),     (int)(.32 * bdh) ) );// SystemManager::windowWidth/2-512+250,245+104) ) ;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.FIELD_VALUE2, (int)(.24f * bdw + 4), (int)(.32 * bdh + 8) ) );// SystemManager::windowWidth/2-512+250+4,245+104+8) );
    initHudComponent -> setDisplay(true);
	result = ((HUDField*) initHudComponent)->Initialize();
	field2->setFocus(false);
	((HUDField*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_4) );
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Field2" << endl;
        return false;
    } hudSelector.FIELD2 = AddHudElement( initHudComponent );

	//Initialize field3
	initHudComponent = field3;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.TEST_FIELD, SystemManager::windowWidth/2-512+250,245+104) ) ;
    initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.FIELD_VALUE3, SystemManager::windowWidth/2-512+250+4,245+104+8) );
    initHudComponent -> setDisplay(false);
	result = ((HUDField*) initHudComponent)->Initialize();
	field3->setFocus(false);
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: Field3" << endl;
        return false;
    } hudSelector.FIELD3 = AddHudElement( initHudComponent );

	//Initialize focus
	initHudComponent = focus1;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.FOCUS, (int)(.24f * bdw), (int)(.23f * bdh) ) );
    initHudComponent -> setDisplay(true);
	result = ((HUDMenuItem*) initHudComponent)->Initialize();
	
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: focus1" << endl;
        return false;
	} hudSelector.FOCUS1 = AddHudElement( initHudComponent );

	//Initialize focus2
	initHudComponent = focus2;
	initHudComponent -> getItemConfig().push_back( HUDMenuItem::createHUDItemConfig( displayablesSelector.FOCUS, (int)(.24f * bdw), (int)(.32 * bdh) ) );
    initHudComponent -> setDisplay(false);
	result = ((HUDMenuItem*) initHudComponent)->Initialize();
	((HUDMenuButton*) initHudComponent) ->setOnClickFunction( &(HM_OCFXN_4) );
	if(!result)
    {
        cout << "[HUDMANAGER] FAIL: Could not initialize the HUDCOMPONENT: focus2" << endl;
        return false;
	} hudSelector.FOCUS2 = AddHudElement( initHudComponent );
/** END: INITIALIZE THE HUD COMPONENTS **************************************************************/

	//initial values
	getBar2()->setValue(0);
	getBar()->setValue(0);
	//CSE 131 hack
	getField2()->setValue((char *) &*(LoadingManager::serverIP.c_str()));

	//force some rendering to happen
	getField3()->setFocus(true);
	getField()->setFocus(false);
	InputManager * im = InputManager::Instance();
	for (int i = 0x2E; i <= 0x5A; i++) {
		im->KeyDown(i);
		im->KeyUp(i);
	}
	for (int i = 0x61; i <= 0x7A; i++){
		im->KeyDown(i);
		im->KeyUp(i);
	}
	im->KeyDown(0x20);
	im->KeyUp(0x20);
	im->KeyDown(0x2E);
	im->KeyUp(0x2E);
	getField3()->setFocus(false);
	getField()->setFocus(true);
	//getField3()->setValue("abcdefghijklmnopqrstuvwxyz. ABCDEFGHIJKLMNOPQRSTUVWXYZ");

	SoundManager* sound = SystemManager::Instance()->GetSoundManager();
	if (sound) {
	//	cerr << "hi" << endl;
		sound->PlayIntroLoop();
	}
	return true;
}

bool HUDManager::Render(D3DManager * d3dMan, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX orthoMatrix)
{
	
    for (unsigned int i = 0; i < HUDComponents.size(); ++i)
    {
        if( HUDComponents.at(i)->isToBeDisplayed() )
        {
            HUDComponents.at(i)->Render( d3dMan, worldMatrix, viewMatrix, orthoMatrix );
        }
    }
    return true;
}


int HUDManager::AddHudDisplayable(HUDDisplayable * newHUDElement)
{
    hudDisplayables.push_back(newHUDElement);
    return (hudDisplayables.size() - 1);
}
int HUDManager::AddHudElement(HUDMenuItem * newHUDElement)
{
    HUDComponents.push_back( newHUDElement );
    return (HUDComponents.size() - 1);
}

void HUDManager::SetShader( ShaderObject* shaderObj )
{
    shader_Ref = &(*shaderObj);
}
ShaderObject * HUDManager::GetShader()
{
    return shader_Ref;
}

bool HUDManager::DisplayPopUpMenu()
{
	displayingPopUpMenu = !displayingPopUpMenu;

	GetHUDComponent( GetHUDElementSelector().MENU_BUTTON_EXIT ).setDisplay(displayingPopUpMenu);
	GetHUDComponent( GetHUDElementSelector().MENU_BUTTON_SHADOWS).setDisplay(displayingPopUpMenu);
	return displayingPopUpMenu;
}
bool HUDManager::HudIsOn()
{
    return displayingPopUpMenu;
}

bool HUDManager::SplashIsOn()
{
    return displayingSplash;
}

void HUDManager::SplashOff()
{
    displayingSplash = false;
}

/***
 * When a mouse event occurs, check against the available HUD elements.  If a 
 * clickable component was intersected, use that components onClickEvent function,
 * and then return true;
 */
bool HUDManager::CheckClickEvent(float x, float y)
{
    bool found = false;
    for (unsigned int i = 0; i < HUDComponents.size(); ++i)
    {
        if( HUDComponents.at(i)->isClickable()  && HUDComponents.at(i)->isToBeDisplayed() )
        {
            //float _w, _h, _x, _y;
            //HUDComponents.at(i)->getBounds(_w,_h);
            //HUDComponents.at(i)->getPosition(_x,_y);

            if ( HUDComponents.at(i)->isInBounds(x,y) )
            {
                HUDComponents.at(i)->onClickEvent();
                found = true;
                break;
            }
        }
    }
    return found;
}

void HUDManager::Shutdown()
{
}