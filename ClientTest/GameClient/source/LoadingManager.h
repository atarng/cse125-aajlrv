//LoadingManager.h

#ifndef _LOADING_MANAGER_
#define _LOADING_MANAGER_

#include <string>

class LoadingManager
{
public:
	//declare variables here
	static bool			fullscreen;
	static std::string	applicationName;
	static int			windowHeight;
	static int			windowWidth;

    static std::string  shaderVersion;
    static bool         multiTextures;


//TestVariables for each type
#ifdef _DEBUG
	static std::string	teststring;
	static int			testint;
	static float		testfloat;
	static bool			testbool;
#endif

    static bool			holdOnExit;
    static bool         printFPS;
    static bool         printDbgMsgs;

    static std::string  testModelFile;
    static std::string  testLightVShader;
    static std::string  testLightPShader;

//// END CONFIG VARIABLES /////////////////////////////////////

	LoadingManager();
	LoadingManager(std::string loadingFileName);
	~LoadingManager();

	static bool Initialize(std::string loadingFileName);
};

#endif