#ifndef _PARTICLESYSTEMOBJECT_H_
#define _PARTICLESYSTEMOBJECT_H_

#include "D3DManager.h"
class ParticleSystem;

enum BillBoardType
{
    NONE=0,
    SYSTEM,
    PARTICLE
};

class ParticleSystemObject
{
private:
    D3DXVECTOR3 position;
    D3DXVECTOR3 rotation;
    D3DXVECTOR3 scale;

    ParticleSystem * particleSystem;
    //bool billBoarded;
    int billBoarded;
    float  lifeTime;
    bool expires;
public:
    ParticleSystemObject();
    ~ParticleSystemObject();

    inline ParticleSystem* GetParticleSystem() { return particleSystem; }
    void SetParticleSystem(ParticleSystem*);

    void GenerateOwnParticleSystem(int particleSystemType = 0);

    void Render(ID3D11DeviceContext* deviceContext);

    //void SetBillBoarded(bool);
    void SetBillBoarded(BillBoardType bbt);
    inline bool isBillBoarded(BillBoardType bbt) { return (billBoarded == bbt); };

    inline void SetRotation (D3DXVECTOR3 newRot)   { rotation = newRot; };
    inline void SetPosition (D3DXVECTOR3 newPos)   { position = newPos; };
    inline void SetScale    (D3DXVECTOR3 newScale) { scale = newScale; };

    inline D3DXVECTOR3 GetRotation()        { return rotation; }
    inline D3DXVECTOR3 GetPosition()        { return position; }
    inline D3DXVECTOR3 GetScale()           { return scale;    }

    void updateLifeTime();
    void SetLifeTime(float );                      
    inline bool isExpired ()                { return expires && (lifeTime <= 0.0f); };

    void setExpires(bool);
    inline bool Expires()                   { return expires; };
};

#endif