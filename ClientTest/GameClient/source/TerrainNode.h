//TerrainNode.h
#ifndef _TERRAINNODE_H_
#define _TERRAINNODE_H_

#include "D3DManager.h"
#include "SceneNode.h"
#include "Terrain.h"
#include "ShaderObject.h"

class TerrainNode : public SceneNode
{

protected:
    Terrain* terrain;
    ShaderObject* shader;


public:
    TerrainNode();
    TerrainNode(const TerrainNode &);
    ~TerrainNode();
    
    inline void SetTerrain(Terrain* newTerrain){terrain = newTerrain;}
    inline void SetShader(ShaderObject* newShader){shader = newShader;}

    inline Terrain* GetTerrain(){return terrain;}
    inline ShaderObject* GetShader(){return shader;}

    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);
    void RenderDepth(D3DManager* d3dMan);

    virtual bool isTerrainNode();

};

#endif