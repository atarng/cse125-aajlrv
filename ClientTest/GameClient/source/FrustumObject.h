////////////////////////////////////////////////////////////////////////////////
// Filename: FrustumObject.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _FRUSTUMOBJECT_H_
#define _FRUSTUMOBJECT_H_


//////////////
// INCLUDES //
//////////////
//#include <d3dx10math.h>
#include "D3DManager.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: FrustumObject
////////////////////////////////////////////////////////////////////////////////
class FrustumObject
{
private:
	D3DXPLANE m_planes[6];
    int objectsCulled;

public:
	FrustumObject();
	FrustumObject(const FrustumObject&);
	~FrustumObject();

	void ConstructFrustum(float, D3DXMATRIX, D3DXMATRIX);

	bool CheckPoint(float, float, float);
	bool CheckCube(float, float, float, float);
	bool CheckSphere(float, float, float, float);
	bool CheckRectangle(float, float, float, float, float, float);

    inline void incCulledObjects() { ++objectsCulled; };
    inline int getObjectsCulled() { return objectsCulled; };
};

#endif