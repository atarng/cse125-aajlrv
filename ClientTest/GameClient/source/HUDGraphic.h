#ifndef _HUDGRAPHIC_H_
#define _HUDGRAPHIC_H_

#include "D3DManager.h"
#include "HUDDisplayable.h"

class TextureObject;

/***
 * For Bitmap Elements currently...
 */

enum HUDPartitionType
{
    hpt_SQUARE = 0,
    hpt_CIRCLE
};

class HUDGraphic : public HUDDisplayable
{
private:
    struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	TextureObject* m_Texture;

	int m_screenWidth, m_screenHeight;
	int m_bitmapWidth, m_bitmapHeight;


    ID3D11Device * deviceRef;

	int m_previousWidth, m_InitWidth;

	int m_previousPosX, m_previousPosY; // not desired probably because it is going to be instanced
    float m_PartitionPositionX, m_PartitionPositionY;
    float m_partitionRadius;

    bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	bool UpdateBuffers(ID3D11DeviceContext*, int, int);
	void RenderBuffers(ID3D11DeviceContext*);

    HUDPartitionType m_HudPartitionType;

protected:


public:
    HUDGraphic();
    HUDGraphic(const HUDGraphic &);
    ~HUDGraphic();

    //bool Initialize(ID3D11Device*, int, int, char*, int, int);
    bool Initialize(ID3D11Device*, int, int, int, int);
	void Shutdown();
	bool Render(ID3D11DeviceContext*, int, int);

	void ChangeWidth(int);
    void ChangePartitionLocation(float, float);

    int GetIndexCount();
    TextureObject * GetTextureObject();
    void SetTextureObject(TextureObject&);

    void GetBitmapDimensions(int &, int &);
    void SetHUDPartitionType(HUDPartitionType hpt);
    inline HUDPartitionType GetHUDPartitionType(){ return m_HudPartitionType; }

    bool isGraphic();
    float m_partitionOffset;
};

#endif