/***
 * Filename:
 * Author: Roy Chen
 * Purpose:
 *
 * TODO:
 */
#include <iostream>
#include "HUDCounter.h"

#include "HUDGraphic.h"
#include "HUDText.h"

using namespace std;

HUDCounter::HUDCounter() {
	value = 0;
	textValue = new HUDText;
	
    sprintf_s(buffer,"%d",getValue());
}
HUDCounter::HUDCounter(int value2) {
	value = value2;
	textValue = new HUDText;
	
    sprintf_s(buffer,"%d",getValue());
}

char * HUDCounter::getBuffer() { return buffer; }

void HUDCounter::setValue(int number) {
	value = number;
	sprintf_s(buffer,"%d",getValue());
	textValue->ChangeText(buffer);
}