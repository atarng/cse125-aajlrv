/***
 * Filename: UnitObject.cpp
 * Contributors: Joey Ly, Alfred Tarng
 * Purpose: Wrapper Class for the Unit.cpp
 *
 * TODO: 
 */
#include "UnitObject.h"
#include "SystemManager.h"
#include "GraphicsManager.h"
#include "MeshManager.h"

#include "TerrainNode.h" // what???

/***
 * For Hard coded units.
 */
UnitObject::UnitObject()
{
    unit  = NULL;
    model = NULL;
    modelID = shaderID = 0;
    radius = 7;	

    position = D3DXVECTOR3(0,0,0);
    rotation = D3DXVECTOR3(0,0,0);
    scale    = D3DXVECTOR3(1,1,1);

    TINT = D3DXVECTOR4(1,1,1,1);
    tintAssigned   = false;

    isTester       = false;
    markForRemoval = false;

    currentAnimationFrame = rand() % 10 ;
}
/***
 * Creates a unit object based off a unit template
 */
UnitObject::UnitObject (Unit & unitRef)
{
    unit  = &unitRef;
    unitRef.GetIDs( modelID, shaderID );
    unitRef.GetRadius( radius );
    SetModel( modelID );
    //(Model*) &(SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMesh( modelID ));

    TINT = D3DXVECTOR4(1,1,1,1);
    tintAssigned = false;

    isTester = false;
    markForRemoval = false;

    currentAnimationFrame = 0;
	syncAnimState = -1;
}
UnitObject::UnitObject(const UnitObject& getUnit)
{
    UnitObject();
    this->unit = getUnit.unit;
}
UnitObject::~UnitObject()
{
}

void UnitObject::GetMatrix(D3DXMATRIX& matrix)
{
    UnitStateUpdate();

    D3DXVECTOR3 m_pos, up, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	m_pos = position;

    up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
    lookAt= D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	pitch = rotation.x * 0.0174532925f;
	yaw   = rotation.y * 0.0174532925f;
	roll  = rotation.z * 0.0174532925f;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
    
	lookAt = m_pos + lookAt;
	D3DXMatrixLookAtLH(&matrix, &m_pos, &lookAt, &up);    
}

void UnitObject::Render(ID3D11DeviceContext* deviceContext)
{
    model->Render( deviceContext, &currentAnimationFrame );
}

/***
 * unsetUnit()
 * Unattaches itself from the unit object, and marks self for removal from scenegraph.
 */
void UnitObject::unsetUnit()
{
    this->unit     = NULL;
    markForRemoval = true;
}

/***
 *
 */
void UnitObject::assignTint(float r,float g,float b) {
    TINT = D3DXVECTOR4(r,g,b,1);
    tintAssigned = true;
}

/***
 * GetUnitHealth()
 *
 */
float UnitObject::GetUnitHealth()
{
	if (unit != NULL) return (float) unit->getHealth();
	else return 0.f;
}

float UnitObject::GetUnitMaxHealth()
{
	if (unit != NULL) return (float) unit->getMaxHealth();
	else return 0.f;
}
/***
 *
 */
float UnitObject::GetUnitCharge()
{
	if (unit != NULL ) return unit->getCurrentCharge();
	else return 0.f;
}
/**
 * Returns the unit id.
 */
int UnitObject::GetUnitId() {
    if (unit != NULL) return unit->getUnitId();
    else              return -1;
}
int UnitObject::GetUnitAnimState()
{
    if (unit != NULL) return unit->getCurrentAnimType();
    else              return syncAnimState;
}
/***
 * bool UnitStateUpdate();
 * 
 * Updates object based on unit. Returns true (for camera?) if it is not the unit position.
 */
bool UnitObject::UnitStateUpdate()
{
    bool result = false;
    float a,b,c;
    if (isTester){
    } else if ( unit )
    {
        unit->GetPosition(a,b,c);

        if(position != D3DXVECTOR3(a,b,c)) result = true;
        position = D3DXVECTOR3(a,b,c);
    
        unit->GetRotation(a,b,c);
        rotation = D3DXVECTOR3(a,b,c);

        unit->GetScale(a,b,c);
        scale = D3DXVECTOR3(a,b,c);

		if ( modelID != SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().ELECTRICSPARK
	      && modelID != SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TABLET
		  && modelID != SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TABLET2)
			unit->GetRadius(radius);
    }else
    {
        markForRemoval = true;
    }
    return result;
}
void UnitObject::SetModel(int modelID)
{
    this->modelID = modelID;
    model = (Model*) &(SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMesh( modelID ));
}
