// PVector3.cpp

#include "PVector3.h"
#include <math.h>

//Modeled after the d3d10math header
PVector3::PVector3(){
}
PVector3::~PVector3(){
}
/////////////////////////////////////////////
// Assignment operators
/////////////////////////////////////////////
void PVector3::operator += ( const PVector3& other ){
    x += other.x;
    y += other.y;
    z += other.z;        
}
void PVector3::operator -= ( const PVector3& other ){
    x -= other.x;
    y -= other.y;
    z -= other.z;
}
void PVector3::operator *= ( const PVector3& other ){ // point-wise multiplication
    x *= other.x;
    y *= other.y;
    z *= other.z;

}
void PVector3::operator /= ( const PVector3& other ){ // point-wise division 
    x /= (other.x != 0)?other.x:1;
    y /= (other.y != 0)?other.y:1;
    z /= (other.z != 0)?other.z:1;

}
void PVector3::operator *= ( float scalar ){
    x *= scalar;
    y *= scalar;
    z *= scalar;        
}
void PVector3::operator /= ( float scalar ){
    if(scalar != 0.0f){
        float inv = 1.0f/scalar; // one division is better than doing 4 division
        x*=inv;
        y*=inv;
        z*=inv;
    }
}

/////////////////////////////////////////////
// Basic Arithmatic
/////////////////////////////////////////////
PVector3 PVector3::operator + ( const PVector3& other ){
    return PVector3(x+other.x, y+other.y, z+other.z);
}
PVector3 PVector3::operator - ( const PVector3& other ){
    return PVector3(x-other.x, y-other.y, z-other.z);
}
PVector3 PVector3::operator * ( float scalar ){
    return PVector3(x*scalar, y*scalar, z*scalar);
}
PVector3 PVector3::operator / ( float scalar ){
    if(scalar != 0.0f){
        float inv = 1.0f/scalar; // one division is better than doing 4 division
        return PVector3(x*inv, y*inv, z*inv);
    }
    // need to return an error message
    // if the scalar is 0 then we do nothing to the vector
    return PVector3(x,y,z);
}
PVector3 PVector3::operator-(){
    return PVector3(-x,-y,-z);
}

/////////////////////////////////////////////
//Boolean Operators
/////////////////////////////////////////////
bool PVector3::operator == ( const PVector3& other ) const{
    return ( x==other.x && y==other.y && z==other.z );
}
bool PVector3::operator != ( const PVector3& other ) const{
    return ( x!=other.x && y!=other.y && z!=other.z );
}

/////////////////////////////////////////////
//USEFUL STUFF
/////////////////////////////////////////////
void PVector3::normalize(){
    float length2 = this->length2();
    *this /= length2;        
}

float PVector3::length(){
    float temp = this->length2();
    if(temp > 0)
        return sqrt(temp);
    return 0;
}
float PVector3::length2(){
    return this->dot(*this);
}

float PVector3::dot(PVector3& other){        
    return x*other.x + y*other.y + z*other.z ;
}

PVector3 PVector3::cross(PVector3& other){        
    return PVector3(
        y*other.z - z*other.y,
        z*other.x - x*other.z,
        x*other.y - y*other.x
        );
}



std::string PVector3::toString() const{
    std::stringstream out;
    out << '(' << x << ',' << y << ',' << z << ')';
    return out.str();
}