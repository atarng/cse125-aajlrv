#ifndef _HUD_FIELD_H_
#define _HUD_FIELD_H_

#include "HUDMenuItem.h"
#include "HUDText.h"
#include "HUDGraphic.h"

#include <vector>

class HUDField : public HUDMenuItem
{
protected:
    typedef HUDMenuItem super;

    HUDText * textValue;
	HUDText * label;
	HUDGraphic * background;
	bool focus;
    char buffer[100];
	int max;

public:
	HUDField(int);
	HUDField(char *, int);
	inline char * getValue() {return buffer; } ;
	void setValue(char *);
	bool appendValue(char);

	void setFocus(bool);

	inline HUDText * getText() {return textValue;};
	inline HUDText * getLabel() {return label;};
	inline HUDGraphic * getBack() {return background;};
	inline bool hasFocus() {return focus;};
	bool backspace();

};

#endif