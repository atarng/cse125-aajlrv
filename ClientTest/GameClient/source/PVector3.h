//PVector3.h

#ifndef _PVECTOR3_H_
#define _PVECTOR3_H_
#include <string>
#include <sstream>
#include <iostream>

class PVector3
{
public:
    float x,y,z;

    PVector3();
    PVector3(const PVector3& rhs){x=rhs.x; y=rhs.y; z=rhs.z;}
    PVector3(float rx, float ry, float rz){x=rx; y=ry; z=rz;}
    ~PVector3();
    
    void operator += ( const PVector3& );
    void operator -= ( const PVector3& );
    void operator *= ( const PVector3& );
    void operator /= ( const PVector3& );
    void operator *= ( float );
    void operator /= ( float );
    
    PVector3 operator + ( const PVector3& );
    PVector3 operator - ( const PVector3& );
    PVector3 operator * ( float );
    PVector3 operator / ( float );
    PVector3 operator - ();

    bool operator == ( const PVector3& ) const;
    bool operator != ( const PVector3& ) const;

    void normalize();

    float length();
    float length2();

    float dot( PVector3& );
    PVector3 cross( PVector3& );

    std::string PVector3::toString() const; 

};
#endif 