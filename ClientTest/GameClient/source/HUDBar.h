#ifndef _HUD_BAR_H_
#define _HUD_BAR_H_

#include "HUDCounter.h"
#include "HUDDisplayable.h"
#include "HUDText.h"
#include "HUDGraphic.h"

#include <vector>

// TODO: INHERIT FROM MENU ITEM NOT BUTTON.
class HUDBar : public HUDCounter
{
private:
    typedef HUDCounter super;

    int width;
    HUDGraphic * blueBar;
	int hb_max;

public:
	HUDBar(int, int);
	HUDBar(int, int, int);

	inline HUDGraphic * getBlue() {return blueBar;};

	int getRight();
	void decValue(int);
	void incValue(int);
	void setValue(int);

	void setMax(int);
};

#endif