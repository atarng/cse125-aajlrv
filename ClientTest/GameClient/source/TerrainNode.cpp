//TerrainNode.cpp
#include "TerrainNode.h"

#include "SystemManager.h"
#include "GraphicsManager.h"
#include "LightObject.h"

#include <iostream>

TerrainNode::TerrainNode()
{
    terrain = NULL;
    nodeID = 0;
}
TerrainNode::TerrainNode(const TerrainNode &)
{
}
TerrainNode::~TerrainNode()
{
    delete terrain;
    terrain = NULL;
}

void TerrainNode::UpdateMatrix(){
    terrain->GetMatrix(matrix);
}

void TerrainNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    float pitch, yaw ,roll;
    UpdateMatrix();
    stack->Push();

        D3DXVECTOR3 scale = terrain->GetScale();
        D3DXVECTOR3 rotation = terrain->GetRotation();
        D3DXVECTOR3 translation = terrain->GetPosition();

        pitch = rotation.x * 0.0174532925f;
	    yaw   = rotation.y * 0.0174532925f;
	    roll  = rotation.z * 0.0174532925f;

        stack->ScaleLocal(scale.x, scale.y, scale.z);
        stack->RotateYawPitchRollLocal( yaw, pitch, roll);
        stack->Translate(translation.x, translation.y, translation.z);

        SetMatrix( *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop()) );

        std::vector<SceneNode *>::iterator it;
        for( it = children.begin(); it < children.end(); it++ )
        {
            if (!((*it)->GetID() < 0)) (*it)->Draw( d3dMan, stack , drawMode );
        }

        if      (drawMode == 0) Render(d3dMan);
        else if (drawMode == 1) RenderDepth(d3dMan);
    
    stack->Pop();
}
void TerrainNode::Render(D3DManager* d3dMan)
{
    terrain->Render( d3dMan->GetDeviceContext() );
    GraphicsManager* gPtr = SystemManager::Instance()->GetGraphicsManager();
    ShaderManager * shaderManPtr = gPtr->GetShaderManager();
    shader =  ( shaderManPtr -> IsUsingShadows() ) ? &shaderManPtr->GetShader( shaderManPtr->GetShaderSelector().TEST_SHADOWSHADER ) : 
                                                     &shaderManPtr->GetShader( shaderManPtr->GetShaderSelector().COLOR_SHADER_0    ) ;

    if (shader)
    {
        D3DXMATRIX camMatrix;
        D3DXMATRIX mat = matrix;

        gPtr->GetCamera()->GetMatrix(camMatrix);
        LightObject* light_Ptr = gPtr->GetLightManager()->getLight(0);

        ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
        myRSC.lightMan = gPtr->GetLightManager();

        if( shader->isShadowed() )
        {
          D3DXMATRIX lvm, lpm;
          myRSC.lightViewMatrix = &lvm;
          myRSC.lightProjectionMatrix = &lpm;
          light_Ptr->GetViewMatrix(*myRSC.lightViewMatrix);
          light_Ptr->GetProjectionMatrix(*myRSC.lightProjectionMatrix);
          myRSC.depthResource = SystemManager::Instance()->GetGraphicsManager()->GetRenderTexture()->GetShaderResourceView();
        }else if(shader->isReflective())
        {
          myRSC.reflectionMatrix = &gPtr->GetCamera()->GetReflectionViewMatrix();
          myRSC.reflectionResource = gPtr->GetRenderTexture2()->GetShaderResourceView();
        }else
        {
          D3DXVECTOR4 tc =D3DXVECTOR4(1.0f,1.0f,1.0f, 0.0f);
          myRSC.tintColor = &tc;
        }
        shader->Render( d3dMan -> GetDeviceContext(), terrain->GetIndexCount(), 0,
                        mat,camMatrix, d3dMan -> GetProjectionMatrix(), 
                        terrain->GetTextureObject()->GetTextures(), terrain->GetTextureObject()->numTextures(),
                        myRSC
                      );
        if( shader->isShadowed() )
        {
            shader->Render( d3dMan -> GetDeviceContext(), 0, 0,
                            mat,camMatrix, d3dMan -> GetProjectionMatrix(), 
                            terrain->GetTextureObject()->GetTextures(), terrain->GetTextureObject()->numTextures(),
                            myRSC
                          );
        }
    }
}
void TerrainNode::RenderDepth(D3DManager* d3dMan)
{
    D3DXMATRIX lightViewMatrix, lightProjectionMatrix;

    LightObject* light_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetLightManager()->getLight(0);
    ShaderObject * depthShader = SystemManager::Instance()->GetGraphicsManager()->GetDepthShader();

    D3DXMATRIX mat = matrix;

    light_Ptr -> GetViewMatrix(lightViewMatrix);
    light_Ptr -> GetProjectionMatrix(lightProjectionMatrix);

    terrain->Render( d3dMan->GetDeviceContext() );

    ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
    D3DXMATRIX identity; D3DXMatrixIdentity(&identity);

    depthShader->Render( d3dMan -> GetDeviceContext(), terrain->GetIndexCount(), 0,
                         mat, lightViewMatrix, lightProjectionMatrix, 
                         NULL, 0,
                         myRSC
                       );
}

bool TerrainNode::isTerrainNode(){ return true; }