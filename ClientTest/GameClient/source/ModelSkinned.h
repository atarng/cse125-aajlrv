////////////////////////////////////////////////////////////////////////////////
// Filename: Model.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODELSKINNED_H_
#define _MODELSKINNED_H_
// Let's assume our entire model will have 10 bones. Let's assume each vertex will have 2 bones
//////////////
// INCLUDES //
//////////////

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "Model.h"

using namespace std;

class ModelSkinned : public Model
{
private:
	struct VertexType_Animated // "Type"?
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;

        D3DXVECTOR3 tangent;  //
		D3DXVECTOR3 binormal; //

        D3DXVECTOR2 weight; // weight on each bone
        D3DXVECTOR2 indices;   // bone/offsetindex
	};
    struct ModelType      // "Type"?
	{
		float x, y, z;    // position
		float tu, tv;     // uv
		float nx, ny, nz; // normals

        float tx, ty, tz; // tangent position
        float bx, by, bz; // normalmap position

        float weight0, weight1;
        float index0, index1;

        bool zeroSet;
	};
    struct MultMeshObject
    {
        ModelType* meshObj;
        int numVerts;

        D3DXMATRIX  modelRelativeMatrix;  //
        D3DXMATRIX  animationMatrix;

        D3DXMATRIX  boneAnimMatrix[10];

        aiBone * bones;
        int numBones;
    };

//////////// For bump map... I believe....
    struct TempVertexType
    {
        float x, y, z;
        float tu, tv;
        float nx, ny, nz;
    };
    struct VectorType
    {
        float x, y, z;
    };

    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	ModelType* m_model;
 // ModelType** m_meshes; // TODO: TO BE REFACTORED?
////////////////////////////
    int numMeshes;

    TextureObject* m_Textures;

    bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();

	void RenderBuffers(ID3D11DeviceContext*);

	bool LoadModel(char*, int);
	void ReleaseModel();

// ASSIMPLOADER
    const aiScene * scene;
    AnimEvaluator * animEvaluator;

public:
	ModelSkinned();
	ModelSkinned(const ModelSkinned&);
	~ModelSkinned();

    virtual bool Initialize(ID3D11Device*, char* mesh, int);
	virtual void Shutdown();

	virtual void Render(ID3D11DeviceContext*);    

	virtual int GetIndexCount();
    virtual int GetNumMeshes();
    virtual TextureObject* GetTextureObject();
    virtual void SetTextureObject(TextureObject &);

    virtual bool isBonedModel();

    MultMeshObject * multMeshPtr;

};
#endif