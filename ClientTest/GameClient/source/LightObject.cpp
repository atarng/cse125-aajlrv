////////////////////////////////////////////////////////////////////////////////
// Filename: LightObject.cpp
////////////////////////////////////////////////////////////////////////////////
#include "LightObject.h"

LightObject::LightObject()
{
}
LightObject::LightObject(const LightObject& other)
{
}
LightObject::~LightObject()
{
}

void LightObject::SetAmbientColor(float red, float green, float blue, float alpha) { m_ambientColor = D3DXVECTOR4(red, green, blue, alpha); }
void LightObject::SetDiffuseColor(float red, float green, float blue, float alpha) { m_diffuseColor = D3DXVECTOR4(red, green, blue, alpha); }
void LightObject::SetSpecularColor(float red, float green, float blue, float alpha) { m_specularColor = D3DXVECTOR4(red, green, blue, alpha); }
void LightObject::SetSpecularPower(float power) { m_specularPower = power; }

void LightObject::SetPosition(float x, float y, float z) { m_position = D3DXVECTOR3(x, y, z); }
void LightObject::SetDirection(float x, float y, float z) { m_direction = D3DXVECTOR3(x, y, z); }

D3DXVECTOR4 LightObject::GetAmbientColor()  { return m_ambientColor; }
D3DXVECTOR4 LightObject::GetDiffuseColor()  { return m_diffuseColor; }
D3DXVECTOR4 LightObject::GetSpecularColor() { return m_specularColor; }
float       LightObject::GetSpecularPower() { return m_specularPower; }
D3DXVECTOR3 LightObject::GetPosition()      { return m_position; }
D3DXVECTOR3 LightObject::GetDirection()     { return m_direction; }

void LightObject::GenerateViewMatrix()
{
	D3DXVECTOR3 up, derp;

	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	D3DXVECTOR3 direction = m_lookAt - m_position;
	D3DXVec3Normalize (&direction, &direction);
	
	D3DXVec3Cross(&derp, &direction, &up);
	if(D3DXVec3Dot(&derp, &derp) < 0.0001){
		up.x = 1.0f;
		up.y = 0.0f;
		up.z = 0.0f;
		D3DXVec3Cross(&derp, &direction, &up);
	}
	D3DXVec3Normalize (&derp, &derp);

	D3DXVec3Cross(&up, &derp, &direction);
	D3DXVec3Normalize (&up, &up);


	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_position, &m_lookAt, &up); // $$$
	
	return;
}
void LightObject::GenerateProjectionMatrix(float screenDepth, float screenNear)
{
	float fieldOfView, screenAspect;

	// Setup field of view and screen aspect for a square light source.
	//fieldOfView = (float)D3DX_PI / 1.1f;
    fieldOfView = (float)D3DX_PI / 2.0f;
	screenAspect = 1.0f;

	// Create the projection matrix for the light.
	D3DXMatrixPerspectiveFovLH(&m_projectionMatrix, fieldOfView, screenAspect, screenNear, screenDepth); // $$$

	return;
}

/***
 *
 */
void LightObject::GetMatrix(D3DXMATRIX& matrix)
{
    D3DXMATRIX rotationMatrix;
    D3DXVECTOR3 lookAt= D3DXVECTOR3(0.0f, 0.0f, 1.0f),
                up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
    FLOAT   pitch = m_direction.x * 0.0174532925f,
	        yaw   = m_direction.y * 0.0174532925f,
	        roll  = m_direction.z * 0.0174532925f;
        
	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.    
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	lookAt = m_position + lookAt;

    D3DXMatrixLookAtRH( &matrix,
                        &m_position,
                        &lookAt,
                        &up);
}