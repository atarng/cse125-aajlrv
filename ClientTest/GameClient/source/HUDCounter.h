#ifndef _HUD_COUNTER_H_
#define _HUD_COUNTER_H_

#include <vector>

#include "HUDMenuItem.h"

class HUDGraphic;
class HUDText;

// TODO: INHERIT FROM MENU ITEM NOT BUTTON.
class HUDCounter : public HUDMenuItem
{
protected:
    typedef HUDMenuItem super;

    int value;
	//int offset;
    HUDText * textValue;
	HUDText * label;
    char buffer[32];

public:
	HUDCounter();
	HUDCounter(int);
	inline int getValue() {return value; } ;
	void setValue(int);

	inline HUDText * getText() {return textValue;};
	inline HUDText * getLabel() {return label;};

	//void incOffset(int);
	char * getBuffer();


};

#endif