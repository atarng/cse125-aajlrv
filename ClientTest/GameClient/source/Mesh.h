//Mesh.h
#ifndef _MESH_H_
#define _MESH_H_

#include "D3DManager.h"
#include "TextureObject.h"
enum MeshType
{
    TEXT,
    OTHER
};

class Mesh{
//private:
//    struct VertexType // "Type"?
//	{
//		D3DXVECTOR3 position;
//	    D3DXVECTOR2 texture;
//		D3DXVECTOR3 normal;
//	};
//
//    struct ModelType   // "Type"?
//	{
//		float x, y, z; // Not arrays?
//		float tu, tv;
//		float nx, ny, nz;
//	};
//
//    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer; // shared component with model object.
//    int m_vertexCount, m_indexCount;       
//    TextureObject* m_Texture;
//
//    virtual bool InitializeBuffers(ID3D11Device*)=0;
//    virtual void ShutdownBuffers()=0;
//    virtual void RenderBuffers(ID3D11DeviceContext*)=0;

public:
	Mesh();
	Mesh(const Mesh& );
	~Mesh();

    virtual bool isModel();
    virtual bool isBonedModel();
    virtual bool isTerrain();
};

#endif