//UnitObject.h
#ifndef _UNITOBJECT_H_
#define _UNITOBJECT_H_

#include "D3DManager.h"
#include "Model.h"
#include <Unit.h>

class UnitObject
{
private:
    Unit * unit;
    Model* model;

    bool markForRemoval, tintAssigned, isTester;

    int modelID, shaderID;
    float radius;

    double currentAnimationFrame;
	int    syncAnimState;

    D3DXVECTOR3 position;
    D3DXVECTOR3 rotation;
    D3DXVECTOR3 scale;

//  MOVE THIS ONTO UNIT OBJECT?
    D3DXVECTOR4 TINT;  // initialized

public:
	UnitObject();
    UnitObject(Unit &);
	UnitObject(const UnitObject&);
	~UnitObject();

	// Transformation Setters
    inline void SetRotation(D3DXVECTOR3 newRot)    { rotation = newRot; }
    inline void SetPosition(D3DXVECTOR3 newPos)    { position = newPos; }
    inline void SetScale(D3DXVECTOR3 newScale)     { scale = newScale; }
    inline void SetModel(Model* newModel)          { model = newModel; }
    void SetModel(int modelID);
    inline void SetBoundingRadius(float newRadius) { radius = newRadius; }

	// Transformation Getters
    inline D3DXVECTOR3 GetRotation()        { return rotation; }
    inline D3DXVECTOR3 GetPosition()        { return position; }
    inline D3DXVECTOR3 GetScale()           { return scale; }
    inline D3DXVECTOR4 * GetTint()          { return &TINT; }
    inline Model* GetModel()                { return model; }
    inline float GetBoundingRadius()        { return radius; }

    inline int GetShaderID() { return shaderID; };
    inline int GetModelID()  { return modelID; };
    inline bool isTinted()   { return tintAssigned; };
	float GetUnitHealth();
	float GetUnitMaxHealth();
	float GetUnitCharge();
    int GetUnitId();
    int GetUnitAnimState();

	inline void SyncAnimState(int sas){ syncAnimState = sas; };
   
    void GetMatrix(D3DXMATRIX& matrix);
    bool UnitStateUpdate();
    void Render(ID3D11DeviceContext* deviceContext);

    void assignTint(float, float, float);
    inline void resetAnimationCounter(){ currentAnimationFrame = 0; };

    void unsetUnit();
    inline bool isToBeRemoved(){ return markForRemoval; };

    // For things Instantiated from client.
    inline void setAsTester()  { isTester = true; };
};

#endif