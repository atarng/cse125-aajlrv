//CameraManager.cpp
#include "CameraManager.h"
CameraManager::CameraManager()
{
}
//CameraManager::CameraManager(const CameraManager &)
//{
//}
CameraManager::~CameraManager()
{
}

void CameraManager::Initialize()
{
    //Used to make sure we have a camera
    currentCamera = new CameraObject();
    addCamera(*currentCamera); 
}

int CameraManager::addCamera(CameraObject& addCamera)
{
    cameraObjects.push_back(&addCamera);

    return (cameraObjects.size() - 1);
}
CameraObject& CameraManager::getCamera(int index)
{
    return *cameraObjects.at(index);
}