//ProjectileNode.cpp

#include "SystemManager.h"
#include "GraphicsManager.h"
#include "LightObject.h"
#include "ProjectileNode.h"
#include "ModelSkinned.h"

#include <iostream>

ProjectileNode::ProjectileNode()
{
    unitObject = NULL;
    shader = NULL;
}
ProjectileNode::ProjectileNode(const ProjectileNode &)
{
}
ProjectileNode::~ProjectileNode()
{
    delete unitObject;
    unitObject = NULL;
}

void ProjectileNode::SetUnitObject(UnitObject * newUnit){ unitObject = newUnit; }
void ProjectileNode::UpdateMatrix(){
    unitObject->GetMatrix(matrix);
}

void ProjectileNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    UpdateMatrix();
    stack->Push();
        D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());
        D3DXVECTOR3 pos = unitObject->GetPosition();
        D3DXVec3TransformCoord(&pos, &pos, &mat);

        D3DXVECTOR3 scale       = unitObject->GetScale();
        D3DXVECTOR3 rotation    = unitObject->GetRotation();
        D3DXVECTOR3 translation = unitObject->GetPosition();

        float pitch, yaw ,roll;

        pitch = rotation.x * 0.0174532925f;
	    yaw   = rotation.y * 0.0174532925f;
	    roll  = rotation.z * 0.0174532925f;

        stack->ScaleLocal(scale.x, scale.y, scale.z);
        stack->RotateYawPitchRollLocal( yaw, pitch, roll);
        stack->Translate(translation.x, translation.y, translation.z);

        FrustumObject * frustumObject_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetFrustumObject();
        float boundingR  = unitObject->GetBoundingRadius();
        bool renderModel = frustumObject_Ptr->CheckSphere(pos.x, pos.y, pos.z, boundingR);

        if( renderModel && drawMode == 0)
        {
            Render(d3dMan);
        }else
        {
            frustumObject_Ptr->incCulledObjects();
        }

        std::vector<SceneNode *>::iterator it;
        for( it = children.begin(); it < children.end(); it++ )
        {
            (*it)->Draw( d3dMan, stack, drawMode );
        }

    stack->Pop();
}

void ProjectileNode::Render(D3DManager* d3dMan)
{
    /*
    unitObject->Render( d3dMan->GetDeviceContext() );

    if (shader)
    {
        D3DXMATRIX camMatrix;
        SystemManager::Instance()->GetGraphicsManager()->GetCamera()->GetMatrix(camMatrix);
        LightObject* light_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetLightManager()->getLight(0);
        D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());

        ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
        myRSC.lightMan = SystemManager::Instance()->GetGraphicsManager()->GetLightManager();
        myRSC.camObj   = SystemManager::Instance()->GetGraphicsManager()->GetCamera();

        if ( unitObject->GetModel()->hasAnimations )
        {// transform mat with animation Matrix
            if (unitObject->GetModel()->hasSkeletalRig())
            {
                for ( int i = 0; i < 10; ++i )
                {// POPULATE BONE MATRIX
                    if ( ( (i < ((ModelSkinned*) (unitObject->GetModel()) ) -> multMeshPtr[0].numBones ) ))
                    {
                        //std::cout << "Populate BoneMatrixIndex: " << i << std::endl;
                        myRSC.boneMatrices.push_back(((ModelSkinned*) ( unitObject->GetModel()))->multMeshPtr[0].boneAnimMatrix[i]);
                    } else
                    {
                        //std::cout << "AutoFillZero BoneMatrixIndex: " << i << std::endl;
                        D3DXMATRIX idMatrix;
                        D3DXMatrixIdentity(&idMatrix);
                        myRSC.boneMatrices.push_back(idMatrix);
                    }
                }

                shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->GetIndexCount(), 0,
                                mat, camMatrix, d3dMan -> GetProjectionMatrix(),
                                //mat, camMatrix, d3dMan -> GetProjectionMatrix(),
                                unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                myRSC
                              );

            } else
            {//
                if(unitObject->GetModel()->isBonedModel())
                {
                    std::cout << "WHY YOU ENTER HERE?!!!: "  << std::endl;
                }

                for (int i = 0; i < unitObject->GetModel()->GetNumMeshes(); ++i)
                {
                    shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->multMeshPtr[i].numVerts, (i > 0) ? unitObject->GetModel()->multMeshPtr[i-1].numVerts : 0,
                                    unitObject->GetModel()->multMeshPtr[i].animationMatrix * mat, camMatrix, d3dMan -> GetProjectionMatrix(),
                                    unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                    myRSC
                                  );
                }
            }
        }else
        {// Non Animated Mesh
            if(unitObject->GetModel()->isBonedModel())
            {
                std::cout << "No. Just NO."  << std::endl;
            }
            shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->GetIndexCount(), 0,
                mat, camMatrix, d3dMan -> GetProjectionMatrix(), 
                unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                myRSC
                );
        }
    }
    */
}