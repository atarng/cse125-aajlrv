#ifndef _SHADERCLASS_H_
#define _SHADERCLASS_H_

//#include "D3DManager.h"
#include "LightManager.h"
#include "CameraObject.h"

#include <vector>

// FORWARD DECLARATIONS
class D3DManager;

class ShaderObject
{
public:
    struct RENDERSHADERCONFIG;
private:
    // Light Reference
    // Texture Reference
    // Vertices to render
/// VS-BUFFERS //////////////////////////////////
    struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};
    // $$$
        struct MatrixBufferType_Append0
        {
    	    D3DXMATRIX lightView;
		    D3DXMATRIX lightProjection;
        };
        struct MatrixBufferType_Append1
        {
    	    D3DXMATRIX animationMatrix;
        };
	struct BoneBufferType
	{
		D3DXMATRIX bones [10];
	};
	struct ParticleTransformBufferType
	{
		D3DXMATRIX particleBillBoard;
	};
    //RRR
    struct ReflectionBufferType
	{
		D3DXMATRIX reflectionMatrix;
	};
    // $$$
    struct CameraBufferType
	{
		D3DXVECTOR3 cameraPosition;
		float padding;
	};
    //$$$
      struct LightBufferType2
	  {
	    D3DXVECTOR3 lightPosition;
		float padding;
	  };
    //$$$
/// PS-BUFFERS //////////////////////////////////
    struct LightBufferType
	{
        D3DXVECTOR4 ambientColor;
		D3DXVECTOR4 diffuseColor;
    //}; //
    //struct LightBufferType1 //
    //{ //
        D3DXVECTOR3 lightDirection; //
        float specularPower;        //
		D3DXVECTOR4 specularColor;  //
    };

    struct PixelBufferType
	{
		D3DXVECTOR4 pixelColor;
		float alphaCap;
		float padding, p2,p3;
	};
    struct SHADERCONFIG
    {
        char * SemanticName;
        DXGI_FORMAT    Format;
        int SemanticIndex;
        int AlignedByteOffset;
    };
    /***
     * not used yet..
     */
    struct SAMPLERDESCCONFIG
    {
        D3D11_FILTER Filter;
        D3D11_TEXTURE_ADDRESS_MODE uAddr,vAddr,wAddr;

    };

    ID3D11VertexShader* m_vertexShader; // C
	ID3D11PixelShader * m_pixelShader;  // C

	ID3D11InputLayout * m_layout;       // C
	ID3D11SamplerState* m_sampleState;  // C Sampler State
    std::vector<ID3D11SamplerState*> m_sampleStates; // $$$

    // BUFFERS used in shader files...
	ID3D11Buffer* m_matrixBuffer;   // C
    ID3D11Buffer* m_matrixBuffer2;  // $$$
    ID3D11Buffer* m_animBuffer;   // C
    ID3D11Buffer* m_boneBuffer;     // C
	ID3D11Buffer* m_particleBuffer; //
	ID3D11Buffer* m_lightBuffer;    // C
    ID3D11Buffer* m_lightBuffer2;   // C
    ID3D11Buffer* m_cameraBuffer;   // C
    ID3D11Buffer* m_pixelBuffer;    // C
    ID3D11Buffer* m_reflectionBuffer; // RRR

    bool hasShadows, givesReflection;

    bool InitializeShader(ID3D11Device*, HWND, char*, char*, char*, char*);
    void ShutdownShader();

    bool SetShaderParameters( ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX,
                              ID3D11ShaderResourceView**, int,
                              RENDERSHADERCONFIG
                             );

    void RenderShader(ID3D11DeviceContext*, int, int);

protected:

public:
    struct RENDERSHADERCONFIG
    {
        LightManager* lightMan;
        CameraObject* camObj;

        D3DXMATRIX* lightViewMatrix, * lightProjectionMatrix; // $$$
        D3DXMATRIX* reflectionMatrix; // RRR

        ID3D11ShaderResourceView* depthResource;
        ID3D11ShaderResourceView* reflectionResource; //RRR

		float alphaCap;
		D3DXVECTOR4*  tintColor;

		D3DXMATRIX*    particleMatrix;
        D3DXMATRIX*   animationMatrix; // $$$

        //std::vector <D3DXMATRIX> boneMatrices; // ### WARNING NOT KNOWN AT COMPILE TIME MAYBE???
    };

    ShaderObject();
	ShaderObject(const ShaderObject&);
	~ShaderObject();

    bool Initialize(ID3D11Device*, HWND, char*, char*, char *, char*);
    void Shutdown();
    bool Render(    ID3D11DeviceContext* deviceContext, int indexCount, int offset,
                    D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
                    ID3D11ShaderResourceView** textures, int numTextures,
                    RENDERSHADERCONFIG
                    );

    std::vector<SHADERCONFIG> shaderConfigVector;
    std::vector<SAMPLERDESCCONFIG> samplerDescConfigVector;

    //static SHADERCONFIG createShaderConfig(char*, DXGI_FORMAT, int, int);
    static SHADERCONFIG createShaderConfig(char*, DXGI_FORMAT, int);
    static SAMPLERDESCCONFIG createSamplerConfig();
    static RENDERSHADERCONFIG createRenderShaderConfig();

    bool isShadowed();
    void setShadowed(bool);

    bool isReflective();
    void setReflective(bool);
};

#endif