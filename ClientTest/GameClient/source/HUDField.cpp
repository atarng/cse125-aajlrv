#include "HUDField.h"
#include <iostream>
using namespace std;

HUDField::HUDField(int max) {
	sprintf_s(buffer,"");
	textValue = new HUDText;
	background = new HUDGraphic;
	focus = false;
	textValue->ChangeColor(0.0f, 0.0f, 0.0f);
	this->max = max;
}

HUDField::HUDField(char * value, int max) {
	textValue = new HUDText;
	focus = false;
	background = new HUDGraphic;
    sprintf_s(buffer,"%s",value);
	this->max = max;
}

void HUDField::setValue(char * value) {
	
    sprintf_s(buffer,"%s",value);
	textValue->ChangeText(buffer);
}

bool HUDField::appendValue(char value) {
	char buffer2[2];
	buffer2[0] = value;
	buffer2[1] = '\0';
	if (strlen(buffer) >= (unsigned int) max)
		return false;
    strncat_s(buffer,buffer2, 1);
	textValue->ChangeText(buffer);
	return true;
}

bool HUDField::backspace() {
	if (strlen(buffer) ==0)
		return false;
	buffer[strlen(buffer)-1] = '\0';
	return true;
}

void HUDField::setFocus(bool value) {
	focus = value;
}



