//CameraManager.h
#ifndef _CAMERAMANAGER_H_
#define _CAMERAMANAGER_H_

#include <vector>
#include "CameraObject.h"

class CameraManager
{
private:
    std::vector<CameraObject*> cameraObjects;
    CameraObject* currentCamera;

public:
    CameraManager();
    //CameraManager(const CameraManager &);
    ~CameraManager();

    void Initialize();
    
    int addCamera(CameraObject&);
    CameraObject& getCamera(int);
    CameraObject* getCurrentCamera(){return currentCamera;}

};

#endif