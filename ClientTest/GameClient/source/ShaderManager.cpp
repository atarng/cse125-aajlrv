/***
 * Filename: ShaderManager.cpp
 * Contributor(s): Alfred Tarng
 * Purpose:
 *
 * TODO:
 */

#include "SystemManager.h"
#include "GraphicsManager.h"
#include "ShaderManager.h"
#include "D3DManager.h"
#include "Log.h"

#include <iostream>

ShaderManager::ShaderManager()
{
	usingShadows = true;
}
ShaderManager::~ShaderManager()
{
    while ( !shaderObjects.empty() )
    {
        ShaderObject * se = shaderObjects.back();
        if ( se != NULL )
        {
            se->Shutdown();
            delete se;
            se = 0;
        }
        shaderObjects.pop_back();
    }
}

bool ShaderManager::Initialize(HWND hwnd)
{
    D3DManager* d3dman = SystemManager::Instance()->GetGraphicsManager()->GetD3DManager();

    bool result = true;

    ShaderObject   * test_ShaderObject;

	// Create the light shader object.
    test_ShaderObject = new ShaderObject;

/// 0:: Initialize the light shader object.
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));    //shaderConfigVector.push_back("TEXCOORD");
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL)); //shaderConfigVector.push_back("NORMAL");
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                           "resource/light.vs", "resource/light.ps",
                                           "LightVertexShader", "LightPixelShader"
                                          );
	if(!result)
	{
        cout << "[ShaderManager]could not load Light Shader file" << endl;
        Log::Instance()->PrintErr("could not load Light Shader file");
		return false;
    }
    shaderSelector.DIR_LIGHT_SHADER_0 = AddShader(*test_ShaderObject);
    
/// 1: Initialize the color shader object (USED BY TERRAIN) /////////////////////////////////////////////////////////////////////////////
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("COLOR"   , DXGI_FORMAT_R32G32B32A32_FLOAT, NULL));
	result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                              "resource/color.vs", "resource/color.ps",
                                              "ColorVertexShader", "ColorPixelShader"
                                            );
	if(!result)
	{
        cout << "[ShaderManager] Could not initialize the color shader object." << endl;
        Log::Instance()->PrintErr("[Graphics Manager] Could not initialize the color shader object.");
		return false;
    }else
    {
        cout << "[ShaderManager] Successfully loaded color shader object." << endl;
        Log::Instance()->PrintErr("[ShaderManager] Successfully loaded color shader object.");        
    }
    shaderSelector.COLOR_SHADER_0 = AddShader(*test_ShaderObject);

/// 2: Initialize the particle shader object /////////////////////////////////////////////////////////////////////////
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION"        , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("OFFSET"          , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD"        , DXGI_FORMAT_R32G32_FLOAT, NULL)); //shaderConfigVector.push_back("TEXCOORD");
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("COLOR"           , DXGI_FORMAT_R32G32B32A32_FLOAT, NULL)); //shaderConfigVector.push_back("NORMAL");

    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 0));//, 0));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 1));//, 16));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 2));//, 32));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 3));//, 48));
	result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/particle.vs", "resource/particle.ps",
                                            "ParticleVertexShader", "ParticlePixelShader"
                                            );
	if(!result)
	{
        cout << "[ShaderManager]Could not initialize the particle shader object." << endl;
        Log::Instance()->PrintErr("Could not initialize the particle shader object.");
		return false;
    }else
    {
        cout << "[ShaderManager]Successfully Initialized Particle System" << endl;
        Log::Instance()->PrintErr("Successfully Initialized Particle System");
    }
    shaderSelector.PARTICLE_SHADER = AddShader(*test_ShaderObject);
//// Initialize the bump shader object /////////////////////////////////////////////////////////////////////////
//3
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TANGENT" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("BINORMAL" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                              "resource/bumpmap.vs", "resource/bumpmap.ps",
                                              "BumpMapVertexShader", "BumpMapPixelShader"
                                            );
    if(!result)
	{
        cout << "[ShaderManager]Could not initialize the bump shader object." << endl;
        Log::Instance()->PrintErr("Could not initialize the bump shader object.");
		return false;
    }
    test_ShaderObject->setShadowed(false);//
    shaderSelector.BUMP_SHADER = AddShader(*test_ShaderObject);


//// bump shader object With Shadow
//4
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TANGENT" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("BINORMAL" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/bumpmap_shadow.vs", "resource/bumpmap_shadow.ps",
                                            "BumpMapVertexShader", "BumpMapPixelShader"
                                            );
    if(!result)
	{
        cout << "[ShaderManager]Could not initialize the bump shader object." << endl;
        Log::Instance()->PrintErr("Could not initialize the bump shader object.");
		return false;
    }
    test_ShaderObject->setShadowed(true);//
    shaderSelector.BUMP_SHADER_PLUS_SHADOW = AddShader(*test_ShaderObject);
//// Initialize the HUDShader //////////////////////////////////////////////////////////////////////////////////////
//5
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/hudshader.vs", "resource/hudshader.ps",
                                            "HudVertexShader", "HudPixelShader"
                                            );
    shaderSelector.HUD_SHADER = AddShader(*test_ShaderObject);

//// Initialize the Animation Shader //////////////////////////////////////////////////////////////////////////////////////
//5
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TANGENT" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("BINORMAL" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));

    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("INDEX" , DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("WEIGHT", DXGI_FORMAT_R32G32_FLOAT, NULL));

    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/animatedcharshader.vs", "resource/animatedcharshader.ps",
                                            "AnimatedVertexShader", "AnimatedPixelShader"
                                            );
	if(!result)
	{
        cout << "[ShaderManager] Could not initialize the animation shader object." << endl;
        Log::Instance()->PrintErr("[ShaderManager] Could not initialize the animation shader object.");
		return false;
    }
    shaderSelector.TEST_ANIMATION_SHADER = AddShader(*test_ShaderObject);

/// INITIALIZE DEPTH SHADER
//6
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/depth.vs", "resource/depth.ps",
                                            "DepthVertexShader", "DepthPixelShader"
                                            );
	if(!result)
	{
        cout << "[ShaderManager] Could not initialize the depth shader object." << endl;
        Log::Instance()->PrintErr("[ShaderManager] Could not initialize the depth shader object.");
		return false;
    } shaderSelector.TEST_DEPTHSHADER = AddShader(*test_ShaderObject);

/// 7: INITIALIZE SHADOWSHADER(merge with other shaders?)
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION", DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"  , DXGI_FORMAT_R32G32B32_FLOAT, NULL));

    //test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TANGENT" , DXGI_FORMAT_R32G32B32_FLOAT, NULL));  // XXX
    //test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("BINORMAL" , DXGI_FORMAT_R32G32B32_FLOAT, NULL)); // XXX

    test_ShaderObject -> samplerDescConfigVector.push_back(ShaderObject::createSamplerConfig()); // $$$
    test_ShaderObject -> samplerDescConfigVector.back().uAddr =
      test_ShaderObject -> samplerDescConfigVector.back().vAddr = 
      test_ShaderObject -> samplerDescConfigVector.back().wAddr = D3D11_TEXTURE_ADDRESS_CLAMP;//D3D11_TEXTURE_ADDRESS_WRAP;
    test_ShaderObject -> samplerDescConfigVector.push_back(ShaderObject::createSamplerConfig()); // $$$
    test_ShaderObject -> samplerDescConfigVector.back().uAddr =
      test_ShaderObject -> samplerDescConfigVector.back().vAddr = 
      test_ShaderObject -> samplerDescConfigVector.back().wAddr = D3D11_TEXTURE_ADDRESS_WRAP;//D3D11_TEXTURE_ADDRESS_CLAMP;

    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/shadow.vs", "resource/shadow.ps",
                                            "ShadowVertexShader", "ShadowPixelShader"
                                            );
    if(!result)
	{
        cout << "[ShaderManager] Could not initialize the shadow shader object." << endl;
        Log::Instance()->PrintErr("[ShaderManager] Could not initialize the shadow shader object.");
		return false;
    }
    test_ShaderObject->setShadowed(true);//
    shaderSelector.TEST_SHADOWSHADER = AddShader(*test_ShaderObject);

/// 8: Initialize the particle shader (2) object /////////////////////////////////////////////////////////////////////////
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION"        , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("OFFSET"          , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD"        , DXGI_FORMAT_R32G32_FLOAT, NULL)); //shaderConfigVector.push_back("TEXCOORD");
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("COLOR"           , DXGI_FORMAT_R32G32B32A32_FLOAT, NULL)); //shaderConfigVector.push_back("NORMAL");

    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 0));//, 0));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 1));//, 16));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 2));//, 32));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("PARTICLEROTATION", DXGI_FORMAT_R32G32B32A32_FLOAT, 3));//, 48));
	result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/particle_billBoarded.vs", "resource/particle_billBoarded.ps",
                                            "ParticleVertexShader", "ParticlePixelShader"
                                            );
	if(!result)
	{
        cout << "Could not initialize the billBoarded particle shader object." << endl;
        Log::Instance()->PrintErr("Could not initialize the billBoarded particle shader object.");
		return false;
    }else
    {
        cout << "[ShaderManager]Successfully Initialized billBoarded Particle System Shader" << endl;
        Log::Instance()->PrintErr("Successfully Initialized billBoarded Particle System Shader");
    }
    shaderSelector.PARTICLE_SHADER_BB = AddShader(*test_ShaderObject);

/// 9: Initialize the particle shader (2) object /////////////////////////////////////////////////////////////////////////
    test_ShaderObject = new ShaderObject;
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("POSITION"        , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("TEXCOORD"        , DXGI_FORMAT_R32G32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("NORMAL"          , DXGI_FORMAT_R32G32B32_FLOAT, NULL));
    test_ShaderObject -> shaderConfigVector.push_back(ShaderObject::createShaderConfig("COLOR"           , DXGI_FORMAT_R32G32B32A32_FLOAT, NULL));

    //test_ShaderObject -> samplerDescConfigVector.push_back(ShaderObject::createSamplerConfig()); // $$$
    //test_ShaderObject->samplerDescConfigVector.back().uAddr = test_ShaderObject->samplerDescConfigVector.back().vAddr = 
    //  test_ShaderObject -> samplerDescConfigVector.back().wAddr = D3D11_TEXTURE_ADDRESS_WRAP;
    result = test_ShaderObject->Initialize( d3dman->GetDevice(), hwnd,
                                            "resource/reflection.vs", "resource/reflection.ps",
                                            "ReflectionVertexShader", "ReflectionPixelShader"
                                            );
    if(!result)
	{
        cout << "Could not initialize the reflection shader object." << endl;
        Log::Instance()->PrintErr("Could not initialize the reflection shader object.");
		return false;
    }else
    {
        cout << "[ShaderManager]Successfully Initialized reflection System Shader" << endl;
        Log::Instance()->PrintErr("Successfully Initialized reflection System Shader");
    }
    shaderSelector.TEST_REFLECTIVE = AddShader(*test_ShaderObject);
    test_ShaderObject->setReflective(true);

    return result;
}
int ShaderManager::AddShader(ShaderObject & addShader)
{
    shaderObjects.push_back(&addShader);
    return (shaderObjects.size() - 1);
}

void ShaderManager::SetUsingShadows(bool shadow)
{
	usingShadows = shadow;
}

ShaderObject & ShaderManager::GetShader(int index)
{
    return *shaderObjects.at(index);
}
ShaderObject * ShaderManager::GetUnitShader()
{
    if ( !usingShadows ) mainUnitShader = &GetShader( shaderSelector.BUMP_SHADER );
    else                 mainUnitShader = &GetShader( shaderSelector.BUMP_SHADER_PLUS_SHADOW );

	return mainUnitShader;
}