#ifndef _TEXTURE_MANAGER_H_
#define _TEXTURE_MANAGER_H_

#include "TextureObject.h"
#include <vector>

class TextureManager
{
private:
    struct TextureSelecter
    {
        int SKYBOX_TEXTURE;           // 0
        int LIGHTNING_TEXTURE;        // 1
        int LIGHTNING_TEXTURE_SPHERE; // 2
        int FONT;                     // 3
		int ETHING_UV;                // 4
        int PROBE_UV;                 // 5
        int DROID_UV;                 // 6
        int EXPLOSION;
		int TABLET_UV_0, TABLET_UV_1;

        // HUD STUFF
        int TEST_HUD_BUTTON;          // 8
		int GRAY_BAR;                 // 9
		int BLUE_BAR;                 // 10
        int MINIMAP;                  
        int MINIMAP_ICON;             // 11
		int FIELD;
		int CROSSHAIRS;
		int SPLASH;
		int FOCUS;
		int LOSE;
		int WIN;

        int TEST_MAP;
        int TEST_STONE_TEXTURE_BUMPED;   // 12
        int TEST_STONE_TEXTURE;          // 13
        int TEST_CAT_TEXTURE;            // 14
        int TEST_GLITTER_TEXTURE;        // 15
        int TEST_SAND_TEXTURE;           // 16
        int TEST_LASER_TEXTURE;          // 17

		int SPARK_BAR;
		int ROBOT_BAR;
		int LIGHTNING_BAR;
		
		int SATELLITESHIP_UV;

    } textureSelecter;

    std::vector<TextureObject *> textureObjects;

public:
    TextureManager();
    ~TextureManager();

    bool Initialize(ID3D11Device*);
    int AddTexture(TextureObject& newTextureObject);
    TextureObject& GetTextureObject(int index);
    
    inline TextureSelecter GetTextureSelecter(){ return textureSelecter; };

    inline std::vector<TextureObject *> GetTextures(){ return textureObjects; };
};

#endif