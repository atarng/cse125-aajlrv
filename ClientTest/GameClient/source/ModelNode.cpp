#include "ModelNode.h"
#include "SystemManager.h"
#include "GraphicsManager.h"
#include <vector>

ModelNode::ModelNode(){}
ModelNode::ModelNode(Model& mo)
{
    geode = &mo;
}
ModelNode::ModelNode(const ModelNode &)
{
}
ModelNode::~ModelNode()
{
}

void ModelNode::UpdateMatrix()
{
// DO NOTHING
}
void ModelNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    stack->Push();
        D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());

        float pitch, yaw ,roll;
        pitch = mn_rotation.x * 0.0174532925f;
	    yaw   = mn_rotation.y * 0.0174532925f;
	    roll  = mn_rotation.z * 0.0174532925f;

        stack->ScaleLocal(mn_scale.x, mn_scale.y, mn_scale.z);
        //stack->RotateYawPitchRollLocal( yaw, pitch, roll);
        stack->Translate(mn_translation.x, mn_translation.y, mn_translation.z);

        Render(d3dMan);

        std::vector<SceneNode *>::iterator it;
        for( it = children.begin(); it < children.end(); it++ )
        {
            if (!((*it)->GetID() < 0)) (*it)->Draw( d3dMan, stack, drawMode );
        }

    stack->Pop();
}
void ModelNode::Render(D3DManager* d3dMan)
{
    geode->Render( d3dMan->GetDeviceContext() );

    if (shader)
    {
        D3DXMATRIX camMatrix;
        SystemManager::Instance()->GetGraphicsManager()->GetCamera()->GetMatrix(camMatrix);
        LightObject* light_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetLightManager()->getLight(0);
        D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());

        ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
        myRSC.lightMan = SystemManager::Instance()->GetGraphicsManager()->GetLightManager();
        myRSC.camObj   = SystemManager::Instance()->GetGraphicsManager()->GetCamera();

        shader->Render( d3dMan -> GetDeviceContext(), geode->GetIndexCount(), 0,
            mat, camMatrix, d3dMan -> GetProjectionMatrix(), 
            geode->GetTextureObject()->GetTextures(), geode->GetTextureObject()->numTextures(),
            myRSC
            );
    }
}

void ModelNode::RenderDepth(D3DManager* d3dMan)
{
    //D3DXMATRIX lightViewMatrix, lightProjectionMatrix;
    //ShaderObject depthShader;

    //LightObject* light_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetLightManager()->getLight(0);
    //ShaderObject* depthShader = SystemManager::Instance()->GetGraphicsManager()->GetDepthShader();
    //light_Ptr -> GenerateViewMatrix();
    //D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());

    //light_Ptr -> GetViewMatrix(lightViewMatrix);
    //light_Ptr -> GetProjectionMatrix(lightProjectionMatrix);

    //// RENDER OBJECT

    // Render depthshader
    //depthShader->Render(m_D3D->GetDeviceContext(), m_CubeModel->GetIndexCount(), worldMatrix, lightViewMatrix, lightProjectionMatrix);
}

void ModelNode::setPosition( D3DXVECTOR3 newPos )      { mn_translation = newPos; }
void ModelNode::setOrientation( D3DXVECTOR3 newOrient ){ mn_rotation    = newOrient; }
void ModelNode::setScale( D3DXVECTOR3 newScale )       { mn_scale       = newScale; }