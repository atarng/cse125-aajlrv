//LightManager.h
#ifndef _LIGHTMANAGER_H_
#define _LIGHTMANAGER_H_

#include <vector>
#include "LightObject.h"

class LightManager
{
private:
    std::vector<LightObject*> lightObjects;

public:
    LightManager();
    //LightManager(const LightManager &);
    ~LightManager();

    bool Initialize();
    int addLight(LightObject&);
    LightObject * getLight(int);

};

#endif

