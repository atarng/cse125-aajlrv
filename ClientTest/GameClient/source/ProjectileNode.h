//ProjectileNode.h
#ifndef _PROJECTILENODE_H_
#define _PROJECTILENODE_H_

#include "D3DManager.h"
#include "ModelNode.h"
#include "UnitObject.h"
#include "ShaderObject.h"

class ProjectileNode : public ModelNode
{
protected:
    UnitObject* unitObject;
    //ShaderObject* shader;

public:
    ProjectileNode();
    ProjectileNode(const ProjectileNode &);
    ~ProjectileNode();

    void SetUnitObject(UnitObject*);

    inline ShaderObject* GetShader()    { return shader; }
    inline UnitObject* GetUnitObject()  { return unitObject;   }

    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);

    inline virtual bool isProjectileNode()    { return true; };
    inline virtual bool isModelNode()   { return true; };
};

#endif