/***
 * Filename: Terrain.cpp
 * Contributors: Alfred Tarng
 * Purpose: Using a heightmap texture, creates a mesh for gameplay usage.
 * Supports textures, dynamic color assignment based on height levels.
 *
 * TODO:
 */

#include "Terrain.h"

#include <math.h>
#include <iostream>

using namespace std;

Terrain::Terrain()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_heightMap = 0;

    maxHeight = 0;
}
Terrain::Terrain(const Terrain& other)
{
}
Terrain::~Terrain()
{
}

D3DXVECTOR3 Terrain::SelectColor(float height, float maxHeight)
{
    float ratio, ratio1;
    ratio = height / maxHeight;
    ratio1 = 1;/// = ratio
    D3DXVECTOR3 heightColor;
    if      (ratio < 0.1) heightColor = D3DXVECTOR3( 0.0f * ratio1, 0.0f * ratio1, 0.0f * ratio1);
    else if (ratio < 0.2) heightColor = D3DXVECTOR3( 0.6f * ratio1, 0.6f * ratio1, 0.6f * ratio1);
    else if (ratio < 0.3) heightColor = D3DXVECTOR3(0.75f * ratio1, 0.0f * ratio1, 0.0f * ratio1);
    else if (ratio < 0.4) heightColor = D3DXVECTOR3( 0.0f * ratio1,0.75f * ratio1, 0.0f * ratio1);
    else if (ratio < 0.5) heightColor = D3DXVECTOR3( 0.0f * ratio1, 0.0f * ratio1,0.75f * ratio1);
    else if (ratio < 0.6) heightColor = D3DXVECTOR3(0.75f * ratio1,0.75f * ratio1, 0.0f * ratio1);
    else if (ratio < 0.7) heightColor = D3DXVECTOR3( 0.0f * ratio1,0.75f * ratio1,0.75f * ratio1);
    else if (ratio < 0.8) heightColor = D3DXVECTOR3(0.75f * ratio1,0.75f * ratio1,0.75f * ratio1);
    else if (ratio < 0.9) heightColor = D3DXVECTOR3( 0.2f * ratio1, 0.2f * ratio1, 0.2f * ratio1);
    else                  heightColor = D3DXVECTOR3( 1.0f * ratio1, 1.0f * ratio1, 1.0f * ratio1);
    return heightColor;
}

bool Terrain::Initialize(ID3D11Device* device, char* heightMapFilename)
{
	bool result;

	// Load in the height map for the terrain.
	result = LoadHeightMap(heightMapFilename);
	if(!result)
	{
        cout << "problems loading height map" << endl;
		return false;
	}

	// Normalize the height of the height map.
	NormalizeHeightMap();

	// Initialize the vertex and index buffer that hold the geometry for the terrain.
	result = InitializeBuffers(device);
	if(!result)
	{
        cout << "problems Initializing buffers for height map" << endl;
		return false;
	}

	return true;
}
bool Terrain::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	int index, i, j;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int index1, index2, index3, index4;

    bool smoothGradient = false;

	// Calculate the number of vertices in the terrain mesh.
	m_vertexCount = (m_terrainWidth - 1) * (m_terrainHeight - 1) * 6; // 6

	// Set the index count to the same as the vertex count.
	m_indexCount = m_vertexCount;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices) return false;

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices) return false;

	// Initialize the index to the vertex buffer.
	index = 0;
    float tileAmount = //1.f;
                       m_terrainHeight/2.f;
    
	// Load the vertex and index array with the terrain data.
	for(j=0; j<(m_terrainHeight-1); j++)
	{
		for(i=0; i<(m_terrainWidth-1); i++)
		{
			index1 = (m_terrainHeight * j) + i;          // Bottom left.
			index2 = (m_terrainHeight * j) + (i+1);      // Bottom right.
			index3 = (m_terrainHeight * (j+1)) + i;      // Upper left.
			index4 = (m_terrainHeight * (j+1)) + (i+1);  // Upper right.

            //D3DXVECTOR3 vcolor  = SelectColor(m_heightMap[index3].y, maxHeight);

            D3DXVECTOR3 normalVector1, normalVector2;
            D3DXVECTOR3 vec1, vec2;
            vec1 = D3DXVECTOR3(m_heightMap[index4].x - m_heightMap[index3].x, m_heightMap[index4].y - m_heightMap[index3].y, m_heightMap[index4].z - m_heightMap[index3].z);
            vec2 = D3DXVECTOR3(m_heightMap[index1].x - m_heightMap[index3].x, m_heightMap[index1].y - m_heightMap[index3].y, m_heightMap[index1].z - m_heightMap[index3].z);
            D3DXVec3Cross(&normalVector1, &vec1, &vec2);

            vec1 = D3DXVECTOR3(m_heightMap[index1].x - m_heightMap[index2].x, m_heightMap[index1].y - m_heightMap[index2].y, m_heightMap[index1].z - m_heightMap[index2].z);
            vec2 = D3DXVECTOR3(m_heightMap[index4].x - m_heightMap[index2].x, m_heightMap[index4].y - m_heightMap[index2].y, m_heightMap[index4].z - m_heightMap[index2].z);
            D3DXVec3Cross(&normalVector2, &vec1, &vec2);

            for (int i = 0; i < 6; ++i)
            {
                int useIndex = 0;
                switch (i)
                {
                    case 0:  useIndex = index3; break; // Top Left,
                    case 1:  useIndex = index4; break; // Top Right
                    case 2:                            // Bottom Left
                    case 3:  useIndex = index1; break; 
                    case 4:  useIndex = index4; break; // Top Right
                    case 5:  useIndex = index2; break; // Bottom Right
                }

                if (i < 3) { m_heightMap[useIndex].nx = normalVector1.x;
                             m_heightMap[useIndex].ny = normalVector1.y;
                             m_heightMap[useIndex].nz = normalVector1.z;
                } else     { m_heightMap[useIndex].nx = normalVector2.x;
                             m_heightMap[useIndex].ny = normalVector2.y;
                             m_heightMap[useIndex].nz = normalVector2.z;
                }

                D3DXVec3Normalize( &normalVector1, &normalVector1 );
                D3DXVec3Normalize( &normalVector2, &normalVector2 );

                D3DXVECTOR3 vcolor  = SelectColor(m_heightMap[useIndex].y, maxHeight);

			    vertices[index].position = D3DXVECTOR3( m_heightMap[useIndex].x, m_heightMap[useIndex].y, m_heightMap[useIndex].z);
                vertices[index].texture  = D3DXVECTOR2( fmod(m_heightMap[useIndex].x , (float)m_terrainWidth /tileAmount ) / ((float)m_terrainWidth/tileAmount),
                                                        fmod(m_heightMap[useIndex].z , (float)m_terrainHeight/tileAmount ) / ((float)m_terrainHeight/tileAmount));
                vertices[index].normal   = ( i < 3 ) ? normalVector1 : normalVector2;

                vertices[index].color    = D3DXVECTOR4(1.0f * vcolor.x, 1.0f * vcolor.y, 1.0f * vcolor.z, 1.0f);
			    indices [index]          = index;
			    index++;
            }
		}
	}

	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem          = vertices;
	vertexData.SysMemPitch      = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}
/***
 * Loads the actual height map.
 */
bool Terrain::LoadHeightMap(char* filename)
{
	FILE* filePtr;
	int error;
	unsigned int count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	int imageSize, k, index;
	unsigned char* bitmapImage;
	unsigned char height;

	// Open the height map file in binary.
	error = fopen_s(&filePtr, filename, "rb");
	if(error != 0)
	{
		return false;
	}

	// Read in the file header.
	count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);
	if(count != 1)
	{
		return false;
	}

	// Read in the bitmap info header.
	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);
	if(count != 1)
	{
		return false;
	}

	// Save the dimensions of the terrain.
	m_terrainWidth  = bitmapInfoHeader.biWidth;
	m_terrainHeight = bitmapInfoHeader.biHeight;

	// Calculate the size of the bitmap image data.
	imageSize = m_terrainWidth * m_terrainHeight * 3;

	// Allocate memory for the bitmap image data.
	bitmapImage = new unsigned char[imageSize];
	if(!bitmapImage)
	{
		return false;
	}

	// Move to the beginning of the bitmap data.
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread(bitmapImage, 1, imageSize, filePtr);
	if(count != imageSize)
	{
		return false;
	}

	// Close the file.
	error = fclose(filePtr);
	if(error != 0)
	{
		return false;
	}

	// Create the structure to hold the height map data.
	m_heightMap = new HeightMapType[m_terrainWidth * m_terrainHeight];
	if(!m_heightMap)
	{
		return false;
	}

	// Initialize the position in the image data buffer.
	k=0;

	// Read the image data into the height map.
	for(int j=0; j < m_terrainHeight; j++)
	{
		for(int i=0; i < m_terrainWidth; i++)
		{
			height = bitmapImage[k];
			
			index = (m_terrainWidth * j) + i;

			m_heightMap[index].x = (float)i;
			m_heightMap[index].y = (float)height;
			m_heightMap[index].z = (float)j;

			k += 3;
		}
	}

	// Release the bitmap image data.
	delete [] bitmapImage;
	bitmapImage = 0;

	return true;
}
/***
 * Scales height map height to appropriate values.
 */
void Terrain::NormalizeHeightMap()
{
	for(int j=0; j<m_terrainHeight; j++)
	{
		for(int i=0; i<m_terrainWidth; i++)
		{
			m_heightMap[(m_terrainHeight * j) + i].y /= 25.0;
            if ( maxHeight < m_heightMap[(m_terrainHeight * j) + i].y )
            {
                maxHeight = m_heightMap[(m_terrainHeight * j) + i].y;
            }
		}
	}
	return;
}

int Terrain::GetIndexCount(){ return m_indexCount; }

/***
 * wxh
 */
void Terrain::getHeightAt(float& x, float& y, float& z)
{
    if (x < 0 || z < 0 || z >= m_terrainHeight || x >= m_terrainWidth )
    {
        y = 0;
        return;
    }
    /*
    int indexFloorHeight  = (int) floor(m_terrainWidth  * z);
    int indexFloorWidth   = (int) floor(x);
    int indexCeilHeight   = (int) ceil(m_terrainWidth  * z);
    int indexCeilWidth    = (int) ceil(x);

    float ifwfh = m_heightMap[indexFloorWidth + indexFloorHeight].y;
    float ifwch = m_heightMap[indexFloorWidth +  indexCeilHeight].y;
    float icwfh = m_heightMap[indexCeilWidth  + indexFloorHeight].y;
    float icwch = m_heightMap[indexCeilWidth  +  indexCeilHeight].y;

    float exactHeight = (1 - sqrt(pow((x - indexFloorWidth),2) + pow((z - indexFloorHeight),2))) * ifwfh +
                        (1 - ((indexCeilWidth  - x ) + (z - indexFloorHeight))) * ifwch +
                        (1 - ((x - indexFloorWidth) + (indexCeilHeight- z))) * icwfh +
                        (1 - ((indexCeilWidth - x)  + (indexCeilHeight- z))) * icwch ;
    */
    int index = (int)(m_terrainWidth * (int)floor(z)) + (int)(floor(x));

    //int oldY = (int) y;
    y = m_heightMap[ index ].y;

    /*
    if (oldY != y)    {
        printf( "Original Position x: %g, z: %g height is: %g \n", x, z, oldY );
        printf( "Updated Position x: %g, z: %g height is: %g \n", x, z, y );
    }*/
}

void Terrain::GetMatrix(D3DXMATRIX& matrix){
    D3DXMatrixTransformation(   &matrix,      // D3DXMATRIX *pOut
                                NULL,         // CONST D3DXVECTOR2* pScalingCenter
                                NULL,         // CONST D3DXQUATERNION *pScalingRotation
                                NULL,         // CONST D3DXVECTOR3 *pScaling
                                NULL,         // CONST D3DXVECTOR3 *pRotationCenter
                                &orientation, // CONST D3DXQUATERNION *pRotation
                                &position);   // CONST D3DXVECTOR3 *pTranslation
}

void Terrain::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);
	return;
}
void Terrain::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case a line list.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    //deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	return;
}

TextureObject* Terrain::GetTextureObject(){ return m_Textures; }
void Terrain::SetTextureObject(TextureObject& textureRef){ m_Textures = &textureRef; }

void Terrain::Shutdown()
{
	// Release the vertex and index buffer.
	ShutdownBuffers();

	// Release the height map data.
	ShutdownHeightMap();

	return;
}
void Terrain::ShutdownHeightMap()
{
	if(m_heightMap)
	{
		delete [] m_heightMap;
		m_heightMap = 0;
	}

	return;
}
void Terrain::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}