/***
 * Filename: ParticleSystemObject.cpp
 * Contributors: Alfred Tarng
 * Purpose: Wraps A Particle System to allow for instancing when desired.
 *
 * TODO: 
 */

#include "ParticleSystemObject.h"
#include "ParticleSystem.h"
#include "SystemManager.h"
#include "GraphicsManager.h"
#include "TextureManager.h"
#include "ParticleSystemManager.h"
#include <iostream>

ParticleSystemObject::ParticleSystemObject()
{
    particleSystem = NULL;

    position = D3DXVECTOR3(0,0,0);
    rotation = D3DXVECTOR3(0,0,0);
    scale    = D3DXVECTOR3(1,1,1);
    
    expires = false;
    lifeTime = 0;
    billBoarded = SYSTEM;
    SystemManager::Instance()->GetGraphicsManager()->GetParticleSystemManager()->AddParticleSystemObject(*this);

}
ParticleSystemObject::~ParticleSystemObject()
{
    if (particleSystem != NULL)
    {
        particleSystem->SetExpiredSystem(true);
        //delete particleSystem;
        //particleSystem = 0;
    }
}

/***
 * Make sure to call this last when initializing a dynamic particle system.
 */
void ParticleSystemObject::GenerateOwnParticleSystem(int particleSystemType)
{
    if (particleSystem != NULL)
      std::cerr << "[ParticleSystemObject] Warning setting nonNull ParticleSystem" <<
                   "to new Particle System. Possible memory leak." << std::endl;

    D3DManager * d3dman = SystemManager::Instance()->GetGraphicsManager()->GetD3DManager();
    TextureManager* texMan = SystemManager::Instance()->GetGraphicsManager()->GetTextureManager();

    particleSystem = new ParticleSystem;
	if ( particleSystemType == 3 )
	{
		particleSystem -> GetParticleBehavior().FREEFALLING = true;
	}else
	{
		particleSystem -> GetParticleBehavior().RADIAL      = true;
	}

    if ( particleSystemType == 2 ) particleSystem -> GetParticleBehavior().COLORTYPE = CT_RAINBOW;
    else                           particleSystem -> GetParticleBehavior().COLORTYPE = CT_FIRE;

    if (particleSystemType == 0)
    {
		particleSystem -> GetParticleBehavior().SPEED     = 10.0f;
        particleSystem -> GetParticleBehavior().WAVE = true ;
        particleSystem -> GetParticleBehavior().P_LIFESPAN  = 300.f;
		//this->SetRotation(D3DXVECTOR3(0,90,0));
    }

    if(!( particleSystem -> Initialize( d3dman->GetDevice()) )) std::cerr<< "Initializing Explosion Particle System failed..." <<std::endl;

    if ( particleSystemType == 2 )  particleSystem->SetTextureObject ( texMan->GetTextureObject(texMan->GetTextureSelecter().LIGHTNING_TEXTURE ) );
    else                            particleSystem->SetTextureObject ( texMan->GetTextureObject(texMan->GetTextureSelecter().EXPLOSION ) );
    if(this->Expires()) particleSystem->SetExpirable();
    
    SystemManager::Instance()->GetGraphicsManager()->GetParticleSystemManager()->AddExpirableSystem(*particleSystem);
}
void ParticleSystemObject::SetParticleSystem(ParticleSystem* setPS)
{
    particleSystem = setPS;
}

void ParticleSystemObject::Render(ID3D11DeviceContext* deviceContext)
{
	// TODO: Move lifetime update to frame call.
    //if ( Expires() ) updateLifeTime(); // does this Particle System Object expire?
    if ( Expires() && this->isExpired()) return;
    if (particleSystem != NULL) particleSystem->Render(deviceContext);
}

void ParticleSystemObject::updateLifeTime()                  { lifeTime -= particleSystem->GetTimeSinceLastFrame(); }
void ParticleSystemObject::SetLifeTime(float lt)             { lifeTime = lt * 1000.0f; }
void ParticleSystemObject::SetBillBoarded(BillBoardType bbt) { billBoarded = bbt; }
/*** This is a particle system object that is meant to expire. ***/
void ParticleSystemObject::setExpires(bool e)                { expires = e;      }