#ifndef _PARTICLESYSTEMCLASS_H_
#define _PARTICLESYSTEMCLASS_H_

#include "D3DManager.h"

class TextureObject;

enum ColorType
{
    CT_RAINBOW = 0,
    CT_FIRE,
    CT_BANDW
};

class ParticleSystem
{
private:
    /// USED FOR SHADER CONSTANT BUFFER
    // Looks like this could inherit from something...
    struct VertexType
	{
		D3DXVECTOR3 position;
        D3DXVECTOR3 offset;
		D3DXVECTOR2 texture;
		D3DXVECTOR4 color;

        D3DXMATRIX rotation; // used for radial immission (roll)
	};

    // DETERMINES THE PARTICLE SYSTEM BEHAVIOR
    struct ParticleBehaviorConfig
    {
     // BEHAVIORS
        bool        RADIAL;  // IMPLEMENTED
        bool        RADIALBRANCHOUT;
        bool        FREEFALLING; // DEFAULT
        bool        TRAILING; // Will probably be too computationally expensive to do...
        bool        WAVE;

     // PROPERTIES
        float        SPEED;
        int          PARTICLE_CAP;
        float        P_LIFESPAN;
        ColorType    COLORTYPE;
    } particleBehavior;

    // Populates Particle
    struct ParticleType
	{
        float red, green, blue;
        float initPosX, initPosY, initPosZ;
		float positionX, positionY, positionZ;
        float dirX, dirY, dirZ;
		float velocity;

        float roll;

		bool  active;// not used.
        float currentLifeSpan, maxLifeSpan;
	};

	bool InitializeParticleSystem();
	void ShutdownParticleSystem();

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();

	void EmitParticles(float);
	void UpdateParticles(float);
	void KillParticles();

	bool UpdateBuffers(ID3D11DeviceContext*);

	void RenderBuffers(ID3D11DeviceContext*);

    float m_particleDeviationX, m_particleDeviationY, m_particleDeviationZ; // 
	float m_particleVelocity, m_particleVelocityVariation;                  //
	float m_particleSize, m_particlesPerSecond;                             //
	int   m_maxParticles;

	int m_currentParticleCount;
	float m_accumulatedTime;

	TextureObject* m_Texture;
	ParticleType* m_particleList;
	int m_vertexCount, m_indexCount;
	VertexType* m_vertices;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;

    float timeSinceLastFrame;
	bool p_expires, p_expired;

public:
    ParticleSystem();
	ParticleSystem(const ParticleSystem&);
	~ParticleSystem();

	//bool Initialize(ID3D11Device*, char*);
    bool Initialize(ID3D11Device*);
	void Shutdown();
	bool Frame(float, ID3D11DeviceContext*);
	void Render(ID3D11DeviceContext*);

    ParticleBehaviorConfig & GetParticleBehavior();

    void SetTextureObject(TextureObject &);
	inline TextureObject* GetTextureObject() { return m_Texture; };
    inline int GetIndexCount(){ return m_indexCount; };

    bool CheckConditionalOnParticle(int);
    inline float GetTimeSinceLastFrame(){ return timeSinceLastFrame; };

	void SetExpirable();
	void SetExpiredSystem(bool);
    inline bool isExpired(){ return p_expires && p_expired; }
};

#endif