#include <iostream>

#include "ParticleSystemManager.h"
#include "GraphicsManager.h"
#include "SystemManager.h"
#include "TextureManager.h"

#include "ParticleSystem.h"

using namespace std;

ParticleSystemManager::ParticleSystemManager()
{
}
ParticleSystemManager::~ParticleSystemManager()
{
}

/***
 *
 */
bool ParticleSystemManager::Initialize()
{
    D3DManager* d3dman = SystemManager::Instance()->GetGraphicsManager()->GetD3DManager();
    TextureManager * texMan = SystemManager::Instance()->GetGraphicsManager()->GetTextureManager();

    bool result = true;

/// Tutorial Particle System
    ParticleSystem * initParticleSystem = new ParticleSystem;
    if(!initParticleSystem ) return false;
    initParticleSystem -> GetParticleBehavior().FREEFALLING = true;
    initParticleSystem -> GetParticleBehavior().COLORTYPE = CT_RAINBOW;
    result = initParticleSystem ->Initialize( d3dman->GetDevice());

    if(!result)
    {
        cout<< "Initializing Glitter Particle System failed..." <<endl;
        return false;
    } particleSelecter.TEST_GLITTER = AddParticleSystem( *initParticleSystem );
    initParticleSystem ->SetTextureObject ( texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_GLITTER_TEXTURE) );

/// CREATE Lighting Particle System Objecct.
    initParticleSystem = new ParticleSystem;
    initParticleSystem -> GetParticleBehavior().RADIAL = true;
    initParticleSystem -> GetParticleBehavior().COLORTYPE = CT_RAINBOW;
    result = initParticleSystem ->Initialize( d3dman->GetDevice());
    if(!result)
    {
        cout<< "Initializing Lightning Particle System failed..." <<endl;
        return false;
    } particleSelecter.TEST_RADIAL_LIGHTNING = AddParticleSystem( *initParticleSystem );
    initParticleSystem ->SetTextureObject ( texMan->GetTextureObject(texMan->GetTextureSelecter().LIGHTNING_TEXTURE) );

///
    initParticleSystem = new ParticleSystem;
    initParticleSystem -> GetParticleBehavior().TRAILING = true;
    initParticleSystem -> GetParticleBehavior().COLORTYPE = CT_RAINBOW;
    result = initParticleSystem ->Initialize( d3dman->GetDevice());
    if(!result)
    {
        cout<< "Initializing JETPACK System failed..." <<endl;
    } particleSelecter.TEST_JETPACK = AddParticleSystem( *initParticleSystem );
    initParticleSystem ->SetTextureObject ( texMan->GetTextureObject(texMan->GetTextureSelecter().TEST_GLITTER_TEXTURE) );
    return result;
}
/***
 * Updates all ParticleSystem Objects. (if active?)
 */
void ParticleSystemManager::Frame(float frameTime, ID3D11DeviceContext* dc)
{
    for ( unsigned int i = 0; i < particleSystemWrappers.size(); ++i)
    {
        if (particleSystemWrappers.at(i).active)
            particleSystemWrappers.at(i).particleSystem -> Frame(frameTime, dc);
    }
}

int ParticleSystemManager::AddParticleSystem(ParticleSystem& addPS)
{
    ParticleSystemWrapper psw;
    psw.particleSystem = &addPS;
    psw.active = true;
    particleSystemWrappers.push_back( psw );
    return (particleSystemWrappers.size() - 1);
}
ParticleSystem & ParticleSystemManager::GetParticleSystem(int index)
{
    return *(particleSystemWrappers.at(index).particleSystem );
}

void ParticleSystemManager::AddExpirableSystem(ParticleSystem& expirableSystem)
{
    expirableParticleSystems.push_back(&expirableSystem);
}

void ParticleSystemManager::AddParticleSystemObject(ParticleSystemObject& addPSO)
{
    particleSystemObjects.push_back( &addPSO );
    return;// (particleSystemObjects.size() - 1);
}

// removes expired particlesystems.
// Can Also function to update Particle Emits.
void ParticleSystemManager::CheckForAndRemoveExpiredSystems(float deltaTime, ID3D11DeviceContext* dCntxt)
{
    std::vector<ParticleSystemObject*>::iterator it_PSO;
    for(it_PSO = particleSystemObjects.begin(); it_PSO != particleSystemObjects.end(); ++it_PSO)
    {// Loop through and update lifetimes of other particlesystems.
        if ( (*it_PSO)->Expires() ) (*it_PSO)->updateLifeTime();
        if ( (*it_PSO)->isExpired() ) it_PSO = particleSystemObjects.erase(it_PSO);
        if ( it_PSO == particleSystemObjects.end() ) break;
    }

    std::vector<ParticleSystem*>::iterator it;
    for(it = expirableParticleSystems.begin(); it != expirableParticleSystems.end(); ++it)
    {
        if((*it)->isExpired())
        {
            delete (*it);
            it = expirableParticleSystems.erase(it);
            if (it == expirableParticleSystems.end()) break;
        }else
        {//Update Frame?
            (*it)->Frame(deltaTime, dCntxt);
        }
    }
}