/***
 * Filename: UnitNode.cpp
 * Contributors: Alfred Tarng, Joey Ly
 * Purpose: 
 *
 * TODO: Implement a method for LOD Swapping.
 */

#define _WINSOCKAPI_
#include "NetworkManager.h"

#include "SystemManager.h"
#include "HUDManager.h"
#include "GraphicsManager.h"
#include "LoadingManager.h"
#include "NetworkManager.h" //remove later
#include "MeshManager.h"

#include "LightObject.h"
#include "ShaderObject.h"

#include "UnitNode.h"
#include "ModelSkinned.h"
#include "RootNode.h"
#include <iostream>

#include "Log.h"

float intersectionPoint;
float dir = 1; // DEPRECATED?
extern bool clientStarted;

UnitObject* UnitNode::lodCube = NULL;

UnitNode::UnitNode()
{
    super();

    unitObject = NULL;
    boundingSphere = (Model *)& SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMesh( 
                                SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TEST_COLLISION_SPHERE);
    shader = NULL;
    nodeID = 0;

    dbs = true; // display bounding sphere
    dynamicShaderSet = false;

	renderAsReflection = false;

    animationCounter = 0.0f;
    currentAnimation = at_DEFAULT;

    if (lodCube == NULL)
    {
        lodCube = new UnitObject;
        lodCube->SetModel(3);
        lodCube->setAsTester();
    }
//  if ( lodCube != NULL ) unitObjectSwapList.insert( pair<AnimationType, UnitObject*>(at_LOD_CUBE, lodCube) );
}
UnitNode::UnitNode(const UnitNode &)
{
}
UnitNode::~UnitNode()
{
//	delete unitObject;
//  unitObject = NULL;

	while( !children.empty() )
	{
		SceneNode* sn = children.back();
		delete sn;
		children.pop_back();
	}
	for(std::map<AnimationType,UnitObject*>::iterator it = unitObjectSwapList.begin();
													  it != unitObjectSwapList.end(); ++it )
	{
		if((it->first) != at_LOD_CUBE)
		{
			delete (it->second);
			it = unitObjectSwapList.erase(it);
			if (it == unitObjectSwapList.end()) break;
		}
	}
}

void UnitNode::SetUnitObject(UnitObject* newUnit)
{
    unitObject = newUnit;
    unitObjectSwapList.insert( pair<AnimationType, UnitObject*>(at_DEFAULT, newUnit) );
}
void UnitNode::SetUnitObject(UnitObject* newUnit, AnimationType at)
{
    unitObjectSwapList.insert( pair<AnimationType, UnitObject*>(at, newUnit) );
}

void UnitNode::SwapTo(AnimationType at, bool fromLODSwap)
{	
	if ( SystemManager::Instance()->GetGraphicsManager()->GetCamera()->GetCameraType() == FIRST_PERSON )
	{
		at = at_DEFAULT;
	}

    map<AnimationType, UnitObject*>::iterator iter = unitObjectSwapList.find(at);
    if ( iter != unitObjectSwapList.end() )
    {
        if (!fromLODSwap) unitObject -> resetAnimationCounter();
        unitObject = (*iter).second;
        if (!fromLODSwap) unitObject -> resetAnimationCounter();
	}else
	{
	}
    if (at != at_LOD_CUBE) currentAnimation = at;
}
void UnitNode::SyncSwapChain()
{
    map<AnimationType, UnitObject*>::iterator iter = unitObjectSwapList.find(at_DEFAULT);
    if ( iter != unitObjectSwapList.end() )
    {// Sync Transformations.
        UnitObject* syncObject = (*iter).second;

		syncObject->UnitStateUpdate();

		D3DXVECTOR4* tintVec = syncObject->GetTint();
        unitObject->assignTint( tintVec->x, tintVec->y, tintVec->z );
        unitObject->SetPosition( syncObject->GetPosition() );
        unitObject->SetRotation( syncObject->GetRotation() );
        unitObject->SetScale   ( syncObject->GetScale()    );
		unitObject->SyncAnimState( syncObject->GetUnitAnimState());
    }
}

void UnitNode::OptimizationCheck( int& lodLevel, bool& frustumCull )
{
    GraphicsManager* gPtr = SystemManager::Instance()->GetGraphicsManager();

    if ( unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().SKYBOX )
    {
        frustumCull = false;
        lodLevel = 0;
        return;
    }

    D3DXMATRIX mat = *(gPtr->d3d_matrixStack->GetTop());
    D3DXVECTOR3 pos = unitObject->GetPosition();
    D3DXVec3TransformCoord(&pos, &pos, &mat); // For Frustum check...

    FrustumObject * frustumObject_Ptr = gPtr->GetFrustumObject();
    float boundingR  = unitObject->GetBoundingRadius();
    bool renderModel = frustumObject_Ptr->CheckSphere(pos.x, pos.y, pos.z, boundingR);

    frustumCull = !renderModel;
    if (frustumCull) frustumObject_Ptr->incCulledObjects();

    CameraObject* camObj = gPtr->GetCamera();
    D3DXVECTOR3 c_Pos = camObj->GetPOffset();

    float distance = 0.0f;

    distance = sqrt((c_Pos.x - pos.x) * (c_Pos.x - pos.x) +
                    (c_Pos.y - pos.y) * (c_Pos.y - pos.y) +
                    (c_Pos.z - pos.z) * (c_Pos.z - pos.z) );

    lodLevel = (int)floor(distance / 50.0f);
}
void UnitNode::UpdateMatrix()
{
    unitObject->GetMatrix(matrix);
}
void UnitNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    GraphicsManager * gPtr = SystemManager::Instance()->GetGraphicsManager();
	if( !SystemManager::Instance()->GetHUDManager()->SplashIsOn() &&
		SystemManager::Instance()->GetCurrentPlayerUnit() == unitObject
		)
    {
        if(NetworkManager::Instance()->getCurrentRobotId() != -1 && gPtr->GetCamera()->GetCameraType() == FIRST_PERSON)                    
            return;
    }

    SyncSwapChain();

    UpdateMatrix(); // NOT REALLY BEING USED FOR THE MATRIX
    stack->Push();
        bool frustumCull = false;
        int  LODLevel = 0;

        OptimizationCheck(LODLevel, frustumCull);

        mn_scale       = unitObject->GetScale();
        mn_rotation    = unitObject->GetRotation();
        mn_translation = unitObject->GetPosition();

        float pitch, yaw ,roll;

        pitch = mn_rotation.x * 0.0174532925f;
	    yaw   = mn_rotation.y * 0.0174532925f;
	    roll  = mn_rotation.z * 0.0174532925f;

        stack->ScaleLocal(mn_scale.x, mn_scale.y, mn_scale.z);
        stack->RotateYawPitchRollLocal( yaw, pitch, roll);

        // CHECK TO SEE IF THIS UNIT IS THE SPARK
        if ( unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().ELECTRICSPARK
          || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().AOE
          || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().METEOR
          || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().BALLS
            )
        {
            animationCounter += 0.1f * dir;
            stack->RotateYawPitchRollLocal( animationCounter, 0, 0);
        }
        stack->Translate(mn_translation.x, mn_translation.y, mn_translation.z);

        /// R ////
        if( !frustumCull && LODLevel < 32 ) //LODLevel < 2
        {// IF NOT CULLED, RENDER THE MODEL.
            if (LODLevel < 16)//2)
            {
                SwapTo( currentAnimation, true );
            }else if ( unitObject->GetModelID() != gPtr->GetMeshManager()->GetMeshSelector().LASER )
            {
                SwapTo( at_LOD_CUBE, true );
            }
            
            calculatedMatrix = *(gPtr->d3d_matrixStack->GetTop());
            if ( drawMode == 0 )
            {
                renderAsReflection = false;
                if( unitObject->GetModel()->isAlphaBlend() )
                {// Needs to ignore Z Buffer, push to back.
                    SceneNode * sn = gPtr->GetWorld();
                    if (sn->isRootNode()) ((RootNode *)(sn))->AppendAlphaChildren(this);
                }else
                {
                    Render(d3dMan);
                }
            } else if (  drawMode == 1 && unitObject->GetModel()->CastsShadow() )
            {// Running Depth Pass
                RenderDepth(d3dMan);
            } else if ( drawMode == 2 )
            {// Draw Reflection
                renderAsReflection = true;
                Render(d3dMan);
            }

            std::vector<SceneNode *>::iterator it;
            for( it = children.begin(); it < children.end(); it++ )
            {
                if ( unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().ELECTRICSPARK
                  
                    )
                {// Special Animation.
                    stack->RotateYawPitchRollLocal( 0, animationCounter, 0);
                    stack->ScaleLocal( ( abs( (animationCounter - 45) )/9.0f + 10.0f ) / 15.0f,
                                       ( abs( (animationCounter - 45) )/9.0f + 10.0f ) / 15.0f,
                                       ( abs( (animationCounter - 45) )/9.0f + 10.0f ) / 15.0f
                                     );
                    if( animationCounter >= 90 ) animationCounter = 0; // dir *= -1;
                }
                if ( !(*it)->isParticleNode() || LODLevel < 2)
                {
                    if (!((*it)->GetID() < 0)) (*it)->Draw( d3dMan, stack, drawMode );
                }
            }
        }

        /// AR ////
    stack->Pop();
}

/***
 *
 */
void UnitNode::Render(D3DManager* d3dMan)
{
    GraphicsManager * gPtr = SystemManager::Instance()->GetGraphicsManager();
    ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
    D3DXMATRIX mat = calculatedMatrix, camMatrix, animMatrix;
    CameraObject* camObj = gPtr->GetCamera();
    if (renderAsReflection)
    {
        camMatrix = camObj->GetReflectionViewMatrix();
    }else
    {
        camObj->GetMatrix(camMatrix);
    }

	if( currentAnimation != this->unitObject->GetUnitAnimState() &&
	   !( unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().ELECTRICSPARK )
	  )
	{
		switch(this->unitObject->GetUnitAnimState())
		{
            case 0: /*printf("swapTo_Default: ");*/ SwapTo( at_DEFAULT ); break;
			case 1: /*printf("swapTo_Walk: ");*/    SwapTo( at_WALK    );  break;
			case 2: /*printf("swapTo_Attack: ");*/  SwapTo( at_ATTACK  ); break;
		}
	}

    if ( isDynamic() &&
       !( unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().ELECTRICSPARK
       || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().TEST_CUBE
       || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().AOE
       || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().METEOR
       || unitObject->GetModelID()  == gPtr->GetMeshManager()->GetMeshSelector().BALLS
       ) )
    {// Assign shader to unit based on shadow mode.
        shader = (gPtr->GetShaderManager()->GetUnitShader());
    }

    bool INSTANCINGWORKING = LoadingManager::useInstancing ;
    if( INSTANCINGWORKING && !renderAsReflection &&
        ( this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().PROBE
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().PROBE_WALK
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().TEST_DROID
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().DROID_WALK
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().TEST_ELECTRICTHING
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().LASER
      ))
    {// Instanced Render
        SceneNode * sn = gPtr->GetWorld();
        RootNode::InstanceConfiguration ic = RootNode::getInstanceConfig();

        ic.shader      = shader;
        ic.modelRef    = unitObject;
        ic.instanceMatrix = calculatedMatrix;
        ic.viewMat        = camMatrix;
        ic.projMat        = d3dMan->GetProjectionMatrix();

        D3DXMatrixIdentity(&animMatrix);

        myRSC.lightMan        = gPtr->GetLightManager();
        myRSC.camObj          = camObj;
        myRSC.tintColor       = (unitObject->GetTint());
        myRSC.animationMatrix = &animMatrix;
        ic.shaderRSC = myRSC;

        if (sn->isRootNode()) ((RootNode *)(sn))->AppendInstancedModel( ic );

    }else
    {// Non Instanced objects.
        if(unitObject->GetModel()->isAlphaBlend())
        {
            d3dMan->TurnZBufferOff(1);
            d3dMan->EnableAlphaBlending(1);
            d3dMan->TurnOffBackFaceCull();
        }

        unitObject->Render( d3dMan->GetDeviceContext() );

        if (shader)
        {// Actual Render call
            LightObject* light_Ptr = gPtr->GetLightManager()->getLight(0);

            myRSC.lightMan  = gPtr->GetLightManager();
            myRSC.camObj    = camObj;
            myRSC.tintColor = (unitObject->GetTint());

            D3DXMatrixIdentity(&animMatrix);
            myRSC.animationMatrix = &animMatrix;

            if( shader->isShadowed() )
            {
              D3DXMATRIX lvm, lpm;
              myRSC.lightViewMatrix = &lvm;
              myRSC.lightProjectionMatrix = &lpm;
              light_Ptr->GetViewMatrix(*myRSC.lightViewMatrix);
              light_Ptr->GetProjectionMatrix(*myRSC.lightProjectionMatrix);
              myRSC.depthResource = SystemManager::Instance()->GetGraphicsManager()->GetRenderTexture()->GetShaderResourceView();
            }
            if(shader->isReflective())
            {
              myRSC.reflectionMatrix = &camObj->GetReflectionViewMatrix();
              myRSC.reflectionResource = SystemManager::Instance()->GetGraphicsManager()->GetRenderTexture2()->GetShaderResourceView();
            }

            if ( unitObject->GetModel()->hasAnimations )
            {// transform mat with animation Matrix
                if (unitObject->GetModel()->hasSkeletalRig())
                {// NOT CURRENTLY WORKING
                    for ( int i = 0; i < 10; ++i )
                    {// POPULATE BONE MATRIX
                        if ( ( (i < ((ModelSkinned*) (unitObject->GetModel()) ) -> multMeshPtr[0].numBones ) ));
                            //myRSC.boneMatrices.push_back(((ModelSkinned*) ( unitObject->GetModel()))->multMeshPtr[0].boneAnimMatrix[i]);
                        else
                        {// Autofill with identity matrices due to error?
                            D3DXMATRIX idMatrix;
                            D3DXMatrixIdentity(&idMatrix);
                            //myRSC.boneMatrices.push_back(idMatrix);
                        }
                    }
                    shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->GetIndexCount(), 0,
                                    mat, camMatrix, d3dMan -> GetProjectionMatrix(),
                                    unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                    myRSC
                                  );
                } else
                {// Multi Meshed Object.
                    for (int i = 0; i < unitObject->GetModel()->GetNumMeshes(); ++i)
                    {
                        //animMatrix = unitObject->GetModel()->multMeshPtr[i].animationMatrix;
                        memcpy(&animMatrix, &unitObject->GetModel()->multMeshPtr[i].animationMatrix, sizeof(D3DXMATRIX));

                        shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->multMeshPtr[i].numVerts,
                                        unitObject->GetModel()->multMeshPtr[i].offset,
                                        mat, camMatrix, d3dMan -> GetProjectionMatrix(),
                                        unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                        myRSC
                                      );
                        if( shader->isShadowed() )
                        {// EXTRA SHADER CALL BECAUSE THE SHADOW SHADER IS STUPID...
                            shader->Render( d3dMan -> GetDeviceContext(),
                                            0,0,
                                            mat, camMatrix, d3dMan -> GetProjectionMatrix(), 
                                            unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                            myRSC
                                            );
                        }
                    }
                }
            }else
            {// Non Animated Mesh
                shader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->GetIndexCount(), 0,
                                mat, camMatrix, d3dMan -> GetProjectionMatrix(), 
                                unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                myRSC
                              );

                if( shader->isShadowed() )
                {// EXTRA SHADER CALL BECAUSE THE SHADOW SHADER IS STUPID...
                    shader->Render( d3dMan -> GetDeviceContext(), 0, 0,
                                    mat, camMatrix, d3dMan -> GetProjectionMatrix(), 
                                    unitObject->GetModel()->GetTextureObject()->GetTextures(), unitObject->GetModel()->GetTextureObject()->numTextures(),
                                    myRSC
                                    );
                }
            }
        }

        if(unitObject->GetModel()->isAlphaBlend())
        {
            d3dMan->RestoreBackFaceCull();
            d3dMan->DisableAlphaBlending();
            d3dMan->TurnZBufferOn();
        }

    }
    // DRAW BOUNDING SPHERE
    bool drawBoundingSphere = LoadingManager::displayBoundingSpheres;
    if ( dbs && drawBoundingSphere )
    {
        if(boundingSphere != NULL)
        {
            myRSC = ShaderObject::createRenderShaderConfig();
            boundingSphere->Render( d3dMan->GetDeviceContext() ); //, stack, drawMode );

            D3DXMATRIX br, animMatrix;
            D3DXMatrixScaling(&br,unitObject->GetBoundingRadius(), unitObject->GetBoundingRadius(), unitObject->GetBoundingRadius());
            D3DXMatrixIdentity(&animMatrix);
            myRSC.animationMatrix = &animMatrix;

            shader->Render( d3dMan -> GetDeviceContext(), boundingSphere->GetIndexCount(), 0,
                            br * mat, camMatrix, d3dMan->GetProjectionMatrix(), 
                            boundingSphere->GetTextureObject()->GetTextures(), boundingSphere->GetTextureObject()->numTextures(),
                            myRSC
                            );
        }
    }



/////////// Check intersect with camera
    if (
        GetUnitObject()->GetModelID() != gPtr->GetMeshManager()->GetMeshSelector().SKYBOX
     && GetUnitObject()->GetModelID() != gPtr->GetMeshManager()->GetMeshSelector().LASER
     && GetUnitObject()->GetModelID() != gPtr->GetMeshManager()->GetMeshSelector().TABLET
     && GetUnitObject()->GetModelID() != gPtr->GetMeshManager()->GetMeshSelector().TABLET2
        )
    {
        D3DXVECTOR3 cameraDir = camObj->GetLookAt() - camObj->GetPosition();//GetPOffset();
        D3DXVECTOR3 cameraPos = camObj->GetPosition();//GetPOffset();
        D3DXVECTOR3 oMinusC = cameraPos - GetUnitObject()->GetPosition();

        // compute coefficients
        float a = D3DXVec3Dot     (&cameraDir, &cameraDir);
        float b = 2 * D3DXVec3Dot (&cameraDir, &oMinusC);
        float c =     D3DXVec3Dot (&oMinusC, &oMinusC) - ( unitObject->GetBoundingRadius() * unitObject->GetBoundingRadius() );

        // find descriminant
        float disc = b*b - 4*a*c;

        // ray misses sphere
        if (disc < 0) intersectionPoint = -1;
        else
        {// Ray Does intersect Sphere
            float distSqrt = sqrtf(disc);
            float q;
            if (b < 0) q = (-b - distSqrt/2.0f);
            else       q = (-b + distSqrt/2.0f);

            float t0 = q/a;
            float t1 = c/q;

            if (t0 > t1)
            {// swap if t0 > t1
                float temp = t0; t0 = t1; t1 = temp;
            }

            if (t1 < 0) intersectionPoint = -1;
            else
            {// Found Intersection
                if (t0 < 0)  intersectionPoint = t1;
                else         intersectionPoint = t0;
            }
            if ( clientStarted && 
               ( GetUnitObject()->GetUnitId() != NetworkManager::Instance()->getCurrentRobotId() 
              && GetUnitObject()->GetUnitId() != NetworkManager::Instance()->getCurrentSparkId()
              && (SystemManager::Instance()->GetCurrentTarget().closestDist < 0
              ||  SystemManager::Instance()->GetCurrentTarget().closestDist > intersectionPoint) ) )
              if (intersectionPoint > 0) SystemManager::Instance()->SetCurrentTarget(*(this->unitObject), intersectionPoint);
        }
    }

}
/***
 * On the first pass render to the depth texture target.
 */
void UnitNode::RenderDepth(D3DManager* d3dMan)
{
    D3DXMATRIX lightViewMatrix, lightProjectionMatrix, animMatrix;
    GraphicsManager * gPtr = SystemManager::Instance()->GetGraphicsManager();
    LightObject* light_Ptr = gPtr->GetLightManager()->getLight(0);
    ShaderObject * depthShader = gPtr->GetDepthShader();
    D3DXMATRIX mat = calculatedMatrix;
    ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();

    light_Ptr -> GetViewMatrix(lightViewMatrix);
    light_Ptr -> GetProjectionMatrix(lightProjectionMatrix);

    bool INSTANCINGWORKING = LoadingManager::useInstancing ;
    if(   INSTANCINGWORKING && 
       (  this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().PROBE
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().TEST_DROID
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().DROID_WALK
       || this->unitObject->GetModelID() == gPtr->GetMeshManager()->GetMeshSelector().TEST_ELECTRICTHING
       ))
    {// Instanced Render
        SceneNode * sn = gPtr->GetWorld();
        RootNode::InstanceConfiguration ic = RootNode::getInstanceConfig();

        ic.shader         = depthShader; //shader;
        ic.modelRef       = unitObject;
        ic.instanceMatrix = calculatedMatrix;
        ic.viewMat        = lightViewMatrix;
        ic.projMat        = lightProjectionMatrix;

        D3DXMatrixIdentity(&animMatrix);

        ic.shaderRSC = myRSC;
        if (sn->isRootNode()) ((RootNode *)(sn))->AppendInstancedModel( ic );

    }else
    {
        unitObject->Render( d3dMan->GetDeviceContext() );
        depthShader->Render( d3dMan -> GetDeviceContext(), unitObject->GetModel()->GetIndexCount(), 0,
                             mat, lightViewMatrix, lightProjectionMatrix, 
                             NULL, 0,
                             myRSC
                           );
    }
}

void UnitNode::setDynamic(bool dynamic){ dynamicShaderSet = dynamic; }
void UnitNode::displayBoundingSphere(bool dbs){ this->dbs = dbs; }

bool UnitNode::checkForRemoval() { 
    bool toBeRemoved = true;
    map<AnimationType, UnitObject*>::iterator iter = unitObjectSwapList.find(at_DEFAULT);
    if ( iter != unitObjectSwapList.end() ) toBeRemoved = iter->second->isToBeRemoved();
    else                                    toBeRemoved = GetUnitObject()->isToBeRemoved();
    return toBeRemoved;
}