//PVecto4.cppp

#include "PVector4.h"
#include <math.h>

//Modeled after the d3d10math header
PVector4::PVector4(){
}
PVector4::~PVector4(){
}

/////////////////////////////////////////////
// Assignment operators
/////////////////////////////////////////////
void PVector4::operator += ( const PVector4& other ){
    x += other.x;
    y += other.y;
    z += other.z;
    w += other.w;
}
void PVector4::operator -= ( const PVector4& other ){
    x -= other.x;
    y -= other.y;
    z -= other.z;
    w -= other.w;
}
void PVector4::operator *= ( const PVector4& other ){ // point-wise multiplication
    x *= other.x;
    y *= other.y;
    z *= other.z;
    w *= other.w;
}
void PVector4::operator /= ( const PVector4& other ){ // point-wise division
    x /= (other.x != 0)?other.x:1;
    y /= (other.y != 0)?other.y:1;
    z /= (other.z != 0)?other.z:1;
    w /= (other.w != 0)?other.w:1;
}
void PVector4::operator *= ( float scalar ){
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;               
}
void PVector4::operator /= ( float scalar ){
    if(scalar != 0.0f){
        float inv = 1.0f/scalar; // one division is better than doing 4 division
        x*=inv;
        y*=inv;
        z*=inv;
        w*=inv;
    }
}

/////////////////////////////////////////////
// Basic Arithmatic
/////////////////////////////////////////////
PVector4 PVector4::operator + ( const PVector4& other ){
    return PVector4(x+other.x, y+other.y, z+other.z, w+other.w);
}
PVector4 PVector4::operator - ( const PVector4& other ){
    return PVector4(x-other.x, y-other.y, z-other.z, w-other.w);
}
PVector4 PVector4::operator * ( float scalar ){
    return PVector4(x*scalar, y*scalar, z*scalar, w*scalar);
}
PVector4 PVector4::operator / ( float scalar ){
    if(scalar != 0.0f){
        float inv = 1.0f/scalar; // one division is better than doing 4 division
        return PVector4(x*inv, y*inv, z*inv, w*inv);
    }
    //need to return an error message
    // if the scalar is 0 then we do nothing to the vector
    return PVector4(x,y,z,w);
}
PVector4 PVector4::operator-(){
    return PVector4(-x,-y,-z,-w);
}

/////////////////////////////////////////////
//Boolean Operators
/////////////////////////////////////////////
bool PVector4::operator == ( const PVector4& other ) const{
    return ( x==other.x && y==other.y && z==other.z && w==other.w );
}
bool PVector4::operator != ( const PVector4& other ) const{
    return ( x!=other.x && y!=other.y && z!=other.z && w!=other.w );
}

/////////////////////////////////////////////
//USEFUL STUFF
/////////////////////////////////////////////
void PVector4::normalize(){
    float length2 = this->length2();
    *this /= length2;        
}

float PVector4::length(){
    float temp = this->length2();
    if(temp > 0)
        return sqrt(temp);
    return 0;
}
float PVector4::length2(){
    return this->dot(*this);
}

float PVector4::dot(PVector4& other){        
    return x*other.x + y*other.y + z*other.z + w*other.w;
}

PVector4 PVector4::cross(PVector4& b, PVector4& c){        
    float Pxy = b.x*c.y - c.x*b.y;
    float Pxz = b.x*c.z - c.x*b.z;
    float Pxw = b.x*c.w - c.x*b.w;
    float Pyz = b.y*c.z - c.y*b.z;
    float Pyw = b.y*c.w - c.y*b.w;
    float Pzw = b.z*c.w - c.z*b.w;
    return PVector4(
        y*Pzw - z*Pyw + w*Pyz,
        z*Pxw - x*Pzw + w*Pxz,
        x*Pyw - y*Pxw + w*Pxy,
        y*Pxz - x*Pyz + z*Pxy
        );
}

std::string PVector4::toString() const{
    std::stringstream out;
    out << '(' << x << ',' << y << ',' << z << ',' << w << ',' << ')';
    return out.str();
}