#include "HUDMenuItem.h"
#include "HUDGraphic.h"
#include "HUDText.h"
#include "HUDManager.h"

#include <iostream>

using namespace std;

HUDMenuItem::HUDMenuItem()
{
	displayOn = true;
    clickable = false;
    originX = originY = 0;
	originSet = false;
	alphaFloor = 0.0f;
    alphaCap   = 1.0f;

    tintColor.x = tintColor.y = tintColor.z = 1.f;
}
HUDMenuItem::~HUDMenuItem()
{
}
/***
 *
 */
bool HUDMenuItem::Initialize( )
{
    cout << "[HUDMenuButton] Initializing with " << getItemConfig().size() << " items" << endl;

    for (unsigned int i = 0; i < getItemConfig().size(); ++i)
    {
        HUDDisplayable * hd = HUDManager::Instance()->GetHUDDisplayables().at( getItemConfig().at(i).id );
        DisplayableElements.push_back( hd );

        // temp
        if ( hd->isGraphic() )
        {
            int w, h;
            ((HUDGraphic *) hd)->GetBitmapDimensions(w,h);
            if ( w > mi_width )
            {
                mi_width = (float)w;
            }
            if ( h > mi_height )
            {
                mi_height = (float)h;
            }

			if(!originSet)
			{
				if (this->originX < getItemConfig().at(i).positionX)
				{
					this->originX = (float) getItemConfig().at(i).positionX;
				}
				if (this->originY < getItemConfig().at(i).positionY)
				{   
					this->originY = (float) getItemConfig().at(i).positionY;
				}
				originSet = true;
			}
        }
    }
    return true;
}

HUDMenuItem::HUDItemConfig HUDMenuItem::createHUDItemConfig(int id, int x, int y)
{
    HUDItemConfig retHUDConfig;

    retHUDConfig.id        = id;
    retHUDConfig.positionX = x;
    retHUDConfig.positionY = y;

    return retHUDConfig;
}

vector<HUDMenuItem::HUDItemConfig>& HUDMenuItem::getItemConfig()
{
    return HUDItemConfigVector;
}

void HUDMenuItem::getBounds(float & width, float & height)
{
    width  = mi_width;
    height = mi_height;
}
void HUDMenuItem::getPosition(float & x, float & y)
{
    x = originX;
    y = originY;
}

void HUDMenuItem::setDisplay(bool display){ this->displayOn = display; }
void HUDMenuItem::setClickable(bool clickable) { this->clickable = clickable; }
void HUDMenuItem::setBounds(float w, float h)
{
}
void HUDMenuItem::setPosition(float x, float y, int index)
{
    if ( index < 0 )
    {
        originX = x;
        originY = y;
    }
    else
    {
        if (index < (int) getItemConfig().size())
        {
            getItemConfig().at(index).positionX = (int)x;
            getItemConfig().at(index).positionY = (int)y;
        }
    }
}
void HUDMenuItem::setAlphaCap(float a){ alphaCap = a; }

bool HUDMenuItem::isToBeDisplayed()
{
    return this->displayOn;
}
bool HUDMenuItem::isClickable()
{
    return this->clickable;
}
bool HUDMenuItem::isInBounds( float targetX, float targetY )
{
    bool result = true;
    result =  (targetX > this->originX)  && (targetX < (this->originX + this->mi_width));
    result =  result && ((targetY > this->originY)  && (targetY < (this->originY + this->mi_height)));

    return result;
}

void HUDMenuItem::setOnClickFunction(void (* clickFunction)() )
{
    this->m_clickFunction = clickFunction;
    this->clickable = true;
}
void HUDMenuItem::onClickEvent()
{
    (m_clickFunction)();
}

int HUDMenuItem::getTotalIndexCount()
{
    int totalIndices = 0;
    for(unsigned int j = 0; j < DisplayableElements.size(); ++j)
    {
        if ( DisplayableElements.at(j)->isGraphic() )
        {
            totalIndices += ((HUDGraphic*) DisplayableElements.at(j))->GetIndexCount();
        }else if ( DisplayableElements.at(j)->isText() )
        {
            totalIndices += ((HUDText*) DisplayableElements.at(j))->getSentence()->indexCount; //GetIndexCount();
        }
    }
    return totalIndices;
}

void HUDMenuItem::Render( D3DManager * d3dMan, //ID3D11DeviceContext* d3dC,
                            D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX orthoMatrix)
{
    if( isToBeDisplayed() )
    {
        D3DXMATRIX testGUIViewMatrix;
        D3DXVECTOR3 testPosition = D3DXVECTOR3( 0.0, 0.0, -10.0f ); //D3DXVECTOR3(0,0,0);
        D3DXVECTOR3 testLookAtVec = D3DXVECTOR3(0,0,0);
        D3DXVECTOR3 testUp = D3DXVECTOR3(0,1,0);
        D3DXMatrixLookAtLH(&testGUIViewMatrix, &testPosition, &testLookAtVec, &testUp);

        for(unsigned int j = 0; j < DisplayableElements.size(); ++j)
        {
            d3dMan->TurnZBufferOff( 0 );
            if   ( DisplayableElements.at(j)->hasAlphaComponent() ) d3dMan->EnableAlphaBlending(0);
            else                                                  d3dMan->DisableAlphaBlending();

            if ( DisplayableElements.at(j)->isGraphic() )
            {// Use Graphics Render
                ((HUDGraphic*) DisplayableElements.at(j))->
                    Render( d3dMan->GetDeviceContext(), getItemConfig().at(j).positionX, getItemConfig().at(j).positionY );

                ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();

                D3DXVECTOR4 textColor = D3DXVECTOR4( tintColor.x, tintColor.y, tintColor.z, alphaFloor );
                myRSC.tintColor = &( textColor );
                myRSC.alphaCap  = alphaCap;
                HUDManager::Instance() -> GetShader() ->
                    Render( d3dMan->GetDeviceContext(), ((HUDGraphic*) DisplayableElements.at(j))->GetIndexCount(), 0,
                            worldMatrix, testGUIViewMatrix, orthoMatrix,
                            ((HUDGraphic*) DisplayableElements.at(j))->GetTextureObject()->GetTextures(),
                            ((HUDGraphic*) DisplayableElements.at(j))->GetTextureObject()->numTextures(),
                            myRSC
                           );

            }else if ( DisplayableElements.at(j)->isText() )
            {// Use Text Render
                ((HUDText*) DisplayableElements.at(j))-> Render( d3dMan->GetDeviceContext(), getItemConfig().at(j).positionX, getItemConfig().at(j).positionY ); //edit here!

                ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
                D3DXVECTOR4 textColor = D3DXVECTOR4(
                                                    ((HUDText*) DisplayableElements.at(j))->getSentence()->red,
                                                    ((HUDText*) DisplayableElements.at(j))->getSentence()->green,
                                                    ((HUDText*) DisplayableElements.at(j))->getSentence()->blue,
                                                    0.0f
                                                   );
                myRSC.tintColor = &( textColor );
                // Render Shader
                HUDManager::Instance() -> GetShader() ->
                    Render( d3dMan->GetDeviceContext(), ((HUDText*) DisplayableElements.at(j))->getSentence()->indexCount, 0,
                            worldMatrix, testGUIViewMatrix, orthoMatrix,
                            ((HUDText*) DisplayableElements.at(j))->getFont()->GetTextureObject()->GetTextures(),
                            ((HUDText*) DisplayableElements.at(j))->getFont()->GetTextureObject()->numTextures(),
                            myRSC );
            }

            if ( DisplayableElements.at(j)->hasAlphaComponent() ) d3dMan->DisableAlphaBlending();
            d3dMan->TurnZBufferOn();
        }
    }else 
    {
    }
}

void HUDMenuItem::setTint( float r, float g, float b )
{
    tintColor.x = r;
    tintColor.y = g;
    tintColor.z = b;
}

bool HUDMenuItem::isButton()  { return false; }
bool HUDMenuItem::isMap()     { return false; }
bool HUDMenuItem::isCounter() { return false; }
bool HUDMenuItem::isBar()     { return false; }