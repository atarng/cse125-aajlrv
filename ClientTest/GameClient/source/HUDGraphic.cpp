////////////////////////////////////////////////////////////////////////////////
// Filename: HUDGraphic.cpp
// Contributors: Alfred Tarng, Roy Chen
// Purpose:
//
// TODO: Allow for circular partitioning of Graphic.
////////////////////////////////////////////////////////////////////////////////
#include "HUDGraphic.h"
#include "TextureObject.h"

#include <iostream>

#include <math.h>
#define PI 3.14159265f

HUDGraphic::HUDGraphic()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_Texture = 0;

    m_HudPartitionType = hpt_SQUARE;
    m_PartitionPositionX = m_PartitionPositionY = .5;
    m_partitionRadius = .25;
    m_partitionOffset = 0.f;
}
HUDGraphic::HUDGraphic(const HUDGraphic& other)
{
}
HUDGraphic::~HUDGraphic()
{
    Shutdown();
}

bool HUDGraphic::Initialize( ID3D11Device* device,
                             int screenWidth, int screenHeight,
                             int bitmapWidth, int bitmapHeight)
{
	bool result;
    deviceRef = device;

	// Store the screen size.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Store the size in pixels that this bitmap should be rendered at.
	m_InitWidth = m_bitmapWidth = bitmapWidth;
	m_bitmapHeight = bitmapHeight;

	// Initialize the previous rendering position to negative one.
	m_previousPosX = -1;
	m_previousPosY = -1;
	m_previousWidth = bitmapWidth;

	// Initialize the vertex and index buffers.
	result = InitializeBuffers(device);
	if(!result)
	{
		return false;
	}

	return true;
}
bool HUDGraphic::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
    if( m_HudPartitionType == hpt_CIRCLE)
    	m_indexCount = m_vertexCount = 24; // Vertices for an octagonal approximation of a Circle
    else
    	m_indexCount = m_vertexCount = 6;  //Vertices for a square

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices) return false;

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices) return false;

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * m_vertexCount));

	// Load the index array with data.
	for(int i=0; i<m_indexCount; i++) indices[i] = i;

	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result)) return false;

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result)) return false;

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

bool HUDGraphic::Render(ID3D11DeviceContext* deviceContext, int positionX, int positionY)
{
	bool result;

	// Re-build the dynamic vertex buffer for rendering to possibly a different location on the screen.
	result = UpdateBuffers(deviceContext, positionX, positionY);
	if(!result)	return false;

	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return true;
}
void HUDGraphic::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

bool HUDGraphic::UpdateBuffers(ID3D11DeviceContext* deviceContext, int positionX, int positionY)
{
	float left, right, top, bottom;
	VertexType* vertices;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;
	HRESULT result;

	// If the position we are rendering this bitmap to has not changed then don't update the vertex buffer since it
	// currently has the correct parameters.
	if(!m_HudPartitionType == hpt_CIRCLE && (positionX == m_previousPosX) && (positionY == m_previousPosY) && (m_previousWidth == m_bitmapWidth) ) return true;
	
	// If it has changed then update the position it is being rendered to.
	m_previousPosX = positionX;
	m_previousPosY = positionY;
	m_previousWidth = m_bitmapWidth;

	// Calculate the screen coordinates of the left side of the bitmap.
	left = (float)((m_screenWidth / 2) * -1) + (float)positionX;
	// Calculate the screen coordinates of the right side of the bitmap.
	right = left + (float)m_bitmapWidth;
	// Calculate the screen coordinates of the top of the bitmap.
	top = (float)(m_screenHeight / 2) - (float)positionY;
	// Calculate the screen coordinates of the bottom of the bitmap.
	bottom = top - (float)m_bitmapHeight;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices) return false;

	// Load the vertex array with data.
    if( m_HudPartitionType == hpt_CIRCLE )
    {
        if (m_vertexCount < 24)
            std::cerr << "You have a problem" << std::endl;

        memset( vertices, 0 , sizeof(VertexType) * m_vertexCount );

        float lengthOfSide = m_bitmapWidth * (sqrt(2.0f)/(2+sqrt(2.0f))) ;
        float centerXOffset = m_bitmapWidth/2.f, centerYOffset = m_bitmapHeight/2.f;

        float v_offset = 2.00;
        for(int i=0; i<8; ++i)
        {// VERTEX POSITIONS
            vertices[i*3+0].position = D3DXVECTOR3((left + centerXOffset) + cos(PI/4 *(v_offset))*centerXOffset,
                                                   (top  - centerYOffset) - sin(PI/4 *(v_offset))*centerYOffset,
                                                   0.0f);  // Center
            vertices[i*3+1].position = D3DXVECTOR3((left + centerXOffset) + cos(PI/4 *(v_offset+1))*centerXOffset,
                                                   (top  - centerYOffset) - sin(PI/4 *(v_offset+1))*centerYOffset,
                                                   0.0f);  // Center
            vertices[i*3+2].position = D3DXVECTOR3((left + centerXOffset), top - centerYOffset, 0.0f);  // Center
            v_offset++;
        }
        float offset = 2.00;
        for (int i = 0; i < 8; ++i)
        {// UV PARTITION
            vertices[i*3].texture    = D3DXVECTOR2( m_PartitionPositionX + cos(PI/4 * (offset  +m_partitionOffset) )*m_partitionRadius,
                                                    m_PartitionPositionY + sin(PI/4 * (offset  +m_partitionOffset) )*m_partitionRadius ); //
	        vertices[i*3+1].texture  = D3DXVECTOR2( m_PartitionPositionX + cos(PI/4 * (offset+1+m_partitionOffset) )*m_partitionRadius,
                                                    m_PartitionPositionY + sin(PI/4 * (offset+1+m_partitionOffset) )*m_partitionRadius );
	        vertices[i*3+2].texture  = D3DXVECTOR2(m_PartitionPositionX, m_PartitionPositionY); // CENTER OF AREA
            ++offset;
        }

    }else
    {
        // First triangle.
	    vertices[0].position = D3DXVECTOR3(left, top, 0.0f);  // Top left.
	    vertices[1].position = D3DXVECTOR3(right, bottom, 0.0f);  // Bottom right.
	    vertices[2].position = D3DXVECTOR3(left, bottom, 0.0f);  // Bottom left.

        vertices[0].texture = D3DXVECTOR2(0.0f, 0.0f);
	    vertices[1].texture = D3DXVECTOR2(((float)m_bitmapWidth/(float)m_InitWidth), 1.0f);
	    vertices[2].texture = D3DXVECTOR2(0.0f, 1.0f);

	    // Second triangle.
	    vertices[3].position = D3DXVECTOR3(left, top, 0.0f);  // Top left.
	    vertices[4].position = D3DXVECTOR3(right, top, 0.0f);  // Top right.
	    vertices[5].position = D3DXVECTOR3(right, bottom, 0.0f);  // Bottom right.

        vertices[3].texture = D3DXVECTOR2(0.0f, 0.0f);
	    vertices[4].texture = D3DXVECTOR2(((float)m_bitmapWidth/(float)m_InitWidth), 0.0f);
	    vertices[5].texture = D3DXVECTOR2(((float)m_bitmapWidth/(float)m_InitWidth), 1.0f);
    }
	// Lock the vertex buffer so it can be written to.
	result = deviceContext->Map(m_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
    {
        std::cerr << "[HUDGraphic] Failed to map vertex buffers." << std::endl;
        return false;
    }

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * m_vertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(m_vertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	return true;
}

int HUDGraphic::GetIndexCount()
{
	return m_indexCount;
}
TextureObject * HUDGraphic::GetTextureObject() { return m_Texture; }
void HUDGraphic::SetTextureObject(TextureObject& toRef) { m_Texture = &toRef; }

void HUDGraphic::Shutdown()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();
	return;
}
void HUDGraphic::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}

void HUDGraphic::GetBitmapDimensions(int & retWidth, int & retHeight){
    retWidth  = m_bitmapWidth;
    retHeight = m_bitmapHeight;
}
void HUDGraphic::SetHUDPartitionType(HUDPartitionType hpt)
{
    m_HudPartitionType = hpt;
}

void HUDGraphic::ChangeWidth(int newWidth)
{
	m_bitmapWidth = newWidth;
}
void HUDGraphic::ChangePartitionLocation(float newPPosX, float newPPosY)
{
    m_PartitionPositionX = newPPosX;
    m_PartitionPositionY = newPPosY;
}

bool HUDGraphic::isGraphic() { return true; }