////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsManager.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICS_MANAGER_H_
#define _GRAPHICS_MANAGER_H_

//////////////
// INCLUDES //
//////////////
#include <windows.h>

/***********************************
 * Local includes
 ***********************************/
#include "Timer.h"
#include "D3DManager.h"

#include "SceneNode.h"
#include "FrustumObject.h"
#include "UnitObject.h"

#include "ShaderManager.h"

#include "CameraManager.h"
#include "LightManager.h"

#include "RenderTexture.h"

class Unit;
class ParticleSystemManager;
class TextureManager;
class MeshManager;
class UnitNode;
class HUDManager;

/////////////
// GLOBALS //
/////////////

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsManager
// TODO: Make this a singleton? NEVER!
////////////////////////////////////////////////////////////////////////////////
class GraphicsManager
{
private:
    //INSTANCE
    static GraphicsManager * m_Instance;

    Timer graphicsTimer; //used to keep track of fps

    /*************** SCENE VARIABLES ****************/
    SceneNode* root;
    SceneNode* SkyBox;

    FrustumObject  *    frustumObj_Ptr;
    RenderTexture  *    renderTexture_Ptr, *renderTexture_Ptr2;
    /***************  End SCENE VARIABLES ****************/

    /*************** Manager Variables *******************/
    D3DManager*         D3DManager_Ptr;
    CameraManager *     cameraManager_Ptr;
    LightManager *      lightManager_Ptr;
    ShaderManager *     shaderManager_Ptr;
    MeshManager *       meshManager_Ptr;
    TextureManager *    textureManager_Ptr;

    HUDManager*            hudManager_Ptr;
    ParticleSystemManager* particleSysMan_Ptr;
    /*************** END: Manager Variables *******************/

    // Private Member Functions
    bool Render(float deltaTime);

    /*************** TEST VARIABLES **************************/
    ShaderObject   *    depthShader;

    UnitObject     *    defaultUnit; // this is a test unit until we can get the spark working.
    UnitNode       *    testNode;

	vector<Unit*> SatelliteVector;

    // Test Function
    bool RenderShadowPatch(float deltaTime);
    /*************** END: TEST VARIABLES *********************/

public:
	GraphicsManager();
	GraphicsManager(const GraphicsManager&);
	~GraphicsManager();

    ID3DXMatrixStack * d3d_matrixStack;	

    static GraphicsManager* Instance();

	bool Initialize(int, int, HWND);
	void Shutdown();
	bool Frame();

    inline CameraObject * GetCamera()       { return cameraManager_Ptr->getCurrentCamera(); }
    inline SceneNode    * GetWorld()        { return root; }

    inline D3DManager     * GetD3DManager()       { return D3DManager_Ptr; }
    inline LightManager   * GetLightManager()     { return lightManager_Ptr; }
    inline MeshManager    * GetMeshManager()      { return meshManager_Ptr; }
    inline ShaderManager  * GetShaderManager()    { return shaderManager_Ptr; }
    inline TextureManager * GetTextureManager()   { return textureManager_Ptr; }
    inline ParticleSystemManager * GetParticleSystemManager() { return particleSysMan_Ptr; };

    inline FrustumObject  * GetFrustumObject()    { return frustumObj_Ptr; };    
    inline ShaderObject   * GetDepthShader()      { return depthShader; };

    // Holds Resource for Depth Shading.
    inline RenderTexture*   GetRenderTexture()    { return renderTexture_Ptr; }
    inline RenderTexture*   GetRenderTexture2()   { return renderTexture_Ptr2; }

    //// TEST FUNCTIONS
    void CreateExplosion(float, float, float, int explosionType = 0);
    void SwapUnitChain();
	void AddSatellites();
    inline UnitObject* getDefaultUnit() { return  defaultUnit; }
    inline UnitNode*   getTestNode()    { return  testNode; }
};


#endif