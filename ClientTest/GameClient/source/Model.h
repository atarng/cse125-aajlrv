////////////////////////////////////////////////////////////////////////////////
// Filename: Model.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODEL_H_
#define _MODEL_H_

//////////////
// INCLUDES //
//////////////
#include "D3DManager.h"

#include <fstream>
#include <vector>

#include <assimpCollada/assimp.h>
#include "AnimEvaluator.h"

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "Mesh.h"

using namespace std;

class Model : public Mesh
{
private:
	struct VertexType // "Type"?
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
//        D3DXVECTOR4 color;
        D3DXVECTOR3 tangent;  //###
        D3DXVECTOR3 binormal; //###
	};
 //struct VertexType_appendBump // "Type"?
 //{
 //     D3DXVECTOR3 tangent;  //###
 //     D3DXVECTOR3 binormal; //###
 //   };
    struct ModelType      // "Type"?
	{
		float x, y, z;    // position
		float tu, tv;     // uv
		float nx, ny, nz; // normals

//        float r, g, b;

        float tx, ty, tz; // tangent position
        float bx, by, bz; // normalmap position
	};
    struct MultMeshObject
    {
        ModelType* meshObj;
        int numVerts;
        int offset;

        //aiMatrix4x4 aiMat; //
        D3DXMATRIX  modelRelativeMatrix;  //
        D3DXMATRIX  animationMatrix;

        aiBone * bones;
        int numBones;
    };

////////
    struct TempVertexType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	struct VectorType
	{
		float x, y, z;
	};

    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	ModelType* m_model;
    int numMeshes;
    TextureObject* m_Textures;
////

    bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();

	void RenderBuffers(ID3D11DeviceContext*);

	bool LoadModel(char*, int);
	void ReleaseModel();

// ASSIMPLOADER
    const aiScene * scene;
    AnimEvaluator * animEvaluator;

    ID3D11Device  * deviceRef;
    
    void CalculateModelVectors();
    void CalculateTangentBinormal(TempVertexType, TempVertexType, TempVertexType, VectorType&, VectorType&);
	void CalculateNormal(VectorType, VectorType, VectorType&);

    bool evalOnce;

protected:
    bool skeletalRigged, drawAsWireFrame, alphaBlend, castsShadow;

public:
	Model();
	Model(const Model&);
	~Model();

    virtual bool Initialize(ID3D11Device*, char* mesh, int fileType);
	virtual void Shutdown();

	virtual void Render(ID3D11DeviceContext*, double* animationFrame = NULL);

	virtual int GetIndexCount();
    virtual int GetNumMeshes();
    virtual TextureObject* GetTextureObject();
    virtual void SetTextureObject(TextureObject &);

    bool hasAnimations;
    MultMeshObject * multMeshPtr;

    virtual bool hasSkeletalRig();

    virtual void setDrawAsWireFrame(bool);
    virtual bool isDrawAsWireFrame();

    virtual void  setAlphaBlend(bool);
    virtual bool  isAlphaBlend();

    inline void setEval(){ evalOnce = true; };

    inline bool CastsShadow() { return castsShadow; };
    void        SetNoShadow();
};
#endif