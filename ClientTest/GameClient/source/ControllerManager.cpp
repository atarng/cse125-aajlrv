////////////////////////////////////////////////////////////////////////////////
// Filename: ControllerManager.cpp
// Contributors: Roy Chen
// Purpose: 
////////////////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "SystemManager.h"

#include "LoadingManager.h"
#include "ControllerManager.h"
#include "InputManager.h"
#include "InputHandler.h"
#include <iostream>

ControllerManager::ControllerManager()
{    
}
ControllerManager::ControllerManager(const ControllerManager& other)
{
}
ControllerManager::~ControllerManager()
{
}

ControllerManager* ControllerManager::m_Instance = NULL;
ControllerManager* ControllerManager::Instance()
{
    if (!m_Instance) m_Instance = new ControllerManager;
    return m_Instance;
}
bool ControllerManager::Initialize()
{  
	start = false;
	back = false;
	space=false;
	shift = false;
	left_shoulder = false;
	right_shoulder = false;
	w = false;
	a = false;
	s = false;
	d = false;
	trigger = false;
	sw = false;
	z = false;
	q = false;
	im = InputManager::Instance();
	DWORD dwResult;
	ZeroMemory ( &state, sizeof(XINPUT_STATE));

	dwResult = XInputGetState (0, &state);

	if (dwResult != ERROR_SUCCESS) {
		return false;
	}
	return true;
}
void ControllerManager::Frame()
{  
	XInputGetState (0, &state);
	
	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_START) {
		
		if( !start) {
			start = true;
			HandleKey(13);
		}
	}
	else {
		start = false;
	}

	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) {
		
		if( !back) {
			back = true;
			im->KeyDown(VK_ESCAPE);
		}
	}
	else {
		im->KeyUp(VK_ESCAPE);
		back = false;
	}

	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_A) {
		
		if( !space) {
			space = true;
			im->KeyDown(VK_SPACE);
		}
	}
	else {
		im->KeyUp(VK_SPACE);
		space = false;
	}

	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_X) {
		
		if( !shift) {
			shift = true;
			im->KeyDown(VK_SHIFT);
		}
	}
	else {
		im->KeyUp(VK_SHIFT);
		shift = false;
	}

	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) {
		
		if( !left_shoulder) {
			left_shoulder= true;
			im->KeyDown(VK_F1);
		}
	}
	else {
		im->KeyUp(VK_F1);
		left_shoulder = false;
	}

	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) {
		
		if( !right_shoulder) {
			right_shoulder = true;
			im->KeyDown(VK_TAB);
		}
	}
	else {
		im->KeyUp(VK_TAB);
		right_shoulder = false;
	}
	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) {
		w = true;
		im->KeyDown(VK_W);
	}
	else {
		if (w) {
			im->KeyUp(VK_W);
			w = false;
		}
	}
	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) {
		a = true;
		im->KeyDown(VK_A);
	}
	else {
		if (a) {
			im->KeyUp(VK_A);
			a = false;
		}
	}
	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) {
		s = true;
		im->KeyDown(VK_S);
	}
	else {
		if (s) {
			im->KeyUp(VK_S);
			s = false;
		}
	}
	if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) {
		d = true;
		im->KeyDown(VK_D);
	}
	else {
		if (d) {
			im->KeyUp(VK_D);
			d = false;
		}
	}

	
	if (state.Gamepad.bLeftTrigger && state.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) {
		trigger = true;
		im->UpdateMouseState(true, LEFT);
	}
	else {
		if (trigger) {
			im->UpdateMouseState(false, LEFT);
			trigger = false;
		}
}
	
	if (state.Gamepad.bRightTrigger && state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) {
		sw = true;
		im->UpdateMouseState(true, RIGHT);
	}
	else {
		if (sw) {
			sw = false;
			im->UpdateMouseState(false, RIGHT);
		}
	}

	// Check to make sure we are not moving during the dead zone (taken from kmcgrail.com tutorial)

      if( (state.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
            state.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
            (state.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
            state.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) )
            {    
                  state.Gamepad.sThumbLX = 0;
                  state.Gamepad.sThumbLY = 0;
            }

      if( (state.Gamepad.sThumbRX < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE &&
            state.Gamepad.sThumbRX > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) &&
            (state.Gamepad.sThumbRY < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE &&
            state.Gamepad.sThumbRY > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) )
            {
                  state.Gamepad.sThumbRX = 0;
                  state.Gamepad.sThumbRY = 0;
            }
	  if (state.Gamepad.sThumbRX || state.Gamepad.sThumbRY) {
		  im->UpdateMousePosition(-1*state.Gamepad.sThumbRX/5000.0f, state.Gamepad.sThumbRY/5000.0f);
	  }
	  if (state.Gamepad.sThumbLY < 0) {
			z = true;
			im->KeyDown(VK_Z);
	  }
	  else {
		if (z) {
			im->KeyUp(VK_Z);
			z = false;
		}
	  }
	  if (state.Gamepad.sThumbLY > 0) {
			q = true;
			im->KeyDown(VK_Q);
	  }
	  else {
		if (q) {
			im->KeyUp(VK_Q);
			q = false;
		}
	}
}