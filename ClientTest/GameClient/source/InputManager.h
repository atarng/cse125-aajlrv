////////////////////////////////////////////////////////////////////////////////
// Filename: InputManager.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _INPUT_MANAGER_H_
#define _INPUT_MANAGER_H_

////////////////////////////////////////////////////////////////////////////////
// Class name: InputManager
////////////////////////////////////////////////////////////////////////////////
enum MouseButton{
    LEFT,
    MIDDLE,
    RIGHT
};

class InputManager
{   
public:
	InputManager();
	InputManager(const InputManager&);
	~InputManager();

	static InputManager * Instance();

	void Initialize();

	void KeyDown(unsigned int);
	void KeyUp(unsigned int);

	bool IsKeyDown(unsigned int);
    bool IsKeyPressed( unsigned int);

    bool  IsButtonPressed(MouseButton button);
    bool  IsButtonDown(MouseButton button);
    int   GetWheelState();
    float GetMouseSensitivity(){ return mouseSensitivity; }

    void UpdateMousePosition(LPARAM lParam);
	void UpdateMousePosition(float, float);
    void UpdateMouseWheel(WPARAM wParam, LPARAM lParam);
    void UpdateMouseState(bool down, MouseButton button);
    void ResetMouse();

    //mouse cursor is expected to be visible at start of program
    inline void HideMouseCursor(){if(mouseVisible){ShowCursor(false);mouseVisible=false;}};
    inline void ShowMouseCursor(){if(!mouseVisible){ShowCursor(true);mouseVisible=true;}};;
    void SetCenterTheMouse(bool newState);
    void ToggleCenterTheMouse();
	
    
//MOUSE
    float mouseSensitivity;
    int mouseX, mouseY, deltaMouseX, deltaMouseY;
    bool repositionMouse;

private:
    bool mouseVisible;
    int wheel;
    bool lButtonPressed, mButtonPressed, rButtonPressed;
    bool lButtonHold, mButtonHold, rButtonHold;

	static InputManager * m_Instance;
    //KEYBOARD
    bool m_keys[256];
    bool m_keyPressed[256];     
};

#endif
