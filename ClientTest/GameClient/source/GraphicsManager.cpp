////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsManager.cpp
// Author(s): Alfred Tarng, Joey Ly
// Purpose: Stores, Initializes, and Calls the actual rendering operations.
//
// TODO: Remove all testing code.
////////////////////////////////////////////////////////////////////////////////
#include <math.h>
#include <iostream>
#include <Unit.h>

#include "GraphicsManager.h"
#include "SystemManager.h"
#include "UnitManagerClient.h"
#include "LoadingManager.h"
#include "ParticleSystemManager.h"
#include "TextureManager.h"
#include "MeshManager.h"
#include "HUDManager.h"

#include "RootNode.h"
#include "TerrainNode.h"
#include "ModelNode.h"
#include "UnitNode.h"
#include "ParticleNode.h"

#include "Terrain.h"
#include "UnitObject.h"

#include "HUDMiniMap.h"

#include "Log.h"

#define SHADOWMAP_WIDTH  2048
#define SHADOWMAP_HEIGHT 2048
#define SHADOWSWORKING   true
#define VSYNC_ENABLED    true
#define SCREEN_DEPTH    5000.0f
#define SCREEN_NEAR     0.1f

#define REFLECTIONWORKING false

class Model;

GraphicsManager::GraphicsManager()
{
    graphicsTimer = Timer();
    root = NULL;
    d3d_matrixStack = NULL;
    frustumObj_Ptr = NULL;
    hudManager_Ptr = NULL; 
    textureManager_Ptr = NULL;

    renderTexture_Ptr = NULL;
    depthShader = NULL;
}
GraphicsManager::GraphicsManager(const GraphicsManager& other)
{
}
GraphicsManager::~GraphicsManager()
{
    delete root;
    root = NULL;

    if ( D3DManager_Ptr )
    {
        D3DManager_Ptr->Shutdown();
        delete D3DManager_Ptr;
        D3DManager_Ptr = 0;
    }

    if (shaderManager_Ptr)
    {
        delete shaderManager_Ptr;
        meshManager_Ptr = 0;
    }

    if (meshManager_Ptr)
    {
        delete meshManager_Ptr;
        meshManager_Ptr = 0;
    }

    if (textureManager_Ptr)
    {
        delete textureManager_Ptr;
        textureManager_Ptr = 0;
    }

}
// DEPRECATED
void GraphicsManager::Shutdown(){ return; }

bool GraphicsManager::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{    
    bool result = true;
    // Create the Direct3D object.
    D3DManager_Ptr = new D3DManager;

    // Initialize the Direct3D object.
    result = D3DManager_Ptr->Initialize( screenWidth, screenHeight, VSYNC_ENABLED, hwnd,
                                         LoadingManager::fullscreen, SCREEN_DEPTH, SCREEN_NEAR );
    if ( result == false ){        
        cout << "D3D Manager Initialization failed." << endl;
        Log::Instance()->PrintErr("D3D Manager Initialization failed.");
    }
    // Create Matrix Stack.
    D3DXCreateMatrixStack( 0, &d3d_matrixStack );

    //Initialize CameraManager
    cameraManager_Ptr = new CameraManager;
    cameraManager_Ptr->Initialize();

    //Initialize LightManager
    lightManager_Ptr = new LightManager;
    lightManager_Ptr -> Initialize();

    // Initialize Texture Manager
    textureManager_Ptr = new TextureManager;
    textureManager_Ptr -> Initialize( D3DManager_Ptr->GetDevice() );

    //Initialize ShaderManager
    shaderManager_Ptr = new ShaderManager;
    shaderManager_Ptr->Initialize(hwnd);

    // Set Depth Shader
    depthShader = &(shaderManager_Ptr->GetShader(shaderManager_Ptr->GetShaderSelector().TEST_DEPTHSHADER));

    //Initialize MeshManager
    meshManager_Ptr = new MeshManager;
    meshManager_Ptr->Initialize();

    // Create the frustum object.
    frustumObj_Ptr = new FrustumObject;

    // Initializing particle system manager
    particleSysMan_Ptr = new ParticleSystemManager;
    particleSysMan_Ptr -> Initialize();

    // Initialize rendertextureobject
    if (SHADOWSWORKING)
    {
        renderTexture_Ptr = new RenderTexture;
        result = renderTexture_Ptr->Initialize( D3DManager_Ptr->GetDevice(), SHADOWMAP_WIDTH, SHADOWMAP_HEIGHT, 2000, SCREEN_NEAR );
        if(!result)
	    {
		    cerr << "[GraphicsManager] RenderToTexture (Depth/Shadow) failed to initialize." << endl;
            Log::Instance()->PrintErr("[GraphicsManager] RenderToTexture (Depth/Shadow) failed to initialize.");
		    return false;
	    }
        if ( REFLECTIONWORKING )
        {
            renderTexture_Ptr2 = new RenderTexture;
            // RRR
            result = renderTexture_Ptr2 -> Initialize( D3DManager_Ptr->GetDevice(), SystemManager::windowWidth, SystemManager::windowHeight );
            if(!result)
	        {
                cerr << "[GraphicsManager] RenderToTexture (Reflective) failed to initialize." << endl;
                Log::Instance()->PrintErr("[GraphicsManager] RenderToTexture (Reflective) failed to initialize.");
                return result;
            }
        }
    }
/////////////////// HUD SETUP //////////////////////////////////////////////////////
    hudManager_Ptr = SystemManager::Instance()->GetHUDManager();
    if (!hudManager_Ptr->Initialize(D3DManager_Ptr))
    {
        cerr << "[GraphicsManager] HUDManager failed to initialize." << endl;
        return false;
    }
    hudManager_Ptr->SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().HUD_SHADER )) );

    RootNode* initRoot = new RootNode();
    root = initRoot;

/// INITIALIZE SCENE ////////////////////////////////////////        
    TerrainNode* worldNode = new TerrainNode();
    Terrain* worldObj = (Terrain*) &(meshManager_Ptr->GetMesh( meshManager_Ptr->GetMeshSelector().TERRAIN_OBJECT )); //fix the hardcoding
    worldObj->SetPosition(D3DXVECTOR3(0.0,0.0,0.0));
    worldObj->SetRotation( D3DXVECTOR3(0,0,0) );
    worldObj->SetScale( D3DXVECTOR3(5.0,5.0,5.0) );
    worldNode->SetTerrain(worldObj);
    if (SHADOWSWORKING) worldNode->SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().TEST_SHADOWSHADER )) );
    else                worldNode->SetShader(&(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().COLOR_SHADER_0)));
	root->addChild(*worldNode);

//////////////////////////////// INITIALIAZE SCENE OBJECTS //////////////////////////

    UnitNode  * unitNode;
    UnitObject* UnitObj;

    ////////////////////////////////////////////////////////////
    // Initialize SkyBox
    unitNode = new UnitNode();
    UnitObj  = new UnitObject(); UnitObj -> setAsTester();
    UnitObj -> SetModel(meshManager_Ptr->GetMeshSelector().SKYBOX );
    UnitObj -> SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
    UnitObj -> SetRotation( D3DXVECTOR3(0,0,0) );
    UnitObj -> SetScale( D3DXVECTOR3(2500.0,2500.0,2500.0) );
    UnitObj -> assignTint(1,1,1);
    unitNode->SetUnitObject( UnitObj );
    unitNode->SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );
    SkyBox = unitNode;
    ((UnitNode*) SkyBox)->displayBoundingSphere(false);
    root->addChild(*unitNode);
////////////////////////////////////////////////////////////
/// TEST INITIALIZATIONS
 // TEST SPARK OBJECT
    unitNode =  new UnitNode();
    UnitObj  =  new UnitObject(); UnitObj -> setAsTester();
    UnitObj ->  SetModel(meshManager_Ptr->GetMeshSelector().ELECTRICSPARK );
    UnitObj ->  SetPosition( D3DXVECTOR3(640.0f, 200.0f, 640.0f) );
    UnitObj ->  SetRotation( D3DXVECTOR3(0,0,0) );
    UnitObj ->  SetBoundingRadius(1);
    UnitObj ->  SetScale( D3DXVECTOR3(3.0,3.0,3.0) );
    UnitObj ->  assignTint(0.2f,0.2f,0.2f);
    unitNode -> SetUnitObject( UnitObj );
    unitNode -> SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );
    // for Testing purposes.
    this->defaultUnit = UnitObj;

        UnitNode * testSubNode =  new UnitNode();
        UnitObject * testObj   = new UnitObject(); testObj -> setAsTester();
        testObj ->  SetModel(meshManager_Ptr->GetMeshSelector().ELECTRICSPARK );
        testObj -> SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
        testObj -> SetRotation( D3DXVECTOR3(0,0,90) );
        testObj -> SetBoundingRadius(1);
        testObj -> SetScale( D3DXVECTOR3(1.0,1.0,1.0) );
        testObj -> assignTint( 0.3f, 0.3f, 0.3f );
        testSubNode -> SetUnitObject( testObj );
        testSubNode -> SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );

        // Test MiniMap Attaching
        HUDMiniMap * hmm = (HUDMiniMap *) &hudManager_Ptr->GetHUDComponent(hudManager_Ptr->GetHUDElementSelector().MINIMAP);
        hmm->addMiniMapItem( *(hudManager_Ptr->GetHUDDisplayables().at(
                               hudManager_Ptr->GetHUDDisplayablesSelector().MINIMAP_ICON1 )),
                             *UnitObj);
        hmm = (HUDMiniMap *) &hudManager_Ptr->GetHUDComponent(hudManager_Ptr->GetHUDElementSelector().MINIMAP_OVERLAY);
        hmm->addMiniMapItem( *(hudManager_Ptr->GetHUDDisplayables().at(
                             hudManager_Ptr->GetHUDDisplayablesSelector().MINIMAP_ICON1 )),
                             *UnitObj);
        // SET DEFAULT CameraUnit
        SystemManager::Instance()->SetCurrentPlayerUnit(UnitObj);

        // Attach Lightning Particle Effect
	    ParticleNode* particleNode;         particleNode = new ParticleNode();
        ParticleSystemObject* particleObj;  particleObj = new ParticleSystemObject();
        particleObj  -> SetParticleSystem( &(particleSysMan_Ptr->GetParticleSystem(particleSysMan_Ptr->GetParticleSelector().TEST_RADIAL_LIGHTNING)) );
        particleNode -> SetParticleObject(particleObj);
        particleNode -> SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().PARTICLE_SHADER )) );

        // Attach to sceneGraph
	    unitNode -> addChild( *particleNode );
        unitNode -> addChild( *testSubNode  );
        root->addChild( *unitNode );

    unitNode =  new UnitNode();
    UnitObj  = new UnitObject(); UnitObj -> setAsTester();
    //UnitObj -> SetModel( meshManager_Ptr->GetMeshSelector().TABLET );
    UnitObj -> SetModel( meshManager_Ptr->GetMeshSelector().TEST_DROID );
    UnitObj->SetPosition( D3DXVECTOR3(0.0f, -0.0f, 0.0f) );
    UnitObj->SetRotation( D3DXVECTOR3(0,0,0) );
    UnitObj->SetBoundingRadius(2);
    UnitObj->SetScale( D3DXVECTOR3(1.0,1.0,1.0) );
    UnitObj->assignTint(0.9f,1.0f,0.9f);
    unitNode->SetUnitObject( UnitObj );
    unitNode->SetShader( (shaderManager_Ptr->GetUnitShader()) );

        // Add Swap Unit "Animation"
        testObj  = new UnitObject(); testObj -> setAsTester();
        testObj -> SetModel( meshManager_Ptr->GetMeshSelector().DROID_WALK );
        testObj->SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
        testObj->SetRotation( D3DXVECTOR3(0,0,0) );
        testObj->SetScale( D3DXVECTOR3(1.0,1.0,1.0) );
        testObj->SetBoundingRadius(1);
        testObj->assignTint(0.9f,1.0f,0.9f);
        unitNode->SetUnitObject(testObj, at_WALK);

    this->testNode    = unitNode;

    root->addChild( *unitNode );

    return true;
}
float satelliteAnim = 0;
/***
 * Updates time based frame operations.  Calls render
 */
bool GraphicsManager::Frame()
{    
    static clock_t lastGraphicsFrameFPS = 0; 
    clock_t currTime = graphicsTimer.GetClockCurr();
    float deltaTime = graphicsTimer.Update();

    // Updates Particle System State
    DWORD temp = GetTickCount();
    particleSysMan_Ptr->Frame( deltaTime * 1000 , D3DManager_Ptr->GetDeviceContext() );
    DWORD particleTime = GetTickCount() - temp;    
    particleSysMan_Ptr->CheckForAndRemoveExpiredSystems( deltaTime * 1000 , D3DManager_Ptr->GetDeviceContext() );
    
	// UPDATE SATELLITES
	std::vector<Unit*>::iterator s_it;
	int index = 0;
	satelliteAnim = (satelliteAnim + deltaTime < 360) ? (satelliteAnim + deltaTime) : 0;
	for( s_it = SatelliteVector.begin(); s_it != SatelliteVector.end(); ++s_it )
	{
		
		D3DXVECTOR3 satellitePos;
		satellitePos.x = 500 * cos( (satelliteAnim * (3.14159f / 180)) + (index * 2.f / 3.f * 3.14159f) );
		satellitePos.z = 500 * sin( (satelliteAnim * (3.14159f / 180)) + (index * 2.f / 3.f * 3.14159f) );
		(*s_it)->SetPosition(satellitePos.x + 640, 200, satellitePos.z  + 640 );
		(*s_it)->SetRotation( (*s_it)->getPitch(), (*s_it)->getPitch(), satelliteAnim + (index * 90) );
		++index;
	}
	

    temp = GetTickCount();
    Render(deltaTime);
    DWORD renderTime = GetTickCount() - temp;

    //this->meshManager_Ptr->resetAnims();
#ifdef _DEBUG
    Log::Instance()->Print("particleUpdateTime = %5lu, renderTime = %5lu", particleTime, renderTime);

    if(currTime-lastGraphicsFrameFPS > 1000)
	{ //output the fps every 1 minute
        //Log::Instance()->Print("FPS = %f", 1/deltaTime);
    }
#endif
    return true;
}
/***
 * Graphic's Render Function
 */
bool GraphicsManager::Render(float deltaTime)
{
    bool result = true;

    // p' = D*P*(C^-1)*M*p
    // M: Object to World Matrix    || p: Object Space, Mp : World Space
    // C: Camera Matrix             || (C^-1)*M*p : Camera Space
    // P: Projection Matrix         || P*(C^-1)*M*p : Canonical View Volume
    // D: ViewPort Matrix           || D*P*(C^-1)*M*p : Image Space
    D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix, orthogonalMatrix, cameraMatrix;
    //D3DXMATRIX lightViewMatrix, lightProjectionMatrix; //$$$
    D3DXMATRIX identity;
    D3DXMatrixIdentity(&identity);

    // Get the world, view, and projection matrices from the camera and d3d objects
    cameraManager_Ptr->getCurrentCamera()->SetToFollowUnit(SystemManager::Instance()->GetCurrentPlayerUnit());
    cameraManager_Ptr->getCurrentCamera()->GetMatrix(viewMatrix, deltaTime);  //sets the viewmatrix to the current camera view
    worldMatrix      = D3DManager_Ptr->GetWorldMatrix();
    projectionMatrix = D3DManager_Ptr->GetProjectionMatrix();
    orthogonalMatrix = D3DManager_Ptr->GetOrthogonalMatrix();

//  Update SkyBox    
    ((UnitNode *)SkyBox)->GetUnitObject()->SetPosition( GetCamera()->GetPosition() );

    D3DXVECTOR3 lightPos, oldLightPos;
    if(LoadingManager::lockLightToCamera)
    {
        oldLightPos = lightPos = GetLightManager()->getLight(0)->GetPosition();
        lightPos = GetCamera()->GetPOffset();
        lightPos.y += 50;
        lightPos.x = (oldLightPos.x + lightPos.x) / 2.f;
        lightPos.y = (oldLightPos.y + lightPos.y) / 2.f;
        lightPos.z = (oldLightPos.z + lightPos.z) / 2.f;
        GetLightManager()->getLight(0)->SetPosition( lightPos );
        GetLightManager()->getLight(0)->SetLookAt   ( GetCamera()->GetPOffset() );
    }
    GetLightManager()->getLight(0) -> GenerateViewMatrix();
	GetLightManager()->getLight(0) -> GenerateProjectionMatrix(1280, 100); // SHOULD ONLY HAVE TO BE GENERATED ONCE...


//  Construct the Frustum
    frustumObj_Ptr->ConstructFrustum(SCREEN_DEPTH, projectionMatrix, viewMatrix);

/// RENDER MODELS/SHADERS HERE ///////////////////////////////////////////////////////////////////

    if ( SHADOWSWORKING && this->GetShaderManager()->IsUsingShadows() )
    {
        d3d_matrixStack->LoadIdentity();
    
	    //// Set the render target to be the render to texture.
	    renderTexture_Ptr->SetRenderTarget(D3DManager_Ptr->GetDeviceContext());
        // Clear the render to texture.
        renderTexture_Ptr->ClearRenderTarget( D3DManager_Ptr->GetDeviceContext(), 0.0f, 0.0f, 0.0f, 1.0f );
           // TODO: Implement a (zero-eth) pass for shadow mapping.
           int getDepth = 1;
           root->Draw( D3DManager_Ptr , d3d_matrixStack, getDepth  );
        // Reset the render target back to the original back buffer and not the render to texture anymore.
        D3DManager_Ptr->RestoreRenderTarget(); // $$$
    }
    if ( REFLECTIONWORKING )
    {
        //// Set the render target to be the render to texture.
	    renderTexture_Ptr2->SetRenderTarget(D3DManager_Ptr->GetDeviceContext(), D3DManager_Ptr->GetDepthStencilView());
	    //// Clear the render to texture.
	    renderTexture_Ptr2->ClearRenderTarget(D3DManager_Ptr->GetDeviceContext(), D3DManager_Ptr->GetDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);
        // Use the camera to calculate the reflection matrix.
        this->GetCamera()->RenderReflection(-1.5f);
        // Get the camera reflection view matrix instead of the normal view matrix.
        int getReflection = 2;
        root->Draw( D3DManager_Ptr , d3d_matrixStack, getReflection );
        D3DManager_Ptr->RestoreRenderTarget();
    }

    //  Clears the buffer.
    D3DManager_Ptr -> BeginScene(0.75f, 0.5f, 0.1f, 1.0f);
//// 
    d3d_matrixStack->LoadIdentity();

    //RenderSceneGraph
    root->Draw(D3DManager_Ptr, d3d_matrixStack, 0 );

/// END: RENDER MODELS/SHADERS HERE /////////////////////////////////////////////////////////////

/*************************** RENDER GUI/HUD HERE ******************************************/    

    // Initialization of render
    // Creates the Ortho View
    D3DXMATRIX testGUIViewMatrix;
    D3DXVECTOR3 testPosition = D3DXVECTOR3(0.0, 0.0, -10.0); //D3DXVECTOR3(0,0,0);
    D3DXVECTOR3 testLookAtVec = D3DXVECTOR3(0,0,0);
    D3DXVECTOR3 testUp = D3DXVECTOR3(0,1,0);
    D3DXMatrixLookAtLH(&testGUIViewMatrix, &testPosition, &testLookAtVec, &testUp);

    result = SystemManager::Instance()->GetHUDManager()->Render(D3DManager_Ptr, worldMatrix, testGUIViewMatrix, orthogonalMatrix);

/*************************** END: RENDER GUI/HUD HERE *************************************/
	
    if(LoadingManager::lockLightToCamera) GetLightManager()->getLight(0)->SetPosition ( oldLightPos );

    // Present the rendered scene to the screen.
    D3DManager_Ptr->EndScene();

    return true;
};

/***
 * Test Code for shadows, Should be able to be removed
 * Also can serve as test area for reflection?
 */
float testX = 0.0f;
bool GraphicsManager::RenderShadowPatch(float deltaTime)
{
    D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix, orthogonalMatrix, lightViewMatrix, lightProjectionMatrix;

    worldMatrix      = D3DManager_Ptr->GetWorldMatrix();
    projectionMatrix = D3DManager_Ptr->GetProjectionMatrix();
    orthogonalMatrix = D3DManager_Ptr->GetOrthogonalMatrix();

    // Set Render Target to texture, and clear the render to texture.
	renderTexture_Ptr->SetRenderTarget(D3DManager_Ptr->GetDeviceContext());
    renderTexture_Ptr->ClearRenderTarget( D3DManager_Ptr->GetDeviceContext(), 0.0f, 0.0f, 0.0f, 1.0f );

    LightObject * light_Ptr = GetLightManager()->getLight(0);

    testX += 0.1f;
    light_Ptr->SetPosition( D3DXVECTOR3(testX, light_Ptr->GetPosition().y, light_Ptr->GetPosition().z) );
    //light_Ptr->SetPosition( GetCamera()->GetPosition() );
    //light_Ptr->SetLookAt  ( cameraManager_Ptr->getCurrentCamera()->GetLookAt() );

    light_Ptr -> GenerateProjectionMatrix(SCREEN_DEPTH , SCREEN_NEAR);
    light_Ptr -> GenerateViewMatrix();

    light_Ptr -> GetViewMatrix(lightViewMatrix);
    light_Ptr -> GetProjectionMatrix(lightProjectionMatrix);

//    if (testX > 10) testX = 0.0f;

    ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();

    Model *   model_0 =    (Model*) &GetMeshManager()->GetMesh(GetMeshManager()->GetMeshSelector().TEST_DROID );
    Terrain * model_1 = (Terrain *) &GetMeshManager()->GetMesh(GetMeshManager()->GetMeshSelector().TERRAIN_OBJECT);
    Model *   model_2 =    (Model*) &GetMeshManager()->GetMesh(GetMeshManager()->GetMeshSelector().PROBE);
    Model *   model_3 =    (Model*) &GetMeshManager()->GetMesh(GetMeshManager()->GetMeshSelector().TEST_ELECTRICTHING );


    D3DXVECTOR3  testVec= D3DXVECTOR3(0, 0, 15);
    D3DXVECTOR3 testVec1 = D3DXVECTOR3(15, 10, 10);
    D3DXVECTOR3 testVec2 = D3DXVECTOR3(-15, 0, -15);

    // TERRAIN
    D3DXMatrixTranslation(&worldMatrix, testVec2.x, testVec2.y,testVec2.z);
    model_1 -> Render( D3DManager_Ptr->GetDeviceContext() );
    depthShader->Render( D3DManager_Ptr->GetDeviceContext(), model_1->GetIndexCount(),0,
                         worldMatrix, lightViewMatrix, lightProjectionMatrix,
                         NULL, 0,
                         myRSC
                         );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    // DROID
    D3DXMatrixTranslation(&worldMatrix, testVec.x, testVec.y,testVec.z);
    model_0 -> Render( D3DManager_Ptr->GetDeviceContext() );
    depthShader->Render( D3DManager_Ptr->GetDeviceContext(), model_0->GetIndexCount(),0,
                         worldMatrix, lightViewMatrix, lightProjectionMatrix,
                         NULL, 0,
                         myRSC
                         );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    D3DXMatrixTranslation(&worldMatrix, testVec.x, testVec.y,testVec.z);
    model_2 -> Render( D3DManager_Ptr->GetDeviceContext() );
    depthShader->Render( D3DManager_Ptr->GetDeviceContext(), model_2->GetIndexCount(),0,
                         worldMatrix, lightViewMatrix, lightProjectionMatrix,
                         NULL, 0,
                         myRSC
                         );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    D3DXMatrixTranslation(&worldMatrix, testVec1.x, testVec1.y,testVec1.z);
    model_3 -> Render( D3DManager_Ptr->GetDeviceContext() );
    depthShader->Render( D3DManager_Ptr->GetDeviceContext(), model_3->GetIndexCount(),0,
                         worldMatrix, lightViewMatrix, lightProjectionMatrix,
                         NULL, 0,
                         myRSC
                         );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);


// SWITCH TO RENDERING ACTUAL SCENE
    D3DManager_Ptr->RestoreRenderTarget(); // $$$
//  Clears the buffer.
    D3DManager_Ptr -> BeginScene(0.1f, 0.75f, 0.75f, 1.0f);

    cameraManager_Ptr->getCurrentCamera()->GetMatrix(viewMatrix, deltaTime);  //sets the viewmatrix to the current camera view

    ShaderObject*  shadowShader = &GetShaderManager()->GetShader(GetShaderManager()->GetShaderSelector().TEST_SHADOWSHADER);
	ShaderObject*  bumpShader   = GetShaderManager()->GetUnitShader();

    myRSC = ShaderObject::createRenderShaderConfig();
        myRSC.lightViewMatrix       = &lightViewMatrix;
        myRSC.lightProjectionMatrix = &lightProjectionMatrix;
        myRSC.lightMan = this->GetLightManager();
        myRSC.depthResource = GetRenderTexture()->GetShaderResourceView();

    //DRAW TERRAIN
    D3DXMatrixTranslation(&worldMatrix, testVec2.x, testVec2.y,testVec2.z);
    model_1 -> Render( D3DManager_Ptr->GetDeviceContext() );
    for (int s = 0; s < 2; ++s)
        shadowShader->Render( D3DManager_Ptr->GetDeviceContext(), model_1->GetIndexCount() * (1-s), 0,
                              worldMatrix, viewMatrix, projectionMatrix,
                              model_1 ->GetTextureObject()->GetTextures(), model_1 -> GetTextureObject()->numTextures(),
                              myRSC
                             );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    myRSC.camObj = cameraManager_Ptr->getCurrentCamera();
    
    D3DXMatrixTranslation(&worldMatrix, testVec.x, testVec.y,testVec.z);
    model_0 -> Render( D3DManager_Ptr->GetDeviceContext() );
    for (int s = 0; s < 2; ++s)
        bumpShader ->Render( D3DManager_Ptr->GetDeviceContext(), model_0 ->GetIndexCount(),0,
                              worldMatrix, viewMatrix, projectionMatrix,
                              model_0 ->GetTextureObject()->GetTextures(), model_0 -> GetTextureObject()->numTextures(),
                              myRSC
                             );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    D3DXMatrixTranslation(&worldMatrix, testVec.x, testVec.y,testVec.z);
    model_2 -> Render( D3DManager_Ptr->GetDeviceContext() );
    for (int s = 0; s < 2; ++s)
        bumpShader -> Render( D3DManager_Ptr->GetDeviceContext(), model_2 ->GetIndexCount()  * (1-s),0,
                              worldMatrix, viewMatrix, projectionMatrix,
                              model_2 ->GetTextureObject()->GetTextures(), model_2 -> GetTextureObject()->numTextures(),
                              myRSC
                            );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    D3DXMatrixTranslation(&worldMatrix, testVec1.x, testVec1.y,testVec1.z);
    model_3 -> Render( D3DManager_Ptr->GetDeviceContext() );
    for (int s = 0; s < 2; ++s)
       bumpShader -> Render( D3DManager_Ptr->GetDeviceContext(), model_3 -> GetIndexCount() * (1-s) ,0,
                             worldMatrix, viewMatrix, projectionMatrix,
                             model_3 ->GetTextureObject()->GetTextures(), model_3 -> GetTextureObject()->numTextures(),
                             myRSC
                           );
    worldMatrix = *D3DXMatrixIdentity(&worldMatrix);

    D3DManager_Ptr->EndScene();
    return true;
}

/***
 * Creates an explosion
 */
void GraphicsManager::CreateExplosion( float ex, float ey, float ez, int explosionType )
{
    ParticleNode* particleNode;         particleNode = new ParticleNode();
    ParticleSystemObject* particleObj;  particleObj = new ParticleSystemObject();

    float e_scale = 3.0;
    int   genType = 0;
    switch( explosionType )
    {
	case 3:  genType = explosionType;
			 e_scale = 2.5f;
			 particleObj -> SetLifeTime(3);
			 break;
    case 2:  e_scale = 10.0f;
             genType = 2;
			 particleObj -> SetLifeTime(3);
             break;
    case 1:  e_scale = 2.5f;
             genType = 1;
			 particleObj -> SetLifeTime(1);
             break;
    default: e_scale = 7.0f;
			 particleObj -> SetLifeTime(3);
             break;
    } 

    particleObj -> SetBillBoarded( PARTICLE );
    particleObj -> setExpires( true );
    particleObj -> SetScale(D3DXVECTOR3(e_scale,e_scale,e_scale));
    particleObj -> SetPosition( D3DXVECTOR3(ex,ey,ez) );

    particleObj -> GenerateOwnParticleSystem(genType);

    particleNode -> SetParticleObject(particleObj);
	particleNode -> SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().PARTICLE_SHADER_BB )) );

    this->GetWorld() -> addChild(*particleNode);

	if (explosionType == 3)
	{
		particleObj -> SetBillBoarded( SYSTEM );
		particleNode -> SetShader( &(shaderManager_Ptr->GetShader( shaderManager_Ptr->GetShaderSelector().PARTICLE_SHADER )) );
		particleObj -> SetRotation( D3DXVECTOR3(0,0,180) );
	}
}

/***
 *
 */
void GraphicsManager::SwapUnitChain()
{
    if ( testNode -> PollCurrentAnimationType() != at_WALK)
        testNode->SwapTo(at_WALK);
    else
        testNode->SwapTo(at_DEFAULT);
}

void GraphicsManager::AddSatellites()
{
    for (int i = 0; i < 3; ++i)
    {
        Unit * testUnit = new Unit;
        int useID = 0;
        switch (i % 3)
        {
			//case 1:  useID = meshManager_Ptr->GetMeshSelector().TABLET2; break;
			default: useID = meshManager_Ptr->GetMeshSelector().SATELLITE_SHIP;  break;
        }
        testUnit->SetIDs(useID, shaderManager_Ptr->GetShaderSelector().BUMP_SHADER );
        testUnit->SetPosition((float)((i * 10) % 1000), 10.0f * (i/100) , 10.0f);
		testUnit->SetRadius( 50 );
        testUnit->setUnitId(-(i+10));
        UnitManagerClient::Instance()->updateClient( testUnit );
		SatelliteVector.push_back(testUnit);
    }
}