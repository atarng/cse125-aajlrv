/***
 * Filename: ShaderObject.cpp
 * Contributors: Alfred Tarng
 * Purpose: Handles shader files (.vs/.ps but should also be able to support .hlsl).
 *
 * TODO: Support multiple lights.
 */

#include "ShaderObject.h"
#include "Log.h"

#include <iostream>
#include <fstream>
using namespace std;

ShaderObject::ShaderObject()
{
    m_pixelShader = 0;
    m_vertexShader = 0;

    m_layout = 0;
	m_sampleState = 0;

	m_matrixBuffer = 0;
      m_matrixBuffer2 = 0; // $$$
	m_lightBuffer = 0;
      m_lightBuffer2 = 0;  // $$$
    m_cameraBuffer = 0;
    m_pixelBuffer = 0;

    m_animBuffer = 0;

    givesReflection = hasShadows = false;

    m_sampleStates = std::vector<ID3D11SamplerState*>();
}
ShaderObject::ShaderObject(const ShaderObject& other)
{
}
ShaderObject::~ShaderObject()
{
    Shutdown();
}

/***
 * vsfn: vertex shader file name
 * psfn: pixel shader file name
 */
bool ShaderObject::Initialize(ID3D11Device* device, HWND hwnd,
                              char * vsFileName, char * psFileName,
                              char * vsFunctionName, char * psFunctionName)
{
    bool result = true;

	// Initialize the vertex and pixel shaders.
	result = InitializeShader( device, hwnd, vsFileName, psFileName, vsFunctionName, psFunctionName);

    return result;
}

// WCHAR* WAS USED HERE FOR VSFILENAME/PSFILENAME
bool ShaderObject::InitializeShader(ID3D11Device* device, HWND hwnd,
                                    char* vsFilename, char* psFilename, char * vsFunctionName, char * psFunctionName)
{
	HRESULT result;

    ID3D10Blob* vertexShaderBuffer, * pixelShaderBuffer;
    D3D11_INPUT_ELEMENT_DESC * polygonLayout = (D3D11_INPUT_ELEMENT_DESC *) malloc(sizeof(D3D11_INPUT_ELEMENT_DESC) * shaderConfigVector.size());
	unsigned int numElements;

    D3D11_SAMPLER_DESC samplerDesc;
	D3D11_BUFFER_DESC matrixBufferDesc, matrixBufferDesc2;
	D3D11_BUFFER_DESC lightBufferDesc, cameraBufferDesc;

	// Initialize the pointers this function will use to null.
	vertexShaderBuffer = 0;
	pixelShaderBuffer = 0;

    // ##################
    // Compile the vertex shader code.
    //HRESULT WINAPI D3DX11CompileFromFileA(LPCSTR pSrcFile,
    //                                      CONST D3D10_SHADER_MACRO* pDefines,
    //                                      LPD3D10INCLUDE pInclude,
    //                                      LPCSTR pFunctionName,
    //                                      LPCSTR pProfile,
    //                                      UINT Flags1,
    //                                      UINT Flags2,
    //                                      ID3DX11ThreadPump* pPump,
    //                                      ID3D10Blob** ppShader, ########!!!!!! Of note this is what the items are extracted to !!!!!!######
    //                                      ID3D10Blob** ppErrorMsgs,
    //                                      HRESULT* pHResult);
    //Load VertexShader
	result = D3DX11CompileFromFile(vsFilename, 0, 0, vsFunctionName,
                                   "vs_4_0",
                                   D3D10_SHADER_ENABLE_STRICTNESS,
                                   0, 0,
                                   &vertexShaderBuffer, 0, 0);
	if(FAILED(result))
	{
        cout << "[ShaderObject] Failed to load Vertex Shader...." << endl;
        Log::Instance()->PrintErr("Failed to load Vertex Shader....");
        return false;
	}

    // Load Pixel Shader
    result = D3DX11CompileFromFile( psFilename, 0, 0, psFunctionName,
                                    "ps_4_0",
                                    D3D10_SHADER_ENABLE_STRICTNESS,
                                    0, 0,
                                    &pixelShaderBuffer, 0, 0);
	if(FAILED(result))
	{
        cout << "Failed to load Pixel Shader..." << endl;
        Log::Instance()->PrintErr("Failed to load Pixel Shader...");
		return false;
	}

    // Create the vertex shader from the buffer.
    result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &m_vertexShader);
	if(FAILED(result))
	{
        cout << "FAILED:: Create the vertex shader from the buffer.." << endl;
        Log::Instance()->PrintErr("FAILED:: Create the vertex shader from the buffer..");
		return false;
	}

    // Create the pixel shader from the buffer.
    result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &m_pixelShader);
	if(FAILED(result))
	{
        cout << "failed to create pixel shader..." << endl;
        Log::Instance()->PrintErr("failed to create pixel shader...");
		return false;
	}

	// Create the vertex input layout description.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
    polygonLayout[0].SemanticName = shaderConfigVector.at(0).SemanticName; // "POSITION";
	polygonLayout[0].SemanticIndex = 0;
    polygonLayout[0].Format = shaderConfigVector.at(0).Format;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

    for (unsigned int i = 1; i < shaderConfigVector.size() ; ++i)
    {
	    polygonLayout[i].SemanticName         = shaderConfigVector.at(i).SemanticName;
        polygonLayout[i].SemanticIndex        = shaderConfigVector.at(i).SemanticIndex; //0;
	    polygonLayout[i].Format               = shaderConfigVector.at(i).Format;
	    polygonLayout[i].InputSlot            = 0;
	    polygonLayout[i].AlignedByteOffset    = D3D11_APPEND_ALIGNED_ELEMENT;
	    polygonLayout[i].InputSlotClass       = D3D11_INPUT_PER_VERTEX_DATA;
	    polygonLayout[i].InstanceDataStepRate = 0;
    }

    // Get a count of the elements in the layout.
    numElements = shaderConfigVector.size();

	// Create the vertex input layout.
    // CreateInputLayout( ied, 2, VS->GetBufferPointer(), VS->GetBufferSize(), &InputLayoutPtr);
	result = device->CreateInputLayout( polygonLayout, numElements,
                                        vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(),
		                                &m_layout);
	if(FAILED(result))
	{
        cout << "[ShaderObject] FAILED: Create the vertex input layout." << endl;
        Log::Instance()->PrintErr("[ShaderObject] FAILED: Create the vertex input layout.");
		return false;
	}

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;
    
    if (!samplerDescConfigVector.empty())
    {// If multiple sample states
        for (unsigned int i = 0; i < samplerDescConfigVector.size() ; ++i)
        {
            samplerDesc.Filter   = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
            samplerDesc.AddressU = samplerDescConfigVector.at(i).uAddr;
            samplerDesc.AddressV = samplerDescConfigVector.at(i).vAddr;
            samplerDesc.AddressW = samplerDescConfigVector.at(i).wAddr;
            samplerDesc.MipLODBias = 0.0f;
            samplerDesc.MaxAnisotropy = 1;
            samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
            samplerDesc.BorderColor[0] = 0;
	        samplerDesc.BorderColor[1] = 0;
	        samplerDesc.BorderColor[2] = 0;
	        samplerDesc.BorderColor[3] = 0;
            samplerDesc.MinLOD = 0;
            samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

            ID3D11SamplerState* ss_ptr = NULL;
            m_sampleStates.push_back(ss_ptr);

            //Log::Instance()->Print("create sample-state: %d \n", i);

        	// Create the texture sampler state.
            result = device->CreateSamplerState(&samplerDesc, &m_sampleStates.back()); // $$$
	        if(FAILED(result))
	        {
                cout << "FAILED: Create the texture sampler state:" << i << endl;
                Log::Instance()->PrintErr("FAILED: Create the texture sampler state %d.", i);
		        return false;
	        }
            m_sampleState = NULL;
        }
    } else
    {// Create a texture sampler state description.
        samplerDesc.Filter   = D3D11_FILTER_ANISOTROPIC;
                            // D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.MipLODBias = 0.0f;
        samplerDesc.MaxAnisotropy = 1;
        samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
        samplerDesc.BorderColor[0] = 0;
	    samplerDesc.BorderColor[1] = 0;
	    samplerDesc.BorderColor[2] = 0;
	    samplerDesc.BorderColor[3] = 0;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	    // Create the texture sampler state.
        result = device->CreateSamplerState(&samplerDesc, &m_sampleState);
	    if(FAILED(result))
	    {
            cout << "FAILED: Create the texture sampler state." << endl;
            Log::Instance()->PrintErr("FAILED: Create the texture sampler state.");
		    return false;
	    }
    }

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
    matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
    matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);
	if(FAILED(result))
	{
        cout << "FAILED: Create the constant buffer pointer so we can access the" <<
                " vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class." );        
		return false;
	}

    matrixBufferDesc2.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc2.ByteWidth = sizeof(MatrixBufferType_Append0);
    matrixBufferDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    matrixBufferDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    matrixBufferDesc2.MiscFlags = 0;
    //matrixBufferDesc2.StructureByteStride = 0;
    result = device->CreateBuffer(&matrixBufferDesc2, NULL, &m_matrixBuffer2);
    if(FAILED(result))
	{
        cout << "FAILED (2): Create the constant buffer pointer so we can access the" <<
                " vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED (2): Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class." );        
		return false;
	}

    // lazy method works.
    matrixBufferDesc.ByteWidth = sizeof(BoneBufferType);
    result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_boneBuffer);
	if(FAILED(result))
	{
        cout << "FAILED: Create the constant buffer pointer so we can access the" <<
                " vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class." );        
		return false;
	}

	// lazy method works.
    matrixBufferDesc.ByteWidth = sizeof(ParticleTransformBufferType);
    result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_particleBuffer);
	if(FAILED(result))
	{
        cout << "FAILED: Create the constant buffer pointer so we can access the" <<
                " vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class." );        
		return false;
	}

    //
    matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType_Append1);
    result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_animBuffer);
    if(FAILED(result))
	{
        cout << "FAILED: Create the (Animation) constant buffer pointer so we can access the" <<
                    " vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the (Animation) constant buffer pointer so we can access the vertex shader constant buffer from within this class." );
        return false;
    }

    matrixBufferDesc.ByteWidth = sizeof(ReflectionBufferType);
    result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_reflectionBuffer);

	// Setup the description of the light dynamic constant buffer that is in the pixel shader.
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&lightBufferDesc, NULL, &m_lightBuffer);
	if(FAILED(result))
	{
        cout << "FAILED: Create the constant buffer pointer so we can access the" <<
                " pixel shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.");
		return false;
	}

    lightBufferDesc.ByteWidth = sizeof(LightBufferType2);
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&lightBufferDesc, NULL, &m_lightBuffer2);
	if(FAILED(result))
	{
		cout << "FAILED: Create the lazy Light buffer pointer2 so we can access the" <<
        " lb2 shader constant buffer from within this class." << endl;
		return false;
	}

	//pixelBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    lightBufferDesc.ByteWidth = sizeof(PixelBufferType);
	//pixelBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//pixelBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//pixelBufferDesc.MiscFlags = 0;
	//pixelBufferDesc.StructureByteStride = 0;
    result = device->CreateBuffer(&lightBufferDesc, NULL, &m_pixelBuffer);
	if(FAILED(result))
	{
        cout << "FAILED: Create the pixel buffer pointer so we can access the" <<
                " pixel shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("FAILED: Create the constant buffer pointer so we can access the pixel shader constant buffer from within this class.");
		return false;
	}

    // Setup the description of the camera dynamic constant buffer that is in the vertex shader.
	cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cameraBufferDesc.ByteWidth = sizeof(CameraBufferType);
	cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cameraBufferDesc.MiscFlags = 0;
	cameraBufferDesc.StructureByteStride = 0;
	// Create the camera constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&cameraBufferDesc, NULL, &m_cameraBuffer);
	if(FAILED(result))
	{
        cout << "Create the camera constant buffer pointer so we can access the vertex shader constant buffer from within this class." << endl;
        Log::Instance()->PrintErr("Create the camera constant buffer pointer so we can access the vertex shader constant buffer from within this class.");
		return false;
	}


/// CLEAN STUFF UP
    if (polygonLayout != NULL)
    {
        delete polygonLayout;
        polygonLayout = NULL;
    }

    return true;
}

/***
 * Main Render Call... calls SetShaderParams and RenderShader.
 */
bool ShaderObject::Render( ID3D11DeviceContext* deviceContext, int indexCount, int offset,
                           D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,  //D3DXMATRIX  boneMatrix
                           ID3D11ShaderResourceView** textures, int numTextures,
                           RENDERSHADERCONFIG rsc
                           )                          
{
	bool result = true;

	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters( deviceContext, worldMatrix, viewMatrix, projectionMatrix,
                                  textures, numTextures,
                                  rsc );
	if(!result)
	{
		return false;
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount, offset);

	return true;
}

/***
 * SetShaderParameters
 * Sets DeviceContext with matrix params, texture resouces, light resource, etc...
 */
bool ShaderObject::SetShaderParameters( ID3D11DeviceContext* deviceContext,
                                        D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, //D3DXMATRIX  boneMatrix
                                        ID3D11ShaderResourceView** textures, int textureCount,
                                        RENDERSHADERCONFIG rsc
                                      )
{
	HRESULT result;
    D3D11_MAPPED_SUBRESOURCE mappedResource;
	unsigned int bufferNumber;
	MatrixBufferType  * MatrixBufferPtr;
      MatrixBufferType_Append0* MatrixBufferPtr2;
      MatrixBufferType_Append1* AnimationBufferPtr;
	ParticleTransformBufferType    * ParticleTransformBufferPtr;
    LightBufferType   * LightBufferPtr;
      LightBufferType2  * LightBufferPtr2;
    CameraBufferType  * CameraBufferPtr; // For Specular...
    PixelBufferType   * PixelBufferPtr;  // For Specular...
    ReflectionBufferType* ReflectionBufferPtr;

    int vsBufferNumber = 0;
    int psBufferNumber = 0;

	// Transpose the matrices to prepare them for the shader.
	D3DXMatrixTranspose(&worldMatrix, &worldMatrix);           // S
	D3DXMatrixTranspose(&viewMatrix, &viewMatrix);             // S
	D3DXMatrixTranspose(&projectionMatrix, &projectionMatrix); // S
    //for (unsigned int i = 0; i < rsc.boneMatrices.size(); ++i)
    //{
    //    D3DXMatrixTranspose(&rsc.boneMatrices.at(i), &rsc.boneMatrices.at(i));
    //}
    if (rsc.lightViewMatrix != NULL && rsc.lightProjectionMatrix != NULL)
    {
        D3DXMatrixTranspose(rsc.lightViewMatrix, rsc.lightViewMatrix);
	    D3DXMatrixTranspose(rsc.lightProjectionMatrix, rsc.lightProjectionMatrix);
    }
    if( rsc.particleMatrix != NULL )
    {
        D3DXMatrixTranspose(rsc.particleMatrix, rsc.particleMatrix); // S
    }
    if (rsc.animationMatrix != NULL )
    {
        D3DXMatrixTranspose(rsc.animationMatrix, rsc.animationMatrix); // A
    }
    if (rsc.reflectionMatrix != NULL )
    {// Transpose the reflection matrix to prepare it for the shader.
	    D3DXMatrixTranspose(rsc.reflectionMatrix, rsc.reflectionMatrix);
    }

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
    if(FAILED(result))
    {
        cerr<< "[ShaderObject] FAILED:: Lock the constant buffer so it can be written to." << endl;
        Log::Instance()->PrintErr("[ShaderObject] FAILED:: Lock the constant buffer so it can be written to.");
        return false;
    }

    // Get a pointer to the data in the constant buffer.
	MatrixBufferPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	MatrixBufferPtr->world      = worldMatrix;
	MatrixBufferPtr->view       = viewMatrix;
	MatrixBufferPtr->projection = projectionMatrix;
	// Unlock the constant buffer.
    deviceContext->Unmap(m_matrixBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = vsBufferNumber++;

	// Now set the constant buffer in the vertex shader with the updated values.
    //__in_range( 0, D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - 1 )  UINT StartSlot,
    //__in_range( 0, D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - StartSlot )  UINT NumBuffers,
    //__in_ecount(NumBuffers)  ID3D11Buffer *const *ppConstantBuffers)
    deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

///////

    if ( rsc.animationMatrix != NULL )
    {
        // Lock the camera constant buffer so it can be written to.
    	result = deviceContext->Map(m_animBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
        if(FAILED(result)) return false;

        // Get a pointer to the data in the constant buffer.
	    AnimationBufferPtr = (MatrixBufferType_Append1*)mappedResource.pData;

        AnimationBufferPtr->animationMatrix = *rsc.animationMatrix;

        // Unlock the camera constant buffer.
	    deviceContext->Unmap(m_animBuffer, 0);

        bufferNumber = vsBufferNumber++;
        deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_animBuffer);
    }

    // If Light Matrices are set pupulate those buffers.
    if (rsc.lightViewMatrix != NULL && rsc.lightProjectionMatrix != NULL)
    {
        result = deviceContext->Map(this->m_matrixBuffer2, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
        if(FAILED(result)) return false;

        MatrixBufferPtr2 = (MatrixBufferType_Append0*)mappedResource.pData;

        MatrixBufferPtr2->lightView       = *rsc.lightViewMatrix;
        MatrixBufferPtr2->lightProjection = *rsc.lightProjectionMatrix;

        deviceContext->Unmap(m_matrixBuffer2, 0);

        bufferNumber = vsBufferNumber++;
        deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer2);
    }
    // $$$
    //if ( !rsc.boneMatrices.empty() )
    //{// FILL IN BONE BUFFER
    // // Lock the constant buffer so it can be written to.
	   // result = deviceContext->Map(m_boneBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
    //    if(FAILED(result)) return false;

    //    BoneBufferPtr  = (BoneBufferType*)mappedResource.pData;
    //    for (unsigned int b = 0; b < rsc.boneMatrices.size(); ++b)
    //    {
    //        if ( b >= 10 ) break;
    //        BoneBufferPtr->bones[b] = rsc.boneMatrices.at(b);
    //    }

    //    deviceContext->Unmap(m_boneBuffer, 0);
    //    bufferNumber = vsBufferNumber++;
    //    deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_boneBuffer);
    //}
	if ( rsc.particleMatrix != NULL )
	{
        result = deviceContext->Map(m_particleBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
        if(FAILED(result)) return false;
		
        ParticleTransformBufferPtr = (ParticleTransformBufferType *)mappedResource.pData;
        ParticleTransformBufferPtr -> particleBillBoard = *rsc.particleMatrix;

        deviceContext->Unmap(m_particleBuffer, 0);
        bufferNumber = vsBufferNumber++;
        deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_particleBuffer);
	}

    if (rsc.camObj != NULL)
    {
        // Lock the camera constant buffer so it can be written to.
    	result = deviceContext->Map(m_cameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	    if(FAILED(result)) return false;

    	// Get a pointer to the data in the constant buffer.
	    CameraBufferPtr = (CameraBufferType*)mappedResource.pData;

	    // Copy the camera position into the constant buffer.
        CameraBufferPtr->cameraPosition = rsc.camObj->GetPosition(); //cameraPosition;
	    CameraBufferPtr->padding = 0.0f;

	    // Unlock the camera constant buffer.
	    deviceContext->Unmap(m_cameraBuffer, 0);

	    // Set the position of the camera constant buffer in the vertex shader.
	    bufferNumber = vsBufferNumber++;

	    // Now set the camera constant buffer in the vertex shader with the updated values.
	    deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_cameraBuffer);
    }

    if ( rsc.reflectionMatrix != NULL )
    {
        // Lock the reflection constant buffer so it can be written to.
	    result = deviceContext->Map(m_reflectionBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
        ReflectionBufferPtr = (ReflectionBufferType*)mappedResource.pData;
        ReflectionBufferPtr->reflectionMatrix = *rsc.reflectionMatrix;
        deviceContext->Unmap(m_reflectionBuffer, 0);
        bufferNumber = vsBufferNumber++;
        deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_reflectionBuffer);
    }

//////
    if (textures)
    {// Set shader texture resource in the pixel shader.
        int textureOffset = 0;
        if(rsc.depthResource != NULL)
        {// 1 value is currently hardcoded.
            deviceContext->PSSetShaderResources(textureOffset, 1, &rsc.depthResource );
            ++textureOffset;
        }
        if(rsc.reflectionResource != NULL)
        {
            deviceContext->PSSetShaderResources(textureOffset, 1, &rsc.depthResource );
            ++textureOffset;
        }
	    deviceContext->PSSetShaderResources(textureOffset, textureCount, textures);
    }

    if ( rsc.lightMan != NULL )
    {// Lock the light constant buffer so it can be written to.
	    result = deviceContext->Map(m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	    if(FAILED(result))
	    {
		    return false;
	    }

	    // Get a pointer to the data in the constant buffer.
	    LightBufferPtr = (LightBufferType*)mappedResource.pData;

	    // Copy the lighting variables into the constant buffer.
        LightObject * lightObject_0    = rsc.lightMan->getLight(0);
        LightBufferPtr->ambientColor   = lightObject_0->GetAmbientColor(); // ambientColor;
	    LightBufferPtr->diffuseColor   = lightObject_0->GetDiffuseColor(); // diffuseColor;
        // only if directional light.
        LightBufferPtr->lightDirection = lightObject_0->GetDirection();    // lightDirection;
        // only if specular
        LightBufferPtr->specularColor  = lightObject_0->GetSpecularColor();
        LightBufferPtr->specularPower  = lightObject_0->GetSpecularPower();

	    // Unlock the constant buffer.
	    deviceContext->Unmap(m_lightBuffer, 0);

	    // Set the position of the light constant buffer in the pixel shader.
	    bufferNumber = psBufferNumber++;

	    // Finally set the light constant buffer in the pixel shader with the updated values.
	    deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_lightBuffer);

        //LightBufferPtr2
        if( rsc.lightProjectionMatrix != NULL && rsc.lightViewMatrix != NULL )
        {// $$$
            // Lock the second light constant buffer so it can be written to.
	        result = deviceContext->Map(m_lightBuffer2, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	        if(FAILED(result))
            {
                cerr << "[FAILED] Lock the second light constant buffer so it can be written to." << endl;
                return false;
            }

        	// Get a pointer to the data in the constant buffer.
	        LightBufferPtr2 = (LightBufferType2*)mappedResource.pData;

	        //// Copy the lighting variables into the constant buffer.
	        LightBufferPtr2->lightPosition = lightObject_0->GetPosition(); //lightPosition;
	        LightBufferPtr2->padding = 0.0f;

       	    // Unlock the constant buffer.
	        deviceContext->Unmap(m_lightBuffer2, 0);

            bufferNumber = vsBufferNumber++;
    	    // Finally set the light constant buffer in the pixel shader with the updated values.
    	    deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_lightBuffer2);
        }
    }

    if ( rsc.tintColor != NULL )
    {// Lock the pixel constant buffer so it can be written to.
	    result = deviceContext->Map(m_pixelBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

        PixelBufferPtr = (PixelBufferType*)mappedResource.pData;

        //D3DXVECTOR4 tColor = D3DXVECTOR4 (1.0,1.0,1.0,1.0);
       // memcpy(&PixelBufferPtr->pixelColor, &rsc.tintColor, sizeof(D3DXVECTOR4));

        PixelBufferPtr->pixelColor = *(rsc.tintColor);
		PixelBufferPtr->alphaCap   = rsc.alphaCap; //0.5f;

        // Unlock the constant buffer.
	    deviceContext->Unmap(m_pixelBuffer, 0);

        bufferNumber = psBufferNumber++;
        deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_pixelBuffer);
    }

	return true;
}

void ShaderObject::RenderShader(ID3D11DeviceContext* deviceContext, int indexCount, int offset)
{
	// Set the vertex input layout.
	deviceContext->IASetInputLayout(m_layout);

    // Set the vertex and pixel shaders that will be used to render this triangle.
    deviceContext->VSSetShader(m_vertexShader, NULL, 0);
    deviceContext->PSSetShader(m_pixelShader, NULL, 0);

    // Set the sampler state in the pixel shader.
    if ( !m_sampleStates.empty() )
    {// If multiple sample states ... for... clamp and stuff?
        for(unsigned int i = 0; i < m_sampleStates.size(); ++i)
        {
          deviceContext->PSSetSamplers(i, 1, &m_sampleStates.at(i));
        }
    }else
    {// single sample state
        deviceContext->PSSetSamplers(0, 1, &m_sampleState);
    }

	// Draws the last <indexCount> vertices/pixel shader stuff.
    //STDMETHODCALLTYPE DrawIndexed( 
    //        __in  UINT IndexCount,
    //        __in  UINT StartIndexLocation,
    //        __in  INT BaseVertexLocation)
	deviceContext->DrawIndexed(indexCount, offset, 0);

	return;
}

//ShaderObject::SHADERCONFIG ShaderObject::createShaderConfig(char * sn, DXGI_FORMAT format, int semInd, int alignedBytOffset)
ShaderObject::SHADERCONFIG ShaderObject::createShaderConfig(char * sn, DXGI_FORMAT format, int semInd)
{
    SHADERCONFIG retSC;

    retSC.SemanticName      = sn;
    retSC.Format            = format;
    retSC.SemanticIndex     = semInd;
    //retSC.AlignedByteOffset = alignedBytOffset;

    return retSC;
}
ShaderObject::SAMPLERDESCCONFIG ShaderObject::createSamplerConfig()
{
    SAMPLERDESCCONFIG retSDC;
    memset(&retSDC, 0, sizeof(retSDC));
    return retSDC;
}
ShaderObject::RENDERSHADERCONFIG ShaderObject::createRenderShaderConfig()
{
    RENDERSHADERCONFIG retRSC;
    memset(&retRSC, 0, sizeof(retRSC));
	retRSC.alphaCap = 1.0f;
    return retRSC;
}

bool ShaderObject::isShadowed() { return hasShadows; }
void ShaderObject::setShadowed(bool shadowed){ hasShadows = shadowed; }

bool ShaderObject::isReflective() { return givesReflection; }
void ShaderObject::setReflective(bool reflective) { givesReflection = reflective; }

void ShaderObject::Shutdown()
{
    ShutdownShader();
}
void ShaderObject::ShutdownShader()
{
	// Release the sampler state.
	if(m_sampleState != NULL && m_sampleStates.empty())
	{
		m_sampleState->Release();
		m_sampleState = 0;
	}

	// Release the layout.
	if(m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}
	
	if(m_vertexShader)
	{// Release the vertex shader.
		m_vertexShader->Release();
		m_vertexShader = 0;
	}
	if(m_pixelShader)
	{// Release the pixel shader.
		m_pixelShader->Release();
		m_pixelShader = 0;
	}

    if (m_matrixBuffer)
    {
        m_matrixBuffer->Release();
        m_matrixBuffer = 0;
    }

    if (m_lightBuffer)
    {
        m_lightBuffer->Release();
        m_lightBuffer= 0;
    }
    if(m_lightBuffer2)
	{
		m_lightBuffer2->Release();
		m_lightBuffer2 = 0;
	}

    if (m_cameraBuffer)
    {
        m_cameraBuffer->Release();
        m_cameraBuffer = 0;
    }

    if (m_animBuffer)
    {
        m_animBuffer->Release();
        m_animBuffer = 0;
    }

	return;
}