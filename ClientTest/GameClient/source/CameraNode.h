#ifndef _CAMERANODE_H_
#define _CAMERANODE_H_

#include "SceneNode.h"
#include "CameraObject.h"

class D3DManager;

class CameraNode : public SceneNode
{
protected:
    CameraObject* camera;
public:
    CameraNode();
    CameraNode(const CameraNode &);
    ~CameraNode();

    void CreateCamera();
    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);
};

#endif