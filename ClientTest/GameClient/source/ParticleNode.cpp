/***
 * ParticleNode.cpp
 * Author: Alfred Tarng
 * Purpose: 
 *
 */

#include <iostream>

#include "ParticleNode.h"
#include "RootNode.h"

#include "SystemManager.h"
#include "GraphicsManager.h"
#include "ParticleSystem.h"


ParticleNode::ParticleNode()
{
    particleObject = NULL;
    shader = NULL;
}
ParticleNode::~ParticleNode()
{
    delete particleObject;
	particleObject = NULL;
}

void ParticleNode::SetParticleObject(ParticleSystemObject* setParticle)
{
    particleObject = setParticle;
}
void ParticleNode::UpdateMatrix(){};
void ParticleNode::Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode )
{
    stack->Push();
        D3DXMATRIX mat = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());

        D3DXVECTOR3 pScale, pTrans;
        D3DXQUATERNION pRot;
        D3DXMatrixDecompose( &pScale, &pRot, &pTrans,&mat);

        if ( particleObject->isBillBoarded( SYSTEM ) )//|| particleObject->isBillBoarded( PARTICLE ) )
        {// Do Not inherit rotation from parent.
            stack->LoadIdentity();
            stack -> ScaleLocal( pScale.x,pScale.y, pScale.z  );
            stack -> Translate ( pTrans.x, pTrans.y, pTrans.z );
        }
        //stack->RotateYawPitchRollLocal( pRot.x, pRot.y, pRot.z );

        D3DXVECTOR3 scale       = particleObject->GetScale();
        D3DXVECTOR3 rotation    = particleObject->GetRotation();
        D3DXVECTOR3 translation = particleObject->GetPosition();

        float pitch, yaw ,roll;
        pitch = rotation.x * 0.0174532925f;
	    yaw   = rotation.y * 0.0174532925f;
	    roll  = rotation.z * 0.0174532925f;

        stack->ScaleLocal(scale.x, scale.y, scale.z);
        stack->RotateYawPitchRollLocal( yaw, pitch, roll);
        stack->Translate(translation.x, translation.y, translation.z);

        calculatedMatrix = *(SystemManager::Instance()->GetGraphicsManager()->d3d_matrixStack->GetTop());
        D3DXMatrixDecompose( &pScale, &pRot, &pTrans, &calculatedMatrix);

        FrustumObject * frustumObject_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetFrustumObject();
        float boundingR  = 5;
        bool renderModel = frustumObject_Ptr->CheckSphere(pTrans.x, pTrans.y, pTrans.z, boundingR);

        // save this matrix so particles can be drawn last
        if(renderModel)
        { 
            SceneNode * sn = SystemManager::Instance()->GetGraphicsManager()->GetWorld();
            if (sn->isRootNode() && (drawMode == 0))
                ((RootNode *)(sn))->AppendAlphaChildren(this);//alphaChildren.push_back(this);
        }else 
        {
            frustumObject_Ptr->incCulledObjects();
        }

        // most likely will not actually have children
        std::vector<SceneNode *>::iterator it;
        for( it = children.begin(); it < children.end(); it++ )
        {
            (*it)->Draw( d3dMan, stack, drawMode );
        }
    stack->Pop();

}
void ParticleNode::Render(D3DManager* d3dMan)
{
    d3dMan->TurnZBufferOff(1);
    d3dMan->EnableAlphaBlending(1);
    particleObject -> Render( d3dMan->GetDeviceContext() );
    if (shader)
    {
        D3DXMATRIX camMatrix, billBoardMatrix, billBoardMatrix2, cameraPositionMatrix;
        CameraObject* cam = SystemManager::Instance()->GetGraphicsManager()->GetCamera();
        ShaderObject::RENDERSHADERCONFIG myRSC = ShaderObject::createRenderShaderConfig();
        myRSC.camObj  = cam;

        cam->GetMatrix(camMatrix);
        if ( particleObject->isBillBoarded( SYSTEM ) )
        {

            D3DXMatrixInverse( &billBoardMatrix, NULL, &camMatrix );
            D3DXMatrixTranslation(&cameraPositionMatrix, - (cam->GetPOffset().x), - (cam->GetPOffset().y), - (cam->GetPOffset().z));
            D3DXMatrixMultiply(&billBoardMatrix, &billBoardMatrix, &cameraPositionMatrix);

        }else if ( particleObject->isBillBoarded( PARTICLE ) )
        {   
            D3DXMatrixInverse( &billBoardMatrix2, NULL, &camMatrix );
            D3DXMatrixTranslation(&cameraPositionMatrix, - (cam->GetPOffset().x), - (cam->GetPOffset().y), - (cam->GetPOffset().z));
            D3DXMatrixMultiply(&billBoardMatrix2, &billBoardMatrix2, &cameraPositionMatrix);

            D3DXVECTOR3 pScale, pTrans;
            D3DXQUATERNION pRot;
            D3DXMatrixDecompose( &pScale, &pRot, &pTrans,&calculatedMatrix);

            D3DXMatrixRotationQuaternion(&billBoardMatrix, &pRot);
            D3DXMatrixInverse( &billBoardMatrix, NULL, &billBoardMatrix);
            D3DXMatrixMultiply(&billBoardMatrix2, &billBoardMatrix2, &billBoardMatrix);

            myRSC.particleMatrix = &billBoardMatrix2;

            D3DXMatrixIdentity(&billBoardMatrix);
        }else
        {
            D3DXMatrixIdentity(&billBoardMatrix);
        }

        shader->Render( d3dMan -> GetDeviceContext(), particleObject->GetParticleSystem()->GetIndexCount(), 0,
                        billBoardMatrix * calculatedMatrix, camMatrix, d3dMan -> GetProjectionMatrix(), 
                        particleObject->GetParticleSystem()->GetTextureObject()->GetTextures(), particleObject->GetParticleSystem()->GetTextureObject()->numTextures(),
                        myRSC
                      );
    }
    d3dMan->DisableAlphaBlending();
    d3dMan->TurnZBufferOn();
}

/***
 *
 */
bool ParticleNode::checkForRemoval()
{
    if (particleObject->Expires())
        return particleObject ->isExpired();
    else return false;
}