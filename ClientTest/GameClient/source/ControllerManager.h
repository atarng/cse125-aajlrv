////////////////////////////////////////////////////////////////////////////////
// Filename: ControllerManager.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CONTROLLER_MANAGER_H_
#define _CONTROLLER_MANAGER_H_

////////////////////////////////////////////////////////////////////////////////
// Class name: ControllerManager
////////////////////////////////////////////////////////////////////////////////

#include <Xinput.h>
#pragma comment (lib, "Xinput.lib")

#include "InputManager.h"
#include "InputHandler.h"
#include <iostream>
#include "SystemManager.h"

class ControllerManager {
	public:
	ControllerManager();
	ControllerManager(const ControllerManager&);
	~ControllerManager();

	static ControllerManager * Instance();

	bool Initialize();

	void Frame();

	

private:
	static ControllerManager * m_Instance;
	XINPUT_STATE state;
	InputManager * im;
	bool start;
	bool back;
	bool left_shoulder;
	bool right_shoulder;
	bool w;
	bool a;
	bool s;
	bool d;
	bool trigger;
	bool sw;
	bool space;
	bool shift;
	bool q;
	bool z;
};
#endif