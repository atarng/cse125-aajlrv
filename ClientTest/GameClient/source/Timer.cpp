//Timer.cpp
#include <stdio.h>
#include "Timer.h"


Timer::Timer()
{
    startTime = clock();
	currTime = clock();
}

Timer::Timer(const Timer& other)
{
    startTime = other.startTime;
    currTime = other.currTime;
}

Timer::~Timer()
{
}

//Get the starting time in cycles
float Timer::GetStartTime()
{
    return (float)startTime/CLOCKS_PER_SEC;
}

//Get the currentTime in cycles
float Timer::GetCurrTime()
{
    return (float)currTime/CLOCKS_PER_SEC;
}
    
//Get the time since the start in cycles
float Timer::GetTimeSinceStart()
{
    currTime = clock();
    return (float)(currTime - startTime)/CLOCKS_PER_SEC;
}

//Get the change in time since the last update
float Timer::Update()
{
    clock_t lastTime = currTime;
    currTime = clock();
    return (float)(currTime - lastTime)/CLOCKS_PER_SEC;
}