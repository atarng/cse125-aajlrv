#ifndef _TERRAINOBJECT_H_
#define _TERRAINOBJECT_H_

#include "Mesh.h"
#include "D3DManager.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: Terrain
////////////////////////////////////////////////////////////////////////////////
class Terrain : public Mesh
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
        D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
        D3DXVECTOR4 color;
	};

	struct HeightMapType 
	{ 
		float x, y, z;
        float nx, ny, nz;
	};

    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer; // shared component with model object.
    int m_vertexCount, m_indexCount;              // shared component with model object.
    HeightMapType* m_heightMap;                   // kinda shared
    int m_terrainWidth, m_terrainHeight;
    float maxHeight;
    TextureObject* m_Textures;

    bool LoadHeightMap(char*);
	void NormalizeHeightMap();
	void ShutdownHeightMap();

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

    D3DXVECTOR3 position;
    D3DXQUATERNION orientation;
    D3DXVECTOR3 rotation;

    D3DXVECTOR3 scale;

public:
	Terrain();
	Terrain(const Terrain&);
	~Terrain();

    bool Initialize(ID3D11Device*, char*);
	void Shutdown();
	

    //float getHeightAt(float x, float z);
    void getHeightAt(float& x, float& y, float& z);

    inline void SetPosition(D3DXVECTOR3 newPos){ position = newPos; }
    inline void SetRotation(D3DXVECTOR3 newRot){ rotation = newRot; }
    inline void SetOrientation(D3DXQUATERNION newOrient){orientation = newOrient; }
    inline void SetScale(D3DXVECTOR3 newScale){scale = newScale;}

    inline D3DXVECTOR3 GetPosition()       {return position;}
    inline D3DXQUATERNION GetOrientation() {return orientation;}
    inline D3DXVECTOR3 GetRotation()       {return rotation;}
    inline D3DXVECTOR3 GetScale()          {return scale;}

    void GetMatrix(D3DXMATRIX& m_viewMatrix);
    void Render(ID3D11DeviceContext*);

	int GetIndexCount();

    D3DXVECTOR3 SelectColor(float height, float maxHeight);
    virtual TextureObject* GetTextureObject();
    virtual void SetTextureObject(TextureObject &);
};

#endif