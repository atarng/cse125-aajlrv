#ifndef _D3D_HEADER_
#define _D3D_HEADER_

#include <D3DX/dxgi.h>
#include <D3DX/d3dcommon.h>
#include <D3DX/d3d11.h>
#include <D3DX/d3dx11.h>
//#include <D3DX/d3dx10.h>
#include <D3DX/d3dx10math.h> // Not on lab computers?

#include "AnimEvaluator.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: D3DManager
////////////////////////////////////////////////////////////////////////////////
class D3DManager
{
public:
    D3DManager();
	D3DManager(const D3DManager&);
	~D3DManager();

	bool Initialize(int, int, bool, HWND, bool, float, float);
    void Shutdown();
	
	void BeginScene(float, float, float, float);
	void EndScene();

    void ClearRenderTarget(ID3D11DeviceContext*, float, float, float, float);

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();

	D3DXMATRIX& GetProjectionMatrix();
	D3DXMATRIX& GetWorldMatrix();
    D3DXMATRIX& GetOrthogonalMatrix();

    ID3D11DepthStencilView* GetDepthStencilView();

    void RestoreRenderTarget(); // $$$

    void EnableAlphaBlending( int );
    void DisableAlphaBlending();

    void TurnZBufferOn();
	void TurnZBufferOff( int );

    void TurnOffBackFaceCull();
    void RestoreBackFaceCull();

    //ID3D11ShaderResourceView* GetShaderResourceView();
    static D3DXMATRIX AssimpMat2D3DMat( aiMatrix4x4 );
    static void PrintMatrix(D3DXMATRIX);

private:
	bool m_vsync_enabled;               // S
	int m_videoCardMemory;              // S
	char m_videoCardDescription[128];   // S

	IDXGISwapChain*          m_swapChain;               // C,S
	ID3D11Device*            m_device;                  // C,S
	ID3D11DeviceContext*     m_deviceContext;           // C,S

    //ID3D11RenderTargetView* RT_m_renderTargetView; // $$$
	ID3D11RenderTargetView*  m_renderTargetView;        // S
    ID3D11RasterizerState*   m_rasterState;             // C,S
    // To allow for backface display
    ID3D11RasterizerState*   m_BackFaceEnabled;         // C,S

    ID3D11Texture2D*         m_depthStencilBuffer;      // C,S
	ID3D11DepthStencilState* m_depthStencilState;            // C,S
    ID3D11DepthStencilState* m_depthStencilState_Disabled_0; // C
    ID3D11DepthStencilState* m_depthStencilState_Disabled_1; // C
	ID3D11DepthStencilView*  m_depthStencilView;             // C,S
	
    D3DXMATRIX m_projectionMatrix;  // S
	D3DXMATRIX m_worldMatrix;       // S
	D3DXMATRIX m_orthoMatrix;       // S
    D3D11_VIEWPORT viewPort;


    ID3D11BlendState* m_alphaEnableBlendingState_0; // C
    ID3D11BlendState* m_alphaEnableBlendingState_1; // C
	ID3D11BlendState* m_alphaDisableBlendingState;  // C
};

#endif