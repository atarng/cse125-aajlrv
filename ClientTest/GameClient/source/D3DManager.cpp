////////////////////////////////////////////////////////////////////////////////
// Filename: D3DManager.cpp
////////////////////////////////////////////////////////////////////////////////
#include "D3DManager.h"

// include the Direct3D Library file
#pragma comment (lib, "dxgi.lib")
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

#include "Log.h"
#include <iostream>

using namespace std;

D3DManager::D3DManager()
{
    m_swapChain = 0; // S
    m_device = 0;    // S
    m_deviceContext = 0; //S

    m_renderTargetView = 0;
    m_rasterState = 0;
    m_BackFaceEnabled = 0;

    m_depthStencilBuffer = 0;
    m_depthStencilState = 0;
    m_depthStencilState_Disabled_0 = 0;
    m_depthStencilState_Disabled_1 = 0;
    m_depthStencilView = 0;

    m_alphaEnableBlendingState_0 = 0;
    m_alphaEnableBlendingState_1 = 0;
	m_alphaDisableBlendingState = 0;

    //m_viewPort
    //viewPort = NULL;
}
D3DManager::D3DManager(const D3DManager& other)
{
}
D3DManager::~D3DManager()
{
    Shutdown();
}

void D3DManager::Shutdown()
{
    if(m_swapChain)
    {
        m_swapChain->SetFullscreenState(false, NULL);
        m_swapChain->Release();
        m_swapChain = 0;
    }
    if(m_device)
    {
        m_device->Release();
        m_device = 0;
    }
    if (m_deviceContext)
    {
        m_deviceContext->Release();
        m_deviceContext = 0;
    }
    if (m_rasterState)
    {
        m_rasterState->Release();
        m_rasterState = 0;
    }
    if (m_BackFaceEnabled)
    {
        m_BackFaceEnabled->Release();
        m_BackFaceEnabled = 0;
    }
    if(m_alphaEnableBlendingState_0)
    {
        m_alphaEnableBlendingState_0->Release();
        m_alphaEnableBlendingState_0 = 0;
    }
    if(m_alphaEnableBlendingState_1)
    {
        m_alphaEnableBlendingState_1->Release();
        m_alphaEnableBlendingState_1 = 0;
    }
    if ( m_depthStencilState )
    {
		m_depthStencilState -> Release();
		m_depthStencilState = 0;    
    }
    if ( m_depthStencilState_Disabled_0 )
	{
		m_depthStencilState_Disabled_0->Release();
		m_depthStencilState_Disabled_0 = 0;
	}
    if ( m_depthStencilState_Disabled_1 )
	{
		m_depthStencilState_Disabled_1->Release();
		m_depthStencilState_Disabled_1 = 0;
	}
    if ( m_depthStencilView )
	{
		m_depthStencilView->Release();
		m_depthStencilView = 0;
	}
    if ( m_alphaDisableBlendingState )
    {
        m_alphaDisableBlendingState->Release();
        m_alphaDisableBlendingState = 0;
    }
}
bool D3DManager::Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, 
                            float screenDepth, float screenNear)

{
    HRESULT result; // S
    int error;      // S

    DXGI_SWAP_CHAIN_DESC swapChainDesc; // V, S
    ID3D11Texture2D* backBufferPtr;     // S
    DXGI_MODE_DESC* displayModeList;    // S

    IDXGIFactory* factory;
    IDXGIAdapter* adapter;
    IDXGIOutput* adapterOutput;
    DXGI_ADAPTER_DESC adapterDesc;

    unsigned int numModes, i, numerator, denominator, stringLength;
	numerator = denominator = 1;
    D3D_FEATURE_LEVEL featureLevel;                     // V
    D3D11_TEXTURE2D_DESC depthBufferDesc;               // V
    D3D11_DEPTH_STENCIL_DESC depthStencilDesc;          // V
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc; // V
    D3D11_RASTERIZER_DESC rasterDesc;   // V
    float fieldOfView, screenAspect;    // V

    D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
    D3D11_BLEND_DESC blendStateDescription;

    // Store the vsync setting.
    m_vsync_enabled = vsync;

    // Create a DirectX graphics interface factory.
    result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
    if(FAILED(result))
    {
        return false;
    }

    // Use the factory to create an adapter for the primary graphics interface (video card).
    result = factory->EnumAdapters(0, &adapter);
    if(FAILED(result))
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <factory->EnumAdapters();> failed");
        return false;
    }

    // Enumerate the primary adapter output (monitor).
    result = adapter->EnumOutputs(0, &adapterOutput);
    if(FAILED(result))
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <adapter->EnumOutputs();> failed");
        return false;
    }

    // Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
    result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
    if(FAILED(result))
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);> failed");
        return false;
    }

    // Create a list to hold all the possible display modes for this monitor/video card combination.
    displayModeList = new DXGI_MODE_DESC[numModes];
    if(!displayModeList)
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() displayModeList failed init");
        return false;
    }

    // Now fill the display mode list structures.
    result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
    if(FAILED(result))
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);> failed");
        return false;
    }

    // Now go through all the display modes and find the one that matches the screen width and height.
    // When a match is found store the numerator and denominator of the refresh rate for that monitor.
    for(i=0; i<numModes; i++)
    {
        if(displayModeList[i].Width == (unsigned int)screenWidth)
        {
            if(displayModeList[i].Height == (unsigned int)screenHeight)
            {
                numerator = displayModeList[i].RefreshRate.Numerator;
                denominator = displayModeList[i].RefreshRate.Denominator;
            }
        }
    }

    // Get the adapter (video card) description.
    result = adapter->GetDesc(&adapterDesc);
    if(FAILED(result))
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <adapter->GetDesc(&adapterDesc);> failed");
        return false;
    }

    // Store the dedicated video card memory in megabytes.
    m_videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

    // Convert the name of the video card to a character array and store it.
    error = wcstombs_s(&stringLength, m_videoCardDescription, 128, adapterDesc.Description, 128);
    if(error != 0)
    {
        Log::Instance()->PrintErr("D3DManager::Initialize() call to <wcstombs_s(&stringLength, m_videoCardDescription, 128, adapterDesc.Description, 128);> failed");
        return false;
    }

    // Release the display mode list.
    delete [] displayModeList;
    displayModeList = 0;

    // Release the adapter output.
    adapterOutput->Release();
    adapterOutput = 0;

    // Release the adapter.
    adapter->Release();
    adapter = 0;

    // Release the factory.
    factory->Release();
    factory = 0;

    // Initialize the swap chain description.
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

    // Set to a single back buffer.
    swapChainDesc.BufferCount = 1;                                // V
    // Set regular 32-bit surface for the back buffer.
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // V
    // Set the width and height of the back buffer.
    swapChainDesc.BufferDesc.Width = screenWidth;                 // V
    swapChainDesc.BufferDesc.Height = screenHeight;               // V
    // Set the usage of the back buffer.
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;  // V, S
    // Don't set the advanced flags.
    swapChainDesc.Flags = 0;                                      // V, S
    // Set the refresh rate of the back buffer.
    if(m_vsync_enabled)
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator   = numerator;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
    } else
	{
	    swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}
    // Set the handle for the window to render to.
    swapChainDesc.OutputWindow = hwnd;
    // Turn multisampling off.
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;

    // Set to full screen or windowed mode.
    if(fullscreen)
    {
        Log::Instance()->Print("FullScreen Mode");
        swapChainDesc.Windowed = false;
    }
    else
    {
        Log::Instance()->Print("Windowed Mode");
        swapChainDesc.Windowed = true;
    }

    // Set the scan line ordering and scaling to unspecified.
    swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    // Discard the back buffer contents after presenting.
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    // Set the feature level to DirectX 11.
    featureLevel = D3D_FEATURE_LEVEL_11_0;

    //HRESULT WINAPI D3D11CreateDeviceAndSwapChain(
    //__in_opt IDXGIAdapter* pAdapter,
    //D3D_DRIVER_TYPE DriverType,
    //HMODULE Software,
    //UINT Flags,
    //__in_ecount_opt( FeatureLevels ) CONST D3D_FEATURE_LEVEL* pFeatureLevels,
    //UINT FeatureLevels,
    //UINT SDKVersion,
    //__in_opt CONST DXGI_SWAP_CHAIN_DESC* pSwapChainDesc,
    //__out_opt IDXGISwapChain** ppSwapChain,
    //__out_opt ID3D11Device** ppDevice,
    //__out_opt D3D_FEATURE_LEVEL* pFeatureLevel,
    //__out_opt ID3D11DeviceContext** ppImmediateContext );
    // Create the swap chain, Direct3D device, and Direct3D device context.
    UINT createDeviceFlags = 0;
    #if defined(DEBUG) || defined(_DEBUG)  
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    #endif

    // V XSX
    result = D3D11CreateDeviceAndSwapChain( NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0,
                                            NULL, NULL, D3D11_SDK_VERSION, &swapChainDesc,
                                            &m_swapChain, &m_device, &featureLevel, &m_deviceContext);
    if(FAILED(result))
    {
		cout << "FAIL:  Create the swap chain, Direct3D device, and Direct3D device context. " << endl;
        Log::Instance()->PrintErr("FAIL:  Create the swap chain, Direct3D device, and Direct3D device context.");
        return false;
    }

    // Get the pointer to the back buffer.
    result = m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr); // V
    if(FAILED(result))
    {
        cout << "FAIL:  Get the pointer to the back buffer." << endl;
        Log::Instance()->PrintErr("FAIL:  Get the pointer to the back buffer.");
        return false;
    }

    // Create the render target view with the back buffer pointer.
    result = m_device->CreateRenderTargetView(backBufferPtr, NULL, &m_renderTargetView); // V
    if(FAILED(result))
    {
        cout << "FAIL:  Create the render target view with the back buffer pointer." << endl;
        Log::Instance()->PrintErr("FAIL:  Create the render target view with the back buffer pointer.");
        return false;
    }
    // Release pointer to the back buffer as we no longer need it.
    backBufferPtr->Release();
    backBufferPtr = 0;
    // set the render target as the back buffer ### INITIALIZE ***
    //m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, NULL);

    // Initialize the description of the depth buffer.
    ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

    // Set up the description of the depth buffer.
    depthBufferDesc.Width     = screenWidth;
    depthBufferDesc.Height    = screenHeight;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format    = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count   = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage     = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags      = 0;

    // Create the texture for the depth buffer using the filled out description.
    result = m_device -> CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencilBuffer);
    if(FAILED(result))
    {
        cout << "FAIL: Create the texture for the depth buffer using the filled out description." << endl;
        Log::Instance()->PrintErr("FAIL: Create the texture for the depth buffer using the filled out description.");
        return false;
    }

/// ZBUFFER INITIALIZATION
    // Initialize the description of the stencil state.
    ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
    // Set up the description of the stencil state.
    depthStencilDesc.DepthEnable    = true;
    depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    depthStencilDesc.DepthFunc      = D3D11_COMPARISON_LESS;
    // Stencil?
    depthStencilDesc.StencilEnable    = true;
    depthStencilDesc.StencilReadMask  = 0xFF;
    depthStencilDesc.StencilWriteMask = 0xFF;
    // Stencil operations if pixel is front-facing.
    depthStencilDesc.FrontFace.StencilFailOp      = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    depthStencilDesc.FrontFace.StencilPassOp      = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilFunc        = D3D11_COMPARISON_ALWAYS;
    // Stencil operations if pixel is back-facing.
    depthStencilDesc.BackFace.StencilFailOp       = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilDepthFailOp  = D3D11_STENCIL_OP_DECR;
    depthStencilDesc.BackFace.StencilPassOp       = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilFunc         = D3D11_COMPARISON_ALWAYS;

    // Create the depth stencil state.
    result = m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState);
    if(FAILED(result))
    {
        cout << "FAIL: Create the depth stencil state." << endl;
        Log::Instance()->PrintErr("FAIL: Create the depth stencil state.");
        return false;
    }
    // S
    // Set the depth stencil state.
    m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);

    // Initialize the depth stencil view.
    ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

    // Set up the depth stencil view description.
    depthStencilViewDesc.Format        = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    // Create the depth stencil view.
    result = m_device->CreateDepthStencilView(m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
    if(FAILED(result))
    {
        cout << "FAIL: Create the depth stencil view." << endl;
        Log::Instance()->PrintErr("FAIL: Create the depth stencil view.");
        return false;
    }

    // Bind the render target view and depth stencil buffer to the output render pipeline.
    m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);

    // Setup the raster description which will determine how and what polygons will be drawn.
    rasterDesc.AntialiasedLineEnable = false;
    //rasterDesc.CullMode = D3D11_CULL_NONE;// CULLS BACKFACING
    rasterDesc.CullMode = D3D11_CULL_BACK;
    rasterDesc.DepthBias = 0;
    rasterDesc.DepthBiasClamp = 0.0f;
    rasterDesc.DepthClipEnable = true;
    rasterDesc.FillMode = D3D11_FILL_SOLID;
    rasterDesc.FrontCounterClockwise = false;
    rasterDesc.MultisampleEnable     = false;
    rasterDesc.ScissorEnable         = false;
    rasterDesc.SlopeScaledDepthBias = 0.0f;

    // Create the rasterizer state from the description we just filled out.
    result = m_device->CreateRasterizerState(&rasterDesc, &m_rasterState);
    if(FAILED(result))
    {
        return false;
    }
    // Now set the rasterizer state.
    m_deviceContext->RSSetState(m_rasterState);

    rasterDesc.CullMode = D3D11_CULL_NONE;
    result = m_device->CreateRasterizerState(&rasterDesc, &m_BackFaceEnabled);
    if(FAILED(result)) return false;

//  #########################################
    ZeroMemory(&viewPort, sizeof(D3D11_VIEWPORT)); 
    
    //// Setup the viewport for rendering.
    viewPort.Width = (float)screenWidth;
    viewPort.Height = (float)screenHeight;
    viewPort.MinDepth = 0.0f;
    viewPort.MaxDepth = 1.0f;
    viewPort.TopLeftX = 0.0f;
    viewPort.TopLeftY = 0.0f;
    
    //// Create the viewport.
    m_deviceContext->RSSetViewports(1, &viewPort); // V

    // Setup the projection matrix.
    fieldOfView  = (float)D3DX_PI / 4.0f;
    screenAspect = (fullscreen)?((float)GetSystemMetrics(SM_CXSCREEN)/(float)GetSystemMetrics(SM_CYSCREEN) ):((float)screenWidth / (float)screenHeight);

    // Create the projection matrix for 3D rendering.
    D3DXMatrixPerspectiveFovLH(&m_projectionMatrix, fieldOfView, screenAspect, screenNear, screenDepth);

    // Initialize the world matrix to the identity matrix.
    D3DXMatrixIdentity(&m_worldMatrix);

    // Create an orthographic projection matrix for 2D rendering.
    D3DXMatrixOrthoLH(&m_orthoMatrix, (float)screenWidth, (float)screenHeight, screenNear, screenDepth);

/// DISABLED ZBUFFER DESCRIPTION
    // Clear the second depth stencil state before setting the parameters.
	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));
	// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
	// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
	depthDisabledStencilDesc.DepthEnable = false; // #
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create the state using the device.
	result = m_device->CreateDepthStencilState(&depthDisabledStencilDesc, &m_depthStencilState_Disabled_0);
	if(FAILED(result))
	{
        cout << "[D3DManager] FAILED: Create m_depthStencilState_Disabled using the device." << endl;
		return false;
	}

    // (PARTICLE Z DISABLE BUFFER)
    // Clear the second depth stencil state before setting the parameters.
	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc)); //###
    depthDisabledStencilDesc.DepthEnable = true; // #
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO ;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create the state using the device.
	result = m_device->CreateDepthStencilState(&depthDisabledStencilDesc, &m_depthStencilState_Disabled_1);
	if(FAILED(result))
	{
        cout << "[D3DManager] FAILED: Create m_depthStencilState_Disabled(type 2) using the device." << endl;
		return false;
	}

///////////////////////////////##################################################

    // Clear the blend state description.
	ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));

	// Create an alpha enabled blend state description.
	blendStateDescription.RenderTarget[0].BlendEnable    = TRUE;
    blendStateDescription.RenderTarget[0].SrcBlend       = D3D11_BLEND_SRC_ALPHA;//D3D11_BLEND_ONE;
    blendStateDescription.RenderTarget[0].DestBlend      = D3D11_BLEND_INV_SRC_ALPHA; //DEFAULT:: D3D11_BLEND_ONE;
    blendStateDescription.RenderTarget[0].BlendOp        = D3D11_BLEND_OP_ADD;
    blendStateDescription.RenderTarget[0].SrcBlendAlpha  = D3D11_BLEND_ONE;
    blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendStateDescription.RenderTarget[0].BlendOpAlpha   = D3D11_BLEND_OP_ADD;
    blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	// Create the blend state using the description.
	result = m_device->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState_0);
	if(FAILED(result))
	{
        cout << "[D3DManager] Failed:: Create the blend state using the description." << endl;
		return false;
	}

    // Creates the alternate (shiny) alpha blen.
    // D3D11_BLEND_SRC_ALPHA ALPHA OF THING RETURNED BY SHADER
    blendStateDescription.RenderTarget[0].SrcBlend       = D3D11_BLEND_SRC_ALPHA;
    blendStateDescription.RenderTarget[0].DestBlend      = D3D11_BLEND_ONE;  //THIS IS WHAT WAS DRAWN FIRST
    // DOESN'T SEEM TO DO ANYTHING
    //blendStateDescription.RenderTarget[0].SrcBlendAlpha    = D3D11_BLEND_ZERO;//D3D11_BLEND_SRC_ALPHA;
    //blendStateDescription.RenderTarget[0].DestBlendAlpha   = D3D11_BLEND_ZERO;//D3D11_BLEND_DEST_ALPHA;
    //blendStateDescription.RenderTarget[0].BlendOpAlpha     = D3D11_BLEND_OP_MIN;
	result = m_device->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState_1);
	if(FAILED(result))
	{
        cout << "[D3DManager] Failed:: Create the blend state using the description." << endl;
		return false;
	}


	// Modify the description to create an alpha disabled blend state description.
	blendStateDescription.RenderTarget[0].BlendEnable = FALSE;
    //blendStateDescription.RenderTarget[0].SrcBlend       = D3D11_BLEND_ONE;//D3D11_BLEND_SRC_ALPHA;//D3D11_BLEND_SRC_ALPHA;//D3D11_BLEND_ONE;
    //blendStateDescription.RenderTarget[0].DestBlend      = D3D11_BLEND_ZERO;//D3D11_BLEND_ZERO;
	// Create the blend state using the description.
	result = m_device->CreateBlendState(&blendStateDescription, &m_alphaDisableBlendingState);
	if(FAILED(result))
	{
		return false;
	}

    return true;
}

void D3DManager::BeginScene(float red, float green, float blue, float alpha)
{   float color[4];

    // Setup the color to clear the buffer to.
    color[0] = red;
    color[1] = green;
    color[2] = blue;
    color[3] = alpha;

    // Clear the back buffer.
    m_deviceContext->ClearRenderTargetView(m_renderTargetView, color);
    
    // Clear the depth buffer.
    m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
    return;
}
void D3DManager::EndScene()
{
    if( m_swapChain == NULL )
    {
        cerr << "swap chain is null" << endl;
        return;
    }

    // Present the back buffer to the screen since rendering is complete.
    if(m_vsync_enabled)
    {// Lock to screen refresh rate.
        m_swapChain->Present(1, 0);
    }
    else
    {// Present as fast as possible.
        m_swapChain->Present(0, 0);
    }

    float color[4];

    // Setup the color to clear the buffer to.
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    color[3] = 0;

    m_deviceContext->ClearRenderTargetView(m_renderTargetView, color);
    m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

    return;

}

void D3DManager::EnableAlphaBlending( int type )
{
	float blendFactor[4];
	
	// Setup the blend factor.
	blendFactor[0] = 0.03f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	
	// Turn on the alpha blending.
    if ( type == 0 )
	    m_deviceContext->OMSetBlendState( m_alphaEnableBlendingState_0, blendFactor, 0xffffffff );
    else if ( type == 1 )
        m_deviceContext->OMSetBlendState( m_alphaEnableBlendingState_1, blendFactor, 0xffffffff );

	return;
}
void D3DManager::DisableAlphaBlending()
{
	float blendFactor[4];

	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	
	// Turn off the alpha blending.
	m_deviceContext->OMSetBlendState(m_alphaDisableBlendingState, blendFactor, 0xffffffff);

	return;
}

void D3DManager::TurnZBufferOn()
{
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);
	return;
}
void D3DManager::TurnZBufferOff(int type)
{
    if ( type == 0 ) m_deviceContext->OMSetDepthStencilState(m_depthStencilState_Disabled_0, 1);
    else             m_deviceContext->OMSetDepthStencilState(m_depthStencilState_Disabled_1, 1);
	return;
}

void D3DManager::TurnOffBackFaceCull()
{
    m_deviceContext->RSSetState(m_BackFaceEnabled);
}
void D3DManager::RestoreBackFaceCull()
{
    m_deviceContext->RSSetState(m_rasterState);
}

ID3D11Device* D3DManager::GetDevice()
{
    return m_device;
}
ID3D11DeviceContext* D3DManager::GetDeviceContext()
{
    return m_deviceContext;
}

D3DXMATRIX& D3DManager::GetProjectionMatrix() //D3DXMATRIX& projectionMatrix)
{
    //projectionMatrix = m_projectionMatrix;
    return m_projectionMatrix;
}
D3DXMATRIX& D3DManager::GetWorldMatrix() //D3DXMATRIX& worldMatrix)
{
    //worldMatrix = m_worldMatrix;
    return m_worldMatrix;
}
D3DXMATRIX& D3DManager::GetOrthogonalMatrix() //D3DXMATRIX& orthoMatrix)
{
    return m_orthoMatrix;
}

ID3D11DepthStencilView* D3DManager::GetDepthStencilView()
{
	return m_depthStencilView;
}

// Bind the render target view and depth stencil buffer to the output render pipeline.
void D3DManager::RestoreRenderTarget()
{
    //std::cout << "restore D3D's render targetview " << std::endl;
    //m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);
    m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);
    //m_deviceContext->OMGetRenderTargetsAndUnorderedAccessViews(1, &m_renderTargetView,&m_depthStencilView, 0, 0, NULL);
    m_deviceContext->RSSetViewports(1, &(viewPort));
  //D3D11_KEEP_UNORDERED_ACCESS_VIEWS
}

// HELPERS
D3DXMATRIX D3DManager::AssimpMat2D3DMat(aiMatrix4x4 convertMatrix)
{
    convertMatrix = convertMatrix.Transpose();
    return D3DXMATRIX( convertMatrix.a1,convertMatrix.a2,convertMatrix.a3,convertMatrix.a4,
                       convertMatrix.b1,convertMatrix.b2,convertMatrix.b3,convertMatrix.b4,
                       convertMatrix.c1,convertMatrix.c2,convertMatrix.c3,convertMatrix.c4,
                       convertMatrix.d1,convertMatrix.d2,convertMatrix.d3,convertMatrix.d4 );
}
void D3DManager::PrintMatrix(D3DXMATRIX printMatrix)
{
    printf("Matrix is:\n");
    printf("%f\t%f\t%f\t%f\n", printMatrix(0,0), printMatrix(0,1),printMatrix(0,2),printMatrix(0,3));
    printf("%f\t%f\t%f\t%f\n", printMatrix(1,0), printMatrix(1,1),printMatrix(1,2),printMatrix(1,3));
    printf("%f\t%f\t%f\t%f\n", printMatrix(2,0), printMatrix(2,1),printMatrix(2,2),printMatrix(2,3));
    printf("%f\t%f\t%f\t%f\n", printMatrix(3,0), printMatrix(3,1),printMatrix(3,2),printMatrix(3,3));
}