#include "ModelObject.h"
#include "Debugger.h"

#include <assimpCollada/aiPostProcess.h>

//#pragma comment(lib, "lib/assimp_x86/assimp.lib")

ModelObject::ModelObject()
{
    m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_model = 0;
    m_Textures; // = 0;
}
ModelObject::ModelObject(const ModelObject& other)
{
}
ModelObject::~ModelObject()
{
    //if (m_Textures)
    //{
    //    m_Textures->Shutdown();
    //    delete m_Textures;
    //    m_Textures = NULL;
    //}
}

//bool ModelObject::Initialize(ID3D11Device* device, char* modelFilename)
// fileType
//  0: simplified txt.
bool ModelObject::Initialize(ID3D11Device* device, char* modelFilename, vector<char*> textureFileVector, int fileType)
{
    bool result;
	// Load in the model data,
	result = LoadModel(modelFilename, fileType);
	if(!result)
	{
        cout << "LoadModel(mfn) failed..." << endl;
		return false;
	}
	// Initialize the vertex and index buffers.
	result = InitializeBuffers(device);
	if(!result)
	{
        cout << "Init buffers failed..." << endl;
		return false;
	}

	// Load the texture for this model.
	result = LoadTextures(device, textureFileVector );
	if(!result)
	{
        cout << "[ModelObject::Initialize()] Texture Initialization failed..." << endl;
		return false;
	}

    return true;
}
bool ModelObject::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData; // ????
	HRESULT result;
	int i;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	// Load the vertex array and index array with data.
	for(i = 0; i < m_vertexCount; i++)
	{
		vertices[i].position = D3DXVECTOR3(m_model[i].x,  m_model[i].y, m_model[i].z);
		vertices[i].texture  = D3DXVECTOR2(m_model[i].tu, m_model[i].tv);
		vertices[i].normal   = D3DXVECTOR3(m_model[i].nx, m_model[i].ny, m_model[i].nz);

		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; // V
    vertexBufferDesc.CPUAccessFlags = 0;                   // ???
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

bool ModelObject::LoadModel(char* filename, int fileType)
{
    if ( fileType != 0 )
    {
        scene = aiImportFile(filename, ( aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded ));
        if ( scene == NULL )
        {
            cout << "stop borking..." << endl;
            return false;
        }

        //AnimatedMesh* test_animMesh = new AnimatedMesh();
        cout << "this dae has: " << scene->mNumMeshes << " meshes." << endl;
//        for(unsigned int i = 0; i < scene->mNumMeshes; i++)
//        {
            aiMesh * mesh = scene->mMeshes[3];//i];
            // copy vertex info
            //unsigned int numVertices = mesh -> mNumVertices;
            
            //animMesh->vertexCount = numVertices;
            m_vertexCount = mesh->mNumVertices;
            //animMesh->vertices = new AnimatedVertexType[numVertices];
            m_model = new ModelType[m_vertexCount];

      	    for(int j=0; j < m_vertexCount; ++j)
            {
                aiVector3D vertex  = mesh->mVertices[j];
                aiVector3D normals = mesh->mNormals[j];
                aiVector3D uvVec   = mesh->mTextureCoords[0][j];

                m_model[j].x = vertex.x;
                m_model[j].y = vertex.y;
                m_model[j].z = vertex.z;

                m_model[j].nx = normals.x;
                m_model[j].ny = normals.y;
                m_model[j].nz = normals.z;

                // mesh->mTextureCoords[0][vertexIndex].x, 1 - mesh->mTextureCoords[0][vertexIndex].y
                m_model[j].tu = uvVec.x;
                m_model[j].tv = uvVec.y;
	        }
            m_indexCount = mesh -> mNumFaces * 3;
//        }

    }else
    {
        ifstream fin;
	    char input;
	    int i;

        // Open the model file.
	    fin.open(filename);
	
	    // If it could not open the file then exit.
	    if(fin.fail())
	    {
            cout << "could not open file..." << endl;
		    return false;
	    }

        // Read up to the value of vertex count.
	    fin.get(input);
	    while(input != ':')
	    {
		    fin.get(input);
	    }

        // Read in the vertex count. (Assuming the basic txt...)
	    fin >> m_vertexCount;

        // Set the number of indices to be the same as the vertex count.
	    m_indexCount = m_vertexCount;

	    // Create the model using the vertex count that was read in.
	    m_model = new ModelType[m_vertexCount];
	    if(!m_model) return false;

        // Read up to the beginning of the data.
	    fin.get(input);
	    while(input != ':')
	    {
		    fin.get(input);
	    }
	    fin.get(input); // moves it down a line I assume?
	    fin.get(input);

	    // Read in the vertex data.
        //int interval = m_vertexCount/10;
	    for(i=0; i < m_vertexCount; i++)
	    {
            // Progress of this model.
            //if(i%interval == 0)
            //    cout << (double)i / (double)m_vertexCount * 100.0 << "%" << endl;

		    fin >> m_model[i].x >> m_model[i].y >> m_model[i].z;
		    fin >> m_model[i].tu >> m_model[i].tv;
		    fin >> m_model[i].nx >> m_model[i].ny >> m_model[i].nz;
	    }

	    // Close the model file.
	    fin.close();
    }

	return true;
}
bool ModelObject::LoadTextures( ID3D11Device* device, vector<char*> textureFileVector)
{
	bool result;

	// Create the texture object.
    //((TextureObject **)(malloc( sizeof(TextureObject *) * textureFileVector.size() )));
    m_Textures = new TextureObject;
	if(!m_Textures)
	{
		return false;
	}

    char ** textureNames = ((char **)( malloc( sizeof(char *) * textureFileVector.size() ) ));
    for (unsigned int i = 0; i < textureFileVector.size(); ++i)
    {
        textureNames[i] = textureFileVector.at(i);
    }

	// Initialize the texture objects
	result = m_Textures->Initialize(device, textureNames, textureFileVector.size() );
	if(!result)
	{
        cout << "[ModelObject::LoadTexture()] Texture Initialization failed..." << endl;
		return false;
	}
//  }

	return true;
}

void ModelObject::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);
	return;
}
void ModelObject::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0); // ???

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

int ModelObject::GetIndexCount()
{
    return m_indexCount;
}
//ID3D11ShaderResourceView* ModelObject::GetTexture()
//ID3D11ShaderResourceView** 
TextureObject* ModelObject::GetTextureObject()
{
	return m_Textures;//->GetTextures();
}

void ModelObject::Shutdown()
{
	// Release the model texture.

	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();

	return;
}
void ModelObject::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}
void ModelObject::ReleaseModel()
{
	if(m_model)
	{
		delete [] m_model;
		m_model = 0;
	}

	return;
}

void ModelObject::Debugger()
{
    cout << "My identity is: " << DebugField.c_str() << endl;
}