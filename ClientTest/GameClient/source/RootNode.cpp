#include "RootNode.h"

#include "UnitNode.h"
#include "UnitObject.h"
#include "ParticleNode.h"
#include "Model.h"
#include "MeshManager.h"

#include <iostream>

#include "GraphicsManager.h"
#include "SystemManager.h"

using namespace std;

RootNode::RootNode()
{
}
RootNode::RootNode(const RootNode &)
{
}
RootNode::~RootNode()
{
}
void RootNode::UpdateMatrix()
{
}
void RootNode::Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode )
{
    std::vector<SceneNode*>::iterator it;
    for( it = children.begin(); it != children.end(); it++ )
    {
        if ( (*it)->isUnitNode() && ((UnitNode *)(*it))->checkForRemoval() )
        {
			delete ((UnitNode *)(*it));
            it = children.erase(it);
            if (it == children.end()) break; // for some reason is seems to require this check
        }else if ( (*it)->isParticleNode() && ((ParticleNode *)(*it))->checkForRemoval() )
        {
			delete ((ParticleNode*)(*it));
            it = children.erase(it);
            if (it == children.end()) break;
        } else
        {
            (*it)->Draw( d3dMan, stack, drawMode );
        }
    }

    // Software Instance Rendering

    // RenderAlphaChildren
    if (drawMode == 0) Render(d3dMan);
    else if ( drawMode == 1 )
    {
        InstancedRender_0(d3dMan);
    }
}
void RootNode::Render(D3DManager* d3dMan)
{
    InstancedRender_0(d3dMan);

    while(!alphaChildren.empty())
    {
        SceneNode * sn = alphaChildren.back();
        sn->Render(d3dMan);
        alphaChildren.pop_back();
    }
}

void RootNode::AppendAlphaChildren(SceneNode * sn)
{
    alphaChildren.push_back(sn);
}
void RootNode::AppendInstancedModel( InstanceConfiguration ic )
{
    std::map< Model*, std::vector<InstanceConfiguration> >::iterator iter = instancedConfigMap.find( ic.modelRef->GetModel() );
    if( iter !=  instancedConfigMap.end() )
    {// Found a previous instance, append to list.
        iter->second.push_back(ic);
    }else
    {// Insert a new instance
        std::vector<InstanceConfiguration> vic;
        vic.push_back(ic);
        instancedConfigMap.insert(pair<Model*, std::vector<InstanceConfiguration>> (ic.modelRef->GetModel(), vic) );
    }
}
/***
 *
 */
void RootNode::InstancedRender_0(D3DManager* d3dMan)
{
    Model* prevModel = NULL;
    GraphicsManager * gPtr = SystemManager::Instance()->GetGraphicsManager();
    D3DXMATRIX camMatrix;
    gPtr->GetCamera()->GetMatrix(camMatrix);

    D3DXMATRIX instanceMatrix,animMatrix;
    D3DXMatrixIdentity(&instanceMatrix);

    //  CREATES THE RENDERSHADERCONFIG
        ShaderObject::RENDERSHADERCONFIG myRSCI = ShaderObject::createRenderShaderConfig();
        myRSCI.lightMan        = gPtr->GetLightManager();
        myRSCI.camObj          = gPtr->GetCamera();
        myRSCI.animationMatrix = &instanceMatrix;

    std::map< Model*, std::vector<InstanceConfiguration> >::iterator iter = instancedConfigMap.begin();
    while( iter != instancedConfigMap.end() )
    {
        std::vector<InstanceConfiguration>* f_ic  = &iter->second;

        prevModel = iter->first;
        prevModel->Render( d3dMan->GetDeviceContext() );
        while( !f_ic->empty() )
        {
            InstanceConfiguration ic = f_ic->back();
            if(ic.modelRef->GetModel()->isAlphaBlend())
            {
                d3dMan->TurnZBufferOff(1);
                d3dMan->EnableAlphaBlending(1);
                d3dMan->TurnOffBackFaceCull();
            }

            if ( ic.modelRef->GetModel()->hasAnimations )
            {// MESH HAS ANIMATIONS
                if( ic.shader->isShadowed() )
                {
                        LightObject* light_Ptr = gPtr->GetLightManager()->getLight(0);

                        D3DXMATRIX lvm, lpm;
                        myRSCI.lightViewMatrix = &lvm;
                        myRSCI.lightProjectionMatrix = &lpm;
                        light_Ptr->GetViewMatrix(*myRSCI.lightViewMatrix);
                        light_Ptr->GetProjectionMatrix(*myRSCI.lightProjectionMatrix);
                        myRSCI.depthResource = gPtr->GetRenderTexture()->GetShaderResourceView();
                }
                for (int i = 0; i < ic.modelRef->GetModel()->GetNumMeshes(); ++i)
                {
                    memcpy(myRSCI.animationMatrix, &ic.modelRef->GetModel()->multMeshPtr[i].animationMatrix, sizeof(D3DXMATRIX));
                    myRSCI.tintColor       = (ic.modelRef->GetTint());

                    //_root_shader
                    ic.shader  -> Render( d3dMan->GetDeviceContext(), ic.modelRef->GetModel()->multMeshPtr[i].numVerts,
                                          ic.modelRef->GetModel()->multMeshPtr[i].offset,
                                          ic.instanceMatrix, ic.viewMat, ic.projMat, 
                                          //camMatrix, d3dMan->GetProjectionMatrix(),
                                          ic.modelRef->GetModel()->GetTextureObject()->GetTextures(), ic.modelRef->GetModel()->GetTextureObject()->numTextures(),
                                          myRSCI
                                        );
                    // Dummy render call
                    if( ic.shader->isShadowed() ) ic.shader -> Render( d3dMan->GetDeviceContext(),0,0, ic.instanceMatrix, camMatrix, d3dMan->GetProjectionMatrix(), ic.modelRef->GetModel()->GetTextureObject()->GetTextures(), ic.modelRef->GetModel()->GetTextureObject()->numTextures(), myRSCI );
                }
            }
            else
            {// SINGLE MESH, NON-ANIMATED
                myRSCI.tintColor       = (ic.modelRef->GetTint());

                ic.shader -> Render( d3dMan->GetDeviceContext(), ic.modelRef->GetModel()->GetIndexCount(), 0,
                                      ic.instanceMatrix, ic.viewMat, ic.projMat,
                                      //camMatrix, d3dMan->GetProjectionMatrix(),
                                      ic.modelRef->GetModel()->GetTextureObject()->GetTextures(), ic.modelRef->GetModel()->GetTextureObject()->numTextures(),
                                      myRSCI
                                    );
                // Dummy render call
                if( ic.shader->isShadowed() ) ic.shader -> Render( d3dMan->GetDeviceContext(),0,0, ic.instanceMatrix, camMatrix, d3dMan->GetProjectionMatrix(), ic.modelRef->GetModel()->GetTextureObject()->GetTextures(), ic.modelRef->GetModel()->GetTextureObject()->numTextures(), myRSCI );
            }

            f_ic->pop_back();


            if(ic.modelRef->GetModel()->isAlphaBlend())
            {
                d3dMan->RestoreBackFaceCull();
                d3dMan->DisableAlphaBlending();
                d3dMan->TurnZBufferOn();
            }
        }

        iter = instancedConfigMap.erase(iter);
        if(iter == instancedConfigMap.end()) break;
    }
}
/***
 *
 */
RootNode::InstanceConfiguration RootNode::getInstanceConfig()
{
    InstanceConfiguration retIC;
    memset(&retIC,0, sizeof(retIC));
    return retIC;
}