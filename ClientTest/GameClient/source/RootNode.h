//RootNode.h
#ifndef _ROOTNODE_H_
#define _ROOTNODE_H_

#include "SceneNode.h"
#include "D3DManager.h"

#include <map>

class Model;
//class ShaderObject;
#include "ShaderObject.h"

class RootNode : public SceneNode
{
public:
    struct InstanceConfiguration
    {
        D3DXMATRIX   instanceMatrix, viewMat, projMat;
        UnitObject * modelRef;
        ShaderObject* shader;
        ShaderObject::RENDERSHADERCONFIG shaderRSC;
    };

private:

    std::vector <SceneNode * > alphaChildren; // ...
    std::map <Model*, std::vector<InstanceConfiguration>> instancedConfigMap;

public:
    RootNode();
    RootNode(const RootNode &);
    ~RootNode();
        
    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);

    inline virtual bool isRootNode()    { return true; };

    void InstancedRender_0(D3DManager* d3dMan);

    void RenderAlphaChildren(D3DManager* d3dMan); // Not being used?
    void AppendAlphaChildren(SceneNode* sn);

    static InstanceConfiguration getInstanceConfig();
    void AppendInstancedModel( InstanceConfiguration );
};

#endif