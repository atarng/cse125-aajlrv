#ifndef _HUD_MANAGER_H_
#define _HUD_MANAGER_H_

#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>
#include <vector>

/***********************************
 * Local includes
 ***********************************/
#include "Timer.h"
#include "ShaderObject.h"

#include "HUDDisplayable.h"
#include "HUDField.h"
#include "HUDMenuButton.h"
//#include "D3DManager.h"

class HUDBar;
class HUDMenuItem;
class HUDCounter;
class HUDGraphic;
class D3DManager;

class HUDManager {
private:
    static HUDManager * m_Instance;

    struct HUDElementSelecter
    {
        int MENU_BUTTON_EXIT;
		int MENU_BUTTON_SHADOWS;

		int BAR;
		int BAR2;
		int BAR3;
		int BAR4;
		int COUNTER;
		int LOSE;
		int WIN;

        int MINIMAP;
		int MINIMAP_OVERLAY;
		int FIELD;
		int FIELD2;
		int FIELD3;
		int FIELD4;
		int CROSSHAIRS;
		int CROSSHAIRS2;
		int SPLASH;
		int START_BUTTON;
		int FOCUS1;
		int FOCUS2;
	} hudSelector;
    struct DisplayablesSelecter
    {
        int TEXT_EXIT;
		int SHADOW_TEXT;
		int START_TEXT;
		int LIGHTNING_BAR, ROBOT_BAR, SPARK_BAR;
		int BAR_VALUE, BAR_VALUE2, BAR_VALUE3, BAR_VALUE4;
		int COUNTER_VALUE;
		int FIELD_VALUE, FIELD_VALUE2, FIELD_VALUE3, FIELD_VALUE4;
		int TEST_FIELD;
		int SPLASH;
		int FOCUS;
		int LOSE, WIN;

        int TEST_GRAPHIC;
      //  int TEST_ALPHAGRAPHIC;
		int GRAY_BAR;
		int BLUE_BAR, BLUE_BAR2, BLUE_BAR3, BLUE_BAR4;

        int MINIMAP_GRAPHIC;
        int MINIMAP_PC_GRAPHIC;
        int MINIMAP_ICON1;
		int CROSSHAIRS, CROSSHAIRS2;

    } displayablesSelector;

    std::vector<HUDDisplayable *> hudDisplayables;
    std::vector<HUDMenuItem *>    HUDComponents;

    ShaderObject * shader_Ref;

	HUDBar* bar,* bar2,* bar3, * bar4;
	HUDField * field,* field2, *field3;
	HUDCounter * counter;
	HUDMenuItem * crosshairs, * crosshairs2;
	HUDMenuButton * start;
	HUDMenuItem * splash;
	HUDMenuItem * focus1;
	HUDMenuItem * focus2;
	HUDMenuItem * lose;
	HUDMenuItem * win;
    char buffer[32];

	bool displayingPopUpMenu;
	bool displayingSplash;

public:
	HUDManager();
	~HUDManager();

    static HUDManager * Instance();

	bool Initialize(D3DManager * );
    void Shutdown();

    void SetShader(ShaderObject*);
    ShaderObject* GetShader();

	//bool Render(D3DManager *);
    bool Render(D3DManager *, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
    bool CheckClickEvent(float, float);

	inline HUDBar * getBar() { return bar;} ;
	inline HUDBar * getBar2() { return bar2;} ;
	inline HUDBar * getBar3() { return bar3;} ;
	inline HUDBar * getBar4() { return bar4;} ;
	inline HUDMenuButton * getStart() { return start;} ;
	inline HUDMenuItem * getSplash() { return splash;} ;

	inline HUDCounter * getCounter() { return counter;} ;

	inline HUDField * getField() { return field;} ;
	inline HUDField * getField2() { return field2;} ;
	inline HUDField * getField3() { return field3;} ;
	inline HUDMenuItem * getFocus1() {return focus1;} ;
	inline HUDMenuItem * getFocus2() {return focus2;} ;
	inline HUDMenuItem * getCrosshairs() {return crosshairs;};
	inline HUDMenuItem * getCrosshairs2() {return crosshairs2;};
	inline HUDMenuItem * getLose() {return lose;};
	inline HUDMenuItem * getWin() {return win;};


/// HUDManagement /////////////////////////////////////
    // Text and Pictures
    int AddHudDisplayable (HUDDisplayable * newHUDElement);
    // Menu Buttons, Mini maps, other interfaces.
    int AddHudElement     (HUDMenuItem       * newHUDElement);

    inline DisplayablesSelecter GetHUDDisplayablesSelector(){ return displayablesSelector; };
    inline HUDElementSelecter   GetHUDElementSelector()     { return hudSelector; };

    bool HudIsOn();
	bool SplashIsOn();
	void SplashOff();
	bool DisplayPopUpMenu();

    // Text and Pictures
    inline std::vector<HUDDisplayable *> & GetHUDDisplayables(){ return hudDisplayables; };
    // Menu Buttons, Mini maps, other interfaces.
    inline HUDMenuItem & GetHUDComponent(int index)  { return *(HUDComponents.at(index)) ; };
};
#endif