////////////////////////////////////////////////////////////////////////////////
// Filename: InputManager.cpp
// Contributors: Joey Ly
// Purpose: 
////////////////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "SystemManager.h"

#include "LoadingManager.h"
#include "InputManager.h"
#include "InputHandler.h"
#include <iostream>

#define MAKEPOINTS(l) (*((POINTS FAR *)&(l)))  //found this on a forum post at http://cboard.cprogramming.com/cplusplus-programming/60009-makepoints-structure-win32.html
#define WHEEL_SENSITIVITY 1

InputManager::InputManager()
{    
}
InputManager::InputManager(const InputManager& other)
{
}
InputManager::~InputManager()
{
}

InputManager* InputManager::m_Instance = NULL;
InputManager* InputManager::Instance()
{
    if (!m_Instance) m_Instance = new InputManager;
    return m_Instance;
}

void InputManager::Initialize()
{  
	// Initialize all the keys to being released and not pressed.
	for(int i=0; i<256; i++)
    {
        m_keys[i] = false;
    }

    //Initialize Mouse Stuff
    mouseSensitivity = 10.0; // greater the value = less sensitive
    mouseX = 0;
    mouseY = 0;
    deltaMouseX = 0;
    deltaMouseY = 0;
    wheel = 0;

    repositionMouse = false;
    lButtonPressed = false;
    mButtonPressed = false;
    rButtonPressed = false;
    lButtonHold = false;
    mButtonHold = false;
    rButtonHold = false;
    mouseVisible = true;

    return;
}

////////////////////////////////////////////////////////////////////////////////
// KEYBOARD
////////////////////////////////////////////////////////////////////////////////
void InputManager::KeyDown(unsigned int input)
{
	// If a key is pressed then save that state in the key array.
    if( !m_keys[input] && !m_keyPressed[input] ) {
		m_keyPressed[input] = true;
		HandleKey(input);
	}
	m_keys[input] = true;
	return;
}


void InputManager::KeyUp(unsigned int input)
{
	// If a key is released then clear that state in the key array.
	m_keys[input] = false;
    m_keyPressed[input] = false;
	
	return;
}

bool InputManager::IsKeyDown(unsigned int key)
{
	// Return what state the key is in (pressed/not pressed).
	return m_keys[key];
}

//similar to checking if the key is down except it will only return true on the first call
bool InputManager::IsKeyPressed(unsigned int key)
{
    if (m_keyPressed[key] && m_keys[key]){
        m_keyPressed[key] = false;
        return true;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////
// MOUSE
////////////////////////////////////////////////////////////////////////////////
void InputManager::UpdateMousePosition(LPARAM lParam)
{
    deltaMouseX += mouseX - MAKEPOINTS(lParam).x; 
    deltaMouseY += mouseY - MAKEPOINTS(lParam).y;
    mouseX = MAKEPOINTS(lParam).x; 
    mouseY = MAKEPOINTS(lParam).y; 
}

void InputManager::UpdateMousePosition(float x, float y)
{
    deltaMouseX += mouseX+x; 
    deltaMouseY += mouseY+y;
    mouseX = x; 
    mouseY = y; 
}

void InputManager::UpdateMouseWheel(WPARAM wParam, LPARAM lParam)
{
    wheel += GET_WHEEL_DELTA_WPARAM(wParam)/120;
}

void InputManager::UpdateMouseState(bool isDown, MouseButton mButton)
{
    switch(mButton)
    {
    case LEFT:
        if(!lButtonHold && !lButtonPressed)
            lButtonPressed = true;
        lButtonHold = isDown;
        break;
    case MIDDLE:
        if(!mButtonHold && !mButtonPressed)
            mButtonPressed = true;
        mButtonHold = isDown;
        wheel = 0;
        break;
    case RIGHT:
        if(!rButtonHold && !rButtonPressed)
            rButtonPressed = true;
        rButtonHold = isDown;
        break;
    }
}

// this currently works because we assume the window is centered on the screen
// Reset the position of the mouse to the center to the SCREEN (not window)
void InputManager::ResetMouse()
{
    if(repositionMouse){
        SetCursorPos((GetSystemMetrics(SM_CXSCREEN)) / 2, (GetSystemMetrics(SM_CYSCREEN)) / 2);
        mouseX = (SystemManager::windowWidth) / 2;
        mouseY = (SystemManager::windowHeight) / 2;
    }
    deltaMouseX = deltaMouseY = 0;
}
void InputManager::SetCenterTheMouse(bool state)
{
    repositionMouse = state;
}
void InputManager::ToggleCenterTheMouse()
{
    repositionMouse = !repositionMouse;
}

bool InputManager::IsButtonDown(MouseButton button)
{
    switch(button){
    case LEFT: return lButtonHold;
    case MIDDLE: return mButtonHold;
    case RIGHT: return rButtonHold;
    default: return false;
    }
}

bool InputManager::IsButtonPressed(MouseButton button)
{
    switch(button)
    {
    case LEFT:
        if (lButtonPressed){
            lButtonPressed = false;
            return true;
        } break;
    case MIDDLE:
        if (mButtonPressed)
        {
            mButtonPressed = false;
            return true;
        } break;
    case RIGHT:
        if (rButtonPressed)
        {
            rButtonPressed = false;
            return true;
        } break;
    default: return false;
    }
    return false;        
}

int InputManager::GetWheelState()
{
    int temp = 0; 
    if (abs(wheel)>WHEEL_SENSITIVITY){
        temp = (wheel>0)?1:-1;
        wheel = 0;
    }    
    return temp;
}