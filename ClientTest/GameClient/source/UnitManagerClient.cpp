/***
 * Filename: UnitManagerClient.cpp
 * Author(s): Alfred Tarng
 * Purpose: Manages incoming Unit objects from a server and integrates that unit into
 *          the scenegraph for displaying, as well as handling removals
 */
#include <iostream>

#define _WINSOCKAPI_	//used to resolve conflicts when including windows and winsock API
#include "UnitManagerClient.h"
#include "UnitNode.h"
#include "UnitObject.h"
#include "NetworkManager.h"
#include "SystemManager.h"
#include "GraphicsManager.h"
#include "MeshManager.h"

#include "HUDCounter.h"
#include "HUDManager.h"
#include "HUDMiniMap.h"
#include "SoundManager.h"

#include "ProjectileNode.h" // Not used I think...

#include "ParticleNode.h"
#include "ParticleSystemObject.h"
#include "ParticleSystemManager.h"
#include "Log.h"

#include "AttackObject.h"


UnitManagerClient* UnitManagerClient::m_Instance = 0;
UnitManagerClient::UnitManagerClient()
{
}
UnitManagerClient::UnitManagerClient(const UnitManagerClient& other)
{
}
UnitManagerClient::~UnitManagerClient()
{// Clean up code here...
}

UnitManagerClient* UnitManagerClient::Instance()
{
    if (!m_Instance)
        m_Instance = new UnitManagerClient;
    return m_Instance;
}

/***
 * Creates the Updated Unit.
 */
UnitManagerClient::UnitPackage UnitManagerClient::getUnit(Unit * updateUnit, bool & isNewUnit)
{
    GraphicsManager * graphicsMan_Ptr = SystemManager::Instance()->GetGraphicsManager();

    std::map<int, UnitPackage>::iterator it;
    it = units.find(updateUnit->getUnitId());
    if ( isNewUnit = (it == units.end()) )
    { // Insert into unit manager.
        UnitObject     * UnitObj  = new UnitObject(*updateUnit);
        UnitNode       * unitNode = new UnitNode();

        float r = updateUnit->getColorR();
        float g = updateUnit->getColorG();
        float b = updateUnit->getColorB();

        //printf("Assigning Color: (%f, %f, %f) \n", r,g,b);
        UnitObj->assignTint(r, g, b);
		UnitObj->UnitStateUpdate();

        UnitPackage up;
        up.mapIndex = -1;
		
        SetSpecificUnitProperties( *unitNode, *UnitObj, UnitObj->GetModelID() );

        if ( UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().LASER
          || UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().TABLET
		  || UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().TABLET2
          || UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().AOE
          || UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().METEOR
		  || UnitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().SATELLITE_SHIP
          )
        {  
			//r = (r >= .8f) ? 1.0f : 0.0f;
			//g = (g >= .8f) ? 1.0f : 0.0f;
			//b = (b >= .8f) ? 1.0f : 0.0f;
			//UnitObj->assignTint(r, g, b);
        }else
        {// A UnitNode is considered Dynamic when it comes from the server,
         // as opposed to being generated statically on the client side.
         // This is to allow for the shader to switch between being rendered with shadows, or not.

            // USED TO ADD ITEMS TO THE HUD
            HUDMiniMap * hmm = (HUDMiniMap *) &SystemManager::Instance()->GetHUDManager()->GetHUDComponent(
                                               SystemManager::Instance()->GetHUDManager()->GetHUDElementSelector().MINIMAP);
            HUDMiniMap * hmm_Overlay = (HUDMiniMap *) &SystemManager::Instance()->GetHUDManager()->GetHUDComponent(
                                        SystemManager::Instance()->GetHUDManager()->GetHUDElementSelector().MINIMAP_OVERLAY);
            up.mapIndex = hmm->addMiniMapItem( *(SystemManager::Instance()->GetHUDManager()->GetHUDDisplayables().at(
                                               SystemManager::Instance()->GetHUDManager()->GetHUDDisplayablesSelector().MINIMAP_ICON1 )),
                                               *UnitObj);
                          hmm_Overlay->addMiniMapItem( *(SystemManager::Instance()->GetHUDManager()->GetHUDDisplayables().at(
                                               SystemManager::Instance()->GetHUDManager()->GetHUDDisplayablesSelector().MINIMAP_ICON1 )),
                                               *UnitObj);

            unitNode -> setDynamic( true );
        }
        up.unit       = updateUnit;
        up.unitObject = UnitObj;
        units.insert( pair<int,UnitPackage> (updateUnit->getUnitId(), up) );

// CHECK IF THIS IS THE PLAYERs' UNIT
// May want a seperate message that sets the player's Unit 
// TODO: Make it so you can switch between units
		if ( NetworkManager::Instance()->getCurrentSparkId() == updateUnit->getUnitId())
        {// Set camera to follow this unit
#if _DEBUG
            printf("Following UnitId: %d\n", updateUnit->getUnitId() );
#endif
            SystemManager::Instance()->SetCurrentPlayerUnit(UnitObj);
        }
        unitNode->SetUnitObject( UnitObj );

        // TO BE DEPRECATED HOPEFULLY
        unitNode->SetShader( &(SystemManager::Instance()->GetGraphicsManager()->
                               GetShaderManager()->GetShader(UnitObj->GetShaderID()) ) );

        // Attaches the node to the scenegraph.
        SystemManager::Instance()->GetGraphicsManager()->GetWorld()->addChild(*unitNode);

        return up;
    } else 
    {
        // update the unit color
        map<int, UnitPackage>::iterator iter = units.find(updateUnit->getUnitId());

        if (iter != units.end()) 
        {
            UnitObject * unitObj = iter->second.unitObject;

            float r = updateUnit->getColorR();
            float g = updateUnit->getColorG();
            float b = updateUnit->getColorB();

			if ( unitObj->GetModelID() == graphicsMan_Ptr->GetMeshManager()->GetMeshSelector().LASER )
			{
				r = (r > .8) ? 1.f : 0.f;
				g = (g > .8) ? 1.f : 0.f;
				b = (b > .8) ? 1.f : 0.f;
			}

	        if( unitObj->GetModelID() != SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TABLET
		        && unitObj->GetModelID() != SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TABLET2)
		    {// Do Not set tablet tint
                unitObj->assignTint(r, g, b);
		    }

            if (unitObj->GetModelID() == SystemManager::Instance()->GetGraphicsManager()->GetMeshManager()->GetMeshSelector().METEOR &&
                unitObj->GetUnitAnimState() == ANIM_METEOR_DETONATE ) {
                D3DXVECTOR3 currPos = SystemManager::Instance()->GetCurrentPlayerUnit()->GetPosition();
			    D3DXVECTOR3 firePosition = D3DXVECTOR3( unitObj->GetPosition().x,
                                                        unitObj->GetPosition().y,
                                                        unitObj->GetPosition().z);
			    float distance = sqrt(  (firePosition.x - currPos.x) * (firePosition.x - currPos.x) + 
                                        (firePosition.y - currPos.y) * (firePosition.y - currPos.y) +
                                        (firePosition.z - currPos.z) * (firePosition.z - currPos.z));

                SoundManager* sound = SystemManager::Instance()->GetSoundManager();
                if (sound)
                    sound->PlaySoundSpec( SoundManager::METEOR_DETONATE_SOUND, distance );
            }
        }
    }

	updateCounter();
    return it->second;
}

/***
 *
 */
bool UnitManagerClient::updateClient(Unit * up)
{
    bool isNewUnit;

    UnitPackage inClient = getUnit(up, isNewUnit);

	if (!isNewUnit && up->getHealth() < inClient.unit->getHealth()) {
		if (up->getUnitId() == NetworkManager::Instance()->getCurrentRobotId()) {
			//cerr << "!!!!!!!" << endl;
			//play damage sound
			SoundManager* sound = SystemManager::Instance()->GetSoundManager();
			if (sound) {
				sound->PlaySoundSpec(SoundManager::DING_SOUND);
			}
			//SystemManager::Instance()->GetGraphicsManager()->GetCamera()->AddShakeFrames(100);
		}
	}
	else if ((!isNewUnit && up->getHealth() > inClient.unit->getHealth()) && (up->getUnitId() == NetworkManager::Instance()->getCurrentRobotId())) {

		//play powerup sound
		//ncerr << "!!!!!!!" << endl;
		SoundManager* sound = SystemManager::Instance()->GetSoundManager();
		if (sound) {
			sound->PlaySoundSpec(SoundManager::POWERUP_SOUND);
		}
	}

    inClient.unit->cloneValues(up);

	updateCounter();
    return isNewUnit;

}

/***
 * TODO: Remove unit from the minimap as well, if necessary.
 */
void UnitManagerClient::deleteUnit(int networkId)
{
    map<int, UnitPackage>::iterator iter = units.find(networkId);

    if (iter != units.end())
    {
        UnitPackage inClient = iter->second;
        if ( SystemManager::Instance()->GetCurrentPlayerUnit() == inClient.unitObject )
        {// IF THIS IS THE CLIENTS UNIT THAT DIED.
            SystemManager::Instance()->SetCurrentPlayerUnit( SystemManager::Instance()->GetGraphicsManager()->getDefaultUnit());
			
        }

        if(inClient.mapIndex >= 0)
        {// MARK MINIMAP ITEM FOR REMOVAL
            HUDMiniMap * hmm = (HUDMiniMap *) &SystemManager::Instance()->GetHUDManager()->GetHUDComponent(
                                               SystemManager::Instance()->GetHUDManager()->GetHUDElementSelector().MINIMAP);
            HUDMiniMap * hmm_overlay = (HUDMiniMap *) &SystemManager::Instance()->GetHUDManager()->GetHUDComponent(
                                                       SystemManager::Instance()->GetHUDManager()->GetHUDElementSelector().MINIMAP_OVERLAY);

            hmm->markForRemoval(*inClient.unitObject);
            hmm_overlay->markForRemoval(*inClient.unitObject);
        }
        inClient.unitObject->unsetUnit();

        MeshManager * mm_Ptr = SystemManager::Instance()->GetGraphicsManager()->GetMeshManager() ;

        if ( inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().PROBE             ||
             inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().TEST_ELECTRICTHING ||
             inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().TEST_DROID )
        {// IF UNIT IS ONE THAT CAUSES EXPLOSION ON DEATH
            D3DXVECTOR3 deathPosition = D3DXVECTOR3(inClient.unitObject->GetPosition().x,
                                                    inClient.unitObject->GetPosition().y,
                                                    inClient.unitObject->GetPosition().z);
            SystemManager::Instance()->GetGraphicsManager()->CreateExplosion( deathPosition.x, deathPosition.y, deathPosition.z );
			SoundManager* sound = SystemManager::Instance()->GetSoundManager();
			if (sound)
            {
                D3DXVECTOR3 currPos = SystemManager::Instance()->GetCurrentPlayerUnit()->GetPosition();
                /*float distance = sqrt((deathPosition.x - currPos.x) * (deathPosition.x - currPos.x) + 
                                      (deathPosition.y - currPos.y) * (deathPosition.y - currPos.y) +
                                      (deathPosition.z - currPos.z) * (deathPosition.z - currPos.z));*/
				sound->PlaySoundSpec(SoundManager::EXPLODE_SOUND);
			}
        } else if( inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().LASER )
        {
            SystemManager::Instance()->GetGraphicsManager()->CreateExplosion( inClient.unitObject->GetPosition().x,
                                                                              inClient.unitObject->GetPosition().y,
                                                                              inClient.unitObject->GetPosition().z,
                                                                              1 );
        } else if( inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().ELECTRICSPARK )
        {
            SystemManager::Instance()->GetGraphicsManager()->CreateExplosion( inClient.unitObject->GetPosition().x,
                                                                              inClient.unitObject->GetPosition().y,
                                                                              inClient.unitObject->GetPosition().z,
                                                                              2 );
		} else if( inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().TABLET 
			    || inClient.unitObject->GetModelID() == mm_Ptr->GetMeshSelector().TABLET2 )
        {
            SystemManager::Instance()->GetGraphicsManager()->CreateExplosion( inClient.unitObject->GetPosition().x,
                                                                              inClient.unitObject->GetPosition().y,
                                                                              inClient.unitObject->GetPosition().z,
                                                                              1 );
        }


        units.erase(iter);
    }else
    {
        cerr << "[Error] Could Not find deletable unit." << endl;
    }
	updateCounter();
}

void UnitManagerClient::updateCounter() {
	HUDManager * manager = HUDManager::Instance();
	manager->getCounter()->setValue(units.size());
}

void UnitManagerClient::SetSpecificUnitProperties( UnitNode& unitNode, UnitObject& unitObjRef, int ID )
{
    GraphicsManager * gManager_Ptr  = SystemManager::Instance()->GetGraphicsManager();
    MeshManager *  meshManager_Ptr  = gManager_Ptr->GetMeshManager();
    ParticleSystemManager* pManager = gManager_Ptr->GetParticleSystemManager();
    if ( ID == meshManager_Ptr->GetMeshSelector().PROBE )
    { // attach jet pack!
        ParticleNode * pNode = new ParticleNode;
        ParticleSystemObject * psObj = new ParticleSystemObject;
        psObj -> SetScale(D3DXVECTOR3(2.0,2.0,2.0));
        psObj -> SetPosition(D3DXVECTOR3(0.0,0.0,0.0));
        psObj -> SetParticleSystem ( &(pManager->GetParticleSystem( pManager->GetParticleSelector().TEST_JETPACK )) );

        psObj -> SetBillBoarded( PARTICLE );
	    pNode -> SetParticleObject(psObj);

        pNode -> SetShader( &gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().PARTICLE_SHADER_BB ) );
        unitNode.addChild(*pNode);

        // ADD ANIMATION OBJECT
        UnitObject* animObj = new UnitObject(unitObjRef);
        animObj->SetModel( meshManager_Ptr->GetMeshSelector().PROBE_WALK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();
        D3DXVECTOR4* tintVec = unitObjRef.GetTint();
        animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
		// add animation to map
        unitNode.SetUnitObject(animObj, at_WALK);

		animObj = new UnitObject(unitObjRef);
		animObj->SetModel( meshManager_Ptr->GetMeshSelector().PROBE_ATTACK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();
        tintVec = unitObjRef.GetTint();
        animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
        unitNode.SetUnitObject(animObj, at_ATTACK);

    } else if ( ID == meshManager_Ptr->GetMeshSelector().TEST_DROID )
    {// Add Animation Models to swap chain.
        UnitObject* animObj = new UnitObject(unitObjRef);
        animObj->SetModel( meshManager_Ptr->GetMeshSelector().DROID_WALK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();
        D3DXVECTOR4* tintVec = unitObjRef.GetTint();
        animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
        unitNode.SetUnitObject(animObj, at_WALK);

		animObj = new UnitObject(unitObjRef);
		animObj->SetModel( meshManager_Ptr->GetMeshSelector().DROID_ATTACK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();
        tintVec = unitObjRef.GetTint();
        animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
        unitNode.SetUnitObject(animObj, at_ATTACK);
	}else if ( ID == meshManager_Ptr->GetMeshSelector().TEST_ELECTRICTHING )
	{
		UnitObject* animObj = new UnitObject(unitObjRef);
		animObj->SetModel( meshManager_Ptr->GetMeshSelector().FAN_BOT_WALK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();

		D3DXVECTOR4* tintVec = unitObjRef.GetTint();
		animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
		unitNode.SetUnitObject(animObj, at_WALK);
////////
		animObj = new UnitObject(unitObjRef);
		animObj->SetModel( meshManager_Ptr->GetMeshSelector().FAN_BOT_ATTACK );
        animObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
		animObj->setAsTester();

		tintVec = unitObjRef.GetTint();
		animObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
		unitNode.SetUnitObject(animObj, at_ATTACK);

	}else if ( ID == meshManager_Ptr->GetMeshSelector().ELECTRICSPARK )
    {// SPARK SPECIAL SETTINGS
        UnitNode * testSubNode =  new UnitNode();
        UnitObject * testObj  = new UnitObject();

        unitObjRef.SetBoundingRadius( unitObjRef.GetBoundingRadius() * -1 );

        testObj -> SetModel( meshManager_Ptr->GetMeshSelector().ELECTRICSPARK );
        testObj->SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
        testObj->SetRotation( D3DXVECTOR3(0,0,90) );
        testObj->SetBoundingRadius( 1.0 ); //unitObjRef.GetBoundingRadius() );
        testObj->SetScale( D3DXVECTOR3(1.0,1.0,1.0) );

        D3DXVECTOR4* tintVec = unitObjRef.GetTint();
        testObj->assignTint( tintVec->x, tintVec->y, tintVec->z );
        
        testSubNode -> SetUnitObject( testObj );
        testSubNode -> SetShader( &(gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );

        unitNode.addChild( *testSubNode );

        ParticleNode * pNode = new ParticleNode;
        ParticleSystemObject * psObj = new ParticleSystemObject;

        psObj -> SetParticleSystem ( &(pManager->GetParticleSystem( pManager->GetParticleSelector().TEST_RADIAL_LIGHTNING )) );
        psObj -> SetBillBoarded( SYSTEM );
        pNode -> SetParticleObject(psObj);

        pNode -> SetShader( &gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().PARTICLE_SHADER ) );
        unitNode.SetShader( &gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().DIR_LIGHT_SHADER_0 ) );
        unitNode.addChild(*pNode);
	}else if ( ID == meshManager_Ptr->GetMeshSelector().LASER )
	{
		UnitNode * testSubNode =  new UnitNode();
        UnitObject * testObj  = new UnitObject();

		testObj -> SetModel( meshManager_Ptr->GetMeshSelector().LASER2 );
        testObj->SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
        testObj->SetRotation( D3DXVECTOR3(0,0,0) );
        testObj->SetBoundingRadius( 1.0 );
        testObj->SetScale( D3DXVECTOR3(3.0,3.0,3.0) );

        D3DXVECTOR4* tintVec = unitObjRef.GetTint();
        testObj->assignTint( tintVec->x, tintVec->y, tintVec->z );

        testSubNode -> SetUnitObject( testObj );
        testSubNode -> SetShader( &(gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );

        unitNode.addChild( *testSubNode );

		//play shot sound
		SoundManager* sound = SystemManager::Instance()->GetSoundManager();
		if ( sound )
		{
			D3DXVECTOR3 currPos = SystemManager::Instance()->GetCurrentPlayerUnit()->GetPosition();
			D3DXVECTOR3 firePosition = D3DXVECTOR3( unitObjRef.GetPosition().x,
                                                    unitObjRef.GetPosition().y,
                                                    unitObjRef.GetPosition().z);
			float distance = sqrt(  (firePosition.x - currPos.x) * (firePosition.x - currPos.x) + 
                                    (firePosition.y - currPos.y) * (firePosition.y - currPos.y) +
                                    (firePosition.z - currPos.z) * (firePosition.z - currPos.z));
			sound->PlaySoundSpec( SoundManager::SHOT_SOUND, distance );
		}
    } else if ( ID == meshManager_Ptr->GetMeshSelector().AOE ||
                ID == meshManager_Ptr->GetMeshSelector().METEOR  || 
                ID == meshManager_Ptr->GetMeshSelector().BALLS )
    {

        for ( int i = 0; i < 4; ++i )
        {
    	    UnitNode * testSubNode =  new UnitNode();
            UnitObject * testObj  = new UnitObject();

            testObj -> SetModel( meshManager_Ptr->GetMeshSelector().AOE );
            testObj->SetPosition( D3DXVECTOR3(0.0f, 0.0f, 0.0f) );
            testObj->SetRotation( D3DXVECTOR3(0,0,0) );
            testObj->SetBoundingRadius( unitObjRef.GetBoundingRadius() );
            testObj->SetScale( D3DXVECTOR3(1.f, 1.f, 1.f) );
                //D3DXVECTOR3(unitObjRef.GetBoundingRadius() , unitObjRef.GetBoundingRadius() ,unitObjRef.GetBoundingRadius()) );//unitObjRef.GetScale() );

            D3DXVECTOR4* tintVec = unitObjRef.GetTint();
            testObj->assignTint( tintVec->x, tintVec->y, tintVec->z );

            testSubNode -> SetUnitObject( testObj );
            testSubNode -> SetShader( &(gManager_Ptr->GetShaderManager()->GetShader( gManager_Ptr->GetShaderManager()->GetShaderSelector().DIR_LIGHT_SHADER_0 )) );

            unitNode.addChild( *testSubNode );

            SoundManager* sound = SystemManager::Instance()->GetSoundManager();

            if (sound != NULL) {
                D3DXVECTOR3 currPos = SystemManager::Instance()->GetCurrentPlayerUnit()->GetPosition();
			    D3DXVECTOR3 firePosition = D3DXVECTOR3( unitObjRef.GetPosition().x,
                                                        unitObjRef.GetPosition().y,
                                                        unitObjRef.GetPosition().z);
			    float distance = sqrt(  (firePosition.x - currPos.x) * (firePosition.x - currPos.x) + 
                                        (firePosition.y - currPos.y) * (firePosition.y - currPos.y) +
                                        (firePosition.z - currPos.z) * (firePosition.z - currPos.z));

                if (ID == meshManager_Ptr->GetMeshSelector().METEOR) {
			        sound->PlaySoundSpec( SoundManager::METEOR_WHISTLE_SOUND, distance );
                    sound->PlaySoundSpec( SoundManager::NUCLEAR_SOUND );
                } else if (ID == meshManager_Ptr->GetMeshSelector().BALLS)
                    sound->PlaySoundSpec( SoundManager::BULLET2_SOUND, distance );
                else {
                    sound->PlaySoundSpec( SoundManager::AOE_SOUND );
                }
            }
        }
    } else if ( ID == meshManager_Ptr->GetMeshSelector().TABLET )
    {
        //unitNode.setDynamic(true);
		unitObjRef.assignTint(1.f,0.f, 0.f);
	} else if (ID == meshManager_Ptr->GetMeshSelector().TABLET2)
	{
		unitObjRef.assignTint(1.f,1.f, 0.f);
	}
}

UnitManagerClient::UnitPackage* UnitManagerClient::getUnitByID(int unitIndex)
{
	std::map<int, UnitPackage>::iterator iter = units.find(unitIndex);
	if (iter == units.end()) return NULL;
	else
		return &(*iter).second;
}