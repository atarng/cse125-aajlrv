////////////////////////////////////////////////////////////////////////////////
// Filename: TextureObject.cpp
////////////////////////////////////////////////////////////////////////////////
#include "TextureObject.h"
#include <iostream>

using namespace std;

TextureObject::TextureObject()
{
	m_textures = 0;
    n_textureSources = 0;
}
TextureObject::TextureObject(const TextureObject& other)
{
}
TextureObject::~TextureObject()
{
}

bool TextureObject::Initialize(ID3D11Device* device, char** filenames, int numTextureFiles)
{
	HRESULT result;

    m_textures = (ID3D11ShaderResourceView **)( malloc( sizeof(ID3D11ShaderResourceView*) * numTextureFiles ) );

	// Load the textures in.
    for(int i  = 0; i < numTextureFiles; ++i)
    {
        m_textures[i] = 0;

        cout << "[TextureObject] loading: " << filenames[i] << endl;
	    result = D3DX11CreateShaderResourceViewFromFile(device, filenames[i], NULL, NULL, &(m_textures[i]), NULL);
	    if(FAILED(result))
	    {
            cout << "[TextureObject] Could Not Load Texture Resource " << i << endl;
		    return false;
	    }

        n_textureSources += 1;
    }cout << "[TextureObject] Finished loading. " << endl;

	return true;
}

bool TextureObject::Initialize( ID3D11Device* device, std::vector<char*> filenames )
{
	HRESULT result;

    m_textures = (ID3D11ShaderResourceView **)( malloc( sizeof(ID3D11ShaderResourceView*) * filenames.size() ) );

	// Load the textures in.
    for(unsigned int i  = 0; i < filenames.size() ; ++i)
    {
        m_textures[i] = 0;

        cout << "[TextureObject] loading: " << filenames.at(i) << endl;
	    result = D3DX11CreateShaderResourceViewFromFile(device, filenames.at(i), NULL, NULL, &(m_textures[i]), NULL);
	    if(FAILED(result))
	    {
            cout << "[TextureObject] Could Not Load Texture Resource " << i << endl;
		    return false;
	    }

        n_textureSources += 1;
    }cout << "[TextureObject] Finished loading. " << endl;

	return true;
}

void TextureObject::Shutdown()
{
	// Release the texture resource.
    for (int i =0; i < n_textureSources; ++i)
    {
	    if(m_textures[i])
	    {
		    m_textures[i]->Release();
		    m_textures[i] = 0;
	    }
    }

	return;
}

int TextureObject::numTextures(){ return n_textureSources; }
ID3D11ShaderResourceView** TextureObject::GetTextures()
{
	return m_textures;
}
