////////////////////////////////////////////////////////////////////////////////
// Filename: SystemManager.cpp
// Contributors: 
////////////////////////////////////////////////////////////////////////////////
#include "SystemManager.h"
#include "Timer.h"
#include "Log.h"
#include "LoadingManager.h"
#include "GraphicsManager.h"
#include "NetworkManager.h"
#include "UnitManagerClient.h"
#include "HUDManager.h"
#include "MeshManager.h"
#include "UnitObject.h"
#include "InputManager.h"
#include "InputHandler.h"

#include "HUDBar.h" // TEMPORARY: Roy's testing code...

#include <iostream>

#define ROBOT_HEALTH 100
#define FAN_HEALTH 300
#define PROBE_HEALTH 50
#define SENTRY_HEALTH 100

#define PORT 7700
#define SERVER "localhost"

SystemManager* SystemManager::m_Instance = NULL; 

int SystemManager::windowWidth = 0;
int SystemManager::windowHeight = 0;

bool doNotUseController = false;
bool clientStarted = false; // hacky fix...
SystemManager::SystemManager()
{
    m_Input = 0;
    m_Graphics = 0;
	m_Sound = 0;
	m_HUD = 0;    
	m_Control = 0;
}
SystemManager::SystemManager(const SystemManager& other)
{
}
SystemManager::~SystemManager()
{
}

SystemManager* SystemManager::Instance(){
    if (!m_Instance)
        m_Instance = new SystemManager;
    return m_Instance;
}

/////////////////////////////////////////////////////////////////////
// INITIALIZE EVERYTHING!!!
/////////////////////////////////////////////////////////////////////
bool SystemManager::Initialize()
{
    mainTimer = Timer();
    bool result;
    
    /////////////////////////////
    // Load config files here  //
    /////////////////////////////
    LoadingManager::Initialize("resource/loadingConfig.txt");  // the loading manager 
    Log::Instance()->Print("Starting SystemManager");

    windowWidth  = (LoadingManager::fullscreen)?(GetSystemMetrics(SM_CXSCREEN)):LoadingManager::windowWidth;
    windowHeight = (LoadingManager::fullscreen)?(GetSystemMetrics(SM_CYSCREEN)):LoadingManager::windowHeight;

    // Initialize the windows api.
    InitializeWindows();

    // the InputManager is used to update the clients knowledge of the current state of our mouse and keyboard
    //     NOTE: what happens after an input is pressed occurs in the InputHandler (check the InputHandler.h header)
    m_Input = InputManager::Instance();
    if(!m_Input) return false;
    // Initialize the input object.
    m_Input->Initialize();

	m_Control = ControllerManager::Instance();
    if(!m_Control) return false;
	if (doNotUseController = !m_Control->Initialize())
	{
        Log::Instance()->PrintErr("Could not find a controller.");
	}

	m_Sound = new SoundManager;
	if (!m_Sound) return false;
	if (!m_Sound->Initialize(m_hwnd))
    {
		MessageBox(m_hwnd, "Could not initialize DirectSound.","Error", MB_OK);
        Log::Instance()->PrintErr("Could not initialize DirectSound.");
		m_Sound = 0;
	}

    // INITIALIZE HUDManager
    m_HUD = HUDManager::Instance(); // = new HUDManager;

    // Create the graphics object.  This object will handle rendering all the graphics for this application. 
    m_Graphics = new GraphicsManager;
    if(!m_Graphics) return false;

    // Initialize the graphics object.
    result = m_Graphics->Initialize(windowWidth, windowHeight, m_hwnd);
    if(!result) return false;

    m_UnitManager = UnitManagerClient::Instance();
    if (!m_UnitManager) return false;

#ifdef _DEBUG
    printf("Took %f seconds to complete SystemManager Initialize.\n", mainTimer.GetTimeSinceStart());
    Log::Instance()->Print("Took %f seconds to complete SystemManager Initialize.\n", mainTimer.GetTimeSinceStart());
#endif

    memset( &currentTarget, 0, sizeof(CURRENTTARGET) );

    return true;
}

void SystemManager::Shutdown()
{
// Release the sound object.
    if(m_Sound)
    {
        m_Sound->Shutdown();
        delete m_Sound;
        m_Sound = 0;
    }
    // Release the graphics object.
    if(m_Graphics)
    {
        m_Graphics->Shutdown();
        delete m_Graphics;
        m_Graphics = 0;
    }

    // Release the input object.
    if(m_Input)
    {
        delete m_Input;
        m_Input = 0;
    }

	if (m_HUD) {
		m_HUD->Shutdown();
		m_HUD = 0;
	}

    // Shutdown the window.
    ShutdownWindows();
    
    return;
}
/////////////////////////////////////////////////////////////////////
////    MAIN GAME LOOP    ////
/////////////////////////////////////////////////////////////////////
void SystemManager::Run()
{
    //NetworkManager::Instance(); 

    MSG msg;
	int result;
    float deltaTime = 0 ;

    // Initialize the message structure.
    ZeroMemory(&msg, sizeof(MSG));
    
    // Loop until there is a quit message from the window or the user.
    done = false;    
    
    while( !done )
    {        
        DWORD start = GetTickCount();
        deltaTime = mainTimer.Update();
                        
        // Handle the windows messages.
        // User input will be updated at this point         
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            continue; // continue will make sure the InputManager has an up-to-date state of the keyboard before handling input
        }
        
        HandleInput(m_Input, deltaTime);  //add all keyboard and mouse reactions to stuff in the HandleInput call        
		if( !doNotUseController ) m_Control->Frame();
        ///////////////////////////////////////////////////
        // SEND and RECIEVE INPUT MESSAGES TO SERVER HERE
		if ( !this->GetHUDManager()->SplashIsOn() )
		{
			NetworkManager::Instance()->updateFromServer();
		}
		DWORD network = GetTickCount();
        /////////////////////////////////////////////////////

        // If windows signals to end the application then exit out.
        if(msg.message == WM_QUIT)
        {         
            done = false;
        }                
        result = Frame();         
        if(!result)
        {
            //done = true;
        }        
        DWORD graphics = GetTickCount();
//        printf("network= %10lu, graphics %10lu\n", network - start, graphics - network);
    }
    
    return;
}

///////////////////////////////////////
//END   MAIN GAME LOOP   END
///////////////////////////////////////

bool SystemManager::Frame()
{
    bool result;

	// UPDATES HUD VALUES
	HUDManager* hud = GetHUDManager();
	if ( hud && !hud->SplashIsOn() )
	{
        clientStarted = true;
		if ( GetCurrentPlayerUnit() != NULL )
		{
			hud->getBar3()->setMax(300);
            hud->getBar3()->setValue( (int)GetCurrentPlayerUnit()->GetUnitHealth() );
		}
		if ( UnitManagerClient::Instance()->getUnitByID( NetworkManager::Instance()->getCurrentRobotId() ) !=NULL )
		{
			UnitObject * unitObj = UnitManagerClient::Instance()->getUnitByID( NetworkManager::Instance()->getCurrentRobotId() )->unitObject;
			int maxVal = 100;
			if ( unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().PROBE
		      || unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().PROBE_WALK
			  || unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TEST_DROID
			  || unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().DROID_WALK
			  || unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().TEST_ELECTRICTHING
			  )
			{
				// HEALTH
				if ( unitObj->GetModelID() == GetGraphicsManager()->GetMeshManager()->GetMeshSelector().PROBE)
					maxVal = 50;
				else if ( unitObj->GetModelID() == 4)
					maxVal = 300;
				hud->getBar2()->setMax( maxVal );
				hud->getBar2()->setValue( (int)unitObj->GetUnitHealth() );
				// CHARGE
				hud->getBar()->setMax(100);
				hud->getBar()->setValue( (int)unitObj->GetUnitCharge() );

                D3DXVECTOR3 c_Pos  = GetGraphicsManager()->GetCamera()->GetPOffset();
                D3DXVECTOR3 cu_Pos = GetGraphicsManager()->GetCamera()->GetPosition();
                float zOffset = sqrt(
                                      (c_Pos.x - cu_Pos.x) * (c_Pos.x - cu_Pos.x) +
                                      (c_Pos.y - cu_Pos.y) * (c_Pos.y - cu_Pos.y) +
                                      (c_Pos.z - cu_Pos.z) * (c_Pos.z - cu_Pos.z)
                                    );
                zOffset *= ( windowHeight / LoadingManager::cameraWeight );
                zOffset *= ( windowHeight / LoadingManager::cameraWeight );

#ifdef _DEBUG
                //printf( "Offset closer crosshair by: %f\n" , (zOffset) );
#endif

                if ( currentTarget.currentTargetUnit != NULL )
                {// INSERT HEALTH UPDATE OF TARGETTED ENEMY HERE.
					hud->getBar4()->setMax(currentTarget.currentTargetUnit->GetUnitMaxHealth());
					hud->getBar4()->setValue( (int) currentTarget.currentTargetUnit->GetUnitHealth() );

                    hud->getCrosshairs()->setTint(1.f,0.f,0.f);
                }
				else
                {
					hud->getBar4()->setValue(0);
					hud->getBar4()->setMax(1);

                    hud->getCrosshairs()->setTint(1.f,1.f,1.f);
                }
                if(NetworkManager::Instance()->getCurrentRobotId() != -1 && GetGraphicsManager()->GetCamera()->GetCameraType() == FIRST_PERSON)
                {
                    hud->getCrosshairs()->setPosition( (windowWidth/2.f - 64) , 4000+(windowHeight/2.f -   64 + zOffset/2 ), 0);
                    hud->getCrosshairs()->setPosition( (windowWidth/2.f - 128), ( windowHeight/2.f - 64    ), 1);
				}else
				{
					hud->getCrosshairs()->setPosition( (windowWidth/2.f -  64 ), (windowHeight/2.f -   64 + zOffset/2 ), 0);
					hud->getCrosshairs()->setPosition( (windowWidth/2.f - 128 ), ( windowHeight/2.f - 128 + 24 + zOffset   ), 1);
				}
			}
		}
		else 
		{	// HEALTH
			hud->getBar2()->setValue(0);
			hud->getBar2()->setMax(1);

			// CHARGE
			hud->getBar()->setValue(0);
			hud->getBar()->setMax(1);
			hud->getBar4()->setValue(0);
			hud->getBar4()->setMax(1);
			
            hud->getCrosshairs()->setTint(1.f,1.f,1.f);
		}
	}

    currentTarget.closestDist = -1;
    currentTarget.currentTargetUnit = NULL;

    // Do the frame processing for the graphics object.
    result = m_Graphics->Frame();
    if(!result) return false; 

    return true;
}

LRESULT CALLBACK SystemManager::MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        //UPDATE THE INPUTS

        /////////////////
        //  KEYBOARD   //
        /////////////////
        // Check if a key has been pressed on the keyboard.
        case WM_KEYDOWN:
        {
            // If a key is pressed send it to the input object so it can record that state.
            m_Input->KeyDown((unsigned int)wParam);
            return FALSE;
        }

        // Check if a key has been released on the keyboard.
        case WM_KEYUP:
        {
            // If a key is released then send it to the input object so it can unset the state for that key.
            m_Input->KeyUp((unsigned int)wParam);
            return FALSE;
        }

        /////////////////
        //    MOUSE    //
        /////////////////
        case WM_MOUSEMOVE:
        {
            m_Input->UpdateMousePosition(lParam);
            return 0;
        }
        case WM_LBUTTONDOWN:
        {
            m_Input->UpdateMouseState(true, LEFT);
            return 0;
        }
        case WM_LBUTTONUP:
        {
            m_Input->UpdateMouseState(false, LEFT);
            return 0;
        }
        case WM_MBUTTONDOWN:
        {
            m_Input->UpdateMouseState(true, MIDDLE);
            return 0;
        }
        case WM_MBUTTONUP:
        {
            m_Input->UpdateMouseState(false, MIDDLE);
            return 0;
        }

        case WM_RBUTTONDOWN:
        {
            m_Input->UpdateMouseState(true, RIGHT);
            return 0;
        }
        case WM_RBUTTONUP:
        {
            m_Input->UpdateMouseState(false, RIGHT);
            return 0;
        }

        case WM_MOUSEWHEEL:
        {
            m_Input->UpdateMouseWheel(wParam, lParam);
            return 0;
        }

        case WM_DESTROY: 
        {
            return 0;
        }

        // Any other messages send to the default message handler as our application won't make use of them.
        default:
        {
            // handle keypress
            if (wParam==VK_ESCAPE)
            { 
                return FALSE;
            }
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
}

void SystemManager::InitializeWindows()
{
    WNDCLASSEX WindowClassX;
    DEVMODE dmScreenSettings;
    int posX, posY;

    // Get the instance of this application.
    m_hinstance = GetModuleHandle(NULL); // ?

    // Give the application a name.
    m_applicationName = "Charged";

    // Setup the windows class with default settings.
    WindowClassX.cbSize        = sizeof(WNDCLASSEX); // V,S
    WindowClassX.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // ?,S
    WindowClassX.lpfnWndProc   = WndProc;         // V
    WindowClassX.cbClsExtra    = 0;
    WindowClassX.cbWndExtra    = 0;
    WindowClassX.hInstance     = m_hinstance;        // V
    WindowClassX.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
    WindowClassX.hIconSm       = WindowClassX.hIcon;
    WindowClassX.hCursor       = LoadCursor(NULL, IDC_ARROW); // V
    WindowClassX.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); // Turn this off maybe?
    WindowClassX.lpszMenuName  = NULL;
    WindowClassX.lpszClassName = m_applicationName;  // V
    
    // Register the window class.
    RegisterClassEx(&WindowClassX);  // V

    // Determine the resolution of the clients desktop screen.
    int screenWidth  = GetSystemMetrics(SM_CXSCREEN);
    int screenHeight = GetSystemMetrics(SM_CYSCREEN);

    // Setup the screen settings depending on whether it is running in full screen or in windowed mode.
    if(LoadingManager::fullscreen)
    {
        // If full screen set the screen to maximum size of the users desktop and 32bit.
        memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
        dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
        dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
        dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
        dmScreenSettings.dmBitsPerPel = 32;            
        dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

        // Change the display settings to full screen.
        ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

        // Set the position of the window to the top left corner.
        posX = posY = 0;
    } else
    {
		screenWidth  = LoadingManager::windowWidth;
        screenHeight = LoadingManager::windowHeight;

		printf("windowWidth: %d\n", LoadingManager::windowWidth);
		printf("windowHeight: %d\n", LoadingManager::windowHeight);
		printf("screen width: %d\n", GetSystemMetrics(SM_CXSCREEN));
		printf("screen height: %d\n", GetSystemMetrics(SM_CYSCREEN));
        Log::Instance()->Print("windowWidth: %d\nwindowHeight: %d\nscreen width: %d\nscreen height: %d",
                                LoadingManager::windowWidth,
                                LoadingManager::windowHeight,
                                GetSystemMetrics(SM_CXSCREEN),
                                GetSystemMetrics(SM_CYSCREEN));

        // Place the window in the middle of the screen.
        posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
        posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
    }

    // Create the window with the screen settings and get the handle to it.
    m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
                            WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
                            posX, posY,
                            screenWidth, screenHeight,
                            NULL, NULL, m_hinstance, NULL);

    // Bring the window up on the screen and set it as main focus.
    ShowWindow(m_hwnd, SW_SHOW);
    SetForegroundWindow(m_hwnd);
    SetFocus(m_hwnd);
       
    return;
}
void SystemManager::ShutdownWindows()
{
    // Show the mouse cursor.
    //m_Input->ShowMouseCursor(); // unnecessary, program is closing anyway

    // Fix the display settings if leaving full screen mode.
    if(LoadingManager::fullscreen)
    {
        ChangeDisplaySettings(NULL, 0);
    }

    // Remove the window.
    DestroyWindow(m_hwnd);
    m_hwnd = NULL;

    // Remove the application instance.
    UnregisterClass(m_applicationName, m_hinstance);
    m_hinstance = NULL;

    // Release the pointer to this class.
    //ApplicationHandle = NULL;

    return;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        // Check if the window is being destroyed.

        case WM_DESTROY:
        {
            PostQuitMessage(0);
            return 0;            
        }

        // Check if the window is being closed.
        case WM_CLOSE:
        {
            PostQuitMessage(0);
            return 0;           
        }


        // All other messages pass to the message handler in the system class.
        default:
        {
            return SystemManager::Instance()->MessageHandler(hWnd, message, wParam, lParam);
        }
    }
}

void SystemManager::SetCurrentTarget(UnitObject& unit, float distance)
{

    currentTarget.currentTargetUnit = &unit;
    currentTarget.closestDist       = distance;
};