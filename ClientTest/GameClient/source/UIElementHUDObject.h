#ifndef _UIELEMENTHUDOBJECT_H_
#define _UIELEMENTHUDOBJECT_H_

#include "D3DManager.h"
#include "TextureObject.h"

/***
 * For Bitmap Elements currently...
 */
class UIElementHUDObject
{
private:
    struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};
    ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	TextureObject* m_Texture;

	int m_screenWidth, m_screenHeight;
	int m_bitmapWidth, m_bitmapHeight;

	int m_previousPosX, m_previousPosY;

    bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	bool UpdateBuffers(ID3D11DeviceContext*, int, int);
	void RenderBuffers(ID3D11DeviceContext*);

	bool LoadTexture(ID3D11Device*, char*);
	void ReleaseTexture();

public:
    UIElementHUDObject();
    UIElementHUDObject(const UIElementHUDObject &);
    ~UIElementHUDObject();

    bool Initialize(ID3D11Device*, int, int, char*, int, int);
	void Shutdown();
	bool Render(ID3D11DeviceContext*, int, int);

    int GetIndexCount();
    //ID3D11ShaderResourceView
    TextureObject * GetTextureObject();

    void GetBitmapDimensions(int &, int &);

};

#endif