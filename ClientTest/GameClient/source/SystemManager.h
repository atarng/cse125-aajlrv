////////////////////////////////////////////////////////////////////////////////
// Filename: SystemManager.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SYSTEM_MANAGER_H_
#define _SYSTEM_MANAGER_H_

///////////////////////////////
// PRE-PROCESSING DIRECTIVES //
///////////////////////////////
//#define WIN32_LEAN_AND_MEAN

//////////////
// INCLUDES //
//////////////
#define _WINSOCKAPI_    // stops windows.h including winsock.h
#include <windows.h>

///////////////////////
// MY CLASS INCLUDES //
///////////////////////

#include "SoundManager.h"

#include "GuiConsole.h"
#include "Timer.h"
#include "ControllerManager.h"

#include <PlayerClient.h>

// FORWARD DECLARATIONS
class UnitObject;
class HUDManager;
class GraphicsManager;
class UnitManagerClient;
class InputManager;

////////////////////////////////////////////////////////////////////////////////
// Class name: SystemManager
////////////////////////////////////////////////////////////////////////////////
class SystemManager
{
private:
    struct CURRENTTARGET;

public:
    static SystemManager* Instance();
    
	bool Initialize();
    inline void PrepareShutdown(){ done = true;}
	void Shutdown();
	void Run();

    inline InputManager   * GetInputManager(){return m_Input;}
    inline GraphicsManager* GetGraphicsManager(){return m_Graphics;}
	inline SoundManager   * GetSoundManager(){return m_Sound;}
	inline HUDManager     * GetHUDManager(){return m_HUD;}
	inline ControllerManager     * GetControllerManager(){return m_Control;}

    inline UnitObject * GetCurrentPlayerUnit()        { return currentPlayerUnit; }
    inline void SetCurrentPlayerUnit(UnitObject* unit){ currentPlayerUnit = unit;}

    inline CURRENTTARGET GetCurrentTarget() { return currentTarget; }
    void SetCurrentTarget(UnitObject&, float);

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

    //final say in anything regarding resolution
    static int windowWidth;
    static int windowHeight;

protected:
    SystemManager();
    ~SystemManager();

private:
	SystemManager(const SystemManager&);
    SystemManager& operator=(SystemManager const&);

	bool Frame();
	void InitializeWindows();
	void ShutdownWindows();

	LPCSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;

    Timer mainTimer;
    bool done; // if this is true, then the system manager will start shutdown

////////////////////////////////////////////////////////////////////////
    struct CURRENTTARGET
    {
        UnitObject* currentTargetUnit;
        float closestDist;
    } currentTarget;

	InputManager* m_Input;
	GraphicsManager* m_Graphics;
	SoundManager* m_Sound;
	HUDManager* m_HUD;
	ControllerManager* m_Control;

    UnitManagerClient * m_UnitManager;    
    UnitObject* currentPlayerUnit;

    PlayerClient* myPlayerClient;

/////////////////////////////////////////////////////////////////////////    
    //INSTANCE
    static SystemManager* m_Instance;

};


/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/////////////
// GLOBALS //
/////////////


#endif