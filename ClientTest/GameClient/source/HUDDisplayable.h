#ifndef _HUDDISPLAYABLE_H_
#define _HUDDISPLAYABLE_H_

class HUDDisplayable
{
private:
    bool alphaComponent;
    bool toBeRemoved;
protected:

public:
    HUDDisplayable();
    HUDDisplayable(HUDDisplayable &);
    ~HUDDisplayable();

    void setAlphaComponent(bool);
    bool hasAlphaComponent();

    void markForRemoval();

    virtual bool isText();
    virtual bool isGraphic();
};

#endif