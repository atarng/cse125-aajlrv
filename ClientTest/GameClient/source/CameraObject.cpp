////////////////////////////////////////////////////////////////////////////////
// Filename: CameraObject.cpp
////////////////////////////////////////////////////////////////////////////////
//#include "CameraObject.h"
#define _WINSOCKAPI_
#include "NetworkManager.h"

#include "HUDManager.h"
#include "CameraObject.h"



#define LERP_SPEED 0.15f
#define MAX_PITCH 80.0f
float CameraObject::zoom = 30.0f;

CameraObject::CameraObject()
{
	pos = D3DXVECTOR3(0,0,0);
    dir = D3DXVECTOR3(0,0,1);
    cameraZoom = 25.0f;  //default value
    camType = DISABLED;
    shakeFrames = 0;
    followUnit = NULL;
}
CameraObject::CameraObject(const CameraObject& other)
{
}
CameraObject::~CameraObject()
{
}

//used for Free Camera
void CameraObject::Update(D3DXVECTOR3 move, D3DXVECTOR3 rotate)
{
    D3DXMATRIX rotationMatrix;
    D3DXVECTOR3 rot = rotate;
    D3DXVECTOR3 temp = move;

    float yaw, pitch, roll;    
   
    rot.x = (rot.x > MAX_PITCH)? MAX_PITCH:((rot.x < -MAX_PITCH)? -MAX_PITCH: rot.x);
    dir = rot;

    pitch = dir.x * 0.0174532925f;  // this multiplication will convert dir.x into radians
	yaw   = dir.y * 0.0174532925f;  // this multiplication will convert dir.y into radians
	roll  = dir.z * 0.0174532925f;  // this multiplication will convert dir.z into radians

    D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
    D3DXVec3TransformCoord(&temp, &temp, &rotationMatrix);

    pos += temp;
}

// do no use anymore
void CameraObject::FollowTo(D3DXVECTOR3 newPos, D3DXVECTOR3 newDir, bool moved)
{    
    if(cameraZoom != zoom){
        cameraZoom = (1-LERP_SPEED)*cameraZoom + LERP_SPEED*zoom;        
    }
    
    D3DXMATRIX rotationMatrix;
    D3DXVECTOR3 expectedPos = D3DXVECTOR3(0,15,-cameraZoom);
    D3DXVECTOR3 expectedDir = (moved)? newDir : dir;
    float pitch, yaw, roll;
        
    if(shakeFrames > 1){
        expectedPos += D3DXVECTOR3(rand()%4-2.0f, 0.0f, rand()%4-2.0f);        
        expectedDir += D3DXVECTOR3(0,0, rand()%70-35.0f);
        shakeFrames--;
    }

    pitch = dir.x * 0.0174532925f;
	yaw   = dir.y * 0.0174532925f;
	roll  = dir.z * 0.0174532925f;

    D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
    D3DXVec3TransformCoord(&expectedPos, &expectedPos, &rotationMatrix);
    expectedPos += newPos;

    //Adjust lerp float to change how quickly the camexra lerps to the new coords    
    if(camType == FREE)
    {
        D3DXVec3Lerp(&pos, &pos, &expectedPos, LERP_SPEED);    
    }
    else
    {
        D3DXVECTOR3 derp = pos - expectedPos;
        float haha = D3DXVec3Length(&derp);
        D3DXVec3Lerp(&pos, &pos, &expectedPos, LERP_SPEED);
        //pos = expectedPos;
    }   
    
    D3DXVec3Lerp(&dir, &dir, &expectedDir, LERP_SPEED);
    //dir = expectedDir;
}

/***
 * Create the view matrix from the three updated vectors: Position, Look At pnt, and Up Vector
 */
void CameraObject::GetMatrix(D3DXMATRIX& m_viewMatrix, float deltaTime)
{
    if(cameraZoom != zoom){
        cameraZoom = (1-LERP_SPEED)*cameraZoom + LERP_SPEED*zoom;        
    }

	D3DXVECTOR3 posOffset, dirOffset, up, lookAt, tempPos;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;
    dirOffset = D3DXVECTOR3(0.0f, 0.0f, 0.0f);    

    if(followUnit)
    {
        followUnit->UnitStateUpdate();
        D3DXVECTOR3 playerPos = followUnit->GetPosition();
        D3DXVECTOR3 playerDir = followUnit->GetRotation();
        pos = playerPos;        
        dir = playerDir;
                            
        posOffset = D3DXVECTOR3(0.0f, 0.15f, -1.0f)*cameraZoom;                        
    }	              
    else{
        posOffset = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    }

	if( !HUDManager::Instance()->SplashIsOn() &&
		NetworkManager::Instance()->getCurrentRobotId() != -1 && camType == FIRST_PERSON)
            posOffset = D3DXVECTOR3(0.0f, 0.0f, 0.0f);        

    up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);	
    lookAt= D3DXVECTOR3(0.0f, 0.0f, MAX_ZOOM*2 );
    
    if(shakeFrames > 1){
        posOffset += D3DXVECTOR3(rand()%5-2.5f, 0.0f, rand()%5-2.5f);        
        dirOffset += D3DXVECTOR3(0,0, rand()%20-10.0f);
        shakeFrames--;
    }

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = (dir.x + dirOffset.x) * 0.0174532925f;
	yaw   = (dir.y + dirOffset.y) * 0.0174532925f;
	roll  = (dir.z + dirOffset.z) * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);           

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.    
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
    D3DXVec3TransformCoord(&posOffset, &posOffset, &rotationMatrix);
    
	// Translate the rotated camera position to the location of the viewer.
	lookAt    = pos + lookAt;
    posOffset = pos + posOffset;
    

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &posOffset, &lookAt, &up);
    pOffset = posOffset;
    c_lookAt = lookAt;
    //pos = tempPos;

    wtf_matrix = m_viewMatrix;

	return;
}

void CameraObject::GetBillBoardMatrix(D3DXMATRIX& m_viewMatrix)
{
	D3DXVECTOR3 l_pos, up, origin, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

    origin = D3DXVECTOR3(0,0,0);

	// Setup the position of the camera in the world.
	l_pos = pos;

    up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	// Setup where the camera is looking by default.
    lookAt= D3DXVECTOR3(0.0f, 0.0f, 100.0f);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = dir.x * 0.0174532925f;
	yaw   = dir.y * 0.0174532925f;
	roll  = dir.z * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.    
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
    
	// Translate the rotated camera position to the location of the viewer.
	lookAt = l_pos + lookAt;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &l_pos, &lookAt, &up);

	return;
}

void CameraObject::RenderReflection(float height)
{
	D3DXVECTOR3 up, position, lookAt;
	float radians;

	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup the position of the camera in the world.
	// For planar reflection invert the Y position of the camera.
    position.x =  pOffset.x;
	position.y = -pOffset.y + (height * 2.0f);
	position.z =  pOffset.z;

	// Calculate the rotation in radians.
    radians = dir.y * 0.0174532925f;

	// Setup where the camera is looking.
	lookAt.x = sinf(radians) + pOffset.x;
	lookAt.y = position.y;
	lookAt.z = cosf(radians) + pOffset.z;

	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_reflectionViewMatrix, &position, &lookAt, &up);

	return;
}
D3DXMATRIX CameraObject::GetReflectionViewMatrix(){ return m_reflectionViewMatrix; }