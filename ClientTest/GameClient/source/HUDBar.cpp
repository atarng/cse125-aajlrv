/***
 * Filename: HUDBar.cpp
 * Contributors: Roy Chen,
 * Purpose:
 *
 * TODO: HUDBar should support values outside of the 0-100 range.
 */

#include "HUDBar.h"
#include <iostream>
using namespace std;

HUDBar::HUDBar(int width, int max)
{
	super();
	this->width = width;
	blueBar = new HUDGraphic;
	this->hb_max = max;
    sprintf_s(buffer,"%d",getValue());
	textValue->ChangeText(buffer);
	blueBar->ChangeWidth(getRight());
}

HUDBar::HUDBar(int value3, int width, int max) {
	super();
	this->value = value3;
	this->width = width;
	this->hb_max   = max;
	blueBar = new HUDGraphic;
	
    sprintf_s(buffer,"%d",getValue());
	textValue->ChangeText(buffer);
	blueBar->ChangeWidth(getRight());
}



int HUDBar::getRight()
{
	double d = ((double) value)/hb_max;
	int retVal =  (int) (width*d);
	return retVal;
}

/***
 * Will probably be deprecated
 */
void HUDBar::decValue(int number) {
	if (value > 0) value -= number;
	sprintf_s(buffer,"%d",getValue());
	textValue->ChangeText(buffer);
	blueBar->ChangeWidth(getRight());
}
/***
 *
 */
void HUDBar::incValue(int number) {
	if (value < hb_max) value += number;
	sprintf_s(buffer,"%d",getValue());
	textValue->ChangeText(buffer);
	blueBar->ChangeWidth(getRight());
}

/***
 *
 */
void HUDBar::setValue(int number)
{
	//if ( abs(hb_max/2 - value) <= hb_max/2 ) value = number;
	//else                               value = 0;
	value = number;

	sprintf_s(buffer,"%d",getValue());

	textValue->ChangeText(buffer);
	blueBar->ChangeWidth(getRight());
}

void HUDBar::setMax(int newMax)
{
	this->hb_max = newMax;
	blueBar->ChangeWidth(getRight());
}