#ifndef _PARTICLESYSTEMMANAGER_H_
#define _PARTICLESYSTEMMANAGER_H_

#include "ParticleSystemObject.h"
#include <vector>

// FORWARD DECLARATIONS
//class ParticleSystem;

class ParticleSystemManager
{
private:
    struct ParticleSystemWrapper
    {
        ParticleSystem * particleSystem;
        bool active; // might not actually be used...
    };
    struct ParticleSelecter
    {
        int TEST_GLITTER;
        int TEST_RADIAL_LIGHTNING;
        int TEST_JETPACK;
    } particleSelecter;

    std::vector<ParticleSystemWrapper> particleSystemWrappers;
    std::vector<ParticleSystem*>    expirableParticleSystems;
    std::vector<ParticleSystemObject*> particleSystemObjects;

public:
    ParticleSystemManager();
    ~ParticleSystemManager();

    bool Initialize();
    int AddParticleSystem(ParticleSystem&);
    void AddExpirableSystem(ParticleSystem&);
    void AddParticleSystemObject(ParticleSystemObject&);

    ParticleSystem & GetParticleSystem(int index);

    void CheckForAndRemoveExpiredSystems(float, ID3D11DeviceContext*);
    void Frame(float, ID3D11DeviceContext*);

    inline ParticleSelecter GetParticleSelector(){ return particleSelecter; };

};

#endif