////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "SystemManager.h"

/***
 * Start of the program, delegates to System Manager.
 * NOTE: used rastertek.com as main reference
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

#ifdef _DEBUG
	RedirectIOToConsole();
#endif    
	SystemManager * application;
	bool result;

    application = SystemManager::Instance(); // create the System instance for the first time.	
	if(!application) return 0;

    // All initializaions should be comepleted in this call, other singleton instances will be created here
	result = application->Initialize();         

	if( result ) application->Run(); // this will run our main loop.

	// Shutdown and release the system object.
	application->Shutdown();

	return 0;
}