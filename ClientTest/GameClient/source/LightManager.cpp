/***
 * Filename: LightManager.cpp
 * Contributors: Joey Ly
 * Purpose:
 *
 * TODO: Use it...
 */
#include "LightManager.h"
LightManager::LightManager()
{
}
LightManager::~LightManager()
{
}

bool LightManager::Initialize()
{
    bool result = true;
    // Create the light object.
    LightObject* test_Light = new LightObject;
	// Initialize the light object.
    test_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
    test_Light->SetAmbientColor(1.0f, 1.0f, 1.0f, 1.0f);
    test_Light->SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
    test_Light->SetSpecularPower(32.0f);

	test_Light->SetDirection(0.0f, 0.0f, 1.0f); 
    //test_Light->SetLookAt(D3DXVECTOR3(640.0f, 0.0f, 640.0f)); 
    //test_Light->SetLookAt(D3DXVECTOR3(1280.0f, 0.0f, 1280.0f)); 
    test_Light->SetPosition(D3DXVECTOR3(640.0f, 400.f, 640.0f));
    addLight(*test_Light);

    //test_Light->SetAmbientColor(0.3f, 0.3f, 0.3f, 0.3f);

    return result;
}

int LightManager::addLight(LightObject& addLight)
{
    lightObjects.push_back(&addLight);
    return (lightObjects.size() - 1);
}
LightObject * LightManager::getLight(int index)
{
    return lightObjects.at(index);
}
