//ShaderManager.h
#ifndef _SHADER_MANAGER_
#define _SHADER_MANAGER_

#include <vector>
#include "ShaderObject.h"

class ShaderManager
{
private:
    std::vector <ShaderObject *> shaderObjects;

    struct ShaderSelecter
    {
        int PARTICLE_SHADER;     // 0
        int PARTICLE_SHADER_BB;  // 0.5
        int BUMP_SHADER;         // 1
        int DIR_LIGHT_SHADER_0;  // 2
        int COLOR_SHADER_0;      // 3
        int HUD_SHADER;          // 4
        int BUMP_SHADER_PLUS_SHADOW;        // 5

        int TEST_DEPTHSHADER;   // $$$
        int TEST_SHADOWSHADER;  // $$$

        int TEST_REFLECTIVE;

        int TEST_ANIMATION_SHADER; // ON HOLD...
    } shaderSelector;

	ShaderObject * mainUnitShader;

	bool usingShadows;

public:
    ShaderManager();
    ~ShaderManager();

    bool Initialize(HWND hwnd);
    int AddShader(ShaderObject &);
    ShaderObject & GetShader(int);

	ShaderObject* GetUnitShader();// { return mainUnitShader; } ;

	void SetUsingShadows(bool);
	inline bool IsUsingShadows(){ return usingShadows; };

    inline ShaderSelecter GetShaderSelector(){ return shaderSelector; };
};

#endif