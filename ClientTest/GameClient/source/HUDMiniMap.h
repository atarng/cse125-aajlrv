#ifndef _HUD_MINIMAP_H_
#define _HUD_MINIMAP_H_

#include <vector>
#include <map>
#include "D3DManager.h"
#include "HUDMenuItem.h"
#include "HUDDisplayable.h"

class UnitObject;

class HUDMiniMap : public HUDMenuItem
{
public:
    struct MapIconWrapper;

private:
    typedef HUDMenuItem super;
    HUDDisplayable* m_Map;

    std::map <UnitObject*, MapIconWrapper> MapElementsMap; // can probably move this to menu item
public:
    struct MapIconWrapper
    {
        HUDDisplayable*   displayable;
        UnitObject* associatedUnitObj;
        bool toBeRemoved;
    };

    HUDMiniMap();
    ~HUDMiniMap();

    bool Initialize(HUDDisplayable& map);
    int addMiniMapItem(HUDDisplayable& , UnitObject& );
    void markForRemoval(UnitObject&);
    void Render(D3DManager*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);

// INHERITED MEMBERS:
//
//
//
//
//
//
//
};

#endif