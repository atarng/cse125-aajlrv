//SceneNode.h

#ifndef _SCENENODE_H_
#define _SCENENODE_H_

#include "D3DManager.h"
#include <vector>

class SceneNode
{
protected:
    int nodeID;
    D3DXMATRIX matrix;

    std::vector <SceneNode *> children;

public:
    SceneNode();
    SceneNode(const SceneNode &);
    ~SceneNode();

    inline SceneNode* getChild(int index){ return children.at(index); };
    void addChild(SceneNode&);
    int numChildren();
    //void removeChild(int);
    
    inline void SetMatrix(D3DXMATRIX& newMatrix) { matrix = newMatrix; }
    inline D3DXMATRIX GetMatrix() { return matrix; }

    inline int GetID() { return nodeID; };

    virtual void UpdateMatrix()=0;
    virtual void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode  ) = 0;
    virtual void Render(D3DManager* d3dMan)=0;

    virtual void OptimizationCheck( int& lodLevel, bool& frustumCull );

    inline virtual bool isRootNode()     { return false; };

    inline virtual bool isCameraNode()   { return false; };
    inline virtual bool isLightNode()    { return false; };

    inline virtual bool isModelNode()      { return false; }; // genericDrawable
    inline virtual bool isTerrainNode()    { return false; };
    inline virtual bool isUnitNode()       { return false; };
    inline virtual bool isParticleNode()   { return false; };
    inline virtual bool isProjectileNode() { return false; };

    inline virtual bool checkForRemoval()  { return false; };

};

#endif