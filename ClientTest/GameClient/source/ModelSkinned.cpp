#include "ModelSkinned.h"
#include "SystemManager.h"

#include <iostream>
#include <assimpCollada/aiPostProcess.h>
#include "Log.h"

extern double testFrame;
extern int testFrame2;

ModelSkinned::ModelSkinned()
{
    m_vertexBuffer = NULL;
	m_indexBuffer  = NULL;

	m_model       = NULL;
    m_vertexCount = 0;

    animEvaluator = NULL;
    multMeshPtr   = NULL;

    scene      = NULL;
    m_Textures = NULL;

    numMeshes = 0;
    hasAnimations = false;

}
ModelSkinned::ModelSkinned(const ModelSkinned& other)
{
}
ModelSkinned::~ModelSkinned(){ 
    //Shutdown();
}

// fileType
//  0: simplified txt.
bool ModelSkinned::Initialize(ID3D11Device* device, char* modelFilename, int fileType)
{
    bool result;

	// Load in the model data,
	result = LoadModel(modelFilename, fileType);
	if(!result)
	{
        cout << "LoadModel(mfn) failed..." << endl;
		return false;
	}
	// Initialize the vertex and index buffers.
	result = InitializeBuffers(device);
	if(!result)
	{
        cout << "Init buffers failed..." << endl;
		return false;
	}

    return true;
}
bool ModelSkinned::InitializeBuffers(ID3D11Device* device)
{
	VertexType_Animated* vertices = 0;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData; // ????
	HRESULT result;

    cerr << "m_vertexCount is: " << m_vertexCount << endl;

	// Create the vertex array.
	vertices = new VertexType_Animated[m_vertexCount];
	if(!vertices) { return false; }

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices) { return false; }

	// Load the vertex array and index array with data.
    int meshNum = 0;
    unsigned int offset = 0;
	for( unsigned int i = 0; i < (unsigned int) m_vertexCount; i++ )
	{
        if ( multMeshPtr != NULL )
        {
            if ( i >= (multMeshPtr[meshNum].numVerts + offset) )
            {
                ++meshNum;
                offset = i;
                if ( meshNum >= this->numMeshes)
                {
                    cout << "[ModelSkinned] ERROR: You killed it." << endl;
                    return false;
                }
            }

            // SET POSITION TO RELATIVE POSITION....
            D3DXVECTOR3 d3dvec   = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].x,  multMeshPtr[meshNum].meshObj[i - offset].y, multMeshPtr[meshNum].meshObj[i - offset].z);
            D3DXVec3TransformCoord( &(vertices[i].position) , &d3dvec, &multMeshPtr[meshNum].modelRelativeMatrix );

            vertices[i].texture  = D3DXVECTOR2(multMeshPtr[meshNum].meshObj[i - offset].tu, multMeshPtr[meshNum].meshObj[i - offset].tv);
		    vertices[i].normal   = D3DXVECTOR3(multMeshPtr[meshNum].meshObj[i - offset].nx, multMeshPtr[meshNum].meshObj[i - offset].ny, multMeshPtr[meshNum].meshObj[i - offset].nz);
            indices [i] = i;

            vertices[i].weight   = D3DXVECTOR2(multMeshPtr[meshNum].meshObj[i - offset].weight0, multMeshPtr[meshNum].meshObj[i - offset].weight1);
            vertices[i].indices  = D3DXVECTOR2(multMeshPtr[meshNum].meshObj[i - offset].index0,  multMeshPtr[meshNum].meshObj[i - offset].index1);

        } else
        { //
		    vertices[i].position = D3DXVECTOR3(m_model[i].x,  m_model[i].y, m_model[i].z);
		    vertices[i].texture  = D3DXVECTOR2(m_model[i].tu, m_model[i].tv);
		    vertices[i].normal   = D3DXVECTOR3(m_model[i].nx, m_model[i].ny, m_model[i].nz);
            indices [i] = i;
        }
	}

	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType_Animated) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; // V
    vertexBufferDesc.CPUAccessFlags = 0;                   // ???
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
        cout << "[ModelSkinned] FAILED:: Now create the vertex buffer. " << endl;
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

bool ModelSkinned::LoadModel(char* filename, int fileType)
{
    if ( fileType != 0 )
    {
        scene = aiImportFile(filename, ( aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded ));
        if ( scene == NULL )
        {
            cout << "stop borking..." << endl;
            return false;
        }

        cout << filename << " there are: " << scene->mNumMeshes              << " meshes"     << endl;
        cout << filename << " there are: " << scene->mNumAnimations          << " animations" << endl;
        cout << filename << " there are: " << scene->mRootNode->mNumChildren << " children "  << endl;
        
        numMeshes = scene->mNumMeshes; // ###
        multMeshPtr = new MultMeshObject[numMeshes];

        if ( scene->HasAnimations() )
        {
            cout << filename << "This Mesh DOES have animations."<< endl;
            Log::Instance()->PrintErr("This Mesh DOES have animations." );
            animEvaluator = new AnimEvaluator(scene->mAnimations[0]);
            hasAnimations = true;
        }

        int vc = 0; // vertexCounter;
        int badMeshes = 0;
        
        aiNode* travNode = scene->mRootNode;

        for(unsigned int i = 0; i < scene->mNumMeshes; i++)
        {
            vc = 0;
            aiMesh * mesh = scene->mMeshes[i];
            if ( mesh->mNumFaces < 6 )
            {
                cout << "Skipping what is PROBABLY a bad mesh..." << endl;
                ++badMeshes;
                continue;
            }

            m_model = new ModelType[mesh->mNumFaces*3];

            memset(m_model, 0, sizeof(ModelType) * (mesh->mNumFaces*3));

            multMeshPtr[i - badMeshes].meshObj  = m_model;
            multMeshPtr[i - badMeshes].numVerts = mesh->mNumFaces * 3;

            multMeshPtr[i - badMeshes].modelRelativeMatrix = D3DManager::AssimpMat2D3DMat(travNode->mChildren[i - badMeshes]->mTransformation);

            m_vertexCount += mesh->mNumFaces * 3;
            m_indexCount  = m_vertexCount; // match up index count and vertex count

            if ( scene->mMeshes[i]->HasBones() )
            { // If Rigged with a skeleton.
                cout << "[Bones]: " << scene->mMeshes[i]->mNumBones << endl; /// !!!
                multMeshPtr[i - badMeshes].bones    = scene->mMeshes[i]->mBones[0];
                multMeshPtr[i - badMeshes].numBones = scene->mMeshes[i]->mNumBones;
                skeletalRigged = true;

                for (unsigned int j = 0; j < mesh->mNumBones; ++j)
                {// j is the bone index
                    const struct aiBone * bone = mesh->mBones[j];
                    for (unsigned int k = 0; k < bone->mNumWeights; ++k)
                    {
                        aiVertexWeight boneWeight = bone -> mWeights[k];

                        int actualVertexIndex = (m_vertexCount - (int)boneWeight.mVertexId) - 1;

                        if ( (actualVertexIndex) >= 0 && ((actualVertexIndex)) < m_vertexCount )
                        {
                            if ( !m_model[((m_vertexCount - (int)boneWeight.mVertexId))].zeroSet )
                            {
                                m_model[(actualVertexIndex)].index0 = (float) j;
                                m_model[(actualVertexIndex)].weight0 = boneWeight.mWeight;
                                m_model[(actualVertexIndex)].zeroSet = true;
                            } else
                            {
                                m_model[(actualVertexIndex)].index1 = (float) j;
                                m_model[(actualVertexIndex)].weight1 = boneWeight.mWeight;
                            }
                        }else
                        {
                            cerr << "HOKAY.... that's not gonna work...: "<< (actualVertexIndex) << endl;
                            Log::Instance()->PrintErr("HOKAY.... that's not gonna work...: %d ", ((m_vertexCount - (int)boneWeight.mVertexId)) );
                        }
                    }
                }
            }

            int nonSkinnedVertices = 0;
            int poorlySkinnedVertices = 0;
            for (unsigned int j = 0; j < mesh->mNumFaces; ++j)
            {
                const struct aiFace * face = &(mesh->mFaces[j]);
                switch(face->mNumIndices)
                {
                    case 3: //cout << "draw triangles" << endl;
                            break;
                    case 1: //cout << "draw points" << endl;
//                            break;
                    case 2: //cout << "draw lines" << endl;
//                            break;
                    default: //cout << "draw polygons" << endl;
                            Log::Instance()->PrintErr("This mesh is not triangulated...: %d\n", face->mNumIndices );
                            break;
                }
                if ( m_model[vc].index0 >= 10 || m_model[vc].index0 < 0 ||
                     m_model[vc].index1 >= 10 || m_model[vc].index1 < 0)
                { // Weights have not been set on these vertices;
                    m_model[vc].index0 = 0; m_model[vc].index1 = 0;
                    m_model[vc].weight0 = 0; m_model[vc].weight1 = 1;
                    ++nonSkinnedVertices;
                } else if( (m_model[vc].weight0 + m_model[vc].weight1) != 1 )
                {
                    if (m_model[vc].weight0 == 0)
                    {
                        m_model[vc].weight0 = 1 - m_model[vc].weight1;
                    }
                    else
                    {
                        m_model[vc].weight1 = 1 - m_model[vc].weight0;
                    }
                    ++poorlySkinnedVertices;
                }

                for (unsigned int k = 0; k < face->mNumIndices; ++k ){
                    int vertexIndex = face->mIndices[k];

                    if(mesh->mNormals != NULL)
                    {
                        aiVector3D normals = mesh->mNormals[vertexIndex];

                        m_model[vc].nx = normals.x;
                        m_model[vc].ny = normals.y;
                        m_model[vc].nz = normals.z;

                    }
                    if(mesh->HasTextureCoords(0))
					{
                        aiVector3D uvVec   = mesh->mTextureCoords[0][vertexIndex];

                        m_model[vc].tu = uvVec.x;
                        m_model[vc].tv = uvVec.y;

                    }
                    aiVector3D vertex  = mesh->mVertices[vertexIndex];

                    m_model[vc].x   = vertex.x;
                    m_model[vc].y   = vertex.y;
                    m_model[vc++].z = vertex.z;
                }
            }
            Log::Instance()->PrintErr( "Weights have not been set on %d vertices: ", nonSkinnedVertices );
            Log::Instance()->PrintErr( "Weights have not been set PROPERLY on %d vertices: ", poorlySkinnedVertices );
        }

        numMeshes = scene->mNumMeshes - badMeshes;

    }else
    {
        cout << "NOT A SKINNABLE MESH" << endl;
    }

	return true;
}

void ModelSkinned::Render(ID3D11DeviceContext* deviceContext)
{
    if ( scene != NULL && scene->HasAnimations() )
    { // If the scene has animations than
        testFrame = (testFrame > 300) ? 0 : ( ++testFrame );
	    animEvaluator->Evaluate( testFrame / 150.0 );
	    std::vector<aiMatrix4x4> m = animEvaluator->GetTransformations();

        // HANDLING SKELETAL ANIMATIONS
        if( hasSkeletalRig() )
        { // POPULATE BONE MATRIXES
            for(unsigned int i=0; i < (unsigned int) multMeshPtr[0].numBones; ++i)
            {
                unsigned int i2;
                if ( i < m.size() ) { i2 = i; }
                else                { i2 = 0; }
                if (i < 10)
                {
                    //cout << "Populating Bone Matrix" << endl;
                    multMeshPtr[0].boneAnimMatrix[i] = D3DManager::AssimpMat2D3DMat( m.at(i2) );
                    multMeshPtr[0].animationMatrix = D3DManager::AssimpMat2D3DMat( m.at(0) );
                }
            }
        } else
        { // HANDLING MULTIMESH ANIMATIONS
            for (int i = 0; i < GetNumMeshes(); ++i)
            {            
                int i2;
                D3DXMATRIX animMat;
                if ( (unsigned int) i < m.size() ) { i2 = i; }
                else                { i2 = 0; }

                multMeshPtr[i].animationMatrix = D3DManager::AssimpMat2D3DMat( m.at(i2) );
            }
        }
    }

	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}

void ModelSkinned::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType_Animated); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0); // ???

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

int ModelSkinned::GetIndexCount(){ return m_indexCount; }
int ModelSkinned::GetNumMeshes(){ return numMeshes; }

TextureObject* ModelSkinned::GetTextureObject() { return m_Textures; }
void ModelSkinned::SetTextureObject(TextureObject& textureRef){ m_Textures = &textureRef; }

bool ModelSkinned::isBonedModel(){ return true; }

/***
 * Cleans up ModelSkinned
 */ 
void ModelSkinned::Shutdown()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();

	return;
}
void ModelSkinned::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}
void ModelSkinned::ReleaseModel()
{
	if(m_model)
	{
		delete [] m_model;
		m_model = 0;
	}

    if (scene)
    {
        delete scene;
        scene = NULL;
    }

	return;
}