#include "CameraNode.h"

CameraNode::CameraNode()
{
    camera = NULL;
}
CameraNode::CameraNode(const CameraNode &)
{
}
CameraNode::~CameraNode()
{
    delete camera;
    camera = NULL;
}

void CameraNode::CreateCamera(){
    camera = new CameraObject();
    camera->SetCameraType(FREE);
    camera->SetPosition(D3DXVECTOR3(0,0,0));
}

void CameraNode::UpdateMatrix(){
    camera->GetMatrix(matrix);
}

void CameraNode::Draw( D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode ) 
{
    UpdateMatrix();
    stack->Push();   
    stack->MultMatrix(&matrix);

    if ( drawMode == 0 ) Render(d3dMan);

    std::vector<SceneNode *>::iterator it;
    for( it = children.begin(); it < children.end(); it++ )
    {
        (*it)->Draw( d3dMan, stack, drawMode );
    }

    stack->Pop();
}

void CameraNode::Render(D3DManager* d3dMan)
{    
    
}