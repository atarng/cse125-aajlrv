#ifndef _HUD_MENU_ITEM_H_
#define _HUD_MENU_ITEM_H_

#include <vector>
#include "D3DManager.h"
#include "HUDDisplayable.h"

class HUDMenuItem
{
protected:
    struct HUDItemConfig;
	std::vector <HUDDisplayable* > DisplayableElements; // this shouldn't be here..?

private:
    std::vector<HUDItemConfig> HUDItemConfigVector;
	void (*m_clickFunction) ();
    D3DXVECTOR3 tintColor;

protected:
    struct HUDItemConfig
    {
        int positionX; // Currently, the Universal Position of the Displayable with respect to the world.
        int positionY; // TODO: Modify this so it reflects the RELATIVE position to the HUD Item.
        int id;        // HUDDISPLAYABLESID
    }config;

    bool displayOn;
    bool clickable;

    float originX, originY;
    float mi_width, mi_height;

	bool originSet;
	float alphaFloor, alphaCap;

public:
    HUDMenuItem();
    ~HUDMenuItem();

    bool isToBeDisplayed();
    bool isClickable();
    bool isInBounds(float x, float y);
    void getBounds(float &w, float &h);
    void getPosition(float &x, float &y);

    void setBounds(float, float);
    void setPosition(float, float, int index = -1);
    void setDisplay(bool);
    void setClickable(bool);
    void setAlphaCap(float);

	bool Initialize(); // ...

    virtual void Render(D3DManager*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
    virtual int getTotalIndexCount();
    virtual void onClickEvent(); // add position information?

    std::vector<HUDItemConfig>& getItemConfig();

    // IDENTIFIERS
    virtual bool isMap();
    virtual bool isButton();
    virtual bool isCounter();
    virtual bool isBar();

    static HUDItemConfig createHUDItemConfig(int, int, int);

	 void setOnClickFunction( void (*)() );
     void setTint(float r, float g, float b);
   // virtual void onClickEvent();
};

#endif