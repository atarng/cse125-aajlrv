//UnitNode.h
#ifndef _UNITNODE_H_
#define _UNITNODE_H_

#include <map>

#include "ModelNode.h"
#include "UnitObject.h"


class D3DManager;
class ShaderObject;

enum AnimationType
{
    at_DEFAULT=0,
    at_WALK,
    at_ATTACK,

    at_LOD_CUBE,
    at_LOD_NOANIM
};

class UnitNode : public ModelNode
{
private:
    typedef ModelNode super;
    bool dynamicShaderSet, dbs; // dbs : drawBoundingSphere
    float animationCounter;

    bool renderAsReflection;

    static UnitObject* lodCube;

protected:
    UnitObject* unitObject;
    Model * boundingSphere; //Pointer to visible boundingSphere.
    AnimationType currentAnimation;

    map<AnimationType,UnitObject*> unitObjectSwapList;

public:
    UnitNode();
    UnitNode(const UnitNode &);
    ~UnitNode();

    void SetUnitObject(UnitObject*);
    void SetUnitObject(UnitObject*, AnimationType);
    void SwapTo(AnimationType at, bool fromLODSwap = false);
    inline AnimationType PollCurrentAnimationType(){ return currentAnimation; };
    void SyncSwapChain();
    //inline void SetShader(ShaderObject* newShader){shader = newShader;}

    inline ShaderObject* GetShader()     { return shader; }
    inline UnitObject*   GetUnitObject() { return unitObject; }

    void UpdateMatrix();
    void Draw(D3DManager* d3dMan, ID3DXMatrixStack * stack, int drawMode );
    void Render(D3DManager* d3dMan);
    void RenderDepth(D3DManager* d3dMan);
    //void RenderReflection(D3DManager* d3dMan);

    inline virtual bool isUnitNode()    { return true; };
    inline virtual bool isModelNode()   { return true; };
    inline bool isDynamic() { return dynamicShaderSet; };
    void setDynamic( bool );

    //inline bool castsShadow(){}
    void displayBoundingSphere(bool);

    bool checkForRemoval();
    //int  LODLevelCheck();
    void OptimizationCheck( int& lodLevel, bool& frustumCull );
};

#endif