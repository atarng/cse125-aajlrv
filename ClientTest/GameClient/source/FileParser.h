//FileParser.h

#ifndef _FILE_PARSER_
#define _FILE_PARSER_

#include <vector>
#include <iterator>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

class FileParser
{
public:
	std::ifstream inFile;

	FileParser();
	FileParser(std::string fileName);
	~FileParser();

	void Open(std::string fileName);
	void Close();

	bool FileParser::FindValue(std::string &retVal, std::string &name);

	bool ParseBool(std::string name);
	int ParseInt(std::string name);
	float ParseFloat(std::string name);
	std::string ParseString(std::string name);

	
};

#endif