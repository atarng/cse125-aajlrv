//PQuaternion.h

#ifndef _PQUATERNION_H_
#define _PQUATERNION_H_
#include <string>
#include <sstream>
#include <iostream>

class PQuaternion
{
public:
    float x,y,z,w;

    PQuaternion();
    PQuaternion(const PQuaternion& rhs){w=rhs.w; x=rhs.x; y=rhs.y; z=rhs.z;}
    PQuaternion(float rw, float rx, float ry, float rz){w=rw; x=rx; y=ry; z=rz;}
    ~PQuaternion();

    // assignment operators
    PQuaternion& operator += ( const PQuaternion& );
    PQuaternion& operator -= ( const PQuaternion& );
    PQuaternion& operator *= ( const PQuaternion& );
    PQuaternion& operator *= ( float );
    PQuaternion& operator /= ( float );

    // binary operators
    PQuaternion operator + ( const PQuaternion& ) const;
    PQuaternion operator - ( const PQuaternion& ) const;
    PQuaternion operator * ( const PQuaternion& ) const;
    PQuaternion operator * ( float ) const;
    PQuaternion operator / ( float ) const;

    float magnitude();
    float magnitude2();


    bool operator == ( const PQuaternion& ) const;
    bool operator != ( const PQuaternion& ) const;

    std::string toString() const;
};
#endif 