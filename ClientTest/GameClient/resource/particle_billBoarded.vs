////////////////////////////////////////////////////////////////////////////////
// Filename: particle.vs
////////////////////////////////////////////////////////////////////////////////

/////////////
// GLOBALS //
/////////////
cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer billBoardMatrix
{
    matrix billBoardMatrix;
};

//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float4 offset   : OFFSET;
    float2 tex      : TEXCOORD0;
	float4 color    : COLOR;
    float4x4 particleRotation : PARTICLEROTATION;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float4 color : COLOR;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType ParticleVertexShader(VertexInputType input)
{
    PixelInputType output;
    float4 v_offset;
	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;
    input.offset.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, input.particleRotation);

    // For bill boarding of individual particles as opposed to the entire system.
    output.position = mul(output.position, billBoardMatrix);

	// Multiply Against Transforms
    output.position = mul(output.position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

    v_offset = mul(input.offset,  worldMatrix);
    v_offset = mul(v_offset, viewMatrix);
    v_offset = mul(v_offset, projectionMatrix);

    output.position += v_offset;

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;
    
	// Store the particle color for the pixel shader. 
    output.color = input.color;

    return output;
}