#ifndef _MOVEMENT_H_
#define _MOVEMENT_H_

#include "Event.h"

class MovementEvent : public Event {
private:
    int unitId;

    float deltaX;
    float deltaY;
    float deltaZ;    
    float deltaYaw;
    float deltaPitch;

public:
    MovementEvent();
    MovementEvent(int unitId, float deltaX, float deltaY, float deltaZ, float deltaYaw, float deltaPitch);
    ~MovementEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isMovement();

    int getUnitId();
    void setUnitId(int);

    float getDeltaX();
    float getDeltaY();
    float getDeltaZ();
    float getDeltaYaw();
    float getDeltaPitch();
    
    void setDeltaX(float);
    void setDeltaY(float);
    void setDeltaZ(float);
    void setDeltaYaw(float);
    void setDeltaPitch(float);
    
};

#endif