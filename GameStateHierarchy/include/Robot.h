#ifndef _ROBOT_H_
#define _ROBOT_H_

#include <Unit.h>
#include <PlayerUnit.h>
#include <AttackObject.h>

/*
enum robotType{
    SPARK,
    PROBE,
    HIVEMIND,
};
*/
class Robot : public Unit
{
private:
    typedef Unit super;

    // NOTION OF WHICH "TEAM" this robot belongs to.
    PlayerUnit   * currentOwner; // PlayerClient, SOMETHING
    //Attack *     AttackType;
    unsigned int AttackType;

protected:
    float heightOffset; // To Allow for flying over heightmap, while locked on.    

public:
    Robot();
    Robot(int);
    Robot(const Robot&);
    ~Robot();

    virtual void cloneValues(Unit *);
    //virtual Unit asUnit();

    bool isRobot();

    PlayerUnit * getPlayerUnit();
    void setPlayerUnit(PlayerUnit &);

    // AUTOMATED UPDATES FOR AI
	void AI_PerformBehavior();

    inline unsigned int getAttack(){ return AttackType; };
    inline float getMoveSpeed(){ return moveSpeed; };

    inline float getHeightOffset(){ return heightOffset; };
    void setHeightOffset(float newHeightOffset);
};

#endif