#ifndef _BULLET2_H_
#define _BULLET2_H_

#include "AttackObject.h"

#define SPEED          300.0  // units per second
#define BULLET2_CHARGE_COST MAX_CHARGE/30.0f // cost per bullet

class Bullet2 : public AttackObject {
private:
    float dirX;
    float dirY;
    float dirZ;    
    float speed; // defined in attackobject, if attack doesn't move simply set to zero    
    //float distanceLeft; // No.
public:
    Bullet2();
    Bullet2(int networkId);
    Bullet2(int networkId, Unit * unitThatFired);
    ~Bullet2();

    // getter functions
    float getDirX();
    float getDirY();
    float getDirZ();
    float getSpeed();

    // setter functions
    void setDirX(float dirX);
    void setDirY(float dirY);
    void setDirZ(float dirZ);
    void setSpeed(float speed);

    // for the last update time
    //unsigned long getLastUpdateTime();
    //void setLastUpdateTime(unsigned long lastUpdateTime);

    bool Update(float deltaTime);

	virtual inline bool isBullet2(){ return true; };
};

#endif