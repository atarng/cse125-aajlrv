#pragma once

#ifndef _EXIT_EVENT_H_
#define _EXIT_EVENT_H_

#include "Event.h"

class ExitEvent : public Event {
private:
    int playerId;
    int sparkUnitId;
    int robotUnitId;

public:
    ExitEvent();
    ExitEvent(int playerId, int sparkUnitId, int robotUnitId);
    ~ExitEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isExit();

    int getPlayerId();
    void setPlayerId(int);

    int getSparkId();
    void setSparkId(int);

    int getRobotId();
    void setRobotId(int);
};

#endif