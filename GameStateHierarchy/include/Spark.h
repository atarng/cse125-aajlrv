#ifndef _SPARK_H_
#define _SPARK_H_

#include <Unit.h>

class Robot;

class Spark : public Unit
{
private:
    typedef Unit super;

protected:
    float  heightOffset; // To Allow for flying over heightmap, while locked on.
    Robot* controllingRobot;
    bool currentlyControlling;

public:
    Spark();
    Spark(int);
    ~Spark();

    bool isSpark();
    bool isCurrentlyControlling();

    void setCurrentlyControlling(bool);

    inline float getHeightOffset(){ return heightOffset; };
    void setHeightOffset(float newHeightOffset);
};

#endif