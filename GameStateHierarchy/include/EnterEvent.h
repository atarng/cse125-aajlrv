#pragma once

#ifndef _ENTER_EVENT_H_
#define _ENTER_EVENT_H_

#include "Event.h"

class EnterEvent : public Event {
private:
    int playerId;
    int unitId;

public:
    EnterEvent();
    EnterEvent(int playerId, int unitId);
    ~EnterEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isEnter();

    int getPlayerId();
    void setPlayerId(int);

    int getUnitId();
    void setUnitId(int);
};

#endif