#ifndef _ATTACK_H_
#define _ATTACK_H_

#include "Serializable.h"

class Attack : public Serializable
{
private:

protected:
    // standard transformation properties
    float posX, posY, posZ;       // position/translation
    float yaw, pitch, roll;       // rotation
    float scaleX, scaleY, scaleZ; // scale
	float velX, velY, velZ;		  // velocity

    // Possibly useful for collision and frustum culling
    //float boundingRadius;         // bounding radius
    //int damage;
    //int duration;
	//float growth;				  // some attacks might increase in bounding area

    int attackIdentity; // Equivalent to the players Id, so it doesn't harm the player

	int modelID;  // To use to match with models/textures/other.
    int shaderID; // To use to match with models/textures/other.

public:
    Attack();
    Attack(int attackIdentity);
    ~Attack();
	
    virtual void cloneValues(Attack *);
	
/*** NETWORKING COMPONENTS ***********************************/
    //virtual bool updateClient(); // updates Client's Scene Manager.
	virtual std::string Serialize();   
    virtual void Deserialize(char * serializeStr); 

/*** END: NETWORKING COMPONENTS ******************************/

/*** ACCESSORS ***********************************************/
    void GetPosition(float& x, float& y, float& z);
    void GetRotation(float& retYaw, float& retPitch, float& retRoll);
    void GetScale   (float& x, float& y, float& z);
    void GetIDs     (int& model, int& shader);
    void GetRadius  (float& radius);

 // Only For Server Use
    void SetPosition(float x, float y, float z);
    void SetRotation(float retYaw, float retPitch, float retRoll);
    void SetScale   (float x, float y, float z);
    void SetIDs     (int model, int shader);
/*** END: ACCESSORS ******************************************/

/*** GETTERS/SETTERS *****************************************/
/*** END: GETTERS/SETTERS ************************************/

    virtual Attack asAttack();

};

#endif