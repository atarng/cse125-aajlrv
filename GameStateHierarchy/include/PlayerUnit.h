#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <Unit.h>

class PlayerUnit : public Unit
{

private:

protected:
    //float posX, posY, posZ; //
    //float rotation;         //
    //int health;

public:
    PlayerUnit();
    ~PlayerUnit();

    int modelId; // To use to match with models/textures/other.
    int shaderId; // To use to match with models/textures/other.
};

#endif