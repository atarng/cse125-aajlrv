#ifndef _PLAYERCLIENT_H_
#define _PLAYERCLIENT_H_

#include "Serializable.h"
#include "Unit.h"

class PlayerClient : public Serializable
{
private:
	Unit * currentUnit;
    // score?
protected:

public:
    PlayerClient();
    ~PlayerClient();

    virtual std::string Serialize();
    virtual void Deserialize(char *);
};

#endif