#pragma once

#ifndef _UNASSIGN_H_
#define _UNASSIGN_H_

#include "Event.h"

class UnassignEvent : public Event {
private:
    int playerId;

public:
    UnassignEvent();
    UnassignEvent(int playerId);
    ~UnassignEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isUnassign();

    int getPlayerId();
    void setPlayerId(int);
};

#endif