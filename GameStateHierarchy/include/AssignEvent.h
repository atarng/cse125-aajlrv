#pragma once

#ifndef _ASSIGN_H_
#define _ASSIGN_H_

#include "Event.h"

class AssignEvent : public Event {
private:
    int playerId;
    int unitId;

public:
    AssignEvent();
    AssignEvent(int playerId, int unitId);
    ~AssignEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isAssign();

    int getPlayerId();
    void setPlayerId(int);

    int getUnitId();
    void setUnitId(int);
};

#endif