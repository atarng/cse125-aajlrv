#ifndef _UNIT_H_
#define _UNIT_H_

#include "Serializable.h"
#include "rapidxml.hpp"
#include <LoadingManager.h>

#define UNIT "Unit"

#define NEUTRAL_UNIT -1

#define ANIM_DEFAULT 0
#define ANIM_WALK 1
#define ANIM_ATTACK 2

// for attack events
#define ANIM_METEOR_DETONATE 3

#define MAX_CHARGE 100
#define MAX_HEALTH 100

#define SENTRY_MODEL_ID 2
#define PROBE_MODEL_ID 1
#define FANBOT_MODEL_ID 4
#define SPARK_MODEL_ID 8

#ifdef _ONSERVER

#define ROBOT_HEALTH LoadingManager::robotHealth
#define FAN_HEALTH LoadingManager::fanHealth
#define PROBE_HEALTH LoadingManager::probeHealth
#define DOOM_HEALTH LoadingManager::doomHealth
#define SENTRY_HEALTH LoadingManager::sentryHealth
#define SPARK_HEALTH LoadingManager::sparkHealth

#define SENTRY_SPEED LoadingManager::sentrySpeed
#define DOOM_SPEED LoadingManager::doomSpeed
#define PROBE_SPEED LoadingManager::probeSpeed
#define FANBOT_SPEED LoadingManager::fanSpeed
#define SPARK_SPEED LoadingManager::sparkSpeed

#define SENTRY_CHARGE LoadingManager::sentryCharge
#define PROBE_CHARGE LoadingManager::probeCharge
#define FANBOT_CHARGE LoadingManager::fanCharge

#define DISCHARGE LoadingManager::discharge

#endif

class Timer;

/***
 * THIS SHOULD BE "ABSTRACT"
 */
class Unit : public Serializable
{
private:
	float lastPerformedAction;

protected:
    // standard transformation properties
    float posX, posY, posZ;       // position/translation
    float pitch, yaw, roll;       // rotation
    float scaleX, scaleY, scaleZ; // scale	

    // Possibly useful for collision and frustum culling
    float boundingRadius;         // bounding radius

    float health;					  // health is... something to discuss... example: bullets don't have health.
	float maxHealth;

    // perBotMoveSpeed
	float moveSpeed;
    float attackCoolDown;

	//an identifier for the unit in the network
	int unitNetworkId;
    int controllingPlayerId;

	int modelID; // To use to match with models/textures/other.
    int shaderID; // To use to match with models/textures/other.

    // for unit color
    float r, g, b;

	// server fields
	bool modified;
    bool performedAction;
    bool currentlyControlled;

    float currentCharge;
	float chargeValue; // per second
    
    int currentAnimType;

    float elapsedTime;

public:
    Unit();
    Unit(Unit &);
    Unit(int); // constructor with for the networking ID.
    ~Unit();

    virtual void cloneValues(Unit *);
	
/*** NETWORKING COMPONENTS ***********************************/
    //virtual bool updateClient(); // updates Client's Scene Manager.
	virtual std::string Serialize();   
    virtual void Deserialize(char * serializeStr); 
    virtual void Deserialize(rapidxml::xml_node<> * root); 

// CONVERTS A SUBCLASS OF UNIT TO A UNIT FOR EASY SENDING TO SERVER
    virtual Unit asUnit();

/*** END: NETWORKING COMPONENTS ******************************/

/*** IDENTIFIERS ***/
    virtual bool isRobot();
	virtual bool isDoomRobot();
    virtual bool isSpark();
/*** END IDENTIFIERS ***/

/*** ACCESSORS ***********************************************/
    void GetPosition(float& x, float& y, float& z);
    void GetRotation(float& retPitch, float& retYaw, float& retRoll);
    void GetScale   (float& x, float& y, float& z);
    void GetIDs     (int& model, int& shader);
    void GetRadius  (float& radius);    
    float GetMoveSpeed(){return moveSpeed;}
    float GetAttackCoolDown(){return attackCoolDown;};

 // Only For Server Use
    void SetPosition(float x, float y, float z);
    void SetRotation(float retPitch, float retYaw, float retRoll);
    void SetScale   (float x, float y, float z);
    void SetIDs     (int model, int shader);
	void SetRadius  (float radius);
    void SetMoveSpeed(float _spd);
    void SetAttackCoolDown(float _timeTilNextAttack){attackCoolDown = _timeTilNextAttack;};
    void decrementAttackCooldown(float deltaTime) { attackCoolDown -= deltaTime;}

/*** END: ACCESSORS ******************************************/

/*** GETTERS/SETTERS:: NO, USE THE ABOVE ONES PLEASE *********/
	int getUnitId();
	float getPosX();
	float getPosY();
	float getPosZ();
    float getPitch();
	float getYaw();	
	float getRoll();
	float getScaleX();
	float getScaleY();
	float getScaleZ();
	float getBoundingRadius();
	float getHealth();
	float getMaxHealth();
	int getModelID();
	int getShaderID();
    int getControllingPlayerId();
    float getColorR();
    float getColorG();
    float getColorB();
	bool isModified();
    bool isCurrentlyControlled();
    float getCurrentCharge();
    int getCurrentAnimType();
    bool hasPerformedAction();
	
	//Setter functions
	void setUnitId(int);
	void setPosX(float);
	void setPosY(float);
	void setPosZ(float);
    void setPitch(float);
	void setYaw(float);	
	void setRoll(float);
	void setScaleX(float);
	void setScaleY(float);
	void setScaleZ(float);
	void setBoundingRadius(float);
	void setHealth(float);
	void setMaxHealth(float);
	bool decrementHealth(int);
	void setModelID(int);
	void setShaderID(int);
    void setControllingPlayerId(int controllingPlayerId);
    void setColorR(float r);
    void setColorG(float g);
    void setColorB(float b);
    void setColor(float r, float g, float b);
	void setModified(bool modified);
    void setCurrentlyControlled(bool currentlyControlled);
    virtual void setCurrentCharge(float);
	void incrementCharge(float deltaTime);
	virtual void decrementCharge(float deltaTime);
    void setCurrentAnimType(int);
    void setCurrentAnimType(int, float deltaTime);
    void setPerformedAction(bool, float deltaTime = 0);

/*** END: GETTERS/SETTERS ************************************/

};

#endif