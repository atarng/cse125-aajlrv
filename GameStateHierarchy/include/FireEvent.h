#pragma once

#ifndef _FIRE_H_
#define _FIRE_H_

#include "Event.h"

class FireEvent : public Event {
private:
    int unitId;
    int attackType;

public:
    FireEvent();
    FireEvent(int unitId, int attackType);
    ~FireEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isFire();

    int getUnitId();
    void setUnitId(int);

    int getAttackType();

    void setAttackType(int attackType);
};

#endif