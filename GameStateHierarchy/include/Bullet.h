#ifndef _BULLET_H_
#define _BULLET_H_

#include "AttackObject.h"
#include <LoadingManager.h>

#define BULLET_SPEED		LoadingManager::bulletSpeed  // units per second
#define FAN_BULLET_SPEED	LoadingManager::fanBulletSpeed

class Bullet : public AttackObject {
private:
    float dirX;
    float dirY;
    float dirZ;    
    float speed; // defined in attackobject, if attack doesn't move simply set to zero    
    //float distanceLeft; // No.
public:
    Bullet();
    Bullet(int networkId);
    Bullet(int networkId, Unit * unitThatFired);
    ~Bullet();

    // getter functions
    float getDirX();
    float getDirY();
    float getDirZ();
    float getSpeed();

    // setter functions
    void setDirX(float dirX);
    void setDirY(float dirY);
    void setDirZ(float dirZ);
    void setSpeed(float speed);

    // for the last update time
    //unsigned long getLastUpdateTime();
    //void setLastUpdateTime(unsigned long lastUpdateTime);

    bool Update(float deltaTime);

	virtual inline bool isBullet(){ return true; };
};

#endif