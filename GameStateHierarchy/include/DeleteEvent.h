#pragma once

#ifndef _DELETE_H_
#define _DELETE_H_

#include "Event.h"

class DeleteEvent : public Event {
private:
    int unitId;

public:
    DeleteEvent();
    DeleteEvent(int unitId);
    ~DeleteEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isDelete();

    int getUnitId();
    void setUnitId(int);
};

#endif