#pragma once

#ifndef _LOSE_H_
#define _LOSE_H_

#include "Event.h"

class LoseEvent : public Event {
private:
    int playerId;

public:
    LoseEvent();
    LoseEvent(int playerId);
    ~LoseEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isLose();

    int getPlayerId();
    void setPlayerId(int);
};

#endif