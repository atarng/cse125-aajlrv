#ifndef _EVENT_H_
#define _EVENT_H_

#define EVENT "Event"

// event types
#define EVENT_TYPE "eventType"

#define MOVEMENT_EVENT 0
#define FIRE_EVENT 1
#define DELETE_EVENT 2
#define ASSIGN_EVENT 3
#define ENTER_EVENT 4
#define EXIT_EVENT 5
#define UNASSIGN_EVENT 6
#define WIN_EVENT 7
#define LOSE_EVENT 8

#include <Serializable.h>
#include <rapidxml.hpp>

class Event : public Serializable {
protected:
    int eventType;

public:
    Event(int eventType);
    ~Event();

	virtual std::string Serialize() = 0;  
    virtual void Deserialize(char * serializeStr) = 0;
    virtual void Deserialize(rapidxml::xml_node<> * root) = 0;
    static Event * createEvent(char * serializeStr, size_t length);
    static Event * createEvent(rapidxml::xml_node<> * root);

    int getEventType();
    void setEventType(int);

    // specifies what type of event this is
    virtual bool isMovement();
    virtual bool isFire();
    virtual bool isDelete();
    virtual bool isAssign();
    virtual bool isEnter();
    virtual bool isExit();
    virtual bool isUnassign();
    virtual bool isWin();
    virtual bool isLose();
};

#endif