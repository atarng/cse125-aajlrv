#pragma once

#ifndef _WIN_H_
#define _WIN_H_

#include "Event.h"

class WinEvent : public Event {
private:
    int playerId;

public:
    WinEvent();
    WinEvent(int playerId);
    ~WinEvent();

	std::string Serialize();  
    void Deserialize(char * serializeStr);
    void Deserialize(rapidxml::xml_node<> * root);

    bool isWin();

    int getPlayerId();
    void setPlayerId(int);
};

#endif