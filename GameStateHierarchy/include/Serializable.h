#ifndef _SERIALIZABLE_H_
#define _SERIALIZABLE_H_

#include <string>

class Serializable
{
private:
public:
    Serializable();
    ~Serializable();

	virtual std::string Serialize()   = 0;  
    virtual void Deserialize(char *) = 0;
};

#endif