#ifndef _METEOR_H_
#define _METEOR_H_

#include "AttackObject.h"

#define METEOR_CHARGE_COST MAX_CHARGE

class Meteor : public AttackObject
{
private:
    float centerX;    
    float centerZ;        

public:
	Meteor(void) {}
	Meteor(int networkId, Unit * unitThatFired);
	~Meteor(void);

	bool Update(float deltaTime);

	virtual inline bool isMeteor(){ return true; };
};

#endif

