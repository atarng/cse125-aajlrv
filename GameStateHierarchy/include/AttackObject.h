#ifndef _ATTACK_OBJECT_H_
#define _ATTACK_OBJECT_H_

#include "Unit.h"
#include <LoadingManager.h>

#define BULLET_TYPE 1
#define POWERUP_TYPE 2
#define AOE_TYPE 3
#define BULLET2_TYPE 4
#define METEOR_TYPE 5

#ifdef _ONSERVER

#define BULLET_DAMAGE	LoadingManager::bulletDmg
#define MELEE_DAMAGE	LoadingManager::meleeDmg
#define FAN_DAMAGE		LoadingManager::fanDmg

#define BULLET2_DAMAGE	LoadingManager::bullet2Dmg
#define AOE_DAMAGE		LoadingManager::aoeDmg		// this is charge per second
#define METEOR_DAMAGE   LoadingManager::meteorDmg     // total damage over 1 second

// time in seconds
#define BULLET_MAXTIME	LoadingManager::bulletMaxTime
#define MELEE_MAXTIME	LoadingManager::meleeMaxTime
#define BULLET2_MAXTIME	LoadingManager::bullet2MaxTime
#define AOE_MAXTIME		LoadingManager::aoeMaxTime
#define METEOR_MAXTIME  LoadingManager::meteorMaxTime

#endif

class AttackObject : public Unit
{
private:
    int attackType;

protected:
    float lifeTime;	      // check timeExisted against duration
	int controllingUnitId;
	bool alreadyCollided;
    //float velX, velY, velZ;		  // velocity
    //float dirX, dirY, dirZ;		  // direction might be redundant since they should go straight, with respect to their orientation.

	//float growthRate;   // some attacks might increase in bounding area
    //float speed;        // rate at which position updates based on direction (Could be redundant as giving direction scaled values could be a better option)

public:
    AttackObject();
    AttackObject(int networkId, int attackType);
    ~AttackObject();

    int getAttackType();
    void setAttackType(int attackType);
	bool isAlreadyCollided();
	void setAlreadyCollided(bool alreadyCollided);

    virtual bool Update(float deltaTime) = 0;
	void setDuration(float duration);

	inline void setControllingUnitId(int unitId) { controllingUnitId = unitId; }
	inline int getControllingUnitId() { return controllingUnitId; }

	virtual inline bool isBullet(){ return false; };
    virtual inline bool isBullet2(){ return false; };
    virtual inline bool isPowerUp() { return false; };
	virtual inline bool isAoe(){ return false; }
    virtual inline bool isMeteor(){ return false; }
    

	int damage;
};

#endif