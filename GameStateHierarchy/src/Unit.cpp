/***
 * Filename: Unit.cpp
 * Contributors: Anthony Chen, Vincent Chen, Luke Liu, Alfred Tarng, Joey Ly
 * Purpose: Serializable package containing relevant information to the client
 *          for rendering, including positional information and health (pending
 *          discussions as health is a property not needed for all objects)
 *
 * TODO: Find a way to make this as small as possible.
 *       
 */

#include <Unit.h>
#include <iostream>

#include "rapidxml_print.hpp"

#define BASE_10 10
#define BUF_SIZE 1024

//Definitions for attribute names in xml
#define UNIT_ID "unitId"
#define POSX "posX"
#define POSY "posY"
#define POSZ "posZ"
#define YAW "yaw"
#define PITCH "pitch"
#define ROLL "roll"
#define SCALEX "scaleX"
#define SCALEY "scaleY"
#define SCALEZ "scaleZ"
#define BOUNDING_RADIUS "boundingRad"
#define HEALTH "health"
#define MAXHEALTH "maxHealth"
#define MODEL_ID "modelID"
#define SHADER_ID "shaderID"
#define CON_PLAYER_ID "controllingPlayerId"
#define COLOR_R "r"
#define COLOR_G "g"
#define COLOR_B "b"
#define UNIT_CHARGE "unitCharge"
#define ANIM_TYPE "animType"

using namespace rapidxml;

Unit::Unit()
{
    setUnitId(0);

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;

    boundingRadius = 8;
    health = 0;

    modelID = shaderID = 0;

    setColor(1.f, 1.f, 1.f);

	setModified(true);
    
    setControllingPlayerId(NEUTRAL_UNIT);
    setCurrentlyControlled(false);
    setCurrentAnimType(ANIM_DEFAULT);

    attackCoolDown = 0;
	lastPerformedAction = 0;
}
Unit::Unit(Unit & cloneUnit)
{
    this->posX = cloneUnit.posX;
    this->posY = cloneUnit.posY;
    this->posZ = cloneUnit.posZ;

    
    this->pitch = cloneUnit.pitch;
    this->yaw = cloneUnit.yaw;
    this->roll = cloneUnit.roll;

    this->scaleX = cloneUnit.scaleX;
    this->scaleY = cloneUnit.scaleY;
    this->scaleZ = cloneUnit.scaleZ;

	this->modified = cloneUnit.modified;

    setControllingPlayerId(cloneUnit.getControllingPlayerId());
    setCurrentlyControlled(cloneUnit.isCurrentlyControlled());
    setCurrentAnimType(cloneUnit.getCurrentAnimType());

    attackCoolDown = 0;
}
Unit::Unit(int networkingID)
{ // constructor with for the networking ID.
    this->unitNetworkId = networkingID;

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;

    boundingRadius = 0;
    health = 0;

    modelID = shaderID = 0;

    attackCoolDown = 0;
         
    setModified(true);

    setControllingPlayerId(NEUTRAL_UNIT);
    setCurrentlyControlled(false);
    setCurrentAnimType(ANIM_DEFAULT);
}
Unit::~Unit()
{
}

/***
 * This function still has to be tested, however hypothetically it will copy the
 * updated values from a clone to this unit object.
 */
void Unit::cloneValues(Unit * cloneMe)
{
    memcpy ( this, cloneMe, sizeof(Unit));
}

bool Unit::isRobot(){ return false; }
bool Unit::isDoomRobot(){ return false; }
bool Unit::isSpark(){ return false; }

void Unit::GetPosition (float& x, float& y, float& z)
{
        x = posX;
        y = posY;
        z = posZ;
}
void Unit::GetRotation (float& retPitch, float& retYaw, float& retRoll)
{
        retPitch = pitch;
        retYaw = yaw;
        retRoll = roll;
}
void Unit::GetScale    (float& x, float& y, float& z)
{
        x = scaleX;
        y = scaleY;
        z = scaleZ;
}

void Unit::GetIDs      (int& model, int& shader)
{
    model  = modelID;
    shader = shaderID;
}
void Unit::GetRadius    (float& radius)
{
    radius = boundingRadius;
}

//// ONLY SERVER SHOULD CALL THESE /////////////////////////////////////
void Unit::SetPosition (float x, float y, float z)
{
    posX = x;
    posY = y;
    posZ = z;
    setModified(true);
}
void Unit::SetRotation (float inPitch, float inYaw, float inRoll)
{
    pitch = inPitch;
    yaw   = inYaw;
    roll  = inRoll;
    setModified(true);
}
void Unit::SetScale (float x, float y, float z)
{
    scaleX = x;
    scaleY = y;
    scaleZ = z;
    setModified(true);
}
void Unit::SetIDs (int in_Model, int in_Shader)
{
    modelID  =  in_Model;
    shaderID =  in_Shader;
    setModified(true);
}

void Unit::SetRadius (float radius)
{
	boundingRadius = radius;
    setModified(true);
}

void Unit::SetMoveSpeed(float _spd){
    moveSpeed = _spd;
}

/**************Getter and setter functions***************/
int Unit::getUnitId() {
    return this->unitNetworkId;
}

float Unit::getPosX() {
	return this->posX;
}
float Unit::getPosY() {
	return this->posY;
}
float Unit::getPosZ() {
    return this->posZ;
}

float Unit::getYaw() {
    return this->yaw;
}
float Unit::getPitch() {
    return this->pitch;
}
float Unit::getRoll() {
    return this->roll;
}

float Unit::getScaleX() {
    return this->scaleX;
}
float Unit::getScaleY() {
    return this->scaleY;
}
float Unit::getScaleZ() {
    return this->scaleZ;
}

float Unit::getBoundingRadius() {
    return this->boundingRadius;
}
float Unit::getHealth() {
    return this->health;
}
float Unit::getMaxHealth() {
	return this->maxHealth;
}
int Unit::getModelID() {
	return this->modelID;
}
int Unit::getShaderID() {
	return this->shaderID;
}

int Unit::getControllingPlayerId() {
    return controllingPlayerId;
}

float Unit::getColorR() {
    return r;
}

float Unit::getColorG() {
    return g;
}

float Unit::getColorB() {
    return b;
}

bool Unit::isModified() {
	return this->modified;
}

bool Unit::isCurrentlyControlled() {
    return currentlyControlled;
}

float Unit::getCurrentCharge() {
    return currentCharge;
}

int Unit::getCurrentAnimType() {
    return currentAnimType;
}

bool Unit::hasPerformedAction() {
    return performedAction;
}

void Unit::setUnitId(int unitNetworkId) {
	this->unitNetworkId = unitNetworkId;
    setModified(true);
}

void Unit::setPosX(float posX) {
	this->posX = posX;
    setModified(true);
}
void Unit::setPosY(float posY) {
	this->posY = posY;
    setModified(true);
}
void Unit::setPosZ(float posZ) {
	this->posZ = posZ;
    setModified(true);
}

void Unit::setYaw(float yaw) {
	this->yaw = yaw;
    setModified(true);
}
void Unit::setPitch(float pitch) {
	this->pitch = pitch;
    setModified(true);
}
void Unit::setRoll(float roll) {
	this->roll = roll;
    setModified(true);
}

void Unit::setScaleX(float scaleX) {
	this->scaleX = scaleX;
    setModified(true);
}
void Unit::setScaleY(float scaleY) {
	this->scaleY = scaleY;
    setModified(true);
}
void Unit::setScaleZ(float scaleZ) { 
    this->scaleZ = scaleZ;
    setModified(true);
}

void Unit::setBoundingRadius(float boundingRadius) {
	this->boundingRadius = boundingRadius;
    setModified(true);
}
void Unit::setHealth(float health) {
#ifdef _ONSERVER
    switch(modelID) {
    case PROBE_MODEL_ID:
        maxHealth = PROBE_HEALTH;
        break;
    case SENTRY_MODEL_ID:
        maxHealth = SENTRY_HEALTH;
        break;
    case FANBOT_MODEL_ID:
        maxHealth = FAN_HEALTH;
        break;
    case SPARK_MODEL_ID:
        maxHealth = SPARK_HEALTH;
        break;
    default:
        maxHealth = 100;
    }
#endif

    if (health < 1)
        this->health = 0;
    else if (health <= maxHealth)
	{
		this->health = health;
	}

    setModified(true);
}
void Unit::setMaxHealth(float maxHealth) {
	this->maxHealth = maxHealth;
}

bool Unit::decrementHealth(int healthLoss) {
	this->health -= healthLoss;
    setModified(true);
	return (health <= 0); 
}

void Unit::setModelID(int modelID) {
	this->modelID = modelID;
    setModified(true);
}

void Unit::setShaderID(int shaderID) {
	this->shaderID = shaderID;
    setModified(true);
}

void Unit::setControllingPlayerId(int controllingPlayerId) {
    this->controllingPlayerId = controllingPlayerId;
    setModified(true);
}

void Unit::setColorR(float r) {
    this->r = r;
    setModified(true);
}

void Unit::setColorG(float g) { 
	this->g = g; 
    setModified(true);
}

void Unit::setColorB(float b) {
	this->b = b; 
    setModified(true);
}

void Unit::setColor(float r, float g, float b) {
    setColorR(r);
    setColorG(g);
    setColorB(b);
    setModified(true);
}

void Unit::setModified(bool modified ) {
	this->modified = modified;
}

void Unit::setCurrentlyControlled(bool currentlyControlled) {
    this->currentlyControlled = currentlyControlled;
    setModified(true);
}

void Unit::setCurrentCharge(float currentCharge) {
    if (currentCharge > MAX_CHARGE)
        this->currentCharge = MAX_CHARGE;
    else if (currentCharge < 0)
        this->currentCharge = 0;
    else
        this->currentCharge = currentCharge;

    setModified(true);
}

void Unit::incrementCharge(float deltaTime) {
	setCurrentCharge(currentCharge + (chargeValue * deltaTime));
}

void Unit::decrementCharge(float deltaTime) {
#ifdef _ONSERVER
	setCurrentCharge(currentCharge - (DISCHARGE * deltaTime));
#endif
}

void Unit::setCurrentAnimType(int animType) {
    if (animType < 0 || animType > 3)
        this->currentAnimType = ANIM_DEFAULT;
    else
        this->currentAnimType = animType;

    setModified(true);
}

void Unit::setCurrentAnimType(int animType, float deltaTime) {
    if (animType < 0 || animType > 2) {
        elapsedTime += deltaTime;

        if (elapsedTime > 2.f) {
            this->currentAnimType = ANIM_DEFAULT;
            elapsedTime = 0.f;
        }
    } else {
        this->currentAnimType = animType;

        if (animType == ANIM_ATTACK)
            elapsedTime = 0.f;
    }

    setModified(true);
}

void Unit::setPerformedAction(bool performedAction, float deltaTime)
{
	//this->performedAction = performedAction;
	
	if (lastPerformedAction == 0 || deltaTime == -1)
	{
		this->performedAction = performedAction;
		lastPerformedAction   = .5f;
	}else
	{
		lastPerformedAction = (lastPerformedAction - deltaTime > 0) ?  (lastPerformedAction - deltaTime) : 0;
	}
	
}

std::string Unit::Serialize(){
	xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, UNIT);
	doc.append_node(root);

    // copy field id
    char idStr[BUF_SIZE];
    _itoa_s(this->getUnitId(), idStr, BUF_SIZE, BASE_10);
	xml_node<> * id = doc.allocate_node(node_element, UNIT_ID, idStr);
	root->append_node(id);

    // copy over the unit's position
    char posXStr[BUF_SIZE];
    sprintf_s(posXStr, BUF_SIZE, "%f", this->getPosX());
	xml_node<> * posX = doc.allocate_node(node_element, POSX, posXStr);
	root->append_node(posX);

    char posYStr[BUF_SIZE];
    sprintf_s(posYStr, BUF_SIZE, "%f", this->getPosY());
	xml_node<> * posY = doc.allocate_node(node_element, POSY, posYStr);
	root->append_node(posY);

    char posZStr[BUF_SIZE];
    sprintf_s(posZStr, BUF_SIZE, "%f", this->getPosZ());
	xml_node<> * posZ = doc.allocate_node(node_element, POSZ, posZStr);
	root->append_node(posZ);

    // copy over orientation 
    char pitchStr[BUF_SIZE];
    sprintf_s(pitchStr, BUF_SIZE, "%f", this->getPitch());
	xml_node<> * pitch = doc.allocate_node(node_element, PITCH, pitchStr);
	root->append_node(pitch);

    char yawStr[BUF_SIZE];
    sprintf_s(yawStr, BUF_SIZE, "%f", this->getYaw());
	xml_node<> * yaw = doc.allocate_node(node_element, YAW, yawStr);
	root->append_node(yaw);

    char rollStr[BUF_SIZE];
    sprintf_s(rollStr, BUF_SIZE, "%f", this->getRoll());
	xml_node<> * roll = doc.allocate_node(node_element, ROLL, rollStr);
	root->append_node(roll);

    // copy over unit scaling information
    char scaleXStr[BUF_SIZE];
    sprintf_s(scaleXStr, BUF_SIZE, "%f", this->getScaleX());
	xml_node<> * scaleX = doc.allocate_node(node_element, SCALEX, scaleXStr);
	root->append_node(scaleX);

    char scaleYStr[BUF_SIZE];
    sprintf_s(scaleYStr, BUF_SIZE, "%f", this->getScaleY());
	xml_node<> * scaleY = doc.allocate_node(node_element, SCALEY, scaleYStr);
	root->append_node(scaleY);

    char scaleZStr[BUF_SIZE];
    sprintf_s(scaleZStr, BUF_SIZE, "%f", this->getScaleZ());
	xml_node<> * scaleZ = doc.allocate_node(node_element, SCALEZ, scaleZStr);
	root->append_node(scaleZ);

    // copy over unit's bounding radius
    char boundRadStr[BUF_SIZE];
    sprintf_s(boundRadStr, BUF_SIZE, "%f", this->getBoundingRadius());
	xml_node<> * boundingRad = doc.allocate_node(node_element, BOUNDING_RADIUS, boundRadStr);
	root->append_node(boundingRad);

	// copy over unit's model id
    char modelIdStr[BUF_SIZE];
    _itoa_s(this->getModelID(), modelIdStr, BUF_SIZE, BASE_10);
	xml_node<> * modelId = doc.allocate_node(node_element, MODEL_ID, modelIdStr);
	root->append_node(modelId);

    // copy over unit's health
    char healthStr[BUF_SIZE];
    sprintf_s(healthStr, BUF_SIZE, "%f", this->getHealth());
	xml_node<> * health = doc.allocate_node(node_element, HEALTH, healthStr);
	root->append_node(health);

	// copy over unit's max health
    char maxHealthStr[BUF_SIZE];
    sprintf_s(maxHealthStr, BUF_SIZE, "%f", this->getMaxHealth());
	xml_node<> * maxhealth = doc.allocate_node(node_element, MAXHEALTH, maxHealthStr);
	root->append_node(maxhealth);

	// copy over unit's shader id
    char shaderIdStr[BUF_SIZE];
    _itoa_s(this->getShaderID(), shaderIdStr, BUF_SIZE, BASE_10);
	xml_node<> * shaderId = doc.allocate_node(node_element, SHADER_ID, shaderIdStr);
	root->append_node(shaderId);

    // copy over controlling player id
    char controllingPlayerIdStr[BUF_SIZE];
    _itoa_s(this->getControllingPlayerId(), controllingPlayerIdStr, BUF_SIZE, BASE_10);
	xml_node<> * playerIdNode = doc.allocate_node(node_element, CON_PLAYER_ID, controllingPlayerIdStr);
	root->append_node(playerIdNode);

    // copy over unit's color
    char colorRStr[BUF_SIZE];
    //_itoa_s(this->getColorR(), colorRStr, BUF_SIZE, BASE_10);
    sprintf_s(colorRStr, BUF_SIZE, "%f", this->getColorR());
	xml_node<> * colorRNode = doc.allocate_node(node_element, COLOR_R, colorRStr);
	root->append_node(colorRNode);

    char colorGStr[BUF_SIZE];
    //_itoa_s(this->getColorG(), colorGStr, BUF_SIZE, BASE_10);
    sprintf_s(colorGStr, BUF_SIZE, "%f", this->getColorG());
	xml_node<> * colorGNode = doc.allocate_node(node_element, COLOR_G, colorGStr);
	root->append_node(colorGNode);

    char colorBStr[BUF_SIZE];
    //_itoa_s(this->getColorB(), colorBStr, BUF_SIZE, BASE_10);
    sprintf_s(colorBStr, BUF_SIZE, "%f", this->getColorB());
	xml_node<> * colorBNode = doc.allocate_node(node_element, COLOR_B, colorBStr);
	root->append_node(colorBNode);

    // copy over unit charge
    char unitChargeStr[BUF_SIZE];
    sprintf_s(unitChargeStr, BUF_SIZE, "%f", this->getCurrentCharge());
	xml_node<> * unitChargeNode = doc.allocate_node(node_element, UNIT_CHARGE, unitChargeStr);
	root->append_node(unitChargeNode);

    // copy over unit animation 
    char animTypeStr[BUF_SIZE];
    _itoa_s(this->getCurrentAnimType(), animTypeStr, BUF_SIZE, BASE_10);
    xml_node<> * animTypeNode = doc.allocate_node(node_element, ANIM_TYPE, animTypeStr);
	root->append_node(animTypeNode);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}
void Unit::Deserialize(char * serializeStr){
	xml_document<> doc;

	doc.parse<0>(serializeStr);

    xml_node<> * root = doc.first_node(UNIT);

    Deserialize(root);
}

void Unit::Deserialize(xml_node<> * root){

    //copy over id
	xml_node<> * idNode = root->first_node(UNIT_ID);
	int unitId = atoi(idNode->value());
	this->setUnitId(unitId);
	
	//copy over the unit's position
	xml_node<> * posXNode = root->first_node(POSX);
    float posX = (float) atof (posXNode ->value());
	xml_node<> * posYNode = root->first_node(POSY);
    float posY = (float) atof (posYNode ->value());
	xml_node<> * posZNode = root->first_node(POSZ);
    float posZ = (float) atof (posZNode ->value());
    this->SetPosition(posX,posY,posZ);

	//copy over orientation
    xml_node<> * pitchNode = root->first_node(PITCH);
	float pitch = (float) atof (pitchNode->value());
	xml_node<> * yawNode = root->first_node(YAW);
	float yaw = (float) atof (yawNode->value());
	xml_node<> * rollNode = root->first_node(ROLL);
	float roll = (float) atof (rollNode->value());
    this->SetRotation(pitch,yaw,roll);

	// copy over unit scaling information
	xml_node<> * scaleXNode = root->first_node(SCALEX);
	float scaleX = (float) atof (scaleXNode->value());
	xml_node<> * scaleYNode = root->first_node(SCALEY);
	float scaleY = (float) atof (scaleYNode->value());
	xml_node<> * scaleZNode = root->first_node(SCALEZ);
	float scaleZ = (float) atof (scaleZNode->value());
    this->SetScale(scaleX,scaleY, scaleZ);

	//copy over bounding radius
	xml_node<> * radNode = root->first_node(BOUNDING_RADIUS);
	float rad = (float) atof (radNode->value());
	this->setBoundingRadius(rad);

	//copy over model id
	xml_node<> * modelIdNode = root->first_node(MODEL_ID);
	int modelId = atoi (modelIdNode->value());
	this->setModelID(modelId);
	
	//copy over max health
	xml_node<> * maxHealthNode = root->first_node(MAXHEALTH);
	float maxHealth = (float) atof (maxHealthNode->value());
	this->setMaxHealth(maxHealth);

	//copy over health
	xml_node<> * healthNode = root->first_node(HEALTH);
	float health = (float) atof (healthNode->value());
	this->setHealth(health);

	// copy over shader id
	xml_node<> * shaderIdNode = root->first_node(SHADER_ID);
	int shaderId = atoi (shaderIdNode->value());
	this->setShaderID(shaderId);

    // copy over controlling player id
	xml_node<> * playerIdNode = root->first_node(CON_PLAYER_ID);
	int playerId = atoi(playerIdNode->value());
	this->setControllingPlayerId(playerId);

    // copy over unit color
	xml_node<> * colorRNode = root->first_node(COLOR_R);
	float colorR = (float) atof (colorRNode->value());
	xml_node<> * colorGNode = root->first_node(COLOR_G);
	float colorG = (float) atof (colorGNode->value());
	xml_node<> * colorBNode = root->first_node(COLOR_B);
	float colorB = (float) atof (colorBNode->value());
    this->setColor(colorR, colorG, colorB);

    // copy over unit charge
    xml_node<> * unitChargeNode = root->first_node(UNIT_CHARGE);
	float unitCharge = (float) atof (unitChargeNode->value());
    this->setCurrentCharge(unitCharge);

    // copy over unit animation
    xml_node<> * unitAnimNode = root->first_node(ANIM_TYPE);
	int unitAnim = atoi(unitAnimNode->value());
	this->setCurrentAnimType(unitAnim);
}

Unit Unit::asUnit()
{
    Unit retUnit = Unit();
    retUnit.SetPosition(posX,posY,posZ);
    retUnit.SetRotation(pitch,yaw ,roll);
    retUnit.SetScale(scaleX,scaleY,scaleZ);
    retUnit.setBoundingRadius(boundingRadius);

    retUnit.setUnitId(unitNetworkId);
    retUnit.setControllingPlayerId(controllingPlayerId);

    retUnit.SetIDs(modelID,shaderID);

    retUnit.setColor(r,g,b);

    retUnit.setModified(modified);

    return retUnit;
}