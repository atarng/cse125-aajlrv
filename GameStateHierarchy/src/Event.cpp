#include "Event.h"
#include "FireEvent.h"
#include "MovementEvent.h"
#include "DeleteEvent.h"
#include "AssignEvent.h"
#include "EnterEvent.h"
#include "ExitEvent.h"
#include "UnassignEvent.h"
#include "WinEvent.h"
#include "LoseEvent.h"

#include <rapidxml_print.hpp>

#include <iostream>

using namespace rapidxml;

/***
 * What sort of events are these? Keyboard/Mouse Inputs?
 */
Event::Event(int eventType) {
    this->eventType = eventType;
}
Event::~Event() {

}

int Event::getEventType() {
    return eventType;
}

void Event::setEventType(int eventType) {
    this->eventType = eventType;
}

Event * Event::createEvent(char * serializeStr, size_t length) {
    char * copy = new char[length + 1];
    strncpy_s(copy, length + 1, serializeStr, length);
    copy[length] = '\0';

    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    return createEvent(root);
}

Event * Event::createEvent(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * typeNode = root->first_node(EVENT_TYPE);
	int eventType = atoi(typeNode->value());

    switch (eventType) {
        case MOVEMENT_EVENT: {
            MovementEvent * moveEvent = new MovementEvent();
            moveEvent->Deserialize(root);
            
            return moveEvent;
        } case FIRE_EVENT: {
            FireEvent * fireEvent = new FireEvent();
            fireEvent->Deserialize(root);

            return fireEvent;
        } case DELETE_EVENT: {
            DeleteEvent * deleteEvent = new DeleteEvent();
            deleteEvent->Deserialize(root);

            return deleteEvent;
        } case ASSIGN_EVENT: {
            AssignEvent * assignEvent = new AssignEvent();
            assignEvent->Deserialize(root);

            return assignEvent;
        } case ENTER_EVENT: {
            EnterEvent * eventEnter = new EnterEvent();
            eventEnter->Deserialize(root);

            return eventEnter;
        } case EXIT_EVENT: {
            ExitEvent * exitEvent = new ExitEvent();
            exitEvent->Deserialize(root);

            return exitEvent;
        } case UNASSIGN_EVENT: {
            UnassignEvent * unassignEvent = new UnassignEvent();
            unassignEvent->Deserialize(root);

            return unassignEvent;
        } case WIN_EVENT: {
            WinEvent * winEvent = new WinEvent();
            winEvent->Deserialize(root);

            return winEvent;
        } case LOSE_EVENT: {
            LoseEvent * loseEvent = new LoseEvent();
            loseEvent->Deserialize(root);

            return loseEvent;
        }
   }

    return NULL;
}

bool Event::isMovement() {
    return false;
}

bool Event::isFire() {
    return false;
}

bool Event::isDelete() {
    return false;
}

bool Event::isAssign() {
    return false;
}

bool Event::isEnter() {
    return false;
}

bool Event::isExit() {
    return false;
}

bool Event::isUnassign() {
    return false;
}

bool Event::isWin() {
    return false;
}

bool Event::isLose() {
    return false;
}