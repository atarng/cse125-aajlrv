/***
 * Robot.cpp
 * Contributor(s): Alfred Tarng, Luke Liu
 * Purpose:
 *
 * TODO: Fix hardcoding of model/shader ids.
 *       
 */

#include <Robot.h>
#include <iostream>

Robot::Robot()
{
    unitNetworkId = 0; //UnitManagerClient::Instance()->getAvailableID();

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 0;

    boundingRadius = 5;
	maxHealth = 0;
    health = maxHealth;
	currentCharge = 0;

    heightOffset = 0;
}
Robot::Robot(int networkingID)
{ // constructor with for the networking ID.
    //super::Unit(networkingID);
    currentOwner = NULL;

    this->unitNetworkId = networkingID;

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;

    boundingRadius = 5;

    heightOffset = 0;

    setCurrentlyControlled(false);

#ifdef _ONSERVER
    // set the unit's model and shader; hard coded for now
    // 1,2,4, (8::0)
	health = ROBOT_HEALTH;
    maxHealth = ROBOT_HEALTH;
    int setModelID = 1;
    int switchFactor = networkingID % 10;

    if (switchFactor < 5) {
        setModelID = 2;
        health = SENTRY_HEALTH;
        maxHealth = SENTRY_HEALTH;
		chargeValue = SENTRY_CHARGE;
        SetMoveSpeed( SENTRY_SPEED ) ;
    } else if (switchFactor < 9) {
        setModelID = 1;
        health = PROBE_HEALTH;
        maxHealth = PROBE_HEALTH;
		chargeValue = PROBE_CHARGE;
        SetMoveSpeed( PROBE_SPEED ) ;
    } else {
        setModelID = 4;
        health = FAN_HEALTH;
        maxHealth = FAN_HEALTH;
		chargeValue = FANBOT_CHARGE;
        SetMoveSpeed( FANBOT_SPEED ) ;
    }

    SetIDs(setModelID, 4);
#endif
}
Robot::Robot(const Robot& cloneRobot)
{
}
Robot::~Robot()
{
}

// Might not be needed?
void Robot::cloneValues(Unit * cloneMe)
{
    memcpy ( this, cloneMe, sizeof(Unit));
}

PlayerUnit * Robot::getPlayerUnit()
{
    return currentOwner;
}

void Robot::setPlayerUnit(PlayerUnit& playerUnit)
{
    currentOwner = &playerUnit;
}
void Robot::setHeightOffset(float newHeightOffset)
{
    this->heightOffset = newHeightOffset;
}

// called every loop to perform automated behavior.
void Robot::AI_PerformBehavior()
{
	// check if still has charge

	// move if can't reach any targets

	// attack closest target

	// decrement the charge
}

bool Robot::isRobot() { 
    return true; 
}