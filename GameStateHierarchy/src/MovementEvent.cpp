#include <MovementEvent.h>

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

// field names
#define UNIT_ID "unitId"
#define DELTA_X "deltaX"
#define DELTA_Y "deltaY"
#define DELTA_Z "deltaZ"
#define DELTA_YAW "deltaYaw"
#define DELTA_PITCH "deltaPitch"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

MovementEvent::MovementEvent() : Event(MOVEMENT_EVENT) {
    setUnitId(0);
    setDeltaX(0);
    setDeltaY(0);
    setDeltaZ(0);
    setDeltaYaw(0);
    setDeltaPitch(0);    
}
MovementEvent::MovementEvent(int unitId, float deltaX, float deltaY, float deltaZ, float deltaYaw, float deltaPitch) 
    : Event(MOVEMENT_EVENT) {
    setUnitId(unitId);
    setDeltaX(deltaX);
    setDeltaY(deltaY);
    setDeltaZ(deltaZ);
    setDeltaYaw(deltaYaw);
    setDeltaPitch(deltaPitch);    
}
MovementEvent::~MovementEvent() {

}

std::string MovementEvent::Serialize() {
    xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, EVENT);
	doc.append_node(root);

    // copy over event type
    char eventTypeStr[BUF_SIZE];
    _itoa_s(this->getEventType(), eventTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * eventType = doc.allocate_node(node_element, EVENT_TYPE, eventTypeStr);
	root->append_node(eventType);

    // copy over unit id
    char unitIdStr[BUF_SIZE];
    _itoa_s(this->getUnitId(), unitIdStr, BUF_SIZE, BASE_10);
	xml_node<> * unitId = doc.allocate_node(node_element, UNIT_ID, unitIdStr);
	root->append_node(unitId);

    // copy over the unit's deltaition
    char deltaXStr[BUF_SIZE];
    sprintf_s(deltaXStr, BUF_SIZE, "%f", this->getDeltaX());
	xml_node<> * deltaX = doc.allocate_node(node_element, DELTA_X, deltaXStr);
	root->append_node(deltaX);

    char deltaYStr[BUF_SIZE];
    sprintf_s(deltaYStr, BUF_SIZE, "%f", this->getDeltaY());
	xml_node<> * deltaY = doc.allocate_node(node_element, DELTA_Y, deltaYStr);
	root->append_node(deltaY);

    char deltaZStr[BUF_SIZE];
    sprintf_s(deltaZStr, BUF_SIZE, "%f", this->getDeltaZ());
	xml_node<> * deltaZ = doc.allocate_node(node_element, DELTA_Z, deltaZStr);
	root->append_node(deltaZ);

    // copy over orientation 
    char deltaYawStr[BUF_SIZE];
    sprintf_s(deltaYawStr, BUF_SIZE, "%f", this->getDeltaYaw());
	xml_node<> * yaw = doc.allocate_node(node_element, DELTA_YAW, deltaYawStr);
	root->append_node(yaw);

    char deltaPitchStr[BUF_SIZE];
    sprintf_s(deltaPitchStr, BUF_SIZE, "%f", this->getDeltaPitch());
	xml_node<> * pitch = doc.allocate_node(node_element, DELTA_PITCH, deltaPitchStr);
	root->append_node(pitch);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}

void MovementEvent::Deserialize(char * serializeStr) {
    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    Deserialize(root);
}

void MovementEvent::Deserialize(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * typeNode = root->first_node(EVENT_TYPE);
	int eventType = atoi(typeNode->value());
	this->setEventType(eventType);

    // copy over the unit id
    xml_node<> * unitIdNode = root->first_node(UNIT_ID);
	int unitId = atoi(unitIdNode->value());
	this->setUnitId(unitId);
	
	//copy over the unit's position
	xml_node<> * deltaXNode = root->first_node(DELTA_X);
	float deltaX = (float) atof (deltaXNode->value());
	this->setDeltaX(deltaX);

	xml_node<> * deltaYNode = root->first_node(DELTA_Y);
	float deltaY = (float) atof (deltaYNode->value());
	this->setDeltaY(deltaY);

	xml_node<> * deltaZNode = root->first_node(DELTA_Z);
	float deltaZ = (float) atof (deltaZNode->value());
	this->setDeltaZ(deltaZ);

	//copy over orientation
	xml_node<> * deltaYawNode = root->first_node(DELTA_YAW);
    float yaw = (float) atof (deltaYawNode->value());
	this->setDeltaYaw(yaw);

	xml_node<> * deltaPitchNode = root->first_node(DELTA_PITCH);
    float pitch = (float) atof (deltaPitchNode->value());
	this->setDeltaPitch(pitch);

}

bool MovementEvent::isMovement() {
    return true;
}

int MovementEvent::getUnitId() {
    return unitId;
}

void MovementEvent::setUnitId(int unitId) {
    this->unitId = unitId;
}

float MovementEvent::getDeltaX() {
    return deltaX;   
}

float MovementEvent::getDeltaY() {
    return deltaY;   
}
float MovementEvent::getDeltaZ() {
    return deltaZ;   
}

float MovementEvent::getDeltaYaw() {
    return deltaYaw;   
}

float MovementEvent::getDeltaPitch() {
    return deltaPitch;   
}

void MovementEvent::setDeltaX(float deltaX) {
    this->deltaX = deltaX;   
}

void MovementEvent::setDeltaY(float deltaY) {
    this->deltaY = deltaY;   
}
void MovementEvent::setDeltaZ(float deltaZ) {
    this->deltaZ = deltaZ;   
}

void MovementEvent::setDeltaYaw(float deltaYaw) {
    this->deltaYaw = deltaYaw;   
}

void MovementEvent::setDeltaPitch(float deltaPitch) {
    this->deltaPitch = deltaPitch;
}

