#include "AttackObject.h"

AttackObject::AttackObject()
{
	//timeExisted = 0;
}
AttackObject::AttackObject(int networkId, int attackType) 
    : Unit(networkId) {
    setAttackType(attackType);
	setAlreadyCollided(false);
	//timeExisted = 0;
}
AttackObject::~AttackObject() {}

int AttackObject::getAttackType() {
    return attackType;
}

void AttackObject::setAttackType(int attackType) {
    this->attackType = attackType;
}

void AttackObject::setDuration(float duration)
{
	this->lifeTime = duration;
}

bool AttackObject::isAlreadyCollided() {
	return alreadyCollided;
}

void AttackObject::setAlreadyCollided(bool alreadyCollided) {
	this->alreadyCollided = alreadyCollided;
}