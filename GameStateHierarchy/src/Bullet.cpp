#include "Bullet.h"
#include <iostream>

using namespace std;

#include <d3dx10.h>
#include <D3DX10math.h>

Bullet::Bullet() {lifeTime = 0;}
Bullet::Bullet(int networkId) : AttackObject(networkId, BULLET_TYPE) {}
Bullet::Bullet(int networkId, Unit * unitThatFired) 
    : AttackObject(networkId, BULLET_TYPE) {

    // set bullet model
    SetIDs(9, 0);

    // set bullet position position
    setPosX(unitThatFired->getPosX());
    setPosY(unitThatFired->getPosY());
    setPosZ(unitThatFired->getPosZ());
    setPitch(unitThatFired->getPitch());
    setYaw(unitThatFired->getYaw());
    setRoll(unitThatFired->getRoll());
	SetRadius(2.0f);


    // create rotation matrix
    D3DXMATRIX rotationMatrix;
    D3DXVECTOR3 dir = D3DXVECTOR3(0,0,1);

    D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw*0.0174532925f, pitch*0.0174532925f, roll*0.0174532925f);
    D3DXVec3TransformCoord(&dir, &dir, &rotationMatrix);

    setDirX(dir.x);
    setDirY(dir.y);
    setDirZ(dir.z);

	setPosX(dir.x + posX);
    setPosY(dir.y + posY);
    setPosZ(dir.z + posZ);

	//set color
	setColor(unitThatFired->getColorR(), unitThatFired->getColorG(), unitThatFired->getColorB());

	setControllingPlayerId(unitThatFired->getControllingPlayerId());

#ifdef _ONSERVER
	switch(unitThatFired->getModelID())
	{
		case PROBE_MODEL_ID:
			lifeTime = MELEE_MAXTIME;
			damage = MELEE_DAMAGE;
			setSpeed(BULLET_SPEED/4.0f);
			SetIDs(12, 0);
			SetRadius(8);
			SetScale(boundingRadius, boundingRadius, boundingRadius);
			break;
		case FANBOT_MODEL_ID:
			lifeTime = BULLET_MAXTIME*3;
			damage = FAN_DAMAGE;
			setSpeed(FAN_BULLET_SPEED);
			SetRadius(4);
			SetScale(boundingRadius/2, boundingRadius/2, boundingRadius/2);
			break;
		default:
			lifeTime = BULLET_MAXTIME;
			damage = BULLET_DAMAGE;
			setSpeed(BULLET_SPEED);
	}
#endif
}
Bullet::~Bullet() {

}


void Bullet::setDirX(float dirX) {
    this->dirX = dirX;
}
void Bullet::setDirY(float dirY) {
    this->dirY = dirY;
}
void Bullet::setDirZ(float dirZ) {
    this->dirZ = dirZ;
}

float Bullet::getDirX() {
    return dirX;
}
float Bullet::getDirY() {
    return dirY;
}
float Bullet::getDirZ() {
    return dirZ;
}


float Bullet::getSpeed() {
    return speed;
}
void Bullet::setSpeed(float speed) {
    this->speed = speed;
}
/*
float Bullet::getDistanceLeft() {
    return distanceLeft;
}
void Bullet::setDistanceLeft(float distanceLeft) {
    this->distanceLeft = distanceLeft;
}

unsigned long Bullet::getLastUpdateTime() {
    return lastUpdateTime;
}
void Bullet::setLastUpdateTime(unsigned long lastUpdateTime) {
    this->lastUpdateTime = lastUpdateTime;
}
*/
/**
 * Updates this bullet's current position if time has elapsed between the
 * current time and the last update time.
 * @param currentTime  the current time
 * @return false if this bullet's distance left to travel is less than 1
 */
bool Bullet::Update(float deltaTime) {    

    lifeTime -= deltaTime;

    float displacement = speed * deltaTime;

    setPosX(dirX * displacement + posX);
    setPosY(dirY * displacement + posY);
    setPosZ(dirZ * displacement + posZ);

    if(posY < 5.0f)return true;
    return (lifeTime <= 0);

//    setLastUpdateTime(currentTime);

//    float newDistanceLeft = distanceLeft - displacement;
    
    /*
    if (newDistanceLeft < 1)
        return false;
    else {
        setDistanceLeft(newDistanceLeft); 
        return true;
    }*/
}
