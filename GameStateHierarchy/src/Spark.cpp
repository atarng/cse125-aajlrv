#include <Spark.h>
#include <Robot.h>

Spark::Spark()
{
    unitNetworkId = 0;

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;

    boundingRadius = -3;
    maxHealth = 0;
	health = maxHealth;

    heightOffset = 0;

    setCurrentlyControlling(false);
    setCurrentlyControlled(true);
}

Spark::Spark(int networkingID) : Unit(networkingID)
{
    boundingRadius = -3; // default no collision

    heightOffset = 0;

    scaleX = scaleY = scaleZ = 6.0;
#ifdef _ONSERVER
    maxHealth = SPARK_HEALTH;
    health = maxHealth;

    // set the unit's model and shader; hard coded for now
    SetIDs(8, 0);

	SetMoveSpeed(SPARK_SPEED);
#endif
    setCurrentlyControlling(false);
    setCurrentlyControlled(true);
}

Spark::~Spark()
{
}

void Spark::setHeightOffset(float newHeightOffset)
{
    this->heightOffset = newHeightOffset;
}

bool Spark::isSpark(){ return true; }

bool Spark::isCurrentlyControlling() {
    return currentlyControlling;
}

void Spark::setCurrentlyControlling(bool currentlyControlling) {
    this->currentlyControlling = currentlyControlling;
}