#include <DeleteEvent.h>

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

// field names
#define UNIT_ID "unitId"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

DeleteEvent::DeleteEvent() : Event(DELETE_EVENT) {
    setUnitId(0);
}

DeleteEvent::DeleteEvent(int unitId) : Event(DELETE_EVENT) {
    setUnitId(unitId);
}

DeleteEvent::~DeleteEvent() {

}

std::string DeleteEvent::Serialize() {
    xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, EVENT);
	doc.append_node(root);

    // copy over event type
    char eventTypeStr[BUF_SIZE];
    _itoa_s(this->getEventType(), eventTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * eventType = doc.allocate_node(node_element, EVENT_TYPE, eventTypeStr);
	root->append_node(eventType);

    // copy over unit id
    char unitIdStr[BUF_SIZE];
    _itoa_s(this->getUnitId(), unitIdStr, BUF_SIZE, BASE_10);
	xml_node<> * unitId = doc.allocate_node(node_element, UNIT_ID, unitIdStr);
	root->append_node(unitId);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}

void DeleteEvent::Deserialize(char * serializeStr) {
    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    Deserialize(root);
}

void DeleteEvent::Deserialize(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * typeNode = root->first_node(EVENT_TYPE);
	int eventType = atoi(typeNode->value());
	this->setEventType(eventType);

    // copy over the unit id
    xml_node<> * unitIdNode = root->first_node(UNIT_ID);
	int unitId = atoi(unitIdNode->value());
	this->setUnitId(unitId);
}

bool DeleteEvent::isDelete() {
    return true;
}

int DeleteEvent::getUnitId() {
    return unitId;
}

void DeleteEvent::setUnitId(int unitId) {
    this->unitId = unitId;
}

