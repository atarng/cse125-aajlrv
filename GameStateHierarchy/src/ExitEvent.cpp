#include <ExitEvent.h>

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

// field names
#define PLAYER_ID "playerId"
#define SPARK_ID "sparkUnitId"
#define ROBOT_ID "robotUnitId"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

ExitEvent::ExitEvent() : Event(EXIT_EVENT) {
    setPlayerId(0);
    setSparkId(0);
    setRobotId(0);
}

ExitEvent::ExitEvent(int playerId, int sparkUnitId, int robotUnitId) : Event(EXIT_EVENT) {
    setPlayerId(playerId);
    setSparkId(sparkUnitId);
    setRobotId(robotUnitId);
}

ExitEvent::~ExitEvent() {

}

std::string ExitEvent::Serialize() {
    xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, EVENT);
	doc.append_node(root);

    // copy over event type
    char eventTypeStr[BUF_SIZE];
    _itoa_s(this->getEventType(), eventTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * eventType = doc.allocate_node(node_element, EVENT_TYPE, eventTypeStr);
	root->append_node(eventType);

    // copy over player id
    char playerIdStr[BUF_SIZE];
    _itoa_s(this->getPlayerId(), playerIdStr, BUF_SIZE, BASE_10);
	xml_node<> * playerId = doc.allocate_node(node_element, PLAYER_ID, playerIdStr);
	root->append_node(playerId);

    // copy over spark id
    char sparkIdStr[BUF_SIZE];
    _itoa_s(this->getSparkId(), sparkIdStr, BUF_SIZE, BASE_10);
	xml_node<> * sparkId = doc.allocate_node(node_element, SPARK_ID, sparkIdStr);
	root->append_node(sparkId);

    // copy over robot id
    char robotIdStr[BUF_SIZE];
    _itoa_s(this->getRobotId(), robotIdStr, BUF_SIZE, BASE_10);
	xml_node<> * robotId = doc.allocate_node(node_element, ROBOT_ID, robotIdStr);
	root->append_node(robotId);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}

void ExitEvent::Deserialize(char * serializeStr) {
    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    Deserialize(root);
}

void ExitEvent::Deserialize(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * typeNode = root->first_node(EVENT_TYPE);
	int eventType = atoi(typeNode->value());
	this->setEventType(eventType);

    // copy over the player id
    xml_node<> * playerIdNode = root->first_node(PLAYER_ID);
	int playerId = atoi(playerIdNode->value());
	this->setPlayerId(playerId);

    // copy over the spark id
    xml_node<> * sparkIdNode = root->first_node(SPARK_ID);
	int sparkId = atoi(sparkIdNode->value());
	this->setSparkId(sparkId);

    // copy over the robot id
    xml_node<> * robotIdNode = root->first_node(ROBOT_ID);
	int robotId = atoi(robotIdNode->value());
	this->setRobotId(robotId);
}

bool ExitEvent::isExit() {
    return true;
}

int ExitEvent::getPlayerId() {
    return playerId;
}

void ExitEvent::setPlayerId(int playerId) {
    this->playerId = playerId;
}

int ExitEvent::getSparkId() {
    return sparkUnitId;
}

void ExitEvent::setSparkId(int sparkUnitId) {
    this->sparkUnitId = sparkUnitId;
}

int ExitEvent::getRobotId() {
    return robotUnitId;
}

void ExitEvent::setRobotId(int robotUnitId) {
    this->robotUnitId = robotUnitId;
}

