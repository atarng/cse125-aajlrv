#include <Attack.h>

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

Attack::Attack()
{
}
Attack::Attack(int attackIdentity)
{ // constructor with for the networking ID.
    this->attackIdentity = attackIdentity;

    posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;

//    boundingRadius = 0;
//    damage = 0;

    modelID = shaderID = 0;
}
Attack::~Attack()
{
}

/***
 * This function still has to be tested
 */
void Attack::cloneValues(Attack* cloneMe)
{
    memcpy ( this, cloneMe, sizeof(Attack));
}

/***
 * LINKING ISSUES. MOVED TO UNITMANAGERCLIENT.
 * Used to update the Unit in UnitManagerClient with the same network
 * unit id as the calling unit.
 */
// extern bool updateClient(Unit *);
/*
bool Unit::updateClient()
{
    bool isNewUnit;
    Unit* inClient = UnitManager::Instance()->getUnit(this, isNewUnit);
    
    inClient->cloneValues(this);
    return isNewUnit;
}
*/

void Attack::GetPosition (float& x, float& y, float& z)
{
        x = posX;
        y = posY;
        z = posZ;
}
void Attack::GetRotation (float& retYaw, float& retPitch, float& retRoll)
{
        retYaw = yaw;
        retPitch = pitch;
        retRoll = roll;
}
void Attack::GetScale    (float& x, float& y, float& z)
{
        x = scaleX;
        y = scaleY;
        z = scaleZ;
}

void Attack::GetIDs      (int& model, int& shader)
{
    model  = modelID;
    shader = shaderID;
}
void Attack::GetRadius    (float& radius)
{
//    radius = boundingRadius;
}

//// ONLY SERVER SHOULD CALL THESE /////////////////////////////////////
void Attack::SetPosition (float x, float y, float z)
{
    posX = x;
    posY = y;
    posZ = z;
}
void Attack::SetRotation (float inYaw, float inPitch, float inRoll)
{
    yaw   = inYaw;
    pitch = inPitch;
    roll  = inRoll;
}
void Attack::SetScale (float x, float y, float z)
{
    scaleX = x;
    scaleY = y;
    scaleZ = z;
}
void Attack::SetIDs (int in_Model, int in_Shader)
{
    modelID  =  in_Model;
    shaderID =  in_Shader;
}

std::string Attack::Serialize()
{
	//xml_document<> doc;

	//xml_node<> * root = doc.allocate_node(node_element, UNIT);
	//doc.append_node(root);

 //   // copy field id
 //   char idStr[BUF_SIZE];
 //   _itoa_s(this->getUnitId(), idStr, BUF_SIZE, BASE_10);
	//xml_node<> * id = doc.allocate_node(node_element, UNIT_ID, idStr);
	//root->append_node(id);

 //   // copy over the unit's position
 //   char posXStr[BUF_SIZE];
 //   sprintf_s(posXStr, BUF_SIZE, "%f", this->getPosX());
	//xml_node<> * posX = doc.allocate_node(node_element, POSX, posXStr);
	//root->append_node(posX);

 //   char posYStr[BUF_SIZE];
 //   sprintf_s(posYStr, BUF_SIZE, "%f", this->getPosY());
	//xml_node<> * posY = doc.allocate_node(node_element, POSY, posYStr);
	//root->append_node(posY);

 //   char posZStr[BUF_SIZE];
 //   sprintf_s(posZStr, BUF_SIZE, "%f", this->getPosZ());
	//xml_node<> * posZ = doc.allocate_node(node_element, POSZ, posZStr);
	//root->append_node(posZ);

 //   // copy over orientation 
 //   char yawStr[BUF_SIZE];
 //   sprintf_s(yawStr, BUF_SIZE, "%f", this->getYaw());
	//xml_node<> * yaw = doc.allocate_node(node_element, YAW, yawStr);
	//root->append_node(yaw);

 //   char pitchStr[BUF_SIZE];
 //   sprintf_s(pitchStr, BUF_SIZE, "%f", this->getPitch());
	//xml_node<> * pitch = doc.allocate_node(node_element, PITCH, pitchStr);
	//root->append_node(pitch);

 //   char rollStr[BUF_SIZE];
 //   sprintf_s(rollStr, BUF_SIZE, "%f", this->getRoll());
	//xml_node<> * roll = doc.allocate_node(node_element, ROLL, rollStr);
	//root->append_node(roll);

 //   // copy over unit scaling information
 //   char scaleXStr[BUF_SIZE];
 //   sprintf_s(scaleXStr, BUF_SIZE, "%f", this->getScaleX());
	//xml_node<> * scaleX = doc.allocate_node(node_element, SCALEX, scaleXStr);
	//root->append_node(scaleX);

 //   char scaleYStr[BUF_SIZE];
 //   sprintf_s(scaleYStr, BUF_SIZE, "%f", this->getScaleY());
	//xml_node<> * scaleY = doc.allocate_node(node_element, SCALEY, scaleYStr);
	//root->append_node(scaleY);

 //   char scaleZStr[BUF_SIZE];
 //   sprintf_s(scaleZStr, BUF_SIZE, "%f", this->getScaleZ());
	//xml_node<> * scaleZ = doc.allocate_node(node_element, SCALEZ, scaleZStr);
	//root->append_node(scaleZ);

 //   // copy over unit's bounding radius
 //   char boundRadStr[BUF_SIZE];
 //   sprintf_s(boundRadStr, BUF_SIZE, "%f", this->getBoundingRadius());
	//xml_node<> * boundingRad = doc.allocate_node(node_element, BOUNDING_RADIUS, boundRadStr);
	//root->append_node(boundingRad);

 //   // copy over unit's health
 //   char healthStr[BUF_SIZE];
 //   _itoa_s(this->getHealth(), healthStr, BUF_SIZE, BASE_10);
	//xml_node<> * health = doc.allocate_node(node_element, HEALTH, healthStr);
	//root->append_node(health);

	//// copy over unit's model id
 //   char modelIdStr[BUF_SIZE];
 //   _itoa_s(this->getModelID(), modelIdStr, BUF_SIZE, BASE_10);
	//xml_node<> * modelId = doc.allocate_node(node_element, MODEL_ID, modelIdStr);
	//root->append_node(modelId);

	//// copy over unit's shader id
 //   char shaderIdStr[BUF_SIZE];
 //   _itoa_s(this->getShaderID(), shaderIdStr, BUF_SIZE, BASE_10);
	//xml_node<> * shaderId = doc.allocate_node(node_element, SHADER_ID, shaderIdStr);
	//root->append_node(shaderId);

 //   std::string output;
 //   print(std::back_inserter(output), doc, print_no_indenting);

	//return output;
    return "";
}
void Attack::Deserialize(char * serializeStr)
{
	//xml_document<> doc;

	//doc.parse<0>(serializeStr);

	//xml_node<> * root = doc.first_node(UNIT);

	////copy over id
	//xml_node<> * idNode = root->first_node(UNIT_ID);
	//int unitId = atoi(idNode->value());
	//this->setUnitId(unitId);
	//
	////copy over the unit's position
	//xml_node<> * posXNode = root->first_node(POSX);
	//float posX = (float) atof (posXNode->value());
	//this->setPosX(posX);

	//xml_node<> * posYNode = root->first_node(POSY);
	//float posY = (float) atof (posYNode->value());
	//this->setPosY(posY);

	//xml_node<> * posZNode = root->first_node(POSZ);
	//float posZ = (float) atof (posZNode->value());
	//this->setPosZ(posZ);


	////copy over orientation
	//xml_node<> * yawNode = root->first_node(YAW);
	//float yaw = (float) atof (yawNode->value());
	//this->setYaw(yaw);

	//xml_node<> * pitchNode = root->first_node(PITCH);
	//float pitch = (float) atof (pitchNode->value());
	//this->setPitch(pitch);

	//xml_node<> * rollNode = root->first_node(ROLL);
	//float roll = (float) atof (rollNode->value());
	//this->setRoll(roll);

	//// copy over unit scaling information
	//xml_node<> * scaleXNode = root->first_node(SCALEX);
	//float scaleX = (float) atof (scaleXNode->value());
	//this->setScaleX(scaleX);

	//xml_node<> * scaleYNode = root->first_node(SCALEY);
	//float scaleY = (float) atof (scaleYNode->value());
	//this->setScaleY(scaleY);

	//xml_node<> * scaleZNode = root->first_node(SCALEZ);
	//float scaleZ = (float) atof (scaleZNode->value());
	//this->setScaleZ(scaleZ);

	////copy over bounding radius
	//xml_node<> * radNode = root->first_node(BOUNDING_RADIUS);
	//float rad = (float) atof (radNode->value());
	//this->setBoundingRadius(rad);

	////copy over health
	//xml_node<> * healthNode = root->first_node(HEALTH);
	//int health = atoi (healthNode->value());
	//this->setHealth(health);

	////copy over model id
	//xml_node<> * modelIdNode = root->first_node(MODEL_ID);
	//int modelId = atoi (modelIdNode->value());
	//this->setModelID(modelId);

	////copy over shader id
	//xml_node<> * shaderIdNode = root->first_node(SHADER_ID);
	//int shaderId = atoi (shaderIdNode->value());
	//this->setShaderID(shaderId);
}

Attack Attack::asAttack()
{
    Attack retAttack = Attack();
    retAttack.SetPosition(posX,posY,posZ);
    retAttack.SetRotation(yaw, pitch,roll);
    retAttack.SetScale(scaleX,scaleY,scaleZ);
    //retUnit.setBoundingRadius(boundingRadius);
    //retUnit.setUnitId(unitNetworkId);
    retAttack.SetIDs(modelID,shaderID);

    return retAttack;
}