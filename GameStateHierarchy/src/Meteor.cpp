#include "Meteor.h"
#include <iostream>

Meteor::Meteor(int networkId, Unit * unitThatFired) 
    : AttackObject(networkId, METEOR_TYPE) {

    // set bullet model
    SetIDs(13, 0);

    // set bullet position position
    setPosX(unitThatFired->getPosX());
    setPosY(unitThatFired->getPosY()+200);
    setPosZ(unitThatFired->getPosZ());
	SetRadius(30.0f);

    centerX = unitThatFired->getPosX();
    centerZ = unitThatFired->getPosZ();

	//set color
	setColor(unitThatFired->getColorR(), unitThatFired->getColorG(), unitThatFired->getColorB());
	SetScale(boundingRadius, boundingRadius, boundingRadius);

	setControllingPlayerId(1337);
   
#ifdef _ONSERVER
    lifeTime = METEOR_MAXTIME;
	damage = METEOR_DAMAGE;
#endif

}

Meteor::~Meteor(void){}

// Meteor attack detonates on creation
bool Meteor::Update(float deltaTime)
{	
    if(posY > 10.0f ){
        float value = sqrt(10*(posY - 10.0f));
        setPosX(centerX + posY * cos(value));
        setPosY(posY-80.0f*deltaTime);
        setPosZ(centerZ + posY * sin(value));

        if(posY < 10.0f){
            
            // start second phase
            setPosX(centerX);
            setPosY(10.0f);
            setPosZ(centerZ);
            setCurrentAnimType(ANIM_METEOR_DETONATE);
        }
    }else if (posY > 5.0f ){
        setCurrentAnimType(ANIM_DEFAULT);
        setPosY(posY-1.0f*deltaTime);            
        float derp = cos(1.570796327f*(((10.0f-posY))/5.0f));
        float rad = 80.0f *derp;
        SetRadius( rad );
        SetScale(boundingRadius, 400.0f, boundingRadius);

        //if (posY < 6.f)
        //    setCurrentAnimType(ANIM_DEFAULT);
        //SetScale(boundingRadius, 800.0f, boundingRadius);
    }else{
        return true;
    }
    

	return false;
}
