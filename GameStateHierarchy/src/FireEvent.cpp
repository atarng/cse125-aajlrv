#include <FireEvent.h>

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

// field names
#define UNIT_ID "unitId"
#define ATTACK_TYPE "attackType"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

FireEvent::FireEvent() : Event(FIRE_EVENT) {
    setUnitId(0);
    setAttackType(0);
}

FireEvent::FireEvent(int unitId, int attackType) : Event(FIRE_EVENT) {
    setUnitId(unitId);
    setAttackType(attackType);
}

FireEvent::~FireEvent() {

}

std::string FireEvent::Serialize() {
    xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, EVENT);
	doc.append_node(root);

    // copy over event type
    char eventTypeStr[BUF_SIZE];
    _itoa_s(this->getEventType(), eventTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * eventType = doc.allocate_node(node_element, EVENT_TYPE, eventTypeStr);
	root->append_node(eventType);

    // copy over unit id
    char unitIdStr[BUF_SIZE];
    _itoa_s(this->getUnitId(), unitIdStr, BUF_SIZE, BASE_10);
	xml_node<> * unitId = doc.allocate_node(node_element, UNIT_ID, unitIdStr);
	root->append_node(unitId);

    // copy over attack type
    char attackTypeStr[BUF_SIZE];
    _itoa_s(this->getAttackType(), attackTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * attackType = doc.allocate_node(node_element, ATTACK_TYPE, attackTypeStr);
	root->append_node(attackType);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}

void FireEvent::Deserialize(char * serializeStr) {
    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    Deserialize(root);
}

void FireEvent::Deserialize(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * child = root->first_node(EVENT_TYPE);
	int eventType = atoi(child->value());
	this->setEventType(eventType);

    // copy over the unit id
    child = child->next_sibling();
	int unitId = atoi(child->value());
	this->setUnitId(unitId);
	
	// copy over the attack type
    child = child->next_sibling();
	int attackType = atoi(child->value());
	this->setAttackType(attackType);
}

bool FireEvent::isFire() {
    return true;
}

int FireEvent::getUnitId() {
    return unitId;
}

void FireEvent::setUnitId(int unitId) {
    this->unitId = unitId;
}

int FireEvent::getAttackType() {
    return attackType;
}

void FireEvent::setAttackType(int attackType) {
    this->attackType = attackType;
}

