#include "UnassignEvent.h"

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

// field names
#define PLAYER_ID "playerId"
#define UNIT_ID "unitId"

#define BASE_10 10
#define BUF_SIZE 1024

using namespace rapidxml;

UnassignEvent::UnassignEvent() : Event(UNASSIGN_EVENT) {
    setPlayerId(0);
}

UnassignEvent::UnassignEvent(int playerId) : Event(UNASSIGN_EVENT) {
    setPlayerId(playerId);
}

UnassignEvent::~UnassignEvent() {

}

std::string UnassignEvent::Serialize() {
    xml_document<> doc;

	xml_node<> * root = doc.allocate_node(node_element, EVENT);
	doc.append_node(root);

    // copy over event type
    char eventTypeStr[BUF_SIZE];
    _itoa_s(this->getEventType(), eventTypeStr, BUF_SIZE, BASE_10);
	xml_node<> * eventType = doc.allocate_node(node_element, EVENT_TYPE, eventTypeStr);
	root->append_node(eventType);

    // copy over player id
    char playerIdStr[BUF_SIZE];
    _itoa_s(this->getPlayerId(), playerIdStr, BUF_SIZE, BASE_10);
	xml_node<> * playerId = doc.allocate_node(node_element, PLAYER_ID, playerIdStr);
	root->append_node(playerId);

    std::string output;
    print(std::back_inserter(output), doc, print_no_indenting);

	return output;
}

void UnassignEvent::Deserialize(char * serializeStr) {
    xml_document<> doc;

	doc.parse<0>(serializeStr);

	xml_node<> * root = doc.first_node(EVENT);
    Deserialize(root);
}

void UnassignEvent::Deserialize(xml_node<> * root) {

    // copy over the unit's type
    xml_node<> * typeNode = root->first_node(EVENT_TYPE);
	int eventType = atoi(typeNode->value());
	this->setEventType(eventType);

    // copy over the player id
    xml_node<> * playerIdNode = root->first_node(PLAYER_ID);
	int playerId = atoi(playerIdNode->value());
	this->setPlayerId(playerId);
}

bool UnassignEvent::isUnassign() {
    return true;
}

int UnassignEvent::getPlayerId() {
    return playerId;
}

void UnassignEvent::setPlayerId(int playerId) {
    this->playerId = playerId;
}

