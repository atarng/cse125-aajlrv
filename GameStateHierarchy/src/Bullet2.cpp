#include "Bullet2.h"
#include <iostream>

using namespace std;

#include <d3dx10.h>
#include <D3DX10math.h>

Bullet2::Bullet2() {lifeTime = 0;}
Bullet2::Bullet2(int networkId) : AttackObject(networkId, BULLET2_TYPE) {}
Bullet2::Bullet2(int networkId, Unit * unitThatFired) 
    : AttackObject(networkId, BULLET2_TYPE) {

    // set Bullet2 model
    SetIDs(14, 0);

    // set Bullet2 position position
    setPosX(unitThatFired->getPosX());
    setPosY(unitThatFired->getPosY());
    setPosZ(unitThatFired->getPosZ());
    setPitch(unitThatFired->getPitch());
    setYaw(unitThatFired->getYaw());
    setRoll(unitThatFired->getRoll());
	SetRadius(3.0f);
    SetScale(boundingRadius, boundingRadius, boundingRadius);


    // create rotation matrix
    D3DXMATRIX rotationMatrix;
    float randomNum = 1.0f-2.0f*((float)rand()/RAND_MAX);
    D3DXVECTOR3 dir = D3DXVECTOR3(0.2f*(randomNum),0,1);    

    D3DXVec3Normalize(&dir, &dir);

    D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw*0.0174532925f, pitch*0.0174532925f, roll*0.0174532925f);
    D3DXVec3TransformCoord(&dir, &dir, &rotationMatrix);

    setDirX(dir.x);
    setDirY(0.1f + 0.05f*(1.0f-2.0f*(float)rand()/RAND_MAX));
    setDirZ(dir.z);

	setPosX(dir.x + posX);
    setPosY(dir.y + posY);
    setPosZ(dir.z + posZ);

	//set color
	setColor(unitThatFired->getColorR(), unitThatFired->getColorG(), unitThatFired->getColorB());

	setControllingPlayerId(unitThatFired->getControllingPlayerId());

#ifdef _ONSERVER
    lifeTime = BULLET2_MAXTIME;

    setSpeed(200);
	damage = BULLET2_DAMAGE;
#endif
    /*setDistanceLeft(DISTANCE_LEFT);
    setLastUpdateTime(lastUpdateTime);*/

}
Bullet2::~Bullet2() {

}


void Bullet2::setDirX(float dirX) {
    this->dirX = dirX;
}
void Bullet2::setDirY(float dirY) {
    this->dirY = dirY;
}
void Bullet2::setDirZ(float dirZ) {
    this->dirZ = dirZ;
}

float Bullet2::getDirX() {
    return dirX;
}
float Bullet2::getDirY() {
    return dirY;
}
float Bullet2::getDirZ() {
    return dirZ;
}


float Bullet2::getSpeed() {
    return speed;
}
void Bullet2::setSpeed(float speed) {
    this->speed = speed;
}
/*
float Bullet2::getDistanceLeft() {
    return distanceLeft;
}
void Bullet2::setDistanceLeft(float distanceLeft) {
    this->distanceLeft = distanceLeft;
}

unsigned long Bullet2::getLastUpdateTime() {
    return lastUpdateTime;
}
void Bullet2::setLastUpdateTime(unsigned long lastUpdateTime) {
    this->lastUpdateTime = lastUpdateTime;
}
*/
/**
 * Updates this Bullet2's current position if time has elapsed between the
 * current time and the last update time.
 * @param currentTime  the current time
 * @return false if this Bullet2's distance left to travel is less than 1
 */
bool Bullet2::Update(float deltaTime) {    

    lifeTime -= deltaTime;

    float displacement = speed * deltaTime;

    if(posY > 10.0f){
        dirY -= 0.5f*deltaTime;  
    }else{
        posY = 10.0f;
        dirY = -dirY*1.2f;
        /*float temp;
        GetRadius(temp);
        SetRadius(min(temp*1.5f, 70.0f));
        SetScale(boundingRadius, boundingRadius, boundingRadius);
        */
    }

    setPosX(dirX * displacement + posX);
    setPosY(max(dirY * displacement + posY, 10.0f));
    setPosZ(dirZ * displacement + posZ);



    return (lifeTime <= 0);

//    setLastUpdateTime(currentTime);

//    float newDistanceLeft = distanceLeft - displacement;
    
    /*
    if (newDistanceLeft < 1)
        return false;
    else {
        setDistanceLeft(newDistanceLeft); 
        return true;
    }*/
}
