#ifndef _AOE_H_
#define _AOE_H_

#include "AttackObject.h"

#define AOE_CHARGE_COST MAX_CHARGE/3.0f

class Aoe : public AttackObject
{
private:
    float scaleFactor;

public:
	Aoe(void) {}
	Aoe(int networkId, Unit * unitThatFired);
	~Aoe(void);

	bool Update(float deltaTime);

	virtual inline bool isAoe(){ return true; };
};

#endif

