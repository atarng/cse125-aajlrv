//FileParser.cpp

#include "FileParser.h"
#include <cstdlib>
#include <vector>
#include <iterator>

//HelperFunction
//returns the accompanying value of the first occurance of the name
// used code from http://www.webhostingtalk.com/showthread.php?t=371554
bool FileParser::FindValue(std::string &retVal, std::string &name)
{
	if(inFile.is_open())
	{
		inFile.seekg(0, std::ios::beg);
		inFile.clear();
		while(inFile.good())
		{
			std::string line;
			std::getline(inFile, line);
			if ((line.substr (0, 1) == "#") || line.empty ()) {
				continue;
			}
	
			// 2. find the index of first '=' character on the line
			std::string::size_type offset = line.find_first_of ('=');
		
			// 3. extract the key (LHS of the ini line)
			std::string key = line.substr (0, offset);
			if( name.compare(key) == 0)
			{
				// 4. extract the value (RHS of the ini line)
				retVal = line.substr (offset+1, line.length ()-(offset+1));
				return true;
			}
		
		}
		
	}
	return false;
}

//Constructors 
FileParser::FileParser(){}

FileParser::FileParser(std::string fileName)
{
	Open(fileName);
}

FileParser::~FileParser()
{
	Close();
}

void FileParser::Open(std::string fileName)
{
	inFile.open(fileName, std::ifstream::in);
}

void FileParser::Close()
{
	inFile.close();
	inFile.clear();
}


bool FileParser::ParseBool(std::string name)
{
	std::string retVal;
	if(FindValue(retVal, name))
	{
		//may need create a error case
		return !retVal.empty() && (retVal.compare("true") == 0 || atoi (retVal.c_str ()) != 0);
	}
	return false;
}

int FileParser::ParseInt(std::string name)
{
	std::string retVal;
	if(FindValue(retVal, name))
	{
		return atoi(retVal.c_str());
	}
	return 0;
}

float FileParser::ParseFloat(std::string name)
{
	std::string retVal;
	if(FindValue(retVal, name))
	{
		return (float)atof(retVal.c_str());
	}
	return 0.0f;
}

std::string FileParser::ParseString(std::string name)
{
	std::string retVal;
	if(FindValue(retVal, name))
	{
		return retVal;
	}
	return "fail";
}