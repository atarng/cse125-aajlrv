#ifndef _GAME_LOGIC_STRUCTURE_
#define _GAME_LOGIC_STRUCTURE_

#include <winsock2.h>
#include <map>
#include <vector>
#include "Server.h"
#include "sphere.h"
#include "timer.h"

using namespace std;

enum unit_t {SPARK, ROBOT, DOOM_ROBOT};

// FORWARD DECLARATIONS
class Unit;
class AttackObject;
class Bullet;
class Spark;
class Powerup;

class Terrain;
class MovementEvent;
class FireEvent;
class EnterEvent;
class ExitEvent;
class ModelLoader;
class CollisionHandler;

class Server;

struct COLOR {
    float r,g,b;
};

const COLOR RED    = {1.f, 0.6f, 0.6f};
const COLOR GREEN  = {0.6f, 1.f, 0.6f};
const COLOR BLUE   = {0.6f, 0.6f, 1.f};
const COLOR YELLOW = {1.f, 1.f, 0.6f};
const COLOR PURPLE = {1.f, 0.6f, 1.f};
const COLOR WHITE  = {1.f, 1.f, 1.f}; // For Nuetrals

class GameLogicStructure {
private:
    struct UnitMovementWrapper
    {
        Unit* unitRef;
        float newX, newY, newZ;

        // TEMPORARY... because validate Movement is modifying actual position...
        float oldX, oldY, oldZ;
    };

    bool debug;

public:
    GameLogicStructure();
    ~GameLogicStructure();

    // map of remaining players
	static float universalDeltaTime;

    std::vector<int> playersRemaining;

    // map of unit objects
    std::map<int, int> playerToSpark;
    std::map<int, Unit *> existingUnits;
    std::map<int, UnitMovementWrapper> updatedUnits;
    std::map<int, AttackObject *> existingAttackObjects;
    
    CollisionHandler * collisionHandler;

    // map of player id to player color
    std::map<int, COLOR> playerColor;

    // lock methods
    void acquireAttackLock();
    void acquireUnitLock();
    void releaseAttackLock();
    void releaseUnitLock();
    void acquirePlayerToSparkLock();
    void releasePlayerToSparkLock();
    void acquirePlayersRemainingLock();
    void releasePlayersRemainingLock();

    void turnOnDebug();
    void turnOffDebug();
    bool isDebugOn();

    void InitializeWorld();

    void ExecuteGameLogic();

    Terrain * getGameStateWorld();
    Timer* gameTimer;    

    // unit creation
    void addDummyRobots(int number);
	void addDoomRobots(int number);
    void populateArenaWithDoom(int number);
    void populateArenaSection(int num, int diffRangeMin, int diffRangeMax, Server * server, unit_t unitType);
	void addUnitToMap(int networkId, unit_t unitType, D3DXVECTOR3 position);
    void addUnitToMap(int playerId, int networkId, unit_t unitType);

    // game logic
    void assignPlayerColor(int playerId);
    COLOR getNextColor();

    // event handlers
    void ProcessMoveEvent(MovementEvent & moveEvent);
    void ProcessFireEvent(FireEvent & fireEvent);
    void ProcessEnterEvent(EnterEvent & enterEvent);
    void ProcessExitEvent(ExitEvent & exitEvent); 

    void updateSparkFromUnit(Unit & unit);
    void updateUnitCharge(Unit * unit, const float & deltaTime);
    void updateUnitMovement(const float& deltaTime);
    void updateAttackObjects(const float& deltaTime);
    void updateSparkHealth(const float & deltaTime);

    void updateRobotAllegianceOnDelete(Unit * unit);

    void checkForWinLoseConditions(Spark * spark);

    void cleanUp();
    
private: 
    int colorCounter;
	
    float lastHealthUpdateTime;

    std::vector<COLOR> colors;

    Terrain * gameStateWorld;

	ModelLoader * modelLoader_Ptr;

    Unit * createUnit(int playerId, int networkId, unit_t unitType);

    HANDLE H_playerToSparkMutex;
    HANDLE H_existingUnitMutex;
    HANDLE H_existingAttackObjMutex;
    HANDLE H_playersRemainingMutex;

	void processUnitDamage(vector<Unit*> units, AttackObject* attack, vector<Powerup *> & newPowerups, float deltaTime);
    void processMassUnitCharge(vector<Unit*> units, AttackObject* attack, float deltaTime);                
    void processPowerup(vector<Unit*> units, AttackObject* powerup, float deltaTime);

};

#endif
