#include "StdAfx.h"
#include "DoomRobot.h"


DoomRobot::DoomRobot(void){}

DoomRobot::DoomRobot(int networkingID)
{
	this->unitNetworkId = networkingID;
	posX = posY = posZ = 0;
    yaw = pitch = roll = 0;
    scaleX = scaleY = scaleZ = 1;
	boundingRadius = 5;

	setControllingPlayerId(666);
	setCurrentCharge(MAX_CHARGE);
	setColor(0, 0, 0);

	SetIDs(PROBE_MODEL_ID, 4);
#ifdef _ONSERVER
    health = DOOM_HEALTH;
    maxHealth = DOOM_HEALTH;
	chargeValue = PROBE_CHARGE;
    SetMoveSpeed( DOOM_SPEED ) ;
#endif
}

DoomRobot::~DoomRobot(void){}

bool DoomRobot::isDoomRobot() { return true; }

void DoomRobot::setCurrentCharge(float currentCharge)
{
	this->currentCharge = MAX_CHARGE;
}

void DoomRobot::decrementCharge(float deltaTime)
{
	setCurrentCharge(MAX_CHARGE);
}