#pragma once

#ifndef _SERVER_H_
#define _SERVER_H_

#include <winsock2.h>
#include <map>
#include <vector>

#define SERVER_START_ERROR -1
#define SERVER_START_SUCCESS 1

class Unit;
class GameLogicStructure;
class Bullet;
class Timer;

class Server {

public:
    static Server * Instance();
    
    int initializeServer();

    // functions
    void Run();
    void CleanUp();

    void sendMessageToClient(std::string serializeStr);
    void sendMessageToClient(std::string serializeStr, FD_SET & currentSet);
    bool sendMessageToClient(std::string serializeStr, SOCKET clientSocket);
    bool sendPlayerIdToClient(char * message, size_t length, SOCKET clientSocket);

    int getNextPlayerId();
    int getNextUnitId(); 
    std::vector<int> availableID;

private:
    unsigned short port;
    SOCKET serverSocket;

    int playerId;
    int unitNetworkId;

    int numberOfClients;
    int requiredNumOfPlayers;

    static bool gQuitFlag;

    FD_SET masterSet; 
    GameLogicStructure * gamelogicstructure;

    static Server * serverInstance;

    Timer * animTimer;

    bool initMode;

    // server threads
    HANDLE H_thread;
    HANDLE H_InputThread;
    HANDLE H_UpdateClientThread;

    // server locks
    HANDLE H_updateUnitsLock;
    HANDLE H_fdMutex;
    HANDLE H_errorMutex;
    HANDLE H_sendLock;

    // lock methods
    void acquireSendLock();
    void releaseSendLock();
    void acquireUpdateUnitLock();
    void releaseUpdateUnitLock();
    void acquireFDLock();
    void releaseFDLock();
    void acquireErrorLock();
    void releaseErrorLock();
    
    Server(unsigned short port);
    ~Server();

    int InitializeLocks();
    int InitializeServerThreads();

    void EndServer();
    void CloseSocket(SOCKET s);
    
    SOCKET StartServerListening();

    bool checkClientMessages(FD_SET & currentSet);
    void updateInternalUnitsFromMessage(char * serializedStr, size_t length);
    void updateAttackObjects();

    bool checkSocketError(int nBytes, SOCKET clientSocket);
    void handleUnexpectedError(std::string errorMessage, int errorCode);
    void handleClientDisconnect(SOCKET clientSocket);

    static void startAcceptThread(void *);
    static void startUpdateAllClientsThread(void *);
    static void startHandleInputThread(void *);

    void AcceptThread();
    void updateAllClients();
    void HandleInput();

	//In initMode, all units wil be sent. Otherwise, only modified units will be sent.
	void sendUnits();
};

#endif