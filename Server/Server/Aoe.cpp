#include "StdAfx.h"
#include "Aoe.h"

#define AOE_SCALE_FACTOR_EXPAND 0.4f
#define AOE_RADIUS_EXPAND 1.f

Aoe::Aoe(int networkId, Unit * unitThatFired) 
    : AttackObject(networkId, AOE_TYPE) {

    // set bullet model
    SetIDs(12, 0);

    // set bullet position position
    setPosX(unitThatFired->getPosX());
    setPosY(unitThatFired->getPosY());
    setPosZ(unitThatFired->getPosZ());
	SetRadius(0.0f);


	//set color
	setColor(unitThatFired->getColorR(), unitThatFired->getColorG(), unitThatFired->getColorB());
	SetScale(boundingRadius, boundingRadius, boundingRadius);

	setControllingPlayerId(unitThatFired->getControllingPlayerId());
    
#ifdef _ONSERVER
    lifeTime = AOE_MAXTIME;
	damage = AOE_DAMAGE;
#endif
    /*
    scaleFactor = 0.5;
    SetScale(scaleFactor, scaleFactor, scaleFactor);
    */
}

Aoe::~Aoe(void){}

// Aoe attack detonates on creation
bool Aoe::Update(float deltaTime)
{
#ifdef _ONSERVER
	lifeTime -= deltaTime;

    float derp = cos(1.570796327*lifeTime/AOE_MAXTIME);
    float rad = 100.0f *derp;
    SetRadius( (rad < 50.0f )? rad: 50.0f );
    SetScale(boundingRadius, 100.0f*derp, boundingRadius);
#endif

	return (lifeTime <= 0);
}
