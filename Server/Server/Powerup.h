#pragma once

#ifndef _POWER_UP_H_
#define _POWER_UP_H_

#include "AttackObject.h"

#define POWERUP_LIFETIME 30
#define HEALING_POWERUP 1
#define CHARGING_POWERUP 2
#define DOOMBOT_POWERUP 3
#define POWERUP_GRAVITY 75.0f
#define HEALTH_GAIN 50.f
#define CHARGE_GAIN 50.f

class Powerup : public AttackObject
{
private:
	int powerValue;
	int powerUpType;
public:
	Powerup(void);
	Powerup(int networkId, int powerValue, Unit* sourceUnit, int powerUpType);
	~Powerup(void);
    
    bool Update(float deltaTime);

	virtual void applyPowerup(Unit* unit, Unit* sparkUnit);
    virtual void applyPowerup(Unit* unit);

	inline int getPowerValue() { return powerValue; }
	inline void setPowerValue(int powerValue) { this->powerValue = powerValue; }
	inline int getPowerUpType() { return powerUpType; }
	inline void setPowerUpType(int powerUpType) { this->powerUpType = powerUpType; }

    virtual inline bool isPowerUp(){ return true; };
};

#endif
