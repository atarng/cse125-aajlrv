#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <D3DX10math.h>
#include <Unit.h>
#include <vector>

using namespace std;

struct BucketIndex
{
	int row;
	int col;
    BucketIndex(){
        row = -1;
        col = -1;
    }
    BucketIndex(int _row, int _col){
        row = _row;
        col = _col;
    }
    
} ;

class Sphere
{
public:
	Sphere(void)
		: pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
		, extent(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
		, radius(0.0f)
        , currBucket(BucketIndex(-1, -1))
    {
	};
	Sphere(D3DXVECTOR3 _pos, FLOAT _radius)
		: pos(_pos)
		, extent(D3DXVECTOR3(_radius, _radius, _radius))
		, radius(_radius)
        , currBucket(BucketIndex(-1, -1))
	{
	};
	Sphere(D3DXVECTOR3 _pos, D3DXVECTOR3 _extent, FLOAT _radius)
		: pos(_pos)
		, extent(_extent)
		, radius(_radius)
        , currBucket(BucketIndex(-1, -1))
	{
	};
    Sphere(Unit* _unit){
        currUnit = _unit;
        _unit->GetRadius(radius);
        extent = D3DXVECTOR3(radius, radius, radius);
        pos = D3DXVECTOR3(_unit->getPosX(), _unit->getPosY(), _unit->getPosZ());	    	    
        currBucket = BucketIndex(-1, -1);
    }
	~Sphere(void){    	
	    currUnit = NULL;        	    
    };

	//a pretty standard sphere intersection check
	inline bool intersect(const Sphere &_other) const
	{
        if(radius < 0 || _other.radius < 0) // sphere we are checking is not active
			return false;

		D3DXVECTOR3 diff = _other.pos - pos;
		FLOAT sqDist = D3DXVec3LengthSq(&diff);
		FLOAT r = _other.radius + radius;
		if (sqDist <= r * r)
			return true;
		return false;
	}

	//The sphere is considered inactive if the radius is negative or equal to 0;
	void SetActive(){radius = ( radius > 0 )? radius:-radius; }
	void SetInactive(){radius = ( radius > 0 )? -radius:radius; }
	bool isInactive()const { return (radius<=0.0f); }
	bool isActive()const { return (radius > 0); }

	void SetPos(D3DXVECTOR3 _pos){ pos = _pos; }
	void SetRadius(FLOAT _radius){ radius = _radius; }
	D3DXVECTOR3 GetPos() const {return pos;}
	FLOAT GetRadius() const {return radius;}


	D3DXVECTOR3 extent;
	D3DXVECTOR3 pos;
	FLOAT radius;
	Unit * currUnit;
    BucketIndex currBucket;
	vector<BucketIndex> sphBuckets;
};


// PRIORITY_QUEUE: Compare Functions:
// return true if sph2 is higher priority than sph1
// higher priority means closer to the top of the priority queue
// higher priority means closer to the init location
class CompareXPos{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.x > sph2.pos.x) return true;
		return false;
	}
};
class CompareYPos{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.y > sph2.pos.y) return true;
		return false;
	}
};
class CompareZPos{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.z > sph2.pos.z) return true;
		return false;
	}
};
class CompareXNeg{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.x < sph2.pos.x) return true;
		return false;
	}
};
class CompareYNeg{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.y < sph2.pos.y) return true;
		return false;
	}
};
class CompareZNeg{
public:
	bool operator()(Sphere& sph1, Sphere& sph2)
	{
		if (sph1.pos.z < sph2.pos.z) return true;
		return false;
	}
};


#endif