////////////////////////////////////////////////////////////////////////////////
// Filename: Model.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODEL_H_
#define _MODEL_H_

//////////////
// INCLUDES //
//////////////
#include <fstream>
#include <vector>
#include <D3DX10math.h>
///////////////////////
// MY CLASS INCLUDES //
///////////////////////

using namespace std;

class Model
{
private:
	//struct VertexType // "Type"?
	//{
	//	D3DXVECTOR3 position;
	//};
    struct ModelType      // "Type"?
	{
		float x, y, z;    // position
	};
    struct MultMeshObject
    {
        ModelType* meshObj;
        int numVerts;

        D3DXMATRIX  modelRelativeMatrix;  //
        D3DXMATRIX  animationMatrix;
    };

    float minX, minY, minZ, maxX, maxY, maxZ;

    //ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
    int m_vertexCount, m_indexCount;

	ModelType* m_model;
////////////////////////////
    int numMeshes;
    char* modelName;

	bool LoadModel(char*, int);
	void ReleaseModel();
protected:

public:
	Model();
	Model(const Model&);
	~Model();

    virtual bool Initialize( char* mesh, int fileType );
	virtual void Shutdown();

	virtual int GetIndexCount();
    virtual int GetNumMeshes();

    float RetrieveRadius();

    //bool hasAnimations;
    MultMeshObject * multMeshPtr;

};
#endif