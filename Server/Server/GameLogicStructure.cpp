/***
 * GameLogicStructure.cpp (GLS)
 * Contributors: Joey Ly, Luke Liu, Anthony Chen, Vincent Chen, Alfred Tarng
 *       
 */

#include "stdafx.h"

#include "GameLogicStructure.h"
#include "CollisionHandler.h"


#include <map>
#include <iostream>
#include <vector>
#include <math.h>

#include "Terrain.h"

#include "MovementEvent.h"
#include "FireEvent.h"
#include "DeleteEvent.h"
#include "EnterEvent.h"
#include "ExitEvent.h"
#include "AssignEvent.h"
#include "UnassignEvent.h"
#include "WinEvent.h"
#include "LoseEvent.h"

#include "Unit.h"
#include "Robot.h"
#include "DoomRobot.h"
#include "Spark.h"
#include "Bullet.h"
#include "Powerup.h"
#include "Aoe.h"
#include "Bullet2.h"
#include "Meteor.h"
#include "ModelLoader.h"
#include <LoadingManager.h>

#define HEALTH_LOST_PER_SECOND LoadingManager::healthLostPerSecond
#define GRAVITY LoadingManager::gravity

using namespace std;

float GameLogicStructure::universalDeltaTime = 0;

GameLogicStructure::GameLogicStructure() {
    colorCounter = 0;

    // push all colors onto the vector of colors
    colors.push_back(RED);
    colors.push_back(GREEN);
    colors.push_back(BLUE);
    colors.push_back(YELLOW);
    colors.push_back(PURPLE);
    colors.push_back(WHITE);

    // initialize locks
    H_existingUnitMutex = CreateMutex(NULL, false, NULL);
	if (H_existingUnitMutex == NULL)
        cerr << "[ERROR] Server: Could not create lock for existing unit map!" << endl;

    H_existingAttackObjMutex = CreateMutex(NULL, false, NULL);
	if (H_existingAttackObjMutex == NULL)
        cerr << "[ERROR] Server: Could not create lock for existing attack objects map!" << endl;

    H_playerToSparkMutex = CreateMutex(NULL, false, NULL);
	if (H_playerToSparkMutex == NULL)
        cerr << "[ERROR] Server: Could not create lock for player id to spark id map!" << endl;

	H_playersRemainingMutex = CreateMutex(NULL, false, NULL);
	if (H_playersRemainingMutex == NULL)
        cerr << "[ERROR] Server: Could not create lock for player remaining vector!" << endl;

    gameTimer = new Timer();
    collisionHandler = new CollisionHandler();

    turnOffDebug();
}
GameLogicStructure::~GameLogicStructure() {

}

void GameLogicStructure::InitializeWorld()
{
    // CREATE WORLD
    bool result;
    gameStateWorld = new Terrain;
    result = gameStateWorld->Initialize("heightmap01.bmp");
    if(!result){ cerr << "[GameLogicStructure] Failed to load the terrain" << endl; }

    // Load Models
    modelLoader_Ptr = new ModelLoader;
    result = modelLoader_Ptr->Initialize();
    if(!result){ cerr << "[GameLogicStructure] Failed to load the Models" << endl; }

    addDummyRobots(LoadingManager::numRobots);
}

/***
 * Given the input states, modify the game world and update based on ai behavior.
 * Should be called from the server loop.
 * Automated Update?
 */
void GameLogicStructure::ExecuteGameLogic()
{
	float deltaTime = universalDeltaTime = gameTimer->Update();
 
    // Update movements based on Collision behavior to the units
    updateUnitMovement(deltaTime);    

    // update the positions of the attack object
    updateAttackObjects(deltaTime);

    // decrease spark health if not inside a robot
    updateSparkHealth(deltaTime);

}

Terrain* GameLogicStructure::getGameStateWorld() { 
    return gameStateWorld; 
}

/**
 * Assigns a color to the specified player.
 * @param playerId  the player id of the player
 */
void GameLogicStructure::assignPlayerColor(int playerId) {
    COLOR colorToAssign = getNextColor();
    playerColor[playerId] = colorToAssign;

    cerr << "Assigning player " << playerId << " color " << colorToAssign.r 
         << ", " << colorToAssign.g << ", " << colorToAssign.b << endl;
}

void GameLogicStructure::addDummyRobots(int num) {
    Server * server = Server::Instance();
	
    if(num < 3)
		populateArenaSection(num, 1, 5, server, ROBOT);
	else
	{
		int third = num/3;
		populateArenaSection(third, 1, 8, server, ROBOT);
		populateArenaSection(third, 9, 13, server, ROBOT);
		populateArenaSection(num - (third*2), 14, 17, server, ROBOT);
	}
}

void GameLogicStructure::addDoomRobots(int num) 
{
	Server * server = Server::Instance();
	populateArenaSection(num, 1, 10, server, ROBOT);
    populateArenaWithDoom(num);
	//populateArenaSection(num, 1, 17, server, DOOM_ROBOT);
}
void GameLogicStructure::populateArenaWithDoom(int num){
    float pi = 3.14159265f;
    Server * server = Server::Instance();        
    
    float radius = 450.0f; // Edge of arena at 580
	int inner = num/5;
	int outer = num - inner;
    float interval = 2*pi/(float)outer ;
    float currInterval = 0;
    int derp = 0;   
    for(int i = 0; i < outer; i++)
	{
        int networkId = server->getNextUnitId();
        D3DXVECTOR3 pos(640 + radius *cos(currInterval), 450.0f, 640 + radius * sin(currInterval));
        addUnitToMap(networkId, DOOM_ROBOT, pos);
        currInterval += interval;
    }
	radius = 125.0f;
	interval = 2*pi/(float)inner ;
    currInterval = 0;
	for(int i = 0; i < inner; i++)
	{
        int networkId = server->getNextUnitId();
        D3DXVECTOR3 pos(640 + radius *cos(currInterval), 450.0f, 640 + radius * sin(currInterval));
        addUnitToMap(networkId, DOOM_ROBOT, pos);
        currInterval += interval;
    }

}
void GameLogicStructure::populateArenaSection(int num, int diffRangeMin, int diffRangeMax, Server * server, unit_t unitType)
{
	float pi = 3.14f;
	map<int, vector<int>> takenPositions; // key: difflvlspace, value: list of same level spots taken

	for(int i = 0; i < num; i++)
	{
		int networkId = server->getNextUnitId();

        // (Alfred asks) can't this be an int?
		float difflvlspace = (float)(rand() % (diffRangeMax - diffRangeMin + 1) + diffRangeMin);
		difflvlspace *= 30;

		float samelvlspace = atan(30/difflvlspace);
		int spot = rand() % ((int)((2*pi)/samelvlspace));

		if(takenPositions.count((int)difflvlspace) == 0)
		{
			vector<int> currVec;
			currVec.push_back(spot);
			takenPositions[(int)difflvlspace] = currVec;

            float height = 150.0f*(float)rand()/((float) RAND_MAX);
			D3DXVECTOR3 pos(640 + difflvlspace * cos(spot*samelvlspace), height, 640 + difflvlspace * sin(spot*samelvlspace));
		    addUnitToMap(networkId, unitType, pos);
		}
		else
		{
			vector<int>* currVec = &takenPositions[(int)difflvlspace];
			bool exists = false;
			for(unsigned int index = 0; index < currVec->size(); index++)
			{
				if((*currVec)[index] == spot)
				{
					exists = true;
					break;
				}
			}
			if(exists)
			{
				i--;
				continue;
			}

			currVec->push_back(spot);
            float height = 150.0f*(float)rand()/((float) RAND_MAX);
			D3DXVECTOR3 pos(640 + difflvlspace * cos(spot*samelvlspace), height, 640 + difflvlspace * sin(spot*samelvlspace));
			addUnitToMap(networkId, unitType, pos);
		}
	}
}

/**
 * Returns the next available color.
 * @return the next available color
 */
COLOR GameLogicStructure::getNextColor() {
    if ( (unsigned int)colorCounter >= (colors.size() - 1) ) colorCounter = 0;
    return colors[colorCounter++];
}

/**
 * Adds a unit to the unit map with the specified network id and create the unit
 * base on the unit type.
 *
 *
 *
 * @param networkId  the network id for the unit
 * @param unitType the type of unit
 */
void GameLogicStructure::addUnitToMap(int networkId, unit_t unitType, D3DXVECTOR3 position)
{
    Unit * newUnit = createUnit(NEUTRAL_UNIT, networkId, unitType);

    float radius = 1;
    newUnit->SetPosition(position.x, position.y, position.z);
    //newUnit->SetPosition((float)networkId, (float)( networkId % 10 +10), 25.0f);
    newUnit->SetRotation(0.0f, (float)rand(), 0.0f);

    Sphere * newSphere = new Sphere(newUnit);

    acquireUnitLock();
	existingUnits[networkId] = newUnit;
    collisionHandler->addSphere(newSphere, networkId);
    releaseUnitLock();
}

/**
 * Adds an unit to the unit map with the specified network id and unit type.
 * @param  playerId  the player id associated with this unit
 * @param networkId  the network id for the unit
 */
void GameLogicStructure::addUnitToMap(int playerId, int networkId, unit_t unitType)
{
    map<int, COLOR>::iterator iter = playerColor.find(playerId);

    if (iter == playerColor.end()) {
        assignPlayerColor(playerId);
        iter = playerColor.find(playerId);
    }

    COLOR colorToAssign = iter->second;

    Unit  * newUnit = createUnit(playerId, networkId, unitType);

	newUnit->SetPosition(640.0f, 50.0f, 640.0f);
    printf("Bounding radius set to %f\n", newUnit->getBoundingRadius());

    Sphere* newSphere = new Sphere(newUnit);
    newUnit->setColor(colorToAssign.r, colorToAssign.g, colorToAssign.b);

    acquireUnitLock();
	existingUnits[networkId] = newUnit;
    collisionHandler->addSphere(newSphere, networkId);
    releaseUnitLock();
}

/**
 * Creates a unit based on the specified unitType and network id. If the unit created is a
 * spark, the specified player id is taken into account.
 * @param  playerId  the player id 
 * @param networkId  the network id to use for the unit
 * @param  unitType  the type of unit to create
 */
Unit * GameLogicStructure::createUnit(int playerId, int networkId, unit_t unitType) {
     Unit  * newUnit;
	
	switch (unitType) {
    case ROBOT:
        newUnit = new Robot(networkId);
        newUnit->setBoundingRadius( modelLoader_Ptr->GetModelInfo( newUnit->getModelID() ).boundingRadius );
        break;
	case DOOM_ROBOT:
		newUnit = new DoomRobot(networkId);
		newUnit->setBoundingRadius( modelLoader_Ptr->GetModelInfo( newUnit->getModelID() ).boundingRadius );
		break;
    default:
        newUnit = new Spark(networkId);
        newUnit->setControllingPlayerId(playerId);
        newUnit->setBoundingRadius( modelLoader_Ptr->GetModelInfo( newUnit->getModelID() ).boundingRadius * -1 );

        acquirePlayerToSparkLock();
        playerToSpark[playerId] = networkId;
        releasePlayerToSparkLock();
    }

    return newUnit;
}

void GameLogicStructure::turnOffDebug() {
    debug = false;
}

void GameLogicStructure::turnOnDebug() {
    debug = true;
}

bool GameLogicStructure::isDebugOn() {
    return debug;
}

/**
 * Handles a move event.
 * This should update the DESIRED position of the unit, and not the actual position.
 * Should append player to a list (Queue) to be handled (prioritized).
 * @param moveEvent  the MovementEvent object to update units from
 *
 * Contributor(s): Joey Ly, Luke Liu, Alfred Tarng [Terrain Checks].
 */
void GameLogicStructure::ProcessMoveEvent(MovementEvent & moveEvent)
{
    acquireUnitLock();

    int id = moveEvent.getUnitId();
    map<int, Unit *>::iterator iter = existingUnits.find(id);

	bool collisionDetected = false;

    if (iter != existingUnits.end()) {
		//set it to be modified
		iter->second->setModified(true);

        Unit * unit = iter -> second;
        float originalPosX = 0, originalPosY = 0, originalPosZ = 0;
        
        unit -> GetPosition( originalPosX, originalPosY, originalPosZ );		

        float newPitch = unit->getPitch() + moveEvent.getDeltaPitch();        
        newPitch = (newPitch > 80)? 80:((newPitch < -80)? -80: newPitch);

        float newYaw = unit->getYaw()   + moveEvent.getDeltaYaw();
        if(newYaw > 180) newYaw -= 360;
        if(newYaw < -180) newYaw += 360;

        unit->setPitch(newPitch);
        unit->setYaw (newYaw);

        UnitMovementWrapper umw;
        umw.unitRef = unit;
        
        umw.newX = unit->getPosX() + moveEvent.getDeltaX() * unit->GetMoveSpeed() * 0.75f;
        umw.newY = unit->getPosY() + moveEvent.getDeltaY() * unit->GetMoveSpeed() * 0.75f ;  
        umw.newZ = unit->getPosZ() + moveEvent.getDeltaZ() * unit->GetMoveSpeed() * 0.75f;

        float y ;
        getGameStateWorld()->getHeightAt(umw.newX, y, umw.newZ);
		// ... please don't make such hacky code...
        umw.newY = max(y-(unit->getBoundingRadius()/2), umw.newY);

        umw.oldX = originalPosX;
        umw.oldY = originalPosY;
        umw.oldZ = originalPosZ;
        updatedUnits.insert( pair<int,UnitMovementWrapper> ( unit->getUnitId() , umw ));

    }

    releaseUnitLock();
}

/**
 * Handles a Fire Event and generates an attack object if necessary.
 * Authors: Anthony Chen, Vincent Chen
 * @param fireEvent  the FireEvent object to process
 *
 * TODO: Create Robot Specific Attack events.
 */
void GameLogicStructure::ProcessFireEvent(FireEvent& fireEvent) {
	if (debug)
		cerr << "processfire ";
	acquireAttackLock();
	acquireUnitLock();



	map<int, Unit *>::iterator iter = existingUnits.find(fireEvent.getUnitId());
	if (iter != existingUnits.end()) {
		Unit * unit = iter->second;		
		if(unit->GetAttackCoolDown() < 0){
			switch(unit->getModelID()){
			case PROBE_MODEL_ID: // probe
				unit->SetAttackCoolDown(1.0f/10.0f);
				break;
			case SENTRY_MODEL_ID: // turret
				unit->SetAttackCoolDown(1.0f/10.0f);
				break;
			case FANBOT_MODEL_ID:
				unit->SetAttackCoolDown(1.0f/3.0f);
				break;
			}
			int networkId = Server::Instance()->getNextUnitId();

			if (unit->isRobot()) {
				int modelId = unit->getModelID();
				int attackType = fireEvent.getAttackType();
				AttackObject * attackObject = NULL;

				// fan bot
				if(unit->isCurrentlyControlled()){
					if(modelId == 4 && attackType == AOE_TYPE) {
						if (unit->getCurrentCharge() >= AOE_CHARGE_COST) {
							attackObject = new Aoe(networkId, unit);
							attackObject->setControllingUnitId(fireEvent.getUnitId());
							unit->setCurrentCharge(unit->getCurrentCharge() - AOE_CHARGE_COST);
						}
					} else if ( modelId == 2 && attackType == AOE_TYPE) {
						if (unit->getCurrentCharge() >= BULLET2_CHARGE_COST) {
							// Eat's all energy
							// initial case                        
							do {
								int networkId = Server::Instance()->getNextUnitId();

								attackObject = new Bullet2(networkId, unit);	
								attackObject->setControllingUnitId(fireEvent.getUnitId());	
								existingAttackObjects[networkId] = attackObject;

								Sphere * sph = new Sphere(attackObject);
								collisionHandler->addAttackSphere(sph, networkId);

								unit->setCurrentCharge(unit->getCurrentCharge() - BULLET2_CHARGE_COST);
							} while(unit->getCurrentCharge() > BULLET2_CHARGE_COST);

							unit->setCurrentAnimType(ANIM_ATTACK);
							unit->setPerformedAction(true, -1);
							attackObject = NULL;
						}
					} else if ( modelId == 1 && attackType == AOE_TYPE)
					{
						// METEOR!!!
						if (unit->getCurrentCharge() >= METEOR_CHARGE_COST) {
							attackObject = new Meteor(networkId, unit);
							unit->setCurrentCharge(unit->getCurrentCharge() - METEOR_CHARGE_COST);

							///*
							existingAttackObjects[networkId] = attackObject;

							Sphere * sph = new Sphere(attackObject);
							collisionHandler->addAttackSphere(sph, networkId);

							unit->setCurrentAnimType(ANIM_ATTACK);
							unit->setPerformedAction(true, -1);
							//*/
						}
					} else {
						attackObject = new Bullet(networkId, unit);
						attackObject->setControllingUnitId(fireEvent.getUnitId());
					}

				} else {
					attackObject = new Bullet(networkId, unit);
					attackObject->setControllingUnitId(fireEvent.getUnitId());
				}

				if (attackObject != NULL) {
					existingAttackObjects[networkId] = attackObject;

					Sphere * sph = new Sphere(attackObject);
					collisionHandler->addAttackSphere(sph, networkId);

					unit->setCurrentAnimType(ANIM_ATTACK);
					unit->setPerformedAction(true, -1);
				}
			}


		}
	}

    releaseUnitLock();

    if (debug)
            cerr << "processfire ";
    releaseAttackLock();
}

/***
 * Update unit if movement has been changed.
 * Contributors: Alfred
 */
void GameLogicStructure::updateUnitMovement(const float& deltaTime)
{

    acquireUnitLock();
    collisionHandler->Update();
    float terrainHeightAtXZ = 0;
    D3DXVECTOR3 newPos;
    D3DXVECTOR3 worldCenter(640.0f, 0.0f, 640.0f);
	vector<FireEvent> AIFireEvents;

    for (map<int, Unit *>::iterator iter = existingUnits.begin(); iter != existingUnits.end(); ++iter)
    {// Look through existing units to update on frame behavior.
        std::vector<Unit*> nearbyUnits;
        
        Unit * unit = iter->second;          
        int currentPlayerId = unit->getControllingPlayerId();
        unit->decrementAttackCooldown(deltaTime);
        // update unit charges
        updateUnitCharge(unit, deltaTime);
        unit -> GetPosition( newPos.x, newPos.y, newPos.z );

        /////AI MOvement
        // update direction and move
        if(!unit->isCurrentlyControlled() && currentPlayerId != -1 && unit->getPosY() < 20)
        { // only affect the non-player/non-neutral bots
            D3DXVECTOR3 newDir(unit->getPitch(), unit->getYaw(), unit->getRoll());
            collisionHandler->GetNearbyUnits(unit->getUnitId(), nearbyUnits); // get a list of the nearby units for AI movement
            bool attack_enemy = false;
            Unit* targetUnit = NULL;
            for(std::vector<Unit*>::iterator nearUnit = nearbyUnits.begin(); nearUnit < nearbyUnits.end(); nearUnit++)
            {
                
                // direct thyself to the the first enemy you notice
                if((*nearUnit)->getControllingPlayerId() != NEUTRAL_UNIT 
						&& (*nearUnit)->getControllingPlayerId() != currentPlayerId 
						&& !((*nearUnit)->isSpark())){
                            targetUnit = (*nearUnit);
                            attack_enemy = true;
                            break;                                                                      
                }              
            }                        
            if(!attack_enemy || targetUnit == NULL){
                // if no nearby enemy target, find your player and go to him
                std::map<int,int>::iterator playerIt = playerToSpark.find(unit->getControllingPlayerId());
                if(playerIt != playerToSpark.end() ){
                    std::map<int,Unit*>::iterator playerPtr = existingUnits.find(playerIt->second);
                    if(playerPtr != existingUnits.end()){
                        targetUnit = playerPtr->second;
                        
                    }
                }                                            

            }
            if(targetUnit != NULL){
                // ORIENT TO TARGET
                D3DXVECTOR3 targetPos, dirToTarget;
                targetUnit->GetPosition(targetPos.x,targetPos.y,targetPos.z);
                dirToTarget = targetPos - newPos; // represents enemy position - current position
                float distToTarget = D3DXVec3Length(&dirToTarget);            
                float a,b;


                a = atan(sqrt(dirToTarget.x*dirToTarget.x+dirToTarget.z*dirToTarget.z)/dirToTarget.y) / 0.0174532925f;
                b = atan(dirToTarget.x/dirToTarget.z)/0.0174532925f;
                //b /= 0.0174532925f; // convert from radians to degrees

                if(dirToTarget.z < 0) b = (b < 0) ? b+180.0f:b-180.0f; // because atan only gives a 180degree range and we need a 360

                newDir = D3DXVECTOR3(0.0f, b, 0.0f);            
                //printf("unit:%d attacking unit:%d\n",unit->getUnitId(), (*nearUnit)->getUnitId() );
                ////////////

                if(attack_enemy) {
					AIFireEvents.push_back(FireEvent(unit->getUnitId(), 0));
                }



                if(distToTarget > 50.0f || unit->getModelID() == PROBE_MODEL_ID)
                {
                    unit->SetRotation(newDir.x, newDir.y, newDir.z);            
                    newDir *= 0.0174532925f;
                    D3DXMATRIX rotationMatrix;
                    D3DXVECTOR3 deltaMov(0.0f, 0.0f, 10.0f);
                    deltaMov*= deltaTime * unit->GetMoveSpeed();
                    D3DXMatrixRotationYawPitchRoll(&rotationMatrix, newDir.y, newDir.x, newDir.z);
                    D3DXVec3TransformCoord( &deltaMov,&deltaMov, &rotationMatrix);

                    newPos += deltaMov;        
                }
            }
        } 
        /////End AI MOvement

        //GRAVITY and LANDSCAPE collision
        getGameStateWorld()->getHeightAt(newPos.x, terrainHeightAtXZ, newPos.z);
        map<int, UnitMovementWrapper>::iterator foundUpdatedUnit = updatedUnits.find(unit->getUnitId());
        if (unit->isSpark())
        {
        } else {
            float newPosY = 0;            
            newPos.y = max((newPos.y - GRAVITY * deltaTime), (2+terrainHeightAtXZ  + unit->getBoundingRadius()));
            D3DXVECTOR3 derp;
            unit->GetPosition( derp.x, derp.y, derp.z );
            if( newPos.x != derp.x || newPos.y != derp.y || newPos.z != derp.z )
            {// if currentPosX below the ground position the unit back above the ground
                if( foundUpdatedUnit != updatedUnits.end() )
                {// update from unit movement event.
                    (*foundUpdatedUnit).second.newY = ( newPos.y );
                }else
                {// needs to update a new unit.
                    UnitMovementWrapper umw;
                    umw.unitRef = unit;
                    umw.newX = newPos.x;
                    umw.newY = newPos.y;
                    umw.newZ = newPos.z;

                    umw.oldX = derp.x;
                    umw.oldY = derp.y;
                    umw.oldZ = derp.z;

                    // required to keep unit moving with you
                    updatedUnits.insert(pair<int,UnitMovementWrapper> (unit->getUnitId(),umw));     
                }
            }            
        }
    }
    std::map<int, UnitMovementWrapper>::iterator it;
    // Update position based on map and check for wall collisions.        
    for( it = updatedUnits.begin(); it != updatedUnits.end(); ++it )
	{
        UnitMovementWrapper unitMvmntWrapper = (*it).second;//updatedUnits.back();
        Unit * unit = (*it).second.unitRef; //iter->second;
        if (unit != NULL && unit->isSpark()) continue;

        newPos.x = (*it).second.newX;
        newPos.y = (*it).second.newY;
        newPos.z = (*it).second.newZ;
        D3DXVECTOR3 temp = newPos - worldCenter;
        temp.y = 0;
        /*if( D3DXVec3Length(&temp) > 565.0f ){ // dist from center to edge of arena 
            unit->setPosX ( unitMvmntWrapper.oldX );
            unit->setPosY ( unitMvmntWrapper.oldY );
            unit->setPosZ ( unitMvmntWrapper.oldZ );

            it = updatedUnits.erase(it);
            if ( it == updatedUnits.end() ) break;            
            continue;
        }*/
        getGameStateWorld()->getHeightAt(newPos.x, terrainHeightAtXZ, newPos.z);
        // if the change in height is a rise greater than 8, then this movement is not allowed
        if (  ((terrainHeightAtXZ) - ( unit->getPosY() )) > 8 )
        { // Running into a wall, stop moving/ restore old position.
            unit->setPosX ( unitMvmntWrapper.oldX );
            unit->setPosY ( unitMvmntWrapper.oldY );
            unit->setPosZ ( unitMvmntWrapper.oldZ );

            it = updatedUnits.erase(it);
            if ( it == updatedUnits.end() ) break;            
            continue;
        }else
        { // not running into a wall. keep desired positions the same
        }
    }
    
	// Insert final movement collision resolution. 
    // also the only one we need to do
    for( it = updatedUnits.begin(); it != updatedUnits.end(); ++it )
    {
        collisionHandler->validateMovementCollision( *((*it).second.unitRef), (*it).second.newX, (*it).second.newY,(*it).second.newZ );
        updateSparkFromUnit(*((*it).second.unitRef));        
    }

    // empty out vector
    updatedUnits.erase(updatedUnits.begin(), updatedUnits.end());
    
    releaseUnitLock();

	vector<FireEvent>::iterator ai_it = AIFireEvents.begin();
	for(; ai_it < AIFireEvents.end(); ai_it++)
	{
		ProcessFireEvent(*ai_it);
	}
}

/**
 * Updates the charge on a robot. If a robot is currently being controlled with a spark,
 * then its charge will increase; otherwise, decrease the charge.
 * @param      unit  a pointer to the robot to update
 * @param deltaTime  the time that has passed since the last call to this function
 */
void GameLogicStructure::updateUnitCharge(Unit * unit, const float & deltaTime) {
    if (!unit->isRobot())
        return;

    Robot * robot = (Robot *) unit;
    int playerId = robot->getControllingPlayerId();

    if (robot->isCurrentlyControlled()) {
        float currentCharge = robot->getCurrentCharge();
        robot->incrementCharge(deltaTime);
    } else if (playerId != NEUTRAL_UNIT) {
        float currentCharge = robot->getCurrentCharge();
        robot->decrementCharge(deltaTime);

        // if charge is zero, then remove robot affliation
        if (robot->getCurrentCharge() == 0) {
            robot->setColor(1, 1, 1);
            robot->setControllingPlayerId(NEUTRAL_UNIT);
        }
    }
}

/**
 * Updates a spark's position based on the specified unit. A spark's position is only
 * updated if the specified unit is a robot that the spark is currently controlling.
 * @param unit  the unit to update from
 */
void GameLogicStructure::updateSparkFromUnit(Unit & unit) {
    int playerId = unit.getControllingPlayerId();

    if (playerId == NEUTRAL_UNIT || !unit.isCurrentlyControlled())
        return;

    // retrieve the spark that the robot is controlling
    acquirePlayerToSparkLock();
    int sparkId;

    map<int, int>::iterator iter = playerToSpark.find(playerId);

    if (iter == playerToSpark.end()) {
        releasePlayerToSparkLock();
        return;
    }

    sparkId = iter->second;
    releasePlayerToSparkLock();

    // update the spark's position
    //acquireUnitLock();

    map<int, Unit *>::iterator iter2 = existingUnits.find(sparkId);

    if (iter2 == existingUnits.end()) {
        releaseUnitLock();
        return;
    }

    Unit * spark = iter2->second;

    spark->SetPosition(unit.getPosX(), unit.getPosY(), unit.getPosZ());
    spark->setPitch(unit.getPitch());
    spark->setYaw(fmod(unit.getYaw()+180.0f, 360.0f)-180.0f);
    spark->setRoll(unit.getRoll());

    //releaseUnitLock();
}

/**
 * Updates the position for all attack objects and set attacks to render.
 */
void GameLogicStructure::updateAttackObjects(const float& deltaTime)
{
    if (debug)
        cerr << "updateattack ";
    acquireAttackLock();
    
	vector<int> attacksToRemove;
    vector<Powerup *> newPowerups;
    
	for (map<int, AttackObject *>::iterator iter = existingAttackObjects.begin();
		iter != existingAttackObjects.end(); iter++) {
		AttackObject * currentAttack = iter->second;

		//currentAttack->setModified(true);

		vector<Unit*> damagedUnits;

		bool isDead = currentAttack->Update(deltaTime);

        /*if (currentAttack->isAoe()) {
            collisionHandler->removeAttackSphere(currentAttack->getUnitId());
            Sphere * sphere = new Sphere(currentAttack);
            collisionHandler->addAttackSphere(sphere, currentAttack->getUnitId());
        }*/

		bool isCollided = collisionHandler->validateAttackCollision(currentAttack, damagedUnits);

		if (isCollided) {
            if (currentAttack->isPowerUp()) {
                processPowerup(damagedUnits, currentAttack, deltaTime);            
            } else if (currentAttack->isAoe()) {
                processMassUnitCharge(damagedUnits, currentAttack, deltaTime);                
            } else
                processUnitDamage(damagedUnits, currentAttack, newPowerups, deltaTime);

			//if(currentAttack->isAoe())
			//	currentAttack->setAlreadyCollided(true);
		}

        // Update will return true if the bullet is dead
        if (isDead || (isCollided && !currentAttack->isAoe() && !currentAttack->isMeteor())) {
			DeleteEvent deleteEvent(currentAttack->getUnitId());
			Server::Instance()->sendMessageToClient(deleteEvent.Serialize());
            
			// add it to a list to delete from map
			attacksToRemove.push_back(currentAttack->getUnitId());

			collisionHandler->removeAttackSphere(currentAttack->getUnitId());
        }
    }

	// loop thruogh all the units to remove their pointers and deallocate memory
	for(vector<int>::iterator iter = attacksToRemove.begin(); iter != attacksToRemove.end(); iter++) {
        map<int, AttackObject *>::iterator attackIter = existingAttackObjects.find(*iter);

        if (attackIter != existingAttackObjects.end()) {
            AttackObject * attackToDelete = attackIter->second;
            existingAttackObjects.erase(attackToDelete->getUnitId());
            //delete attackToDelete;
        }
    }

    for(vector<Powerup *>::iterator iter = newPowerups.begin(); iter != newPowerups.end(); iter++) {
        Powerup * powerup = *iter;
		int powerUpId = powerup->getUnitId();
        existingAttackObjects[powerUpId] = powerup;

        Sphere * powerupSph = new Sphere(powerup);
        collisionHandler->addAttackSphere(powerupSph, powerup->getUnitId());
    }

    if (debug)
        cerr << "updateAttack ";
    releaseAttackLock();
}

void GameLogicStructure::processPowerup(vector<Unit*> units, AttackObject * potentialPowerup, float deltaTime) {
    if (!potentialPowerup->isPowerUp())
        return;

    Powerup * powerup = (Powerup *) potentialPowerup;

    acquireUnitLock();        

	for(vector<Unit*>::iterator it = units.begin(); it < units.end(); it++) {
        if ((*it)->isCurrentlyControlled()) {            
            int playerId = (*it)->getControllingPlayerId();

            acquirePlayerToSparkLock();
            map<int, int>::iterator playerToSparkIter = playerToSpark.find(playerId);

            if (playerToSparkIter != playerToSpark.end()) {
                int sparkId = playerToSparkIter->second;

                acquireUnitLock();

                map<int, Unit *>::iterator unitIter = existingUnits.find(sparkId);

                if (unitIter != existingUnits.end()) {
                    Unit * potentialSpark = unitIter->second;

                    if (potentialSpark->isSpark()) {
                        Spark * spark = (Spark *) potentialSpark;
                        if(powerup->getPowerUpType() != DOOMBOT_POWERUP)
                            powerup->applyPowerup(*it, spark);
                        else 
                            powerup->applyPowerup(*it);
                    }
                }
                releaseUnitLock();
            }

            releasePlayerToSparkLock();
        } else
            powerup->applyPowerup(*it);
	}
    releaseUnitLock();
}
void GameLogicStructure::processMassUnitCharge(vector<Unit*> units, AttackObject* attack, float deltaTime)
{

    vector<Unit*>::iterator it = units.begin();
    map<int, COLOR>::iterator iter = playerColor.find(attack->getControllingPlayerId());
    bool colorOk = false;
    COLOR colorToAssign;
    if (iter != playerColor.end()) {
        colorToAssign = iter->second;
        colorOk = true;
    }    

    float adjusted = attack->damage*deltaTime;
    float currID = attack->getControllingPlayerId();
    for(; it < units.end(); it++)
	{
        if((*it)->getControllingPlayerId() == NEUTRAL_UNIT && colorOk){
            (*it)->setControllingPlayerId(currID);
            (*it)->setColor(colorToAssign.r, colorToAssign.g, colorToAssign.b);
        }
        if((*it)->getControllingPlayerId() == currID){
            (*it)->setCurrentCharge(min((*it)->getCurrentCharge() + adjusted, 30.0f)); // cap the maximum charge from this at 30
        }else{
            // enemyUnit
            (*it)->setCurrentCharge(max((*it)->getCurrentCharge() - adjusted, 1.0f)); // cap the minimize from this to 1
            
        }
    }
}

/**
 * Applies the specified damage to the specified units.
 * @param  units  the units that damage is to be applied to
 * @param damage  the damage to apply
 */
void GameLogicStructure::processUnitDamage(vector<Unit*> units, AttackObject* attack, vector<Powerup *> & newPowerups, float deltaTime)
{
	vector<Unit*>::iterator it = units.begin();
	vector<int> unitsToRemove;

	for(; it < units.end(); it++)
	{
		//cout << "DAMAGED UNIT ID = " << (*it)->getUnitId() << endl;
		//cout << "CURRENT HEALTH = " << (*it)->getHealth() << " -> ";
		if((*it)->getControllingPlayerId() == attack->getControllingPlayerId())
			continue;

        bool dead = true;
        if(attack->isAoe() || attack->isMeteor())
            dead = (*it)->decrementHealth(attack->damage*deltaTime);
        else
            dead = (*it)->decrementHealth(attack->damage);

		if(dead)
		{
            // true means the unit has died
            updateRobotAllegianceOnDelete(*it);

			// send delete message
            DeleteEvent deleteEvent((*it)->getUnitId());
            Server::Instance()->sendMessageToClient(deleteEvent.Serialize());
            
			// add it to a list to delete from map
			unitsToRemove.push_back((*it)->getUnitId());


            // create power up if the robot is not a neutral unit
            
            int networkId = Server::Instance()->getNextUnitId();

			//create powerup with a certain probability
			int randFactor = rand() % 10;

			Powerup * powerup = NULL;

            if((*it)->isDoomRobot()){
                powerup = new Powerup(networkId, HEALTH_GAIN*4, (*it), DOOMBOT_POWERUP);
            } else if (randFactor < 2) {
				powerup = new Powerup(networkId, HEALTH_GAIN, (*it), HEALING_POWERUP);
			} else if (randFactor < 4) {
				powerup = new Powerup(networkId, CHARGE_GAIN, (*it), CHARGING_POWERUP);
            }

			if (powerup != NULL) {
				newPowerups.push_back(powerup);
			}
		}
        else{
            // unit is still alive, give it a jitter so we know it is taking damage
            float pitch, yaw, roll;
            (*it)->GetRotation(pitch, yaw, roll);
            float randomPitch = ((float)rand()/RAND_MAX * 20.0f)-10.0f;
            float randomYaw = ((float)rand()/RAND_MAX * 40.f)-20.0f;                    
            (*it)->SetRotation(pitch+randomPitch, yaw+randomYaw, roll);

            float x = (*it)->getPosX();
            float z = (*it)->getPosZ();
            float x_offset = x - attack->getPosX();
            float z_offset = z - attack->getPosZ();
            float normalize = 10.0f/sqrt(x_offset*x_offset + z_offset*z_offset);

            x_offset *= deltaTime;
            z_offset *= deltaTime;
            if(attack->isMeteor())
            {
                (*it)->setPosY((*it)->getPosY() + deltaTime*GRAVITY*1.66);
                (*it)->setPosX(x-x_offset);  
                (*it)->setPosZ(z-z_offset);
            }            
            else{
                (*it)->setPosX(x+x_offset);  
                (*it)->setPosZ(z+z_offset);
            }

        }
		//cout << "NEW HEALTH = " << (*it)->getHealth() << endl;
	}

    
    acquireUnitLock();

	for(vector<int>::iterator iter = unitsToRemove.begin(); iter != unitsToRemove.end(); iter++) {
		
        map<int, Unit *>::iterator unitIter = existingUnits.find(*iter);

        if (unitIter != existingUnits.end()) {
            Unit * unitToDelete = unitIter->second;

            existingUnits.erase(unitToDelete->getUnitId());
            collisionHandler->removeSphere(unitToDelete->getUnitId());
            //delete unitToDelete;
        }
    }

	if(existingUnits.size() <= 25)
	{
		addDoomRobots(LoadingManager::addDoomRobots);
	}
    
    releaseUnitLock();
}

/**
 * Disassociates a spark that is controlling the specified unit.
 * @param unit  the unit pending deletion
 */
void GameLogicStructure::updateRobotAllegianceOnDelete(Unit * unit) {
    if (!unit->isCurrentlyControlled())
        return;

    int controllingPlayerId = unit->getControllingPlayerId();

    // look up spark id
    acquirePlayerToSparkLock();
    map<int, int>::iterator iter = playerToSpark.find(controllingPlayerId);

    if (iter != playerToSpark.end()) {
        int sparkId = iter->second;

        // look up the actual spark
        acquireUnitLock();
        map<int, Unit *>::iterator unitIter = existingUnits.find(sparkId);

        if (unitIter != existingUnits.end()) {
            Unit * potentialSpark = unitIter->second;

            if (potentialSpark->isSpark()) {
                Spark * spark = (Spark *) potentialSpark;
				spark->setHealth(spark->getHealth() * LoadingManager::robotDeathPenalty);
                spark->setCurrentlyControlling(false);
            }
        }

        releaseUnitLock();
    }

    releasePlayerToSparkLock();
}

/**
 * Calculates the damage a spark takes if it is not currently controlling a robot.
 * @param deltaTime  the elasped time since the last call to this function
 */
void GameLogicStructure::updateSparkHealth(const float & deltaTime){
    acquirePlayerToSparkLock();

    for (map<int, int>::iterator iter = playerToSpark.begin(); iter != playerToSpark.end(); iter++) {
        int sparkId = iter->second;

        acquireUnitLock();

        map<int, Unit *>::iterator unitIter = existingUnits.find(sparkId);

        if (unitIter != existingUnits.end()) {
            Unit * potentialSpark = unitIter->second;

            if (potentialSpark->isSpark()) {
                Spark * spark = (Spark *) potentialSpark;

                if (!spark->isCurrentlyControlling()) {
                    float currentHealth = spark->getHealth();
                    float newHealth = currentHealth - (deltaTime * HEALTH_LOST_PER_SECOND);

                    spark->setHealth(newHealth);
                    releaseUnitLock();

                    checkForWinLoseConditions(spark);
                }
            }
        }

        releaseUnitLock();
    }

    releasePlayerToSparkLock();
}

/**
 * Checks for a win/lose condition. If the specified spark has lost all its health, then
 * the player associated with the spark loses. If there is only one spark left after a 
 * player loses, then the remaining player wins.
 * @param spark  the spark whose health has just been updated
 */
void GameLogicStructure::checkForWinLoseConditions(Spark * spark) {
    if (spark->getHealth() >= 1)
        return;

    acquirePlayersRemainingLock();

    int playerId = spark->getControllingPlayerId();

    for (vector<int>::iterator iter = playersRemaining.begin(); iter != playersRemaining.end(); iter++) {
        if (*iter == playerId) {
            int unitToDelete = spark->getUnitId();

            acquireUnitLock();

            map<int, Unit *>::iterator unitIter = existingUnits.find(unitToDelete);

            // delete associated spark
            if (unitIter != existingUnits.end()) {
                Unit * sparkToDelete = unitIter->second;
                existingUnits.erase(sparkToDelete->getUnitId());
                //delete sparkToDelete;

                DeleteEvent deleteEvent(unitToDelete);
                Server::Instance()->sendMessageToClient(deleteEvent.Serialize());
            }

            releaseUnitLock();

            LoseEvent loseEvent(playerId);
            Server::Instance()->sendMessageToClient(loseEvent.Serialize());

            playersRemaining.erase(iter);
            break;
        }
    }

    if (playersRemaining.size() == 1) {
        vector<int>::iterator iter = playersRemaining.begin();

        int winningPlayer = *iter;

        WinEvent winEvent(winningPlayer);
        Server::Instance()->sendMessageToClient(winEvent.Serialize());
    }

    releasePlayersRemainingLock();
}

/**
 * Processes an enter event. This determines if the specified enter event is allowed
 * to happen.
 * @param enterEvent  the enter event to process
 */
void GameLogicStructure::ProcessEnterEvent(EnterEvent & enterEvent) {
    acquireUnitLock();

    int id = enterEvent.getUnitId();
    map<int, Unit *>::iterator iter = existingUnits.find(id);

    if (iter != existingUnits.end()) {
        Unit * unit = iter->second;

        if (!unit->isSpark()) {
            releaseUnitLock();
            return;
        }

        Spark * spark = (Spark *) unit;

        // get the first unit that the spark is colliding with
        int collidingUnitId = collisionHandler->GetAccessibleUnits(spark->getUnitId(), enterEvent.getPlayerId());

        if (collidingUnitId == NO_COLLISION) {
            releaseUnitLock();
            return;
        }

        if ( (iter = existingUnits.find(collidingUnitId)) == existingUnits.end() ) {
            releaseUnitLock();
            return;
        }

        Unit * collidingUnit = iter->second;

        // set control information
        spark->setCurrentlyControlling(true);
        collidingUnit->setCurrentlyControlled(true);
        collidingUnit->setControllingPlayerId(enterEvent.getPlayerId());

        // move robot to the spark's position
        collidingUnit->SetPosition(spark->getPosX(), spark->getPosY(), spark->getPosZ());
        collidingUnit->setPitch(spark->getPitch());
        collidingUnit->setYaw(spark->getYaw());
        collidingUnit->setRoll(spark->getRoll());

        // set robot color to match player color
        collidingUnit->setColor(spark->getColorR(), spark->getColorG(), spark->getColorB());

        AssignEvent assignEvent(enterEvent.getPlayerId(), collidingUnit->getUnitId());
        Server::Instance()->sendMessageToClient(assignEvent.Serialize());
    }

    releaseUnitLock();
} 

/**
 * Processes an exit event. Handles the logic for determining if a unit is allowed to 
 * exit from the specified robot.
 * @param exitEvent  the exit event to process
 */
void GameLogicStructure::ProcessExitEvent(ExitEvent & exitEvent) {
    acquireUnitLock();

    int id = exitEvent.getRobotId();
    map<int, Unit *>::iterator iter = existingUnits.find(id);

    if (iter != existingUnits.end()) {
        Unit * controlledRobot = iter->second;
        int playerId = exitEvent.getPlayerId();

        iter = existingUnits.find(exitEvent.getSparkId());

        if (iter == existingUnits.end()) {
            releaseUnitLock();
            return;
        }

        if (controlledRobot->isCurrentlyControlled() && 
            playerId == controlledRobot->getControllingPlayerId()) {
            Unit * potentialSpark = iter->second;
            
            if (potentialSpark->isSpark()) {
                Spark * spark = (Spark *) potentialSpark;
            
                spark->setCurrentlyControlling(false);
                controlledRobot->setCurrentlyControlled(false);

                UnassignEvent unassignEvent(playerId);
                Server::Instance()->sendMessageToClient(unassignEvent.Serialize());
            }
        }
    }

    releaseUnitLock();
}

void GameLogicStructure::cleanUp() {
    
}

/*
 * LOCK METHODS
 */
void GameLogicStructure::acquireAttackLock() {
    if (debug)
        cerr << "acquiring attack lock... ";
    WaitForSingleObject(H_existingAttackObjMutex, INFINITE);
    if (debug)
        cerr << "acquired!" << endl;
}

void GameLogicStructure::releaseAttackLock() {
    if (debug)
        cerr << "releasing attack lock... ";
    ReleaseMutex(H_existingAttackObjMutex);
    if (debug)
        cerr << "released!" << endl;
}

void GameLogicStructure::acquireUnitLock() { 
    //if (debug)
    //    cerr << "acquiring unit lock... ";
    WaitForSingleObject(H_existingUnitMutex, INFINITE); 
    //if (debug)
    //    cerr << "acquired!" << endl;
}

void GameLogicStructure::releaseUnitLock() {
    //if (debug)
    //    cerr << "releasing unit lock... ";
    ReleaseMutex(H_existingUnitMutex);
    //if (debug)
    //    cerr << "released!" << endl;
}

void GameLogicStructure::acquirePlayerToSparkLock() {
    //if (debug)
    //    cerr << "acquiring playerToSpark lock... ";
    WaitForSingleObject(H_playerToSparkMutex, INFINITE); 
    //if (debug)
    //    cerr << "acquired!" << endl;
}

void GameLogicStructure::releasePlayerToSparkLock() {
    //if (debug)
    //    cerr << "releasing playerToSpark lock... ";
    ReleaseMutex(H_playerToSparkMutex);
    //if (debug)
     //   cerr << "released!" << endl;
}

void GameLogicStructure::acquirePlayersRemainingLock() {
    //if (debug)
    //    cerr << "acquiring playerRemaining lock... ";
    WaitForSingleObject(H_playersRemainingMutex, INFINITE); 
    //if (debug)
    //    cerr << "acquired!" << endl;
}

void GameLogicStructure::releasePlayersRemainingLock() {
    //if (debug)
    //    cerr << "releasing playerRemaining lock... ";
    ReleaseMutex(H_playersRemainingMutex);
    //if (debug)
    //    cerr << "released!" << endl;
}
