#include "stdafx.h"

#define _CRTDBC_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <iostream>

#include "Server.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[]) {
    Server * server = Server::Instance();

    if (server->initializeServer() == SERVER_START_ERROR) 
        return 1;

    server->Run();
    server->CleanUp();

    _CrtDumpMemoryLeaks();
    return 0;
}