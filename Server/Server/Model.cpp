/***
 * Filename: Model.cpp
 * Author: Alfred Tarng
 * Purpose: Server version of a model file. Loads the model to get its bounding
 *          radius and returns it as a stripped down package of id,
 *          boundingRadius.
 */

#include "Model.h"

#include <iostream>
#include <assimpCollada/assimp.h>
#include "include/assimpCollada/aiPostProcess.h"
#include "include/assimpCollada/aiScene.h"

// FORWARD DECLARATIONS
D3DXMATRIX AssimpMat2D3DMat(aiMatrix4x4 convertMatrix)
{
    convertMatrix = convertMatrix.Transpose();
    return D3DXMATRIX( convertMatrix.a1,convertMatrix.a2,convertMatrix.a3,convertMatrix.a4,
                       convertMatrix.b1,convertMatrix.b2,convertMatrix.b3,convertMatrix.b4,
                       convertMatrix.c1,convertMatrix.c2,convertMatrix.c3,convertMatrix.c4,
                       convertMatrix.d1,convertMatrix.d2,convertMatrix.d3,convertMatrix.d4 );
}

Model::Model()
{
	m_model       = NULL;
    m_vertexCount = 0;

    numMeshes      = 0;
    multMeshPtr    = NULL;
    modelName      = NULL;
    //scene      = NULL;
    minX = minY = minZ = maxX = maxY = maxZ = 0;
}
Model::Model(const Model& other)
{
}
Model::~Model(){ Shutdown(); }

// fileType
//  0: simplified txt.
//  1: Collada File
//bool Model::Initialize(ID3D11Device* device, char* modelFilename, vector<char*> textureFileVector, int fileType)
bool Model::Initialize(char* modelFilename, int fileType)
{
    bool result = true;
    modelName = modelFilename;

	// Load in the model data,
	result = LoadModel(modelFilename, fileType);
	if(!result)
	{
        printf("LoadModel(%s) failed...", modelFilename );
		return false;
	}

    return true;
}

bool Model::LoadModel(char* filename, int fileType)
{
    if ( fileType != 0 )
    {
        const aiScene * scene = aiImportFile(filename, ( aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded ));

        if ( scene == NULL )
        {
            cout << "stop borking..." << endl;
            return false;
        }
        
        numMeshes = scene->mNumMeshes; // ###
        multMeshPtr = new MultMeshObject[numMeshes];

        int vc = 0; // vertexCounter;
        int badMeshes = 0;
        
        aiNode* travNode = scene->mRootNode;
        for(unsigned int i = 0; i < scene->mNumMeshes; ++i)
        {
            vc = 0;
            aiMesh * mesh = scene->mMeshes[i];
            if ( mesh->mNumFaces < 6 )
            {
                //cout << "Skipping what is PROBABLY a bad mesh..." << endl;
                ++badMeshes;
                continue;
            }

            m_model = new ModelType[mesh->mNumFaces*3];

            multMeshPtr[i - badMeshes].meshObj  = m_model;
            multMeshPtr[i - badMeshes].numVerts = mesh->mNumFaces * 3;

            multMeshPtr[i - badMeshes].modelRelativeMatrix = AssimpMat2D3DMat(travNode->mChildren[i - badMeshes]->mTransformation);

            m_vertexCount += mesh->mNumFaces * 3;
            m_indexCount  = m_vertexCount; // match up index count and vertex count

            for (unsigned int j = 0; j < mesh->mNumFaces; ++j)
            {
                const struct aiFace * face = &(mesh->mFaces[j]);
                switch(face->mNumIndices)
                {
                    case 1:  break;
                    case 2:  break;
                    case 3:  break;
                    default: break;
                }

                for (unsigned int k = 0; k < face->mNumIndices; ++k )
                {
                    int vertexIndex = face->mIndices[k];

                    aiVector3D vertex  = mesh->mVertices[vertexIndex];

                    m_model[vc].x   = vertex.x;
                    m_model[vc].y   = vertex.y;
                    m_model[vc].z = vertex.z;

                    // TODO: Make sure this works for multiple meshed objects...
                    if ( m_model[vc].x < minX ) minX = m_model[vc].x;
                    if ( m_model[vc].x > maxX ) maxX = m_model[vc].x;
                    if ( m_model[vc].y < minY ) minY = m_model[vc].y;
                    if ( m_model[vc].y > maxY ) maxY = m_model[vc].y;
                    if ( m_model[vc].z < minZ ) minZ = m_model[vc].z;
                    if ( m_model[vc].z > maxZ ) maxZ = m_model[vc].z;

                    ++vc;
                }
            }
        }
        numMeshes = scene->mNumMeshes - badMeshes;
    }else
    {// Reading the simple text file.
        ifstream fin;
	    char input;

        // Open the model file.
	    fin.open(filename);
	
	    // If it could not open the file then exit.
	    if(fin.fail())
	    {
            cerr << "[Model] Error: Could not open file..." << endl;
		    return false;
	    }

        // Read up to the value of vertex count.
	    fin.get(input);
	    while(input != ':'){ fin.get(input); }

        // Read in the vertex count. (Assuming the basic txt...)
	    fin >> m_vertexCount;

        // Set the number of indices to be the same as the vertex count.
	    m_indexCount = m_vertexCount;

	    // Create the model using the vertex count that was read in.
	    m_model = new ModelType[m_vertexCount];
	    if(!m_model) return false;

        // Read up to the beginning of the data.
	    fin.get(input);
	    while(input != ':')
	    {
		    fin.get(input);
	    }
	    fin.get(input); // moves it down a line I assume?
	    fin.get(input);

	    // Read in the vertex data.
        //int interval = m_vertexCount/10;
	    for(int i=0; i < m_vertexCount; i++)
	    {
            float paddingTU, paddingTV, paddingNX, paddingNY, paddingNZ;
		    fin >> m_model[i].x  >> m_model[i].y >> m_model[i].z;
		    fin >> paddingTU >> paddingTV;
		    fin >> paddingNX >> paddingNY >> paddingNZ;

            if ( m_model[i].x < minX ) minX = m_model[i].x;
            if ( m_model[i].x > maxX ) maxX = m_model[i].x;

            if ( m_model[i].y < minY ) minY = m_model[i].y;
            if ( m_model[i].y > maxY ) maxY = m_model[i].y;

            if ( m_model[i].z < minZ ) minZ = m_model[i].z;
            if ( m_model[i].z > maxZ ) maxZ = m_model[i].z;

	    }

	    // Close the model file.
	    fin.close();
    }

	return true;
}

int Model::GetIndexCount()
{
    return m_indexCount;
}
int Model::GetNumMeshes(){ return numMeshes; }

/***
 * Cleans up Model
 */ 
void Model::Shutdown()
{
	// Release the model texture.

	// Shutdown the vertex and index buffers.
	//ShutdownBuffers();

	// Release the model data.
	ReleaseModel();

	return;
}
void Model::ReleaseModel()
{
	if(m_model)
	{
		delete [] m_model;
		m_model = 0;
	}

	return;
}

float Model::RetrieveRadius()
{
    //int   greatestIndex = 0;
    float greatestValue = abs(minX);
    greatestValue = (greatestValue>abs(minY) )? greatestValue:abs(minY);
    greatestValue = (greatestValue>abs(minZ) )? greatestValue:abs(minZ);
    greatestValue = (greatestValue>abs(maxX) )? greatestValue:abs(maxX);
    greatestValue = (greatestValue>abs(maxY) )? greatestValue:abs(maxY);
    greatestValue = (greatestValue>abs(maxZ) )? greatestValue:abs(maxZ);
    return greatestValue/2;
    //switch (greatestIndex)
    //{
    //    case 0: return minX;
    //    case 1: return minY;
    //    case 2: return minZ;
    //    case 3: return maxX;
    //    case 4: return maxY;
    //    case 5: return maxZ;
    //}
}