//LoadingManager.h

#define _ONSERVER

#ifndef _LOADING_MANAGER_
#define _LOADING_MANAGER_

#include <string>

class LoadingManager
{
public:
	//declare variables here
	static int			numRobots;
	static int			addDoomRobots;

	static int			robotHealth;
	static int			probeHealth;
	static int			doomHealth;
	static int			sentryHealth;
	static int			fanHealth;
	static int			sparkHealth;

    static int          requiredNumOfPlayers;

	static float		probeSpeed;
	static float		doomSpeed;
	static float		sentrySpeed;
	static float		fanSpeed;
	static float		sparkSpeed;

	static float		probeCharge;
	static float		sentryCharge;
	static float		fanCharge;
	static float		discharge;

	static float		healthLostPerSecond;
	static float		robotDeathPenalty;
	static float		healthGain;

	static int			bulletDmg;
	static int			meleeDmg;
	static int			fanDmg;

	static int			bullet2Dmg;
	static int			aoeDmg;
	static int			meteorDmg;

	static float		bulletSpeed;
	static float		fanBulletSpeed;

	static float		bulletMaxTime;
	static float		meleeMaxTime;
	static float		bullet2MaxTime;
	static float		aoeMaxTime;
	static float		meteorMaxTime;

	static float		gravity;

//// END CONFIG VARIABLES /////////////////////////////////////

	LoadingManager();
	LoadingManager(std::string loadingFileName);
	~LoadingManager();

	static bool Initialize(std::string loadingFileName);
};

#endif