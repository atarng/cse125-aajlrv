#ifndef _DOOM_ROBOT_H_
#define _DOOM_ROBOT_H_

#include "Robot.h"

class DoomRobot : public Robot
{
public:
	DoomRobot();
	DoomRobot(int);
	~DoomRobot();

	bool isDoomRobot();

	void setCurrentCharge(float);
	void decrementCharge(float deltaTime);

	bool hasLanded;
};

#endif

