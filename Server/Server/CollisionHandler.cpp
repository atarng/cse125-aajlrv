#include "stdafx.h"
#include "CollisionHandler.h"
#include <Unit.h>
#include <iostream>

#define _WINSOCK2API_
#include "GameLogicStructure.h"

CollisionHandler::CollisionHandler()
{
	for(int i = 0; i < BUCKET_MAX; i++)
	{
		buckets[i] = new map<int,Sphere*>[BUCKET_MAX];
	}

	H_bucketsMutex = CreateMutex(NULL, false, NULL);
	if (H_bucketsMutex == NULL)
        cerr << "[ERROR] Server: Could not create lock for buckets!" << endl;

	isDelete = false;
}

int CollisionHandler::getBucketSphCount()
{
	int count = 0;
	for(int i = 0; i < BUCKET_MAX; i++)
	{
		for(int j = 0; j < BUCKET_MAX; j++)
		{
			count += buckets[i][j].size();
		}
	}

	return count;
}

void CollisionHandler::addSphere(Sphere* sph, int id)
{
    spheres[id] = sph;
	acquireBucketsLock();
		updateSphereBuckets(sph, id);
	releaseBucketsLock();
}

void CollisionHandler::removeSphere(int id)
{    
	isDelete = true;
    // find self in every bucket and remove self
    map<int, Sphere*>::iterator sph = spheres.find(id);

	if (sph != spheres.end()) {
		Sphere* currSph = sph->second;
		/*vector<BucketIndex>::iterator it = sph->second->sphBuckets.begin();
		for(; it < sph->second->sphBuckets.end(); it++)
		{
			vector<Sphere*> currVec = buckets[(*it).row][(*it).col];
			vector<Sphere*>::iterator currVecIt = currVec.begin();

			for(; currVecIt < currVec.end(); currVecIt++)
			{
				if((*currVecIt) == sph->second)
				{
					currVec.erase(currVecIt);
					break;
				}
			}
		}*/
		acquireBucketsLock();
			removeFromBuckets(currSph);    
		releaseBucketsLock();
		spheres.erase(id);
		//delete currSph;

		isDelete = false;
	}
}

void CollisionHandler::addAttackSphere(Sphere * sph, int id)
{
    attackSpheres[id] = sph;
}

void CollisionHandler::removeAttackSphere(int id)
{
	map<int, Sphere*>::iterator it = attackSpheres.find(id);

	if (it != attackSpheres.end()) {
		Sphere * currSph = it->second;
		attackSpheres.erase(it);
		//delete currSph;
	}
}

bool CollisionHandler::Update(){
    map<int, Sphere*>::iterator it = spheres.begin();
    for(; it != spheres.end(); it++){
        updateSphereBuckets(it->second, it->first);
    }
    return true;
}
void CollisionHandler::updateSphereBuckets(Sphere* sph, int id)
{
    //// determine the current bucket
    //int xpos = sph->GetPos().x;// - ZERO_POS;
    //int zpos = sph->GetPos().z;// - ZERO_POS; 
    //int row = min(max(zpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);
    //int col = min(max(xpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);

	acquireBucketsLock();
    BucketIndex thisBucket = getCurrentBucket(sph);
	
    if( thisBucket.row != sph->currBucket.row || thisBucket.col != sph->currBucket.col){
        //printf("Unit:%d = (%d, %d)\n", sph->currUnit->getUnitId(), row,col);

        sph->currBucket = thisBucket;

		removeFromBuckets(sph);

        //vector<BucketIndex>::iterator it = sph->sphBuckets.begin();
        //// find self in every bucket and remove self
        //for(; it < sph->sphBuckets.end(); it++)
        //{
        //    vector<Sphere*> currVec = buckets[(*it).row][(*it).col];
        //    vector<Sphere*>::iterator currVecIt = currVec.begin();

        //    for(; currVecIt < currVec.end(); currVecIt++)
        //    {
        //        if((*currVecIt) == sph)
        //        {
        //            currVec.erase(currVecIt);
        //            break;
        //        }
        //    }
        //}

        sph->sphBuckets.clear();

        //int row = zpos / BUCKET_SIZE;
        //int col = xpos / BUCKET_SIZE;
        //populateBucket(row, col, sph);


        int radius = (int)(sph->GetRadius());
        
        // insert sphere into nearby buckets
        for(int i = -1; i <= 1;i++){
            for(int j = -1; j <= 1; j++){
				int rowOffset = i + thisBucket.row;
                int colOffset = j + thisBucket.col;
                if( colOffset < 0 || 
                    rowOffset < 0 ||
                    colOffset >= BUCKET_MAX ||
                    rowOffset >= BUCKET_MAX )
                    continue;
                populateBucket(rowOffset, colOffset, sph, id);
                
            }
        }
    }
	releaseBucketsLock();
}

void CollisionHandler::populateBucket(int row, int col, Sphere* sph, int id)
{
	map<int, Sphere*>* currBucket = &(buckets[row][col]);
	currBucket->insert(pair<int, Sphere*>(id, sph));

    BucketIndex index;
    index.row = row;
    index.col = col;
    sph->sphBuckets.push_back(index);
}

void CollisionHandler::removeFromBuckets(Sphere* sph)
{
	vector<BucketIndex>::iterator it = sph->sphBuckets.begin();
    // find self in every bucket and remove self
    for(; it < sph->sphBuckets.end(); it++)
    {
		map<int, Sphere *>::iterator sphereIter = buckets[it->row][it->col].find(sph->currUnit->getUnitId());

		if (sphereIter != buckets[it->row][it->col].end())
		{
			buckets[it->row][it->col].erase(sphereIter);
		}
	}
}

BucketIndex CollisionHandler::getCurrentBucket(Sphere* currSph)
{
	int xpos = currSph->GetPos().x;// - ZERO_POS;
    int zpos = currSph->GetPos().z;// - ZERO_POS; 
    int row = min(max(zpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);
    int col = min(max(xpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);

	return BucketIndex(row, col);
}

bool CollisionHandler::GetNearbyUnits(int currUnit, std::vector<Unit*>& nearbyUnits)
{
	acquireBucketsLock();

    std::map<int,Sphere*>::iterator curr = spheres.find(currUnit);
	BucketIndex currBucket = curr->second->currBucket;

	map<int, Sphere*> appendVec = (buckets[currBucket.row][currBucket.col]);
    std::map<int, Sphere*>::iterator buck = appendVec.begin();
    for(; buck != appendVec.end(); buck++){
        nearbyUnits.push_back(buck->second->currUnit);
	}

	releaseBucketsLock();

    return nearbyUnits.size() > 0;    
}
//void CollisionHandler::getNearbySpheres(Sphere* collidingSphere, vector<Sphere*>& nearbySpheres)
//{
//    vector<BucketIndex> indices = collidingSphere->sphBuckets;
//    vector<BucketIndex>::iterator it = indices.begin();
//
//    for(; it < indices.end(); it++)
//    {
//        vector<Sphere*> appendVec = buckets[(*it).row][(*it).col];
//        appendSphereVector(appendVec, nearbySpheres, collidingSphere);
//    }
//}
//
//void CollisionHandler::appendSphereVector(vector<Sphere*> appendVec, vector<Sphere*>& mainVec, Sphere* collidingSphere)
//{
//    for(int i = 0; i < appendVec.size(); i++)
//    {
//        if(appendVec[i] != collidingSphere)
//        {
//            mainVec.push_back(appendVec[i]);
//        }
//    }
//}

//bool CollisionHandler::validateMovementCollision(MovementEvent & mEvent, Unit & unit)
//{
//    //ACTUAL UPDATES SHOULD BE CHECKED AGAINST THE STATE OF THE WORLD, COLLISIONS/HEIGHT MAP ETC.        
//    float currentPosX = 0, currentPosY = 0, currentPosZ = 0;
//    bool isCollided = false;
//    //unit -> GetPosition(currentPosX, currentPosY, currentPosZ );
//    std::map<int,Sphere*>::iterator it = spheres.find(mEvent.getUnitId());
//
//    unit.setPitch(unit.getPitch() + mEvent.getDeltaPitch());
//    unit.setYaw(  unit.getYaw()   + mEvent.getDeltaYaw());
//
//    D3DXVECTOR3 dir = D3DXVECTOR3(mEvent.getDeltaX(),mEvent.getDeltaY(),mEvent.getDeltaZ());
//    if(it != spheres.end()){
//        Sphere* unitSph = ((*it).second);
//        unitSph->pos.x = unit.getPosX() + mEvent.getDeltaX();
//        unitSph->pos.y = unit.getPosY() + mEvent.getDeltaY();
//        unitSph->pos.z = unit.getPosZ() + mEvent.getDeltaZ();
//
//
//        //unit.GetPosition(x, y, z);
//        //D3DXVECTOR3 newPos(x + mEvent.getDeltaX(), y + mEvent.getDeltaY(), z + mEvent.getDeltaZ());
//
//        ////Sphere * unitSph = unitControl.currentSph;
//        //unitSph->SetPos(newPos);
//        /*
//        // if collided, reset sphere to old position
//        if((isCollided = validateMovement(unitSph, dir)))
//        {
//            //D3DXVECTOR3 oldPos(unit.getPosX(), unit.getPosY(), unit.getPosZ());
//            //unitSph->SetPos(oldPos);
//            unit.SetPosition(unitSph->pos.x, unitSph->pos.y, unitSph->pos.z);
//        }
//        // else, update position of unit to its sphere.
//        else 
//            unit.SetPosition(unitSph->pos.x, unitSph->pos.y, unitSph->pos.z);*/
//
//        validateMovement(unitSph, dir);
//        unit.SetPosition(unitSph->pos.x, unitSph->pos.y, unitSph->pos.z);
//    }else{
//        printf("wtf");
//    }
//
//    return isCollided;
//}

bool CollisionHandler::validateMovementCollision(Unit & unit, float desiredX, float desiredY, float desiredZ)
{
    bool isCollided = false;
    std::map<int,Sphere*>::iterator it = spheres.find(unit.getUnitId());

    if(it != spheres.end())
    {
        Sphere* unitSph = ((*it).second);
        D3DXVECTOR3 dir = D3DXVECTOR3(desiredX-unitSph->pos.x,desiredY-unitSph->pos.y,desiredZ-unitSph->pos.z);
        unitSph->pos.x = desiredX;
        unitSph->pos.y = desiredY;
        unitSph->pos.z = desiredZ;

        //vector<Sphere*> nearbySpheres;
        //getNearbySpheres(unitSph, nearbySpheres);
		/*vector<BucketIndex>::iterator b_it = unitSph->sphBuckets.begin();
		for(; b_it < unitSph->sphBuckets.end(); b_it++)
		{
			if(validateMovement(unitSph, dir, buckets[b_it->row][b_it->col]))
				break;
		}*/
        //validateMovement(unitSph, dir, nearbySpheres);

		BucketIndex currBucket = unitSph->currBucket;
		acquireBucketsLock();
		isCollided = validateMovement(unitSph, dir, buckets[currBucket.row][currBucket.col]);
		releaseBucketsLock();

        unit.setModified(true); //the fact that we are here means we moved
        unit.SetPosition(unitSph->pos.x, unitSph->pos.y, unitSph->pos.z);
		if (!unit.hasPerformedAction())
		{
			unit.setCurrentAnimType(ANIM_WALK);
			unit.setPerformedAction(true , -1);
		}
    }
    return isCollided;
}

bool CollisionHandler::validateMovement(Sphere* collidingSphere, D3DXVECTOR3 dir)
{

    map<int, Sphere *>::iterator it = spheres.begin();
    bool hit = false;
    for(;it != spheres.end();it++)
    {

        if(collidingSphere->intersect(*((*it).second)))
        {
            hit = true;
            D3DXVECTOR3 temp = (collidingSphere->pos-(*it).second->pos);

            float derp = temp.x*temp.x+temp.y*temp.y+temp.z*temp.z; // distance                
            float derp2 = (collidingSphere->radius + (*it).second->radius);                
            derp2 *= derp2;
            float overlap = derp2 - derp;
            if(overlap > 0){
                D3DXVec3Normalize(&temp, &temp);
                temp *= (*it).second->radius;
                collidingSphere->pos += (temp*(overlap/derp2));    			
            }
        }

    }
    return hit;
}

bool CollisionHandler::validateMovement(Sphere* collidingSphere, D3DXVECTOR3 dir, map<int, Sphere*> nearbySpheres)
{

    map<int, Sphere *>::iterator it = nearbySpheres.begin();
    bool hit = false;
    for(;it != nearbySpheres.end();it++)
    {
        if(collidingSphere != it->second && collidingSphere->intersect(*(it->second)))
        {
            hit = true;
            D3DXVECTOR3 temp = (collidingSphere->pos - it->second->pos);

            float derp = temp.x*temp.x+temp.y*temp.y+temp.z*temp.z; // distance                
            float derp2 = (collidingSphere->radius + it->second->radius);                
            derp2 *= derp2;
            float overlap = derp2 - derp;

            D3DXVec3Normalize(&temp, &temp);
            temp *= it->second->radius;
            collidingSphere->pos += (temp*(overlap/derp2));
        }

    }
    return hit;
}

bool CollisionHandler::validateAttackCollision(AttackObject* attack, vector<Unit*> & units)
{
    vector<Sphere*> collidedSpheres;
    Sphere * attackSph = attackSpheres[attack->getUnitId()];    
    float x, y, z;

    attackSph->currUnit->GetRadius(x);
    attackSph->SetRadius(x);    

    attack->GetPosition(x, y, z);
    D3DXVECTOR3 newPos(x, y, z);
    attackSph->SetPos(newPos);

	//int checkCount = 0;

	/*int xpos = attackSph->GetPos().x;
    int zpos = attackSph->GetPos().z;
    int row = min(max(zpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);
    int col = min(max(xpos / BUCKET_SIZE, 0) , BUCKET_MAX-1);*/
	acquireBucketsLock();
	BucketIndex currBucketIndex = getCurrentBucket(attackSph);
	map<int, Sphere*> currBucket = buckets[currBucketIndex.row][currBucketIndex.col];
	int unitId = attack->getControllingUnitId();
	bool isCollided = validateAttack(attackSph, unitId, collidedSpheres, currBucket);

	//for(int i = -1; i <= 1;i++){
 //       for(int j = -1; j <= 1; j++){
 //           int rowOffset = i + row;
 //           int colOffset = j + col;
 //           if( colOffset < 0 || 
 //               rowOffset < 0 ||
 //               colOffset >= BUCKET_MAX ||
 //               rowOffset >= BUCKET_MAX )
 //               continue;

 //           map<int, Sphere*> currBucket = buckets[rowOffset][colOffset];
	//		//checkCount += currBucket.size();
	//		int unitId = attack->getControllingUnitId();
 //           bool tempIsCollided = validateAttack(attackSph, unitId, collidedSpheres, currBucket);
	//		if(!isCollided)
	//			isCollided = tempIsCollided;
 //       }
 //   }

	//cout << "SPHERES CHECKED " << checkCount << endl;

	if(isCollided)
	{
		vector<Sphere*>::iterator it = collidedSpheres.begin();
		for(; it < collidedSpheres.end(); it++)
		{
			Sphere* tempSph = *it;
			units.push_back(tempSph->currUnit);
		}
	}  

	releaseBucketsLock();
    
    return isCollided;
}

//bool CollisionHandler::validateAttack(Sphere* collidingSphere, vector<Sphere*>& retSpheres)
//{	
//    map<int, Sphere *>::iterator it = spheres.begin();
//
//    for(;it != spheres.end();it++)
//    {
//        Sphere derp = *(*it).second;
//        if(collidingSphere->intersect(derp))
//            retSpheres.push_back((*it).second);
//    }
//    return !(retSpheres.empty());
//}

bool CollisionHandler::validateAttack(Sphere* collidingSphere, int unitId, vector<Sphere*>& retSpheres, map<int, Sphere*> currBucket)
{	
	int beforeSphCount = retSpheres.size();
    map<int, Sphere *>::iterator it = currBucket.begin();

    for(;it != currBucket.end();it++)
    {
		// ignore the unit that fired the attack object.
		if(it->first == unitId)
			continue;

        Sphere derp = *(*it).second;
        if(collidingSphere->intersect(derp))
            retSpheres.push_back((*it).second);
    }
	return (beforeSphCount < retSpheres.size());
}

Sphere* CollisionHandler::findSphere(int id)
{
    return (*spheres.find(id)).second;
}

// negative return 
int CollisionHandler::GetAccessibleUnits(int unitId, int playerId){
    std::map<int,Sphere*>::iterator spark = spheres.find(unitId);
    (*spark).second->radius *= -1; // convert to positive for collision
    map<int, Sphere *>::iterator it = spheres.begin();

    int collision = NO_COLLISION;
    for(;it != spheres.end(); it++){     
        if (spark->first == it->first)
            continue;

		// spark not allowed to enter enemy units.
		int targetPlayerId = it->second->currUnit->getControllingPlayerId();
		if(targetPlayerId != playerId && targetPlayerId != NEUTRAL_UNIT)
			continue;

        if( (*spark).second->intersect(*(*it).second))
        {
            collision = (*it).first;
            break;
        }            
    }

    (*spark).second->radius *= -1;// convert to negative so no more collision
    return collision;
}

void CollisionHandler::acquireBucketsLock() {
    //if (debug)
    //    cerr << "acquiring bucket lock... ";
    WaitForSingleObject(H_bucketsMutex, INFINITE); 
    //if (debug)
    //    cerr << "acquired!" << endl;
}

void CollisionHandler::releaseBucketsLock() {
    //if (debug)
    //    cerr << "releasing bucket lock... ";
    ReleaseMutex(H_bucketsMutex);
    //if (debug)
    //    cerr << "released!" << endl;
}