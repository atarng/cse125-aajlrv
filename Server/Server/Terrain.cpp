/***
 * Terrain.cpp
 * Author: Alfred Tarng
 * Purpose: Stripped down version of the Terrain, for server to calculate
 *          position information for the units on the terrain.
 */

#include "Terrain.h"
#include <dxgi.h>

#include <math.h>
#include <iostream>

using namespace std;

Terrain::Terrain()
{
	m_heightMap = 0;

    maxHeight = 0;
}
Terrain::Terrain(const Terrain& other)
{
}
Terrain::~Terrain()
{
}

bool Terrain::Initialize(char* heightMapFilename)
{
    cerr << "[DEBUG] Server: Loading Server Height Map... ";
	bool result = true;

	// Load in the height map for the terrain.
	result = LoadHeightMap(heightMapFilename);
	if(!result)
	{
        cerr << "FAILED!" << endl;
		return false;
	}

	// Normalize the height of the height map.
	NormalizeHeightMap();

    cerr << "Done!" << endl;
	return result;
}

/***
 * Loads the actual height map.
 */
bool Terrain::LoadHeightMap(char* filename)
{
	FILE* filePtr;
	int error;
	unsigned int count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	int imageSize, index;
	unsigned char* bitmapImage;
	unsigned char height;

	// Open the height map file in binary.
	error = fopen_s(&filePtr, filename, "rb");
	if(error != 0)
	{
        cerr << "[ERROR] Server: Open the height map file in binary." << endl;
		return false;
	}

	// Read in the file header.
	count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);
	if(count != 1)
	{
        cerr << "[ERROR] Server: Read in the file header." << endl;
		return false;
	}

	// Read in the bitmap info header.
	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);
	if(count != 1)
	{
        cerr << "[ERROR] Server: Read in the bitmap info header." << endl;
		return false;
	}

	// Save the dimensions of the terrain.
	m_terrainWidth  = bitmapInfoHeader.biWidth;
	m_terrainHeight = bitmapInfoHeader.biHeight;

	// Calculate the size of the bitmap image data.
	imageSize = m_terrainWidth * m_terrainHeight * 3;

	// Allocate memory for the bitmap image data.
	bitmapImage = new unsigned char[imageSize];
	if(!bitmapImage)
	{
        cerr << "[ERROR] Server: Allocate memory for the bitmap image data." << endl;
		return false;
	}

	// Move to the beginning of the bitmap data.
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread(bitmapImage, 1, imageSize, filePtr);
	if(count != imageSize)
	{
        cerr << "[ERROR] Server: Read in the bitmap image data." << endl;
		return false;
	}

	// Close the file.
	error = fclose(filePtr);
	if(error != 0)
	{
        cerr << "[ERROR] Server: Close the file.." << endl;
		return false;
	}

	// Create the structure to hold the height map data.
	m_heightMap = new HeightMapType[m_terrainWidth * m_terrainHeight];
	if(!m_heightMap)
	{
        cerr << "[ERROR] Server: Read in the bitmap image data." << endl;
		return false;
	}

	// Initialize the position in the image data buffer.
	int k=0;
	// Read the image data into the height map.
	for(int j=0; j < m_terrainHeight; j++)
	{
		for(int i=0; i < m_terrainWidth; i++)
		{
			height = bitmapImage[k];
			
			index = (m_terrainWidth * j) + i;

			m_heightMap[index].x = (float)i;
			m_heightMap[index].y = (float)height;
			m_heightMap[index].z = (float)j;

			k += 3;
		}
	}

	// Release the bitmap image data.
	delete [] bitmapImage;
	bitmapImage = 0;

	return true;
}
void Terrain::NormalizeHeightMap()
{
	for(int j=0; j<m_terrainHeight; j++)
	{
		for(int i=0; i<m_terrainWidth; i++)
		{
			m_heightMap[(m_terrainHeight * j) + i].y /= 5.0;
            if ( maxHeight < m_heightMap[(m_terrainHeight * j) + i].y )
            {
                maxHeight = m_heightMap[(m_terrainHeight * j) + i].y;
            }
		}
	}
	return;
}

/***
 * wxh
 */
void Terrain::getHeightAt(float& x, float& y, float& z)
{
    int constantScale = 5;
    int ux =  ((int)ceil(x/constantScale));  int uz =  ((int)ceil(z/constantScale));
    int lx = ((int)floor(x/constantScale));  int lz = ((int)floor(z/constantScale));
    float lly,uly, luy, uuy;
    if (ux < 0 || uz < 0 || uz >= m_terrainHeight || ux >= m_terrainWidth||
        lx < 0 || lz < 0 )
    {
        y = (y - 5);
        return;
    }

    int index_U = ( (m_terrainWidth * uz) + ux );
    int index_L = ( (m_terrainWidth * lz) + lx );

    uuy = m_heightMap[ index_U ].y;
    lly = m_heightMap[ index_L ].y;

    // Calculate Weights of point influence for
    // wl : weight of lower
    // wu : weight of upper
    float wl = sqrt((pow((x - lx),2) + pow((z - lz),2)));
    float wu = sqrt((pow((ux - x),2) + pow((uz - z),2)));
    
    float temp = 0;
    
    if (wl + wu == 0)
    {
        wl = 1.0f;
        wu = 0.0f;
    }else
    {
        temp = wl / (wl + wu);
        wu   = wu / (wl + wu);
        wl   = temp;
    }

    //cerr << "wu * upperY: " << (wu * m_heightMap[ index_U ].y) << endl;
    //cerr << "wl * lowerY: " << (wl * m_heightMap[ index_L ].y) << endl;

    //y = m_heightMap[ index_U ].y;
    y = (wu * m_heightMap[ index_U ].y) + (wl * m_heightMap[ index_L ].y);
}

void Terrain::ShutdownHeightMap()
{
	if(m_heightMap)
	{
		delete [] m_heightMap;
		m_heightMap = 0;
	}

	return;
}