//MeshManager.h
#ifndef _MODEL_LOADER_H_
#define _MODEL_LOADER_H_

#include <vector>

class Model;

class ModelLoader
{
private:
    struct ModelInfo
    {
        int id;             // may be redundant
        float boundingRadius;
        int shaderID;       // may be redundant
    };

    struct MeshSelecter
    {
        int TERRAIN_OBJECT;
        int LASER;
        int ELECTRICSPARK;
        int PROBE;
		int AOE;
        int METEOR;
        int BALLS;

        int TEST_DROID;
        int TEST_SKYBOX;
        int TEST_CUBE;
        int TEST_ELECTRICTHING;

        int TEST_ANIM_DAE;
        int TEST_SKELETAL_ANIM_DAE;
        int TEST_COLLISION_SPHERE;

    } meshSelector;

    std::vector<ModelInfo> modelInformation;

public:
    ModelLoader();
    ~ModelLoader();

    bool Initialize();
    inline MeshSelecter GetMeshSelector(){ return meshSelector; };
    ModelInfo GetModelInfo(int index);
};

#endif