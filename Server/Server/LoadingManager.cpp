//LoadingManage.cpp

#include <LoadingManager.h>
#include "FileParser.h"
#include "Log.h"

//must initialize default variables here
int	LoadingManager::numRobots = 0;
int	LoadingManager::addDoomRobots = 0;

int	LoadingManager::robotHealth = 0;
int	LoadingManager::probeHealth = 0;
int	LoadingManager::doomHealth = 0;
int	LoadingManager::sentryHealth = 0;
int	LoadingManager::fanHealth = 0;
int	LoadingManager::sparkHealth = 0;
int	LoadingManager::requiredNumOfPlayers = 0;

float LoadingManager::probeSpeed = 0.0f;
float LoadingManager::doomSpeed = 0.0f;
float LoadingManager::sentrySpeed = 0.0f;
float LoadingManager::fanSpeed = 0.0f;
float LoadingManager::sparkSpeed = 0.0f;

float LoadingManager::probeCharge = 0.0f;
float LoadingManager::sentryCharge = 0.0f;
float LoadingManager::fanCharge = 0.0f;
float LoadingManager::discharge = 0.0f;

float LoadingManager::healthLostPerSecond = 0.0f;
float LoadingManager::robotDeathPenalty = 0.0f;

float LoadingManager::healthGain = 0.0f;

int	LoadingManager::bulletDmg = 0;
int	LoadingManager::meleeDmg = 0;
int	LoadingManager::fanDmg = 0;

int	LoadingManager::bullet2Dmg = 0;
int	LoadingManager::aoeDmg = 0;
int	LoadingManager::meteorDmg = 0;

float LoadingManager::bulletSpeed = 0.0f;
float LoadingManager::fanBulletSpeed = 0.0f;

float LoadingManager::bulletMaxTime = 0.0f;
float LoadingManager::meleeMaxTime = 0.0f;
float LoadingManager::bullet2MaxTime = 0.0f;
float LoadingManager::aoeMaxTime = 0.0f;
float LoadingManager::meteorMaxTime = 0.0f;

float LoadingManager::gravity = 0.0f;

LoadingManager::LoadingManager(){}
LoadingManager::LoadingManager(std::string loadingFileName)
{
	Initialize(loadingFileName);
}
LoadingManager::~LoadingManager(){}

/***
 * Initialize(std::string loadingFileName)
 * 
 */
bool LoadingManager::Initialize(std::string loadingFileName)
{
    FileParser* parser = new FileParser(loadingFileName);

	numRobots = parser->ParseInt("numRobots");
	addDoomRobots = parser->ParseInt("addDoomRobots");
	
	robotHealth = parser->ParseInt("robotHealth");
	probeHealth = parser->ParseInt("probeHealth");
	doomHealth = parser->ParseInt("doomHealth");
	sentryHealth = parser->ParseInt("sentryHealth");
	fanHealth = parser->ParseInt("fanHealth");
	sparkHealth = parser->ParseInt("sparkHealth");
    requiredNumOfPlayers = parser->ParseInt("requiredNumOfPlayers");

	probeSpeed = parser->ParseFloat("probeSpeed");
	sentrySpeed = parser->ParseFloat("sentrySpeed");
	doomSpeed = parser->ParseFloat("doomSpeed");
	fanSpeed = parser->ParseFloat("fanSpeed");
	sparkSpeed = parser->ParseFloat("sparkSpeed");

	probeCharge = parser->ParseFloat("probeCharge");
	sentryCharge = parser->ParseFloat("sentryCharge");
	fanCharge = parser->ParseFloat("fanCharge");
	discharge = parser->ParseFloat("discharge");

	healthLostPerSecond = parser->ParseFloat("healthLostPerSecond");
	robotDeathPenalty = parser->ParseFloat("robotDeathPenalty");

	healthGain = parser->ParseFloat("healthGain");

	bulletDmg = parser->ParseInt("bulletDmg");
	meleeDmg = parser->ParseInt("meleeDmg");
	fanDmg = parser->ParseInt("fanDmg");

	bullet2Dmg = parser->ParseInt("bullet2Dmg");
	aoeDmg = parser->ParseInt("aoeDmg");
	meteorDmg = parser->ParseInt("meteorDmg");

	bulletSpeed = parser->ParseFloat("bulletSpeed");
	fanBulletSpeed = parser->ParseFloat("fanBulletSpeed");

	bulletMaxTime = parser->ParseFloat("bulletMaxTime");
	meleeMaxTime = parser->ParseFloat("meleeMaxTime");
	bullet2MaxTime = parser->ParseFloat("bullet2MaxTime");
	aoeMaxTime = parser->ParseFloat("aoeMaxTime");
	meteorMaxTime = parser->ParseFloat("meteorMaxTime");

	gravity = parser->ParseFloat("gravity");

	parser->Close();
	delete parser;

	return true;
}