#include "stdafx.h"

#include <ws2tcpip.h>
#include <iostream>

#include "Server.h"
#include "Unit.h"
#include "Robot.h"
#include "Bullet.h"

#include "Timer.h"
#include "Log.h"

#include "Event.h"
#include "MovementEvent.h"
#include "FireEvent.h"
#include "AssignEvent.h"
#include "EnterEvent.h"
#include "ExitEvent.h"

#include "GameLogicStructure.h"
#include "LoadingManager.h"

#pragma comment(lib, "Ws2_32.lib")

#define VERSION_2_2 0x0202
#define MAX_CLIENTS 5
#define MAX_MESSAGE_SIZE 4096
#define MAX_INPUT_SIZE 1024
#define LOCK_SUCCESS 1
#define LOCK_ERROR -1
#define CREATE_THREAD_SUCCESS 1
#define CREATE_THREAD_ERROR -1
#define SERVER_LOOP_INTERVAL 20
#define PORT 7700

using namespace std;

Server * Server::serverInstance = NULL;
bool Server::gQuitFlag = false;

Server::Server(unsigned short port) {
    this->port = port;

    this->playerId = 1;
    this->unitNetworkId = 1;
    availableID.clear();

	//clear out the master set
	FD_ZERO(&(this->masterSet));

    this->gamelogicstructure = new GameLogicStructure();

    this->initMode = false;
    this->animTimer = new Timer();
    this->numberOfClients = 0;

	LoadingManager::Initialize("resource/serverconfig.txt");
    requiredNumOfPlayers = LoadingManager::requiredNumOfPlayers;
}

Server::~Server() {
    delete gamelogicstructure;
    delete animTimer;
}

Server * Server::Instance()
{
    if (!serverInstance) {
        serverInstance = new Server(PORT);
    }

    return serverInstance;
}

/**
 * Initializes the server.
 */
int Server::initializeServer() {
    cerr << "[DEBUG] Starting server..." << endl;
    Log::Instance()->Print("Starting server...");

    this->serverSocket = StartServerListening();

    if (serverSocket == INVALID_SOCKET) {
        cerr << "[ERROR] Server: Failed to start server!" << endl;
        return SERVER_START_ERROR;
    }
    
    if (InitializeLocks() == LOCK_ERROR || InitializeServerThreads() == LOCK_ERROR) {
        EndServer();
        return SERVER_START_ERROR;  
    }

    gamelogicstructure->InitializeWorld();

    return SERVER_START_SUCCESS;
}

/**
 * Runs the main server loop.
 */
void Server::Run() {    
    while (true) {
        acquireErrorLock();
        if (gQuitFlag) {
            releaseErrorLock();
            break;
        }
        releaseErrorLock();

        acquireFDLock();
        FD_SET currentSet = masterSet; 
        releaseFDLock();

        if (currentSet.fd_count != 0) 
            checkClientMessages(currentSet); 
    }
}

/**
 * Handles messages from the client to server.
 * @param TempSet the total list of sockets
 */
bool Server::checkClientMessages(FD_SET & currentSet) {
    timeval waitTime; 
    waitTime.tv_sec = 0; 
    waitTime.tv_usec = 0; 

    // select the total number of sockets that are ready
    int numOfSockets = select(currentSet.fd_count, &currentSet, NULL, NULL, &waitTime); 
         
    if (numOfSockets == 0) 
        return false; 

    if (numOfSockets == SOCKET_ERROR) {
        cerr << "[ERROR] Server: Error in one of the established sockets (" 
             << WSAGetLastError() << ")" << endl;
        return false;
    }

    for (unsigned int i = 0; i < currentSet.fd_count; i++) {
        SOCKET clientSocket = currentSet.fd_array[i];

        char buffer[MAX_MESSAGE_SIZE];

        unsigned long messageSize;
        int nBytes = recv(clientSocket, (char *) &messageSize, sizeof(messageSize), 0);

        if (nBytes == SOCKET_ERROR) {
            int error = WSAGetLastError();
                
            // check if the remote host has forcibly terminated the connection
            if (error == WSAECONNRESET) {
                handleClientDisconnect(clientSocket);
                continue;
            } else {
                handleUnexpectedError("[ERROR] Server: unexpected error...", error);
                return false;
            }
        }

        // if the user exits and closes socket, thus sending 0 bytes
        if (nBytes == 0) {
            handleClientDisconnect(clientSocket);
            continue;
        }

        // the first message received is the size of the message 
        messageSize = ntohl(messageSize);

        if (messageSize >=  MAX_MESSAGE_SIZE) {
            cerr << "[ERROR] Server: Received message from client greater than maximum message size" << endl;
            return false;
        }

        int totalBytesReceived = 0;
        char * bufferPtr = &buffer[0];
        int bytesLeftToReceive = messageSize;

        do {
            nBytes = recv(clientSocket, bufferPtr, bytesLeftToReceive, 0);

            if (nBytes == SOCKET_ERROR) {
                handleUnexpectedError("[ERROR] Server: failed to receive message from client", SOCKET_ERROR);
                return false;
            }

            totalBytesReceived += nBytes;
            bufferPtr += nBytes;
            bytesLeftToReceive -= nBytes;
        } while (totalBytesReceived < messageSize);

		if ( totalBytesReceived != messageSize ) 
			cerr << "[WARNING] Server: Byte received not equal to advertised message size "<< endl;

        // null terminate the message
        buffer[messageSize - 1] = '\0';
        updateInternalUnitsFromMessage(buffer, messageSize);
    }

    return true;
}

/**
 * Updates the internal units based on the specified serialized string.
 * @param serializedStr  the serialized string
 * @param        length  the length of the serialized string
 */
void Server::updateInternalUnitsFromMessage(char * serializedStr, size_t length) {
    Event * event = Event::createEvent(serializedStr, length);

    if (event == NULL) {
        cerr << "[ERROR] Server: could not process event message" << endl;
        Log::Instance()->PrintErr("[ERROR] Server: could not process event message");
        return;
    }

    if (event->isFire()) {
        FireEvent fireEvent = *(FireEvent *) event;
        gamelogicstructure->ProcessFireEvent(fireEvent);
    } else if (event->isMovement()) {
        MovementEvent moveEvent = *(MovementEvent *) event;
        gamelogicstructure->ProcessMoveEvent(moveEvent);
    } else if (event->isEnter()) {
        EnterEvent enterEvent = *(EnterEvent *) event;
        gamelogicstructure->ProcessEnterEvent(enterEvent);
    } else if (event->isExit()) {
        ExitEvent exitEvent = *(ExitEvent *) event;
        gamelogicstructure->ProcessExitEvent(exitEvent);
    }

    delete event;
}

/**
 * Initialize the mutexes.
 */
int Server::InitializeLocks() {
    H_fdMutex = CreateMutex(NULL, false, NULL);

    if (H_fdMutex == NULL) {
        cerr << "[ERROR] Server: Could not create lock for sockets!" << endl;
        return LOCK_ERROR;
    }
       
    H_errorMutex = CreateMutex(NULL, false, NULL);

    if (H_errorMutex == NULL) {
        cerr << "[ERROR] Server: Could not create lock for error flag!" << endl;
        return LOCK_ERROR;
    }

    H_updateUnitsLock = CreateMutex(NULL, false, NULL);

    if (H_updateUnitsLock == NULL) {
        cerr << "[ERROR] Server: Could not create lock for unit update flag!" << endl;
        return LOCK_ERROR;
    }

    H_sendLock = CreateMutex(NULL, false, NULL);

    if (H_sendLock == NULL) {
        cerr << "[ERROR] Server: Could not create send message lock!" << endl;
        return LOCK_ERROR;
    }

	return LOCK_SUCCESS;
}

/**
 * Initializes the threads for this server.
 */
int Server::InitializeServerThreads() {

    //thread for accepting connection
    int thread; 
    H_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) startAcceptThread, this, 0, (LPDWORD) &thread);
    
    if (H_thread == NULL) {
        cerr << "[ERROR] Server: Could not create thread for connection accepting!" << endl;
        Log::Instance()->PrintErr("[ERROR] Server: Could not create thread for connection accepting!");
        EndServer();
        return CREATE_THREAD_ERROR;
    }

	// thread for handling server inputs
    int inputThread;
    H_InputThread =
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) startHandleInputThread, this, 0, (LPDWORD) &inputThread);

    if (H_InputThread == NULL) {
        cerr << "[ERROR] Server: Could not create thread for handling exit input!" << endl;
        Log::Instance()->PrintErr("[ERROR] Server: Could not create thread for handling exit input!");
        EndServer();
        return CREATE_THREAD_ERROR;
    }

    // thread for updating clients
	int updateClientThread;
    H_UpdateClientThread =
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) startUpdateAllClientsThread, this, 0, (LPDWORD) &updateClientThread);

    if (H_UpdateClientThread == NULL) {
        cerr << "[ERROR] Server: Could not create thread for updating clients!" << endl;
        Log::Instance()->PrintErr("[ERROR] Server: Could not create thread for updating clients!");
        EndServer();
        return CREATE_THREAD_ERROR;
    }

    // TODO create proper mechanism to wait for thread startup
    Sleep(100);

    return CREATE_THREAD_SUCCESS;
}

void Server::startAcceptThread(void * ptr) {
    ((Server *) ptr)->AcceptThread();
}

void Server::startUpdateAllClientsThread(void * ptr) {
    ((Server *) ptr)->updateAllClients();
}

void Server::startHandleInputThread(void * ptr) {
    ((Server *) ptr)->HandleInput();
}

/**
 * Handles input in the console window of the server. Checks for the "exit" command
 * and exits the server upon receiving this command.
 *
 * Supports:
 * exit: closes the server
 * spawnMore: spawns 10 more robots.
 * clear: to be implemented
 *
 * @param serverSocket the socket this server accepts connections from 
 */
void Server::HandleInput() {
    char buffer[MAX_INPUT_SIZE];

    while (true) {
        gets_s(buffer, MAX_INPUT_SIZE);
        if (strcmp(buffer, "exit") == 0)
			break;
        else if (strcmp(buffer, "spawn") == 0)
            gamelogicstructure->addDummyRobots(10);
		else if (strcmp(buffer, "doom") == 0)
			gamelogicstructure->addDoomRobots(20);
        else if (strcmp(buffer, "debugOn") == 0)
            gamelogicstructure->turnOnDebug();
        else if (strcmp(buffer, "debugOff") == 0)
            gamelogicstructure->turnOffDebug();
        else if (strcmp(buffer, "help") == 0) {
            cerr << "exit:\tTerminates the server" << endl;
            cerr << "spawn:\tSpawns Ten New Units." << endl;
        } 
    }

    EndServer();
    //exit(0);
}

/**
 * Accepts all incoming connections and adds them to the FD_SET masterSet.
 * @param serverSocket the socket to accept connections from
 */
void Server::AcceptThread() {
    while (true) 
	{
        acquireErrorLock();
        if (gQuitFlag) {
            releaseErrorLock();
            break;
        }
        releaseErrorLock();

        SOCKET clientSocket = accept(serverSocket, NULL, NULL); 
        
        acquireErrorLock();
        if (gQuitFlag) {
            releaseErrorLock();
            break;
        }
        releaseErrorLock();

		// SOCKET error
        if (clientSocket == INVALID_SOCKET) {
            cerr << "[ERROR] Server: Error accepting socket!" << endl;
            continue;
        } 

        cout << "[DEBUG] Client attempting to connect on socket [" << clientSocket << "]" << endl;
        Log::Instance()->Print("[DEBUG] Client attempting to connect on socket [%d]", clientSocket);

        int newPlayerId = getNextPlayerId();

        if (!sendPlayerIdToClient((char *) &newPlayerId, sizeof(newPlayerId), clientSocket))
            continue;

        gamelogicstructure->acquirePlayersRemainingLock();
        gamelogicstructure->playersRemaining.push_back(newPlayerId);
        gamelogicstructure->releasePlayersRemainingLock();

        int networkId = getNextUnitId();

        AssignEvent assignEvent(newPlayerId, networkId);
        string eventSerializeStr = assignEvent.Serialize();

        if (sendMessageToClient(eventSerializeStr, clientSocket)) {
		    gamelogicstructure->addUnitToMap(newPlayerId, networkId, SPARK);
            
            acquireFDLock();
	        FD_SET(clientSocket, &masterSet); 
            releaseFDLock();

		    cerr << "[DEBUG] Server: Client connected on socket [" << clientSocket << "]! " << endl;
            Log::Instance()->Print("[DEBUG] Server: Client connected on socket [%d]! ", clientSocket);     
        }

        acquireUpdateUnitLock();
        initMode = true;
        numberOfClients++;
        releaseUpdateUnitLock();
    }
}

/**
 * Returns the next available player id to use for a client.
 * @return the player id to use
 */
int Server::getNextPlayerId() {
    return playerId++;
}

/**
 * Returns the next available network id to use for a unit.
 * @return the network id to use
 */
int Server::getNextUnitId() {
    return unitNetworkId++;

    /*if (availableID.empty()) {
        return unitNetworkId++;
    } else {
        int id = *(availableID.end()-1);
        availableID.pop_back();
        return id;
    }*/
}

/**
 * Starts a server and creates a socket that binds to the specified port. 
 * The created socket is returned. 
 * @param port the port to bind the created socket to
 * @return the SOCKET that was created
 */
SOCKET Server::StartServerListening() {
    WSAData wsaData;

    if ((WSAStartup(VERSION_2_2, &wsaData)) == SOCKET_ERROR) {
        cerr << "[ERROR] Server: Winsock failed to start!" << endl;
        return INVALID_SOCKET;
    }

    if (wsaData.wVersion != VERSION_2_2) {
        cerr << "[ERROR] Server: Winsock version incorrect!" << endl;
        WSACleanup(); 
        return INVALID_SOCKET;
    }

    SOCKET mySocket = socket(AF_INET, SOCK_STREAM, 0);

    if (mySocket == INVALID_SOCKET) {
        cerr << "[ERROR] Server: Failed to create socket!" << endl;
        WSACleanup();
        return INVALID_SOCKET;
    }

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = INADDR_ANY;

    if (bind(mySocket, (LPSOCKADDR) &server, sizeof(server)) == SOCKET_ERROR) {
        cerr << "[ERROR] Server: Socket failed to bind to port " << port << "!\n";
        CloseSocket(mySocket);
        WSACleanup();
        return INVALID_SOCKET;
    }

    if (listen(mySocket, MAX_CLIENTS) == SOCKET_ERROR) {
        cerr << "[ERORR] Server: Failed to establish listening socket!" << endl;
        CloseSocket(mySocket);
        WSACleanup();
        return INVALID_SOCKET;
    }

    cerr << "[DEBUG] Server started!" << endl;
    return mySocket;
}

/**
 * Shuts down the server instance and cleans up all handles and sockets.
 * @param s the socket to close
 */
void Server::EndServer() {
    cerr << "[DEBUG] Server sending shut down message..." << endl;
    Log::Instance()->Print("[DEBUG] Server sending shut down message...");

    acquireErrorLock();
    gQuitFlag = true;
    releaseErrorLock();
}

void Server::CleanUp() {
    cerr << "[DEBUG] Cleaning up resources..." << endl;
    Log::Instance()->Print("[DEBUG] Cleaning up resources...");

    // close threads
    CloseHandle(H_thread);
    CloseHandle(H_InputThread);
    CloseHandle(H_UpdateClientThread);

    // close mutexes
    CloseHandle(H_fdMutex);
    CloseHandle(H_errorMutex);
    CloseHandle(H_sendLock);
    CloseHandle(H_updateUnitsLock);

    CloseSocket(serverSocket);
    WSACleanup();

    cerr << "[DEBUG] Server shut down." << endl;
    Log::Instance()->Print("[DEBUG] Server shut down.");
}

/**
 * Closes the specified socket.
 * @param s the socket to close
 */
void Server::CloseSocket(SOCKET s) {
    shutdown (s, SD_SEND);  
    closesocket (s);
}

/**
 * Update the clients with all the existing units in the world
 */
void Server::updateAllClients() {
    cerr << "[DEBUG] Server: required of players: " << requiredNumOfPlayers << endl;

    while(true) {
        acquireUpdateUnitLock();

        if(numberOfClients < requiredNumOfPlayers) {
            releaseUpdateUnitLock();
            continue;
        } else {
            releaseUpdateUnitLock();
            gamelogicstructure->gameTimer->Update();
            break;
        }
    }

	while(true) {
        acquireErrorLock();
        if (gQuitFlag) { 
            releaseErrorLock();
            break;
        }
        releaseErrorLock();
        
		DWORD startTime = GetTickCount();

        gamelogicstructure->ExecuteGameLogic();
        sendUnits();

		DWORD endTime = GetTickCount();
        DWORD sleepTime = SERVER_LOOP_INTERVAL - (endTime - startTime);

		if (sleepTime <= SERVER_LOOP_INTERVAL)
			Sleep(sleepTime);
	}
}

/**
 * Helper function used to sent all units.
 * @param initMode When in initMode, all units will be sent. Otherwise, only
 *		  modified units will be sent.
 */
void Server::sendUnits() { // bool initMode) {
    acquireFDLock();
	FD_SET currentSet = masterSet; 
    releaseFDLock();

	if (currentSet.fd_count == 0) 
        return;

	string serializeStr;
    serializeStr.clear();

    size_t totalBytesSend = 0;
        
	// serialize all existing units
    gamelogicstructure->acquireUnitLock();

	for (map<int, Unit *>::iterator iter = gamelogicstructure->existingUnits.begin();
		iter != gamelogicstructure->existingUnits.end(); iter++) {
		Unit * currentUnit = iter->second;

        if (!currentUnit->hasPerformedAction() && currentUnit->getCurrentAnimType() != ANIM_DEFAULT)
            currentUnit->setCurrentAnimType(ANIM_DEFAULT);//, animTimer->Update());
        else
			currentUnit->setPerformedAction(false, GameLogicStructure::universalDeltaTime );

		if (currentUnit->isModified() || initMode) {
			string unitSerStr = currentUnit->Serialize();

            totalBytesSend += sizeof(unitSerStr);

			if (serializeStr.length() + unitSerStr.length() >= MAX_MESSAGE_SIZE) {
				sendMessageToClient(serializeStr, currentSet);
				serializeStr.clear();
			}

			serializeStr += unitSerStr;

			currentUnit->setModified(false);
		}
	}

    gamelogicstructure->releaseUnitLock();

    // serialize all attack objects
    if (gamelogicstructure->isDebugOn())
        cerr << "sendUnits ";
    gamelogicstructure->acquireAttackLock();

	for (map<int, AttackObject *>::iterator iter = gamelogicstructure->existingAttackObjects.begin();
		iter != gamelogicstructure->existingAttackObjects.end(); iter++) {
		AttackObject * currentUnit = iter->second;

		if (currentUnit->isModified() || initMode) {
			string unitSerStr = currentUnit->Serialize();

            totalBytesSend += sizeof(unitSerStr);

			if (serializeStr.length() + unitSerStr.length() >= MAX_MESSAGE_SIZE) {
				sendMessageToClient(serializeStr, currentSet);
				serializeStr.clear();
			}

			serializeStr += unitSerStr;

			currentUnit->setModified(false);
		}
	}

    if (gamelogicstructure->isDebugOn())
        cerr << "sendUnits ";
    gamelogicstructure->releaseAttackLock();

	if (serializeStr.length() != 0) {
		sendMessageToClient(serializeStr, currentSet);
    }

	//acquire the set again to see if new client didn't receive init message
	acquireFDLock();
	FD_SET newestSet = masterSet; 
    releaseFDLock();

	//if the set got updated in the middle of this function, init mode wont
	//be set to false
	if (newestSet.fd_count == currentSet.fd_count) {
		acquireUpdateUnitLock();
		initMode = false;
		releaseUpdateUnitLock();
	}

    //cerr << "sending over " << totalBytesSend << " bytes" << endl;
}



/**
 * Sends the specified message to the client on the specified socket.
 * @param       message  the message to send
 * @param        length  the length of the message
 * @param  clientSocket  the client socket
 * @return true if the send was successful
 */
bool Server::sendPlayerIdToClient(char * message, size_t length, SOCKET clientSocket) {
	int nBytes = send(clientSocket, message, length, 0);

	if (nBytes == SOCKET_ERROR) {
		cerr << "[ERROR] Server: failed to send network id!" << endl;
        Log::Instance()->PrintErr("Server: failed to send player id to %d!", clientSocket);
        return false;
    } else
        return true;
}

/**
 * Sends the specified string to the client.
 * @param serializeStr  the string to send
 */
void Server::sendMessageToClient(string serializeStr) {
    acquireFDLock();
	FD_SET currentSet = masterSet; 
    releaseFDLock();

    sendMessageToClient(serializeStr, currentSet);
}

/**
 * Sends the specified string to the client.
 * @param serializeStr  the string to send
 * @param   currentSet  the set of client sockets
 */
void Server::sendMessageToClient(string serializeStr, FD_SET & currentSet) {
    acquireSendLock();

	int messageSize = serializeStr.length() + 1;

    //cerr << "length: " << messageSize << "; sizeof: " << sizeof(serializeStr.c_str()) + 1 << endl;

	char * message = new char[messageSize];
	strncpy_s(message, messageSize, serializeStr.c_str(), messageSize);
	//message[messageSize-1] = '\0';

	// change byte order and include null character
	unsigned long messageSizeToSend = htonl(messageSize);

	if (messageSize > MAX_MESSAGE_SIZE) {
		cerr << "[WARNING] Server: A message size greater than the maximum message size" << endl;
        Log::Instance()->Print("[WARNING] Server: A message size greater than the maximum message size");
    }

	// update to all clients
	for (unsigned int i = 0; i < currentSet.fd_count; i++) {
		SOCKET currentSocket = currentSet.fd_array[i];

		// send the size to client first
		int nBytes = send(currentSocket, (char *) &messageSizeToSend,
							sizeof(messageSizeToSend), 0);
		bool error = checkSocketError(nBytes, currentSocket);

        // send over the serialized string
		if (!error) {
			nBytes = send(currentSocket, message, messageSize, 0);
			checkSocketError(nBytes, currentSocket);
		}
	}

	// clean up
	delete [] message;

    releaseSendLock();
}

/**
 * Sends the specified string to the client on the specified socket.
 * @param serializeStr  the string to send
 * @param clientSocket  the socket to send to
 * @return true if no error
 */
bool Server::sendMessageToClient(std::string serializeStr, SOCKET clientSocket) {
    acquireSendLock();

    int messageSize = serializeStr.length() + 1;
	char * message = new char[messageSize];
	strncpy_s(message, messageSize, serializeStr.c_str(), messageSize);
	//message[messageSize-1] = '\0';

	// change byte order and include null character
	unsigned long messageSizeToSend = htonl(messageSize);

	if (messageSize > MAX_MESSAGE_SIZE) {
		cerr << "[WARNING] Server: A message size greater than the maximum message size" << endl;
    }

	// send the size to client first
	int nBytes = send(clientSocket, (char *) &messageSizeToSend,
						sizeof(messageSizeToSend), 0);
	bool error = checkSocketError(nBytes, clientSocket);

    // send over the serialized string
	if (!error) {
		nBytes = send(clientSocket, message, messageSize, 0);
		checkSocketError(nBytes, clientSocket);
	}

	// clean up
	delete [] message;

    releaseSendLock();
    return !error;
}

/**
 * Check if there is a socket error or if the client closed the socket.
 * @param nBytes the number of bytes transmitted
 * @param clientSocket the socket of the client
 * @return true if there is an error
 */
bool Server::checkSocketError(int nBytes, SOCKET clientSocket) {
	if (nBytes == SOCKET_ERROR) {
        int error = WSAGetLastError();
                
        // check if the remote host has forcibly terminated the connection
        if (error == WSAECONNRESET) {
            handleClientDisconnect(clientSocket);
        } 

		return true;	
    } else if (nBytes == 0) {
        handleClientDisconnect(clientSocket);
		return true;	
	} else
		return false;	
}

/**
 * Handles an unexpected error. Terminates the server in the case of an unexpected
 * error.
 */
void Server::handleUnexpectedError(string errorMessage, int errorCode) {
	//WaitForSingleObject(H_errorMutex, INFINITE);
    cerr << errorMessage << " code (" << errorCode << ")" << endl;
    Log::Instance()->Print(errorMessage.c_str());
    //gQuitFlag = true;
    //ReleaseMutex(H_errorMutex); 
}

/**
 * Handles the case of a client diconnect.
 * @param clientSocket  the socket that the client is connected on
 */
void Server::handleClientDisconnect(SOCKET clientSocket) {
    acquireFDLock();
	FD_CLR(clientSocket, &masterSet);       
    releaseFDLock();

	CloseSocket(clientSocket);   

	cerr << "[DEBUG] Server: Client on [" << clientSocket << "] has disconnected" << endl;
    Log::Instance()->Print("Server: Client on [%d] has disconnected", clientSocket);
}

void Server::acquireSendLock() {
    WaitForSingleObject(H_sendLock, INFINITE);  
}

void Server::releaseSendLock() {
    ReleaseMutex(H_sendLock);  
}

void Server::acquireUpdateUnitLock() {
    WaitForSingleObject(H_updateUnitsLock, INFINITE);  
}

void Server::releaseUpdateUnitLock() {
    ReleaseMutex(H_updateUnitsLock);  
}

void Server::acquireFDLock() {
    WaitForSingleObject(H_fdMutex, INFINITE); 
}

void Server::releaseFDLock() {
    ReleaseMutex(H_fdMutex);  
}

void Server::acquireErrorLock() {
    WaitForSingleObject(H_errorMutex, INFINITE); 
}

void Server::releaseErrorLock() {
    ReleaseMutex(H_errorMutex);  
}