#ifndef _TERRAINOBJECT_H_
#define _TERRAINOBJECT_H_

////////////////////////////////////////////////////////////////////////////////
// Class name: Terrain
////////////////////////////////////////////////////////////////////////////////
class Terrain
{
private:
	struct HeightMapType 
	{ 
		float x, y, z;
	};

    struct Vec3
    {
        float x, y, z;
    };

    HeightMapType* m_heightMap;                   // kinda shared
    int m_terrainWidth, m_terrainHeight;
    float maxHeight;

    bool LoadHeightMap(char*);
	void NormalizeHeightMap();
	void ShutdownHeightMap();

    bool LoadTexture(char*); // ###

public:
	Terrain();
	Terrain(const Terrain&);
	~Terrain();

    bool Initialize(char*);
	void Shutdown();

    void getHeightAt(float& x, float& y, float& z);

	int GetIndexCount();
//    Vec3 SelectColor(float height, float maxHeight);
};

#endif