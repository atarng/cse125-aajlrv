#ifndef _C_HANDLER_H_
#define _C_HANDLER_H_

#include "sphere.h"
#include "vec3f.h"
#include <vector>
#include <map>

#include <MovementEvent.h>
#include <AttackObject.h>

#define NO_COLLISION	-1
#define BUCKET_SIZE		128
#define ARENA_DIAM		1280  // actual size of area
#define BUCKET_MAX		ARENA_DIAM/BUCKET_SIZE
#define ZERO_POS		90

using namespace std;

class CollisionHandler
{
private:
    std::map<int, Sphere *> spheres;
	std::map<int, Sphere *> attackSpheres;
	map<int, Sphere*>* buckets[BUCKET_MAX];
	bool isDelete;
    
	HANDLE H_bucketsMutex;

    bool validateMovement(Sphere* collidingSphere, D3DXVECTOR3 dir, map<int, Sphere*> nearbySpheres);
	bool validateMovement(Sphere* collidingSphere, D3DXVECTOR3 dir);
	bool validateAttack(Sphere* collidingSphere, vector<Sphere*>& retSpheres);
	bool validateAttack(Sphere* collidingSphere, int unitId, vector<Sphere*>& retSpheres, map<int, Sphere*> currBucket);
	void updateSphereBuckets(Sphere* sph, int id);
	void populateBucket(int row, int col, Sphere* sph, int id);
	void removeFromBuckets(Sphere* sph);
	BucketIndex getCurrentBucket(Sphere* currSph);
	void getNearbySpheres(Sphere* collidingSphere, vector<Sphere*>& nearbySpheres);
	void appendSphereVector(vector<Sphere*> appendVec, vector<Sphere*>& mainVec, Sphere* collidingSphere);

	void acquireBucketsLock();
	void releaseBucketsLock();

	int getBucketSphCount();
public:
	CollisionHandler();
	
    bool Update();
    bool GetNearbyUnits(int currUnit, std::vector<Unit*>& nearbyUnits);
    int GetAccessibleUnits(int unitId, int playerId); // switch event

	void addSphere(Sphere* sph, int id);
	void addAttackSphere(Sphere * sph, int id);
	void removeSphere(int id);
	void removeAttackSphere(int id);
	bool validateMovementCollision(MovementEvent & mEvent, Unit & unit);
    bool validateMovementCollision(Unit & unit, float desiredX, float desiredY, float desiredZ );
	bool validateAttackCollision(AttackObject* attack, vector<Unit*>& units);

    Sphere * findSphere(int id);
};

#endif
