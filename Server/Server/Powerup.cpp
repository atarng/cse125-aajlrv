#include "Powerup.h"
#include <iostream>
using namespace std;

Powerup::Powerup()
{
}

Powerup::Powerup(int networkId, int powerValue, Unit* sourceUnit, int powerUpType) 
    : AttackObject(networkId, POWERUP_TYPE) {

	// set powerup model
	switch (powerUpType) {
    case DOOMBOT_POWERUP:         
            setColor(0, 0, 0);
            SetIDs(10, 3);
            break;
		case CHARGING_POWERUP:
            setColor(1, 1, 1);
			SetIDs(11, 3);
			break;
		default:
            setColor(1, 1, 1);
			SetIDs(10, 3);
	}

	setPowerUpType(powerUpType);

    // set powerup position
    setPosX(sourceUnit->getPosX());
    setPosY(sourceUnit->getPosY());
    setPosZ(sourceUnit->getPosZ());
	SetRadius(4.0f);

    //setColor(1, 1, 1);
    setControllingUnitId(NEUTRAL_UNIT);

	lifeTime = POWERUP_LIFETIME;
	setPowerValue(powerValue);
}

void Powerup::applyPowerup(Unit* unit)
{
    float newHealth;
	switch (powerUpType) {
        
		case CHARGING_POWERUP:
			unit->setCurrentCharge(unit->getCurrentCharge()+powerValue);
			break;
        case DOOMBOT_POWERUP:
            newHealth = min(unit->getHealth()+powerValue, unit->getMaxHealth());
            unit->setCurrentCharge(unit->getCurrentCharge()+powerValue);
			unit->setHealth(newHealth);
            break;
		default:
			newHealth = min(unit->getHealth()+powerValue, unit->getMaxHealth());
			unit->setHealth(newHealth);
	}
}

void Powerup::applyPowerup(Unit* unit, Unit* sparkUnit)
{
    applyPowerup(unit);

	int newHealth = min(sparkUnit->getHealth()+powerValue, sparkUnit->getMaxHealth());
	sparkUnit->setHealth(newHealth);
}

Powerup::~Powerup(void)
{
}

bool Powerup::Update(float deltaTime) {                       
    setPosY(max(posY- POWERUP_GRAVITY * deltaTime, (12.0f)));   
    return false;
}
