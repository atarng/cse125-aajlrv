/***
 * Filename: ModelLoader.cpp
 * Author(s): Alfred Tarng
 * Purpose: 
 *
 * TODO: 
 */
#include "ModelLoader.h"
#include "Model.h"
#include <iostream>

// FORWARD DECLARATIONS
class Model;

ModelLoader::ModelLoader()
{
}
ModelLoader::~ModelLoader()
{
}

bool ModelLoader::Initialize()
{
    bool     result = true;
    int offset = 0;
    ModelInfo modelInfo;

    // Create the model object(s)
    Model* init_Model = 0;
//// 0: Initialize the Skybox ////////////////////////////////////
    modelInformation.push_back(modelInfo);
    ++offset;

//// 1: Initialize the model object(s)////////////////////////////////////
    cout << "[ModelLoader] Initializing Crab (Probe) Object...: " << endl;
    init_Model = new Model;
    result = init_Model->Initialize( "resource/testProbe_UVMapped.dae", 1 );
    if(!result)
    {
        cerr << "could not load probe Model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = init_Model->RetrieveRadius();
    modelInfo.shaderID       = 3;
    modelInformation.push_back(modelInfo);
	meshSelector.PROBE = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// 2: INITIALIZING DROID MESH /////////////////////////////////////////////////////////////
    cout << "[ModelLoader] Initializing Droid (Portal Turret With Legs) Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/droid.dae", 1 );
    if(!result)
    {
        cerr << "could not load testDroid model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = init_Model->RetrieveRadius();
    modelInfo.shaderID       = 3;
    modelInformation.push_back(modelInfo);
	meshSelector.TEST_DROID = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// INITIALIZING TESTCUBE MESH /////////////////////////////////////////////////////////////
    modelInformation.push_back(modelInfo);
    ++offset;
/// 4: INITIALIZING ELECTRICROBOT /////////////////////////////////////////////////////////////
    cout << "[ModelLoader] Initializing FanRobot Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/eThing_Centered.dae", 1 );
    if(!result)
    {
        cerr << "could not load test hover electric dae model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = init_Model->RetrieveRadius();
    modelInfo.shaderID       = 3;
    modelInformation.push_back(modelInfo);
	meshSelector.TEST_ELECTRICTHING = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

/// 5: INITIALIZING TEST ANIMATION OBJECT
    modelInformation.push_back(modelInfo);
    ++offset;

/// 6: INITIALIZING TEST SKELETAL_ANIMATION OBJECT //////////////////////////////////////////////
    modelInformation.push_back(modelInfo);
    ++offset;

//// 7: INITIALIZING TEST SPHERE COLLISION OBJECT //////////////////////////////////////////////
    modelInformation.push_back(modelInfo);
    ++offset;

/// 8: INITIALIZING ELECTRIC SPHERE COLLISION OBJECT //////////////////////////////////////////////
    cout << "[ModelLoader] Initializing Electric Ball Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/sphere.dae", 1 );
    if(!result)
    {
        cerr << "could not load test sphere dae model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = 12.0; //init_Model->RetrieveRadius();
    modelInfo.shaderID       = 0;
    modelInformation.push_back(modelInfo);
    meshSelector.ELECTRICSPARK = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// 9: INITIALIZING LASER OBJECT //////////////////////////////////////////////
    cout << "[ModelLoader] Initializing LASER Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/laser.dae", 1 );
    if(!result)
    {
        cerr << "could not load test laser dae model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = init_Model->RetrieveRadius();
    modelInfo.shaderID       = 0;
    modelInformation.push_back(modelInfo);
    meshSelector.LASER = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// 10: INITIALIZING Tablet OBJECT //////////////////////////////////////////////
    cout << "[ModelLoader] Initializing Tablet Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/tablet.dae", 1 );
    if(!result)
    {
        cerr << "could not load tablet dae model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = init_Model->RetrieveRadius();
    modelInfo.shaderID       = 3;
    modelInformation.push_back(modelInfo);
    meshSelector.LASER = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// 10: INITIALIZING Tablet OBJECT //////////////////////////////////////////////
	cout << "[ModelLoader] Initializing Tablet Object...: " << endl;
	modelInfo.id = modelInformation.size();
	modelInformation.push_back(modelInfo);
    ++offset;
	printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );

//// 12: INITIALIZING AOE OBJECT //////////////////////////////////////////////
    cout << "[ModelLoader] Initializing AOE Object...: " << endl;
    init_Model = new Model;
    result = init_Model -> Initialize( "resource/sphere.dae", 1 );
    if(!result)
    {
        cerr << "could not load test sphere dae model file" << endl;
        return false;
    }
    modelInfo.id             = modelInformation.size();// + offset;
    modelInfo.boundingRadius = 6.0;
    modelInfo.shaderID       = 0;
    modelInformation.push_back(modelInfo);
    meshSelector.AOE = modelInfo.id;
    printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );
    delete init_Model;

//// METEOR

    cout << "[ModelLoader] Initializing Meteor Object...: " << endl;
	modelInfo.id = modelInformation.size();
	modelInformation.push_back(modelInfo);
    ++offset;
	printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );

//// BALLS!

    cout << "[ModelLoader] Initializing Balls! Object...: " << endl;
	modelInfo.id = modelInformation.size();
	modelInformation.push_back(modelInfo);
    ++offset;
	printf("\t ModelIdToUse: %d \n\t boundingRadius:%f \n\t shaderToUse:%d \n", modelInfo.id, modelInfo.boundingRadius, modelInfo.shaderID );

    return result;
}

/***
 * Accessor Function
 */
ModelLoader::ModelInfo ModelLoader::GetModelInfo(int index)
{
    return modelInformation.at(index);
}