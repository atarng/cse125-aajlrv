//Log.h
#ifndef _LOG_H_
#define _LOG_H_

#include <direct.h>
#include <string>

#include <stdio.h>
#include <stdarg.h>
#include <fstream>

class Log
{
    
public:
    static Log* Instance();
    void Print(const char *, ...);
    void PrintErr(const char* format, ...);

    static bool Log::useLog;
    static bool Log::useClockTime;

protected:
    Log();
    Log(const Log& other);
    ~Log();

private:
    void Initialize();    

//////////////////////////////////////////////
    std::ofstream file;
//////////////////////////////////////////////
    static Log* m_Instance;

};

#endif
